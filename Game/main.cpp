/***********************************************************************
File name		: main.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "System/Engine.h"
#include "System/Logger.h"
#include "System/StateManager.h"
#include "Sources/World/World.h"
#include "System/Application.h"
#include "Sources/SplashScreen/SplashScreen.h"
#include "Sources/GameLogo/GameLogo.h"
#include "Sources/MainMenu/MainMenu.h"
#include "Sources/GameModeSelection/GameModeSelection.h"
#include "Sources/QuitConfirmation/QuitConfirmation.h"
#include "Sources/CharacterSelection/CharacterSelection.h"
#include "Sources/MapSelection/MapSelection.h"
#include "Sources/World/World0.h"
#include "Sources/Tutorial/Tutorial.h"
#include "Sources/Tutorial/Tutorial2.h"
#include <spdlog/spdlog.h>
#include "System/Imgui_app.h"
#include "Object/Object.h"

#define MEMDEBUG 1

#if MEMDEBUG
#include <crtdbg.h>
#endif

int main()
{
#if MEMDEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(4417);
#endif

	Engine* engine = new Engine;

    logger::log_handler.PrintLogOnConsoleAndFile("The program start");

	engine->Initailize();

	STATE->Add_State(new SplashScreen(APP->Get_Graphic()));
	STATE->Add_State(new GameLogo(APP->Get_Graphic()));
	STATE->Add_State(new MainMenu(APP->Get_Graphic()));
	STATE->Add_State(new GameModeSelection(APP->Get_Graphic()));
	STATE->Add_State(new QuitConfirmation(APP->Get_Graphic()));
	STATE->Add_State(new CharacterSelection(APP->Get_Graphic()));
	STATE->Add_State(new MapSelection(APP->Get_Graphic()));
	STATE->Add_State(new World0(APP->Get_Graphic()));
	STATE->Add_State(new Tutorial(APP->Get_Graphic()));
	STATE->Add_State(new Tutorial2(APP->Get_Graphic()));

	engine->MainLoop();

	engine->Quit();

	delete engine;

	return 0;
}
