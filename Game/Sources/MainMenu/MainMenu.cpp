/***********************************************************************
File name		: MainMenu.cpp
Project name	: WFPT
Author			: HyunSeok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "MainMenu.h"

MainMenu::MainMenu(Graphic * graphic) : State(graphic) { m_state_num = -1; }

void MainMenu::Initialize()
{
	Object* camera = new Object(this, "Camera");
	camera->Add_Init_Component(new Camera(camera, "Camera"));
	camera->GetTransform()->SetPosition(vector2(0.0f, 0.0f));
	AddRegisterObject(camera);

	// Start button
	Object* startButtonObj = new Object(this, "Start");
	startButtonObj->Add_Init_Component(new Button(startButtonObj));
	startButtonObj->GetTransform()->SetPosition({ 0, 35 });
	startButtonObj->GetTransform()->SetScale(vector2(100.f, 35));
	Button *startButton = startButtonObj->GetComponentByTemplate<Button>();
	startButton->SetCallbackOnClick([&]() {
		STATE->Change_State(SELECTGAMEMODE);
	});
	startButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 0;
		auto button = GetObjectManager()->Get_ObjectByName("Start")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stStart = startButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stStart->SetColor(Color(255, 0, 0, 255));
	stStart->SetString("Start");
	vector2 v1 = startButtonObj->GetTransform()->GetLocalScale();
	stStart->GetOwner()->GetTransform()->SetScale(vector2(1 / v1.x, 1 / v1.y));

	//Quit button
	Object* quitButtonObj = new Object(this, "Quit");
	quitButtonObj->Add_Init_Component(new Button(quitButtonObj));
	quitButtonObj->GetTransform()->SetPosition({ 0, -35 });
	quitButtonObj->GetTransform()->SetScale(vector2(100.f, 35));
	Button *quitButton = quitButtonObj->GetComponentByTemplate<Button>();
	quitButton->SetCallbackOnClick([&]() {
		STATE->Change_State(QUITCONFIRMATION);
	});
	quitButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 1;
		auto button = GetObjectManager()->Get_ObjectByName("Quit")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stQuit = quitButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stQuit->SetColor(Color(255, 0, 0, 255));
	stQuit->SetString("Quit");
	vector2 v2 = quitButtonObj->GetTransform()->GetLocalScale();
	stQuit->GetOwner()->GetTransform()->SetScale(vector2(1 / v2.x, 1 / v2.y));

	AddRegisterObject(startButtonObj);
	AddRegisterObject(quitButtonObj);

	m_buttons = { startButton, quitButton };
	m_buttonIdx = -1;
	m_initialized = true;
}

MainMenu::~MainMenu()
{
}

void MainMenu::Update(float /*dt*/)
{
	m_buttonIdx = m_menu_input_manager.UpdateMenuNavigation(m_buttons, m_buttonIdx);
	//m_menu_input_manager.CheckForPreviousState(GLFW_KEY_BACKSPACE, JOYSTICK_KEY::KEY_B, GAMELOGO);
	m_menu_input_manager.CheckMenuButtonConfirm(GLFW_KEY_SPACE, JOYSTICK_KEY::KEY_A, m_buttons, m_buttonIdx);
}

void MainMenu::Close()
{
	State::Close();
}
