/***********************************************************************
File name		: Tutorial.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "System/State.h"
#include "System/Audio.h"

#include <map>


class Tutorial2 : public State
{
private:
    float m_inner_time = 0.0f;
private:	//object
	Object* m_start_msg = nullptr;
	Object* m_board = nullptr;

	bool m_isClearPortalGen = false;

	unsigned int target_count = 0;
	unsigned int aim_count = 3;

private:
	bool m_real_end=false;
	bool m_start = true;

	Object* target[3] = { nullptr };

public:
	Tutorial2(Graphic * graphic);
    void Initialize() override;
    ~Tutorial2() override;
    void Update(float dt) override;
    void Close() override;

	void Start(float dt);
};

