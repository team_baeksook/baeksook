/***********************************************************************
File name		: World0.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Tutorial.h"
#include <iostream>
#include "System/Input.h"
#include "System/ObjectManager.h"
#include "Component/Sprite.h"
#include "Object/Object.h"
#include "Component/SpriteText.h"
#include "Component/Clock.h"
#include "Component/Player.h"
#include "Component/Weapon/Revolver.h"
#include "Component/Physics.h"
#include "Component/ClearPortal.h"
#include "System/Imgui_app.h"
#include "System/StateManager.h"
#include "Component/Background.h"
#include "System/User.h"

Tutorial::Tutorial(Graphic * graphic) : State(graphic)
{
	m_state_num = 1;
}

void Tutorial::Initialize()
{
	m_CameraList.at(0)->SetFollow(false);
	BackgroundColor = Color(96, 90, 90,200);

	m_start_msg = new Object(this, "Start_message");
	{
		SpriteText* sprite_text_comp = dynamic_cast<SpriteText*>(m_start_msg->Add_Init_Component(new SpriteText(m_start_msg)));
		sprite_text_comp->SetAllign(TEXT_ALLIGN::ALLIGN_CENTER);
		sprite_text_comp->SetString("Ready");
	}
	m_start_msg->GetTransform()->SetPosition(vector2(0.0f, 25.0f));
	m_start_msg->GetTransform()->SetDepth(-1.0f);
	AddRegisterObject(m_start_msg);
	m_start = true;

	Object* clearportal = new Object(this);
	LoadArchetypeinfo(clearportal, "ClearPortal");
	clearportal->GetTransform()->SetScale(vector2(10.f, 10.f));
	clearportal->GetTransform()->SetPosition(vector2(100.0f, 0.0f));
	clearportal->GetComponentByTemplate<ClearPortal>()->Set_NextState(SceneIdentifier::TUTORIAL2);
	AddRegisterObject(clearportal);

	Object* Goal = new Object(this);
	LoadArchetypeinfo(Goal, "Goal");
	Goal->GetTransform()->SetPosition(vector2(100.0f, 15.0f));
	AddRegisterObject(Goal);

	Object* background1 = new Object(this, "background1");
	Sprite* sprite = dynamic_cast<Sprite*>(background1->Add_Component(new Sprite(background1)));
	sprite->SetTextureName("Cityline1");
	sprite->Init();
	Transform* transform = background1->GetTransform();
	transform->SetPosition({ 0.0f, 50.f });
	transform->SetScale(vector2(1200.0f, 400.0f) / 2.5f);
	transform->SetDepth(0.96f);
	Background* bgptr = dynamic_cast<Background*>(background1->Add_Init_Component(new Background(background1)));
	bgptr->SetRate(0.8f);
	AddRegisterObject(background1);

	Object* MovePad = new Object(this);
	LoadArchetypeinfo(MovePad, "MovePad");
	MovePad->GetTransform()->SetPosition(vector2(-100.0f, 40.0f));
	AddRegisterObject(MovePad);

	Object* JumpPad = new Object(this);
	LoadArchetypeinfo(JumpPad, "JumpPad");
	JumpPad->GetTransform()->SetPosition(vector2(-20.0f, 40.0f));
	AddRegisterObject(JumpPad);

	Object* DownJumpPad = new Object(this);
	LoadArchetypeinfo(DownJumpPad, "DownJumpPad");
	DownJumpPad->GetTransform()->SetPosition(vector2(90.0f, 80.0f));
	AddRegisterObject(DownJumpPad);
}

Tutorial::~Tutorial()
{
}

void Tutorial::Update(float dt)
{

	m_inner_time += dt;

	//if(m_inner_time > m_SupplyTime)
	//{
	//	m_inner_time = 0.0f;
	//}
	if (m_start)
	{
		Start(dt);
		return;
	}
	if (m_real_end && Input::Is_Triggered(GLFW_KEY_SPACE))
	{
		STATE->Change_to_Menu_State();
	}
}

void Tutorial::Close()
{
	m_inner_time = 0.0f;
	m_start = true;

	m_playerlist.clear();
	//m_objmanger->Write_Object_to_json();
	State::Close();
}

void Tutorial::Start(float dt)
{
	Input::ResetAll();
	SpriteText* sprite_text = m_start_msg->GetComponentByTemplate<SpriteText>();
	m_start_msg->GetTransform()->AddScale(vector2(500.0f, 500.0f) * dt);
	float degree = GetRandomNumber(-1.f, 1.0f);
	m_start_msg->GetTransform()->AddDegree(degree * 100.0f * dt);
	Color col = sprite_text->GetColor();
	col.color[3] -= 1.0f * dt;
	sprite_text->SetColor(col);
	if(m_start_msg->GetTransform()->GetWorldScale().x > 200.0f)
	{
		if(sprite_text->GetString() == "Fight")
		{
			m_start_msg->SetLifetime(0.0f);
			m_start = false;
		}
		if(sprite_text->GetString() == "Set")
		{
			sprite_text->SetString("Fight");
			m_start_msg->GetTransform()->SetScale(vector2(1.0f, 1.0f));
			sprite_text->SetColor(Color(255, 255, 255, 255));
			m_start_msg->GetTransform()->SetDegree(0.0f);
			return;
		}
		sprite_text->SetString("Set");
		m_start_msg->GetTransform()->SetScale(vector2(1.0f, 1.0f));
		sprite_text->SetColor(Color(255, 255, 255, 255));
		m_start_msg->GetTransform()->SetDegree(0.0f);
	}
}