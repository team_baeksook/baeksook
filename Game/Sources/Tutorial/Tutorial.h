/***********************************************************************
File name		: Tutorial.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "System/State.h"
#include "System/Audio.h"

#include <map>


class Tutorial : public State
{
private:
    float m_inner_time = 0.0f;
private:	//object
	Object* m_start_msg = nullptr;

private:
	bool m_real_end=false;
	bool m_start = true;

public:
	Tutorial(Graphic * graphic);
    void Initialize() override;
    ~Tutorial() override;
    void Update(float dt) override;
    void Close() override;

	void Start(float dt);
};

