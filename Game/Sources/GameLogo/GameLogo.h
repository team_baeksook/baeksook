/***********************************************************************
File name		: GameLogo.h
Project name	: WFPT
Author			: Maxime Cauvin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "System/State.h"
#include "System/Imgui_app.h"

class GameLogo : public State
{
private:
	bool m_mouse_down = false;
public:
	GameLogo(Graphic * graphic);
    void Initialize() override;
    ~GameLogo() override;
    void Update(float dt) override;
    void Close() override;
};

