/***********************************************************************
File name		: GameLogo.cpp
Project name	: WFPT
Author			: Maxime Cauvin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "GameLogo.h"
#include <iostream>
#include "System/Input.h"
#include "System/ObjectManager.h"
#include "System/Logger.h"
#include "Component/Sprite.h"
#include "Object/Object.h"
#include "System/StateManager.h"
#include "Component/Button.h"
#include "Component/SpriteText.h"
#include "System/Engine.h"
#include "System/JoyStick.h"

GameLogo::GameLogo(Graphic * graphic) : State(graphic) { m_state_num = -1; }

void GameLogo::Initialize()
{
	Object* camera = new Object(this, "Camera");
	camera->Add_Init_Component(new Camera(camera, "Camera"));
	camera->GetTransform()->SetPosition(vector2(0.0f, 0.0f));
	AddRegisterObject(camera);

	Object* menu = new Object(this, "MainMenu");
	menu->Add_Component(new Sprite(menu));
	menu->GetComponentByTemplate<Sprite>()->SetTextureName("MainMenu");
	menu->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 255, 255, 255));
	menu->GetComponentByTemplate<Sprite>()->TurnOnOffAnimation(true);
	//menu->GetComponentByTemplate<Sprite>()->GetTexture()->SetFrameSpeed(10.f);
	menu->GetComponentByTemplate<Sprite>()->Init();
	menu->GetTransform()->SetPosition({ 0, 0 });
	menu->GetTransform()->SetScale(vector2(150.f, 150.f));

	AddRegisterObject(menu);

	m_initialized = true;
}

GameLogo::~GameLogo()
{
}

void GameLogo::Update(float /*dt*/)
{
	bool skipState = Input::Is_Triggered(GLFW_KEY_SPACE);
	auto joysticks = Input::Get_Joystick();
	for (size_t i = 0; i < joysticks.size() && !skipState; i++)
		skipState = joysticks[i]->is_triggered(JOYSTICK_KEY::KEY_A);
	skipState = skipState && !IMGUIAPP->IsDebuggingActive() && !IMGUIAPP->IsGlobalSettingsActive();

	if (skipState || (m_mouse_down && Input::Is_Released(GLFW_MOUSE_BUTTON_1)))
	{
		m_objmanger->Get_ObjectByName("MainMenu")->SetLifetime(0);
		STATE->Change_State(MAINMENU);
	}
	m_mouse_down = Input::Is_Pressed(GLFW_MOUSE_BUTTON_1);
}

void GameLogo::Close()
{
	State::Close();
}
