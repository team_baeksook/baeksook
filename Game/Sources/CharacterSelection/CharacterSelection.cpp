/***********************************************************************
File name		: CharacterSelection.cpp
Project name	: WFPT
Author			: Maxime Cauvin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/

#include "CharacterSelection.h"

CharacterSelection::CharacterSelection(Graphic * graphic) : State(graphic) { m_state_num = -1; }

void CharacterSelection::Initialize()
{
	Object* camera = new Object(this, "Camera");
	camera->Add_Init_Component(new Camera(camera, "Camera"));
	camera->GetTransform()->SetPosition(vector2(0.0f, 0.0f));
	AddRegisterObject(camera);

	//Top text
	Object* topTextObj = new Object(this, "TopText");
	topTextObj->Add_Init_Component(new SpriteText(topTextObj));
	SpriteText *stTopText = topTextObj->GetComponentByTemplate<SpriteText>();
	stTopText->SetAllign(TEXT_ALLIGN::ALLIGN_CENTER);
	stTopText->SetColor(Color(255, 255, 255, 255));
	stTopText->SetString("Select a character");
	stTopText->SetFontSize(10);
	topTextObj->GetTransform()->SetPosition(vector2(0, 80));

	//Back button
	Object* backButtonObj = new Object(this, "Back");
	backButtonObj->Add_Init_Component(new Button(backButtonObj));
	backButtonObj->GetTransform()->SetPosition({ -75, -70 });
	backButtonObj->GetTransform()->SetScale(vector2(80, 20));
	Button *backButton = backButtonObj->GetComponentByTemplate<Button>();
	backButton->SetCallbackOnClick([&]() {
		STATE->Change_State(SELECTGAMEMODE);
	});
	backButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 0;
		auto button = GetObjectManager()->Get_ObjectByName("Back")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stBack = backButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stBack->SetColor(Color(255, 0, 0, 255));
	stBack->SetString("Back");
	vector2 v1 = backButtonObj->GetTransform()->GetLocalScale();
	stBack->GetOwner()->GetTransform()->SetScale(vector2(1 / v1.x, 1 / v1.y));

	//Next button
	Object* nextButtonObj = new Object(this, "Next");
	nextButtonObj->Add_Init_Component(new Button(nextButtonObj));
	nextButtonObj->GetTransform()->SetPosition({ 75, -70 });
	nextButtonObj->GetTransform()->SetScale(vector2(80, 20));
	Button *nextButton = nextButtonObj->GetComponentByTemplate<Button>();
	nextButton->SetCallbackOnClick([&]() {
		STATE->Change_State(MAPSELECTION);
	});
	nextButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 1;
		auto button = GetObjectManager()->Get_ObjectByName("Next")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stNext = nextButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stNext->SetColor(Color(255, 0, 0, 255));
	stNext->SetString("Next");
	vector2 v2 = nextButtonObj->GetTransform()->GetLocalScale();
	stNext->GetOwner()->GetTransform()->SetScale(vector2(1 / v2.x, 1 / v2.y));

	AddRegisterObject(topTextObj);
	AddRegisterObject(backButtonObj);
	AddRegisterObject(nextButtonObj);

	m_buttons = { backButton, nextButton };
	m_buttonIdx = -1;
	m_initialized = true;
}

CharacterSelection::~CharacterSelection()
{
}

void CharacterSelection::Update(float /*dt*/)
{
	m_buttonIdx = m_menu_input_manager.UpdateMenuNavigation(m_buttons, m_buttonIdx);
	m_menu_input_manager.CheckMenuButtonBack(GLFW_KEY_BACKSPACE, JOYSTICK_KEY::KEY_B, SELECTGAMEMODE);
	m_menu_input_manager.CheckMenuButtonConfirm(GLFW_KEY_SPACE, JOYSTICK_KEY::KEY_A, m_buttons, m_buttonIdx);
}

void CharacterSelection::Close()
{
	State::Close();
}
