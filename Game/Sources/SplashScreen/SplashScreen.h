/***********************************************************************
File name		: SplashScreen.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "System/State.h"

class SplashScreen : public State
{
private:
    float time = 0.0f;
    bool isclick = false;
	bool isdigipen = false;
	bool isfmod = false;
	bool m_mouse_down = false;
public:
	SplashScreen(Graphic * graphic);
    void Initialize() override;
    ~SplashScreen() override;
    void Update(float dt) override;
    void Close() override;

	void SpawnDigipenLogo();
	void SpawnFmodLogo();

};

