/***********************************************************************
File name		: SplashScreen.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "SplashScreen.h"
#include "System/Input.h"
#include "System/ObjectManager.h"
#include "Object/Object.h"
#include "System/Imgui_app.h"
#include "System/StateManager.h"
#include "System/JoyStick.h"

SplashScreen::SplashScreen(Graphic * graphic) : State(graphic) { m_state_num = -1; }

void SplashScreen::Initialize()
{
	Object* camera = new Object(this,"Camera");
	camera->Add_Init_Component(new Camera(camera, "Camera"));
	camera->GetTransform()->SetPosition(vector2(0.0f, 0.0f));
	AddRegisterObject(camera);
	Object* logo = new Object(this);
	LoadArchetypeinfo(logo, "Logo");
	AddRegisterObject(logo);
	logo->SetLifetime(5.f);
	Object* steam1 = new Object(this);
	LoadArchetypeinfo(steam1, "Steam");
	AddRegisterObject(steam1);
	steam1->SetLifetime(5.f);
	Object* steam2 = new Object(this);
	LoadArchetypeinfo(steam2, "Steam");
	steam2->GetComponentByTemplate<Sprite>()->SetCurrentFrame(1);
	AddRegisterObject(steam2);
	steam2->SetLifetime(5.f);
	steam2->GetTransform()->SetPosition(vector2(0, 55.f));
	Object* steam3 = new Object(this);
	LoadArchetypeinfo(steam3, "Steam");
	steam3->GetComponentByTemplate<Sprite>()->SetCurrentFrame(3);
	AddRegisterObject(steam3);
	steam3->GetTransform()->SetPosition(vector2(25.f, 50.f));
	steam3->SetLifetime(5.f);

	m_initialized = true;
}

SplashScreen::~SplashScreen()
{
}

void SplashScreen::Update(float dt)
{
	bool skipState = Input::Is_Triggered(GLFW_KEY_SPACE);
	auto joysticks = Input::Get_Joystick();
	for (size_t i = 0; i < joysticks.size() && !skipState; i++)
		skipState = joysticks[i]->is_triggered(JOYSTICK_KEY::KEY_A);
	skipState = skipState && !IMGUIAPP->IsDebuggingActive() && !IMGUIAPP->IsGlobalSettingsActive();

	if(skipState || (m_mouse_down && Input::Is_Released(GLFW_MOUSE_BUTTON_1)))
		STATE->Change_State(GAMELOGO);
	m_mouse_down = Input::Is_Pressed(GLFW_MOUSE_BUTTON_1);
	time += dt;

	if(!isdigipen&&time>=5.f)
	{
		SpawnDigipenLogo();
		isdigipen = true;
	}
	else if (!isfmod&&time >= 8.f)
	{
		SpawnFmodLogo();
		isfmod = true;
	}
	else if(time>=11.f)
		STATE->Change_State(GAMELOGO);
}

void SplashScreen::Close()
{
	State::Close();
}

void SplashScreen::SpawnDigipenLogo()
{
	Object* logo = new Object(this);
	logo->Add_Component(new Sprite(logo, "Sprite"));
	logo->GetComponentByTemplate<Sprite>()->SetTextureName("Digipen_logo");
	logo->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 255, 255, 255));
	logo->GetComponentByTemplate<Sprite>()->Init();
	logo->GetTransform()->SetPosition({ 0, 0 });
	logo->GetTransform()->SetScale(vector2(204.8f, 59.4f));

	AddRegisterObject(logo);
	logo->SetLifetime(3.f);
}

void SplashScreen::SpawnFmodLogo()
{
	Object* logo = new Object(this);
	logo->Add_Component(new Sprite(logo, "Sprite"));
	logo->GetComponentByTemplate<Sprite>()->SetTextureName("FMOD_logo");
	logo->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 255, 255, 255));
	logo->GetComponentByTemplate<Sprite>()->Init();
	logo->GetTransform()->SetPosition({ 0, 0 });
	logo->GetTransform()->SetScale(vector2(204.8f, 59.4f));

	AddRegisterObject(logo);
	logo->SetLifetime(3.f);
}
