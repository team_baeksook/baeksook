/***********************************************************************
File name		: World.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "System/State.h"

class World : public State
{
private:
    float time = 0.0f;
    bool isclick = false;
public:
    World(Graphic * graphic);
    void Initialize() override;
    ~World() override;
    void Update(float dt) override;
    void Close() override;
};

