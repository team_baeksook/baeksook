/***********************************************************************
File name		: World0.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "World0.h"
#include "System/Input.h"
#include "System/ObjectManager.h"
#include "Component/Sprite.h"
#include "Object/Object.h"
#include "Component/SpriteText.h"
#include "Component/Clock.h"
#include "Component/Player.h"
#include "Component/Weapon/Revolver.h"
#include "Component/Physics.h"
#include "System/Imgui_app.h"
#include "System/StateManager.h"
#include "Component/Background.h"
#include "Component/RedZone.h"
#include "System/User.h"
#include "Component/Weapon/Laser.h"
#include "System/Badge/Badge.h"

#include <iostream>

bool World0::Start_Event(float t)
{
	for (auto usr : STATE->m_users)
	{
		usr->Setinput(false);
	}
	SpriteText* sprite_text = m_start_msg->GetComponentByTemplate<SpriteText>();
	t *= 400.0f;
	float adjusted_t = t;
	t -= m_start_time;
	m_start_msg->GetTransform()->SetScale(vector2(t));
	float degree = GetRandomNumber(-1.f, 1.0f);
	m_start_msg->GetTransform()->AddDegree(degree * 5.0f);
	Color col = sprite_text->GetColor();
	col.color[3] = 1.0f - (t / 300.0f);
	sprite_text->SetColor(col);
	if (m_start_msg->GetTransform()->GetWorldScale().x > 200.0f)
	{
		m_start_time = adjusted_t;
		if (sprite_text->GetString() == "Fight")
		{
			m_start_msg->SetLifetime(0.0f);
			m_clock->GetComponentByTemplate<Clock>()->Init();
			for (auto usr : STATE->m_users)
			{
				usr->Setinput(true);
			}
			return true;
		}
		if (sprite_text->GetString() == "Set")
		{
			sprite_text->SetString("Fight");
			m_start_msg->GetTransform()->SetScale(vector2(1.0f, 1.0f));
			sprite_text->SetColor(Color(255, 255, 255, 255));
			m_start_msg->GetTransform()->SetDegree(0.0f);
			return false;
		}
		sprite_text->SetString("Set");
		m_start_msg->GetTransform()->SetScale(vector2(1.0f, 1.0f));
		sprite_text->SetColor(Color(255, 255, 255, 255));
		m_start_msg->GetTransform()->SetDegree(0.0f);
	}
	return false;
}

bool World0::Boardfall_Event(float t)
{
	m_board->GetTransform()->SetPositionY(2.0f - t);
	if (t > 2.0f)
	{
		m_board->GetTransform()->SetPositionY(0.0f);
		m_fanfaresound->Play_Audio(false);
		int playersize = static_cast<int>(m_playerlist.size());
		float start_pos = 0.0f - (playersize - 1) * 0.25f;

		int index = 0;
		BestFace = m_playerlist[0];

		for (auto& player : m_playerlist)
		{
			Object* Paper = new Object(this);
			LoadArchetypeinfo(Paper, "Paper");
			Paper->GetTransform()->SetPosition(vector2(start_pos, 0.0f));
			start_pos += 0.5f;
			AddRegisterObject(Paper);

			Player* playerptr = player->GetComponentByTemplate<Player>();
			std::string character_name = playerptr->GetCharacterName();
			unsigned int playernum = playerptr->GetPlayerNum();
			Object* face = new Object(this);
			face_vector[character_name] = face;

			Sprite* sprite_comp = dynamic_cast<Sprite*>(face->Add_Init_Component(new Sprite(face)));
			sprite_comp->SetTextureName(character_name + "_Face");
			sprite_comp->SetHud(true);
			face->GetTransform()->SetScale(vector2(0.2f, 0.2f));
			AddRegisterObject(face);
			face->AttatchToParent(Paper);
			face->GetTransform()->SetPosition(vector2(0.0f, 0.5f));
			face->GetTransform()->SetDepth(-0.03f);

			Object* score = new Object(this);
			score_obj_vector[playernum].kill_score = score;
			Sprite* sprite_comp_score = dynamic_cast<Sprite*>(score->Add_Init_Component(new Sprite(score)));
			sprite_comp_score->SetTextureName("number");
			sprite_comp_score->TurnOnOffAnimation(true);
			sprite_comp_score->SetHud(true);

			SpriteText* sprite_text_comp = dynamic_cast<SpriteText*>(score->Add_Init_Component(new SpriteText(score)));

			sprite_text_comp->SetString("KILL: ");
			sprite_text_comp->SetAllign(TEXT_ALLIGN::ALLIGN_RIGHT);
			sprite_text_comp->SetHud(true);
			sprite_text_comp->SetFontSize(1.0f);
			sprite_text_comp->SetColor(Color(255, 0, 0, 255));

			AddRegisterObject(score);
			score->AttatchToParent(Paper);
			score->GetTransform()->SetScale(vector2(0.15f, 0.15f));
			score->GetTransform()->SetPosition(vector2(0.35f, 0.25f));
			score->GetTransform()->SetDepth(-0.03f);

			score = new Object(this);
			score_obj_vector[playernum].death_score = score;
			sprite_comp_score = dynamic_cast<Sprite*>(score->Add_Init_Component(new Sprite(score)));
			sprite_comp_score->SetTextureName("number");
			sprite_comp_score->TurnOnOffAnimation(true);
			sprite_comp_score->SetHud(true);

			sprite_text_comp = dynamic_cast<SpriteText*>(score->Add_Init_Component(new SpriteText(score)));

			sprite_text_comp->SetString("DEATH: ");
			sprite_text_comp->SetAllign(TEXT_ALLIGN::ALLIGN_RIGHT);
			sprite_text_comp->SetHud(true);
			sprite_text_comp->SetFontSize(1.0f);
			sprite_text_comp->SetColor(Color(255, 0, 0, 255));

			AddRegisterObject(score);
			score->AttatchToParent(Paper);
			score->GetTransform()->SetScale(vector2(0.15f, 0.15f));
			score->GetTransform()->SetPosition(vector2(0.35f, 0.0f));
			score->GetTransform()->SetDepth(-0.03f);

			score = new Object(this);
			score_obj_vector[playernum].dmg_score = score;

			sprite_text_comp = dynamic_cast<SpriteText*>(score->Add_Init_Component(new SpriteText(score)));

			sprite_text_comp->SetString("DMG:");
			sprite_text_comp->SetAllign(TEXT_ALLIGN::ALLIGN_LEFT);
			sprite_text_comp->SetHud(true);
			sprite_text_comp->SetFontSize(1.0f);
			sprite_text_comp->SetColor(Color(255, 0, 0, 255));

			AddRegisterObject(score);
			score->AttatchToParent(Paper);
			score->GetTransform()->SetScale(vector2(0.15f, 0.15f));
			score->GetTransform()->SetPosition(vector2(-0.4f, -0.3f));
			score->GetTransform()->SetDepth(-0.03f);

			score_vector[playernum] = playerptr->GetInfo();

			if (BestScore < score_vector[playernum].m_kill)
			{
				BestScore = score_vector[playernum].m_kill;
				BestPlayer = playernum;
				BestFace = face;
				m_best_character_name = character_name;
			}
			++index;
		}

		return true;
	}
	return false;
}

bool World0::Play_Event(float /*t*/)
{
	if (Clock* clock_comp = m_clock->GetComponentByTemplate<Clock>(); clock_comp->GetSecond() < 0 && !m_real_end)
	{
		if (!m_CameraList.empty())
		{
			m_CameraList.at(0)->SetFollow(false);
		}
		clock_comp->TogglePause();
		for (auto& player : m_playerlist)
		{
			Player* player_comp = player->GetComponentByTemplate<Player>();
			player_comp->SendEndSignal();
		}
		if (!m_board)
		{
			m_board = new Object(this);
			Sprite* sprite_comp = dynamic_cast<Sprite*>(m_board->Add_Init_Component(new Sprite(m_board)));
			sprite_comp->SetTextureName("Board");
			sprite_comp->SetHud(true);
			m_board->GetTransform()->SetScale(vector2(2.0f, 2.0f));
			m_board->GetTransform()->SetPosition(vector2(0.0f, 2.0f));
			m_board->GetTransform()->SetDepth(-0.91f);
			AddRegisterObject(m_board);
		}
		m_event_manager.AddEvent(&World0::Boardfall_Event)->SetNext(&World0::Score_Event)->
			SetNext(&World0::WaitUntilSpace)->SetNext(&World0::ShowBadge);
		return true;
	}
	return false;
}

bool World0::Score_Event(float t)
{
	if (t > 2.3f)
	{
		m_scoresound->Play_Audio(false);
		for (auto& face : face_vector)
		{
			face.second->GetComponentByTemplate<Sprite>()->SetTextureName(
				face.first + "_Face_sad");
		}
		BestFace->GetComponentByTemplate<Sprite>()->SetTextureName(m_best_character_name + "_Face_happy");
		int index = 0;
		for (auto& scores : score_obj_vector)
		{
			auto score_object = scores.second;
			Sprite* sprite_comp = dynamic_cast<Sprite*>(score_object.kill_score->GetComponentByTemplate<Sprite>());
			sprite_comp->SetAnimationActive(false);
			sprite_comp->SetCurrentFrame(score_vector[index].m_kill);

			sprite_comp = dynamic_cast<Sprite*>(score_object.death_score->GetComponentByTemplate<Sprite>());
			sprite_comp->SetAnimationActive(false);
			sprite_comp->SetCurrentFrame(score_vector[index].m_death);

			score_object.dmg_score->GetComponentByTemplate<SpriteText>()->SetString("DMG :" + std::to_string(score_vector[index].m_dmg));

			++index;
		}

		for (auto& player : m_playerlist)
		{
			Player* temp_player = player->GetComponentByTemplate<Player>();
			if (BestScore != 0 && temp_player->m_playerNum == static_cast<unsigned>(BestPlayer))
			{
				temp_player->m_playerhud->GetComponentByTemplate<Sprite>()->SetTextureName(temp_player->m_hudpngbasename + "_happy");
			}
			else
			{
				temp_player->m_playerhud->GetComponentByTemplate<Sprite>()->SetTextureName(temp_player->m_hudpngbasename + "_sad");
			}
		}
		return true;
	}
	return false;
}

bool World0::WaitUntilSpace(float /*t*/)
{
	if (Input::IsProceed())
	{
		return true;
	}
	return false;
}

bool World0::ShowBadge(float t)
{
	if (t < 1.0f)
	{
		return 0;
	}
	for (auto& scores : score_obj_vector)
	{
		scores.second.kill_score->SetLifetime(0);
		scores.second.death_score->SetLifetime(0);
		scores.second.dmg_score->SetLifetime(0);
	}
	m_badge->Decipher();
	int i = 0;
	int playersize = m_playerlist.size();

	float offset[4] = { 0.3f, 0.3f, 0.3f, 0.3f };

	for (int i = 0; i <= 2; ++i)
	{
		Object* badge = new Object(this);
		SpriteText* badge_text_comp = dynamic_cast<SpriteText*>(badge->Add_Init_Component(new SpriteText(badge)));
		unsigned int data = m_badge->Getbadgedata(static_cast<BADGELIST>(i));
		if (data == -1)
		{
			std::cout << "no" << std::endl;
			continue;
		}

		switch (i)
		{
		case BADGELIST::BADGE_FIRSTBLOOD:
		{
			badge_text_comp->SetString("FIRSTBLOOD");
			break;
		}
		case BADGELIST::BADGE_DUMB:
		{
			badge_text_comp->SetString("DUMB");
			break;
		}
		case BADGELIST::BADGE_DOMINATOR:
		{
			badge_text_comp->SetString("DOMINATOR");
			break;
		}
		default:
			break;
		}
		badge_text_comp->SetAllign(TEXT_ALLIGN::ALLIGN_CENTER);
		badge_text_comp->SetHud(true);
		badge_text_comp->SetFontSize(1.0f);
		badge_text_comp->SetColor(Color(255, 0, 0, 255));

		float start_pos = 0.0f - (playersize - 1) * 0.25f + 0.5f * (playersize - data - 1);
		badge->GetTransform()->SetPosition(vector2(start_pos, offset[playersize - data - 1] * 0.5f));
		badge->GetTransform()->SetScale(vector2(0.1f, 0.1f));
		offset[playersize - data - 1] -= 0.3f;
		badge->GetTransform()->SetDepth(-0.98f);

		AddRegisterObject(badge);
	}
	return true;
}

World0::World0(Graphic * graphic) : State(graphic)
{
	m_state_num = 0;
}

void World0::Initialize()
{
	m_start_time = 0.0f;

	//TODO::Trash..I do not want this to File i/o
	BackgroundColor = Color(96, 90, 90);

	m_clock = new Object(this, "Clock");
	m_clock->Add_Init_Component(new SpriteText(m_clock));
	m_clock->Add_Component(new Clock(m_clock));
	m_clock->GetTransform()->SetScale(vector2(0.1f, 0.1f));
	m_clock->GetTransform()->SetPosition(vector2(0.0f, 0.95f));
	AddRegisterObject(m_clock);

	m_start_msg = new Object(this, "Start_message");
	{
		SpriteText* sprite_text_comp = dynamic_cast<SpriteText*>(m_start_msg->Add_Init_Component(new SpriteText(m_start_msg)));
		sprite_text_comp->SetAllign(TEXT_ALLIGN::ALLIGN_CENTER);
		sprite_text_comp->SetString("Ready");
	}
	m_start_msg->GetTransform()->SetPosition(vector2(0.0f, 25.0f));
	m_start_msg->GetTransform()->SetDepth(-1.0f);
	AddRegisterObject(m_start_msg);

	m_scoresound = new Audio();
	m_scoresound->Load_Audio_From_File("asset/Sound/score_exhibited.wav");
	m_scoresound->Set_Volume(5.f);
	m_scoresound->Set_Name("Exhibited_Sound");

	m_fanfaresound = new Audio();
	m_fanfaresound->Load_Audio_From_File("asset/Sound/fanfare_score.wav");
	m_fanfaresound->Set_Volume(5.f);
	m_fanfaresound->Set_Name("Fanfare_Sound");

	Object* WeaponGen1 = new Object(this);
	LoadArchetypeinfo(WeaponGen1, "WeaponGenerator");
	WeaponGen1->GetTransform()->SetScale(vector2(20.f, 25.f));
	WeaponGen1->GetTransform()->SetPosition(vector2(0.0f, 65.0f));
	AddRegisterObject(WeaponGen1);

	Object* WeaponGen2 = new Object(this);
	LoadArchetypeinfo(WeaponGen2, "WeaponGenerator");
	WeaponGen2->GetTransform()->SetScale(vector2(20.f, 25.f));
	WeaponGen2->GetTransform()->SetPosition(vector2(170.0f, 7.0f));
	AddRegisterObject(WeaponGen2);

	Object* WeaponGen3 = new Object(this);
	LoadArchetypeinfo(WeaponGen3, "WeaponGenerator");
	WeaponGen3->GetTransform()->SetScale(vector2(20.f, 25.f));
	WeaponGen3->GetTransform()->SetPosition(vector2(-170.0f, 7.0f));
	AddRegisterObject(WeaponGen3);

	/*Object* WeaponGen4 = new Object(this);
	LoadArchetypeinfo(WeaponGen4, "WeaponGenerator");
	WeaponGen4->GetTransform()->SetScale(vector2(0.1f, 0.1f));
	WeaponGen4->GetTransform()->SetPosition(vector2(-172.0f, 3.0f));
	AddRegisterObject(WeaponGen4);

	Object* WeaponGen5 = new Object(this);
	LoadArchetypeinfo(WeaponGen5, "WeaponGenerator");
	WeaponGen5->GetTransform()->SetScale(vector2(0.1f, 0.1f));
	WeaponGen5->GetTransform()->SetPosition(vector2(115.0f, 45.0f));
	AddRegisterObject(WeaponGen5);

	Object* WeaponGen6 = new Object(this);
	LoadArchetypeinfo(WeaponGen6, "WeaponGenerator");
	WeaponGen6->GetTransform()->SetScale(vector2(0.1f, 0.1f));
	WeaponGen6->GetTransform()->SetPosition(vector2(180.0f, 3.0f));
	AddRegisterObject(WeaponGen6);*/

	Object* background1 = new Object(this, "background1");
	Sprite* sprite = dynamic_cast<Sprite*>(background1->Add_Component(new Sprite(background1)));
	sprite->SetTextureName("Cityline1");
	sprite->Init();
	Transform* transform = background1->GetTransform();
	transform->SetPosition({ 0.0f, 50.f });
	transform->SetScale(vector2(1200.0f, 400.0f) / 2.0f);
	transform->SetDepth(0.96f);
	Background* bgptr = dynamic_cast<Background*>(background1->Add_Init_Component(new Background(background1)));
	bgptr->SetRate(0.8f);
	AddRegisterObject(background1);

	Object* background2 = new Object(this, "background2");
	sprite = dynamic_cast<Sprite*>(background2->Add_Component(new Sprite(background2)));
	sprite->SetTextureName("Cityline2");
	sprite->Init();
	transform = background2->GetTransform();
	transform->SetPosition({ 0.0f, 50.f });
	transform->SetScale(vector2(1200.0f, 400.0f) / 2.0f);
	transform->SetPosition({ 0.0f, 20.f });
	transform->SetScale(vector2(1200.0f, 800.0f) / 2.5f);
	transform->SetDepth(0.97f);
	bgptr = dynamic_cast<Background*>(background2->Add_Init_Component(new Background(background2)));
	bgptr->SetRate(0.5f);
	AddRegisterObject(background2);

	Object* background3 = new Object(this, "background3");
	sprite = dynamic_cast<Sprite*>(background3->Add_Component(new Sprite(background3)));
	sprite->SetTextureName("Cityline3");
	sprite->Init();
	transform = background3->GetTransform();
	transform->SetPosition({ 0.0f, 50.f });
	transform->SetScale(vector2(1200.0f, 400.0f) / 2.0f);
	transform->SetPosition({ 0.0f, 20.f });
	transform->SetScale(vector2(1200.0f, 400.0f) / 2.5f);
	transform->SetDepth(0.98f);
	bgptr = dynamic_cast<Background*>(background3->Add_Init_Component(new Background(background3)));
	bgptr->SetRate(0.3f);
	AddRegisterObject(background3);

	Object* background4 = new Object(this, "background4");
	sprite = dynamic_cast<Sprite*>(background4->Add_Component(new Sprite(background4)));
	sprite->SetTextureName("Cityline4");
	sprite->Init();
	transform = background4->GetTransform();
	transform->SetPosition({ 0.0f, 50.f });
	transform->SetScale(vector2(1200.0f, 400.0f) / 2.0f);
	transform->SetPosition({ 0.0f, 20.f });
	transform->SetScale(vector2(1200.0f, 400.0f) / 2.5f);
	transform->SetDepth(0.99f);
	bgptr = dynamic_cast<Background*>(background4->Add_Component(new Background(background4)));
	bgptr->Setshouldflip(false);
	bgptr->SetRate(-0.8f);
	bgptr->Init();
	AddRegisterObject(background4);

	m_event_manager.SetOwner(this);
	m_event_manager.AddEvent(&World0::Start_Event)->SetNext(&World0::Play_Event);

	m_badge = new Badge(m_playerlist.size());
}

World0::~World0()
{
}

void World0::Update(float dt)
{
	m_inner_time += dt;

	m_event_manager.Update(dt);

	if (m_event_manager.m_queue.empty() && Input::Is_Triggered(GLFW_KEY_SPACE))
	{
		STATE->Change_to_Menu_State();
		return;
	}

	if(Input::Is_Triggered(GLFW_KEY_KP_ADD) || Input::Is_Triggered(GLFW_KEY_P))
	{
		m_clock->GetComponentByTemplate<Clock>()->SetSecond(999.f);
	}
}

void World0::Close()
{
	m_inner_time = 0.0f;

	m_playerlist.clear();
	//m_objmanger->Write_Object_to_json();
	State::Close();

	//For restart
	m_board = nullptr;
	BestScore = 0;
	BestPlayer = 0;
	face_vector.clear();
	score_vector.clear();
	score_obj_vector.clear();
	BestFace = nullptr;
	m_real_end = false;

	m_start_msg = nullptr;
	m_clock = nullptr;
	m_board = nullptr;
	m_event_manager.Close();

	m_badge->Close();
	delete m_badge;
}