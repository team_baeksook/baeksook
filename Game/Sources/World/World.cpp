/***********************************************************************
File name		: World.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "World.h"
#include <iostream>
#include "System/Input.h"
#include "System/ObjectManager.h"
#include "Component/Sprite.h"
#include "Object/Object.h"

World::World(Graphic * graphic) : State(graphic) {}

void World::Initialize()
{
	//TODO::Trash..I do not want this to File i/o

	//Object* test_hud = new Object(this, "test");
	//test_hud->Add_Component(new Sprite(test_hud));
	//Sprite* sprite = test_hud->GetComponentByTemplate<Sprite>();
	//sprite->SetTextureName("Chicken_Face");
	//sprite->Init();
	//sprite->SetHud(true);
	//test_hud->GetTransform()->SetPosition(vector2(-0.9f, -0.9f));
	//test_hud->GetTransform()->SetScale(vector2(0.09f, 0.16f));
	//AddRegisterObject(test_hud);
}

World::~World()
{
}

void World::Update(float dt)
{
	//graphic_system->Draw_curvedline_bezier(Color(255, 0, 0, 255), true, nullptr, 5, vector2(-300, -300), vector2(-300, 300), vector2(300, 300), vector2(300, -300), vector2(0.0f, 0.0f));
	//graphic_system->DrawCircle(vector2(0.0f, 0.0f), 10.0f);

    if (Input::Is_Triggered(GLFW_KEY_Z) && isclick == false)
    {
        time = 0;

		graphic_system->Toggle_chaos(true);

        isclick = true;
    }
    if (isclick)
    {
        time += dt;

        if (time >= 1.01f)
        {
			graphic_system->Toggle_chaos(false);
            isclick = false;
        }
    }

	if (Input::Is_Triggered(GLFW_KEY_X) && isclick == false)
	{
		time = 0;

		graphic_system->Toggle_confuse(true);

		isclick = true;
	}
	if (isclick)
	{
		time += dt;

		if (time >= 1.01f)
		{
			graphic_system->Toggle_confuse(false);
			isclick = false;
		}
	}

	if (Input::Is_Triggered(GLFW_KEY_C) && isclick == false)
	{
		time = 0;

		graphic_system->Toggle_shake(true);

		isclick = true;
	}
	if (isclick)
	{
		time += dt;

		if (time >= 1.01f)
		{
			graphic_system->Toggle_shake(false);
			isclick = false;
		}
	}
}

void World::Close()
{
	//m_objmanger->Write_Object_to_json();
	State::Close();
}
