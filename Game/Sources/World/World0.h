/***********************************************************************
File name		: World0.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "System/State.h"
#include "System/Audio.h"
#include "System/EventManager.h"

#include <map>

struct ScorePtr
{
	Object* kill_score = nullptr;
	Object* death_score = nullptr;
	Object* dmg_score = nullptr;
};

struct PlayerInfo;
class Badge;

class World0 : public State
{
private:
    float m_inner_time = 0.0f;
private:	//object
	Object* m_start_msg = nullptr;
	Object* m_clock = nullptr;
	Object* m_board = nullptr;

	bool Start_Event(float t);
	float m_start_time = 0.0f;
	bool Boardfall_Event(float t);
	bool Play_Event(float t);
	bool Score_Event(float t);
	bool WaitUntilSpace(float t);
	bool ShowBadge(float t);

	EventManager<World0> m_event_manager;

private:	//result screen
	unsigned BestScore = 0;
	int BestPlayer = 0;
	std::map<std::string, Object*> face_vector;
	std::map<unsigned int, ScorePtr> score_obj_vector;
	std::map<unsigned int, PlayerInfo> score_vector;
	Object* BestFace = nullptr;
	bool m_real_end=false;

	std::string m_best_character_name;

	Audio* m_scoresound;
	Audio* m_fanfaresound;
public:
    World0(Graphic * graphic);
    void Initialize() override;
    ~World0() override;
    void Update(float dt) override;
    void Close() override;
};

