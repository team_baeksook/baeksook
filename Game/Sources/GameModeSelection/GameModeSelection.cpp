/***********************************************************************
File name		: GameModeSelection.cpp
Project name	: WFPT
Author			: Maxime Cauvin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/

#include "GameModeSelection.h"

GameModeSelection::GameModeSelection(Graphic * graphic) : State(graphic) { m_state_num = -1; }

void GameModeSelection::Initialize()
{
	Object* camera = new Object(this, "Camera");
	camera->Add_Init_Component(new Camera(camera, "Camera"));
	camera->GetTransform()->SetPosition(vector2(0.0f, 0.0f));
	AddRegisterObject(camera);

	//Training button
	Object* trainingButtonObj = new Object(this, "Training");
	trainingButtonObj->Add_Init_Component(new Button(trainingButtonObj));
	trainingButtonObj->GetTransform()->SetPosition({ -100, 20 });
	trainingButtonObj->GetTransform()->SetScale(vector2(60, 60));
	Button *trainingButton = trainingButtonObj->GetComponentByTemplate<Button>();
	trainingButton->SetCallbackOnClick([&]() {
		STATE->Change_State(CHARACTERSELECTION);
	});
	trainingButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 0;
		auto button = GetObjectManager()->Get_ObjectByName("Training")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stTraining = trainingButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stTraining->SetColor(Color(255, 0, 0, 255));
	stTraining->SetString("Training");
	vector2 v1 = trainingButtonObj->GetTransform()->GetLocalScale();
	stTraining->GetOwner()->GetTransform()->SetScale(vector2(1 / v1.x, 1 / v1.y));

	//Local button
	Object* localButtonObj = new Object(this, "Local");
	localButtonObj->Add_Init_Component(new Button(localButtonObj));
	localButtonObj->GetTransform()->SetPosition({ 0, 20 });
	localButtonObj->GetTransform()->SetScale(vector2(60, 60));
	Button *localButton = localButtonObj->GetComponentByTemplate<Button>();
	localButton->SetCallbackOnClick([&]() {
		STATE->Change_State(CHARACTERSELECTION);
	});
	localButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 1;
		auto button = GetObjectManager()->Get_ObjectByName("Local")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stLocal = localButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stLocal->SetColor(Color(255, 0, 0, 255));
	stLocal->SetString("Local");
	vector2 v2 = localButtonObj->GetTransform()->GetLocalScale();
	stLocal->GetOwner()->GetTransform()->SetScale(vector2(1 / v2.x, 1 / v2.y));

	//Online button
	Object* onlineButtonObj = new Object(this, "Online");
	onlineButtonObj->Add_Init_Component(new Button(onlineButtonObj));
	onlineButtonObj->GetTransform()->SetPosition({ 100, 20 });
	onlineButtonObj->GetTransform()->SetScale(vector2(60, 60));
	Button *onlineButton = onlineButtonObj->GetComponentByTemplate<Button>();
	onlineButton->SetCallbackOnClick([&]() {
		STATE->Change_State(GAMESCREEN);
	});
	onlineButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 2;
		auto button = GetObjectManager()->Get_ObjectByName("Online")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stOnline = onlineButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stOnline->SetColor(Color(255, 0, 0, 255));
	stOnline->SetString("Online");
	vector2 v3 = onlineButtonObj->GetTransform()->GetLocalScale();
	stOnline->GetOwner()->GetTransform()->SetScale(vector2(1 / v3.x, 1 / v3.y));

	//Back button
	Object* backButtonObj = new Object(this, "Back");
	backButtonObj->Add_Init_Component(new Button(backButtonObj));
	backButtonObj->GetTransform()->SetPosition({ 0, -50 });
	backButtonObj->GetTransform()->SetScale(vector2(80, 20));
	Button *backButton = backButtonObj->GetComponentByTemplate<Button>();
	backButton->SetCallbackOnClick([&]() {
		STATE->Change_State(MAINMENU);
	});
	backButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 3;
		auto button = GetObjectManager()->Get_ObjectByName("Back")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stBack = backButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stBack->SetColor(Color(255, 0, 0, 255));
	stBack->SetString("Back");
	vector2 v4 = backButtonObj->GetTransform()->GetLocalScale();
	stBack->GetOwner()->GetTransform()->SetScale(vector2(1 / v4.x, 1 / v4.y));

	AddRegisterObject(trainingButtonObj);
	AddRegisterObject(localButtonObj);
	AddRegisterObject(onlineButtonObj);
	AddRegisterObject(backButtonObj);

	m_buttons = { trainingButton, localButton, onlineButton, backButton };
	m_buttonIdx = -1;
	m_initialized = true;
}

GameModeSelection::~GameModeSelection()
{
}

void GameModeSelection::Update(float /*dt*/)
{
	m_buttonIdx = m_menu_input_manager.UpdateMenuNavigation(m_buttons, m_buttonIdx);
	m_menu_input_manager.CheckMenuButtonBack(GLFW_KEY_BACKSPACE, JOYSTICK_KEY::KEY_B, MAINMENU);
	m_menu_input_manager.CheckMenuButtonConfirm(GLFW_KEY_SPACE, JOYSTICK_KEY::KEY_A, m_buttons, m_buttonIdx);
}

void GameModeSelection::Close()
{
	State::Close();
}
