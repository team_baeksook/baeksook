/***********************************************************************
File name		: QuitConfirmation.h
Project name	: WFPT
Author			: Maxime Cauvin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include <iostream>
#include "Component/Button.h"
#include "Component/Sprite.h"
#include "Component/SpriteText.h"
#include "Object/Object.h"
#include "System/Engine.h"
#include "System/Input.h"
#include "System/Logger.h"
#include "System/MenuInputManager.h"
#include "System/ObjectManager.h"
#include "System/StateManager.h"
#include "System/State.h"

class QuitConfirmation : public State
{
private:
	std::vector<Button *> m_buttons;
	int m_buttonIdx;
	MenuInputManager m_menu_input_manager;
public:
	QuitConfirmation(Graphic * graphic);
	void Initialize() override;
	~QuitConfirmation() override;
	void Update(float dt) override;
	void Close() override;
};

