/***********************************************************************
File name		: QuitConfirmation.cpp
Project name	: WFPT
Author			: Maxime Cauvin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/

#include "QuitConfirmation.h"

QuitConfirmation::QuitConfirmation(Graphic * graphic) : State(graphic) { m_state_num = -1; }

void QuitConfirmation::Initialize()
{
	Object* camera = new Object(this, "Camera");
	camera->Add_Init_Component(new Camera(camera, "Camera"));
	camera->GetTransform()->SetPosition(vector2(0.0f, 0.0f));
	AddRegisterObject(camera);

	//Top text
	Object* topTextObj = new Object(this, "TopText");
	topTextObj->Add_Init_Component(new SpriteText(topTextObj));
	SpriteText *stTopText = topTextObj->GetComponentByTemplate<SpriteText>();
	stTopText->SetAllign(TEXT_ALLIGN::ALLIGN_CENTER);
	stTopText->SetColor(Color(255, 255, 255, 255));
	stTopText->SetString("Are you sure you want\nto quit the game?");
	stTopText->SetFontSize(16);
	topTextObj->GetTransform()->SetPosition(vector2(0, 20));

	//Cancel button
	Object* cancelButtonObj = new Object(this, "Cancel");
	cancelButtonObj->Add_Init_Component(new Button(cancelButtonObj));
	cancelButtonObj->GetTransform()->SetPosition({ -75, -70 });
	cancelButtonObj->GetTransform()->SetScale(vector2(80, 20));
	Button *cancelButton = cancelButtonObj->GetComponentByTemplate<Button>();
	cancelButton->SetCallbackOnClick([&]() {
		STATE->Change_State(MAINMENU);
	});
	cancelButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 0;
		auto button = GetObjectManager()->Get_ObjectByName("Cancel")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stCancel = cancelButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stCancel->SetColor(Color(255, 0, 0, 255));
	stCancel->SetString("Cancel");
	vector2 v1 = cancelButtonObj->GetTransform()->GetLocalScale();
	stCancel->GetOwner()->GetTransform()->SetScale(vector2(1 / v1.x, 1 / v1.y));

	//Confirm button
	Object* confirmButtonObj = new Object(this, "Confirm");
	confirmButtonObj->Add_Init_Component(new Button(confirmButtonObj));
	confirmButtonObj->GetTransform()->SetPosition({ 75, -70 });
	confirmButtonObj->GetTransform()->SetScale(vector2(80, 20));
	Button *confirmButton = confirmButtonObj->GetComponentByTemplate<Button>();
	confirmButton->SetCallbackOnClick([&]() {
		ENGINE->SetQuit();
	});
	confirmButton->SetCallbackOnHoverEnter([&]() {
		if (m_buttonIdx >= 0 && m_buttonIdx < m_buttons.size())
			m_buttons[m_buttonIdx]->SetSelected(false);
		m_buttonIdx = 1;
		auto button = GetObjectManager()->Get_ObjectByName("Confirm")->GetComponentByTemplate<Button>();
		button->SetSelected(true);
	});
	SpriteText *stConfirm = confirmButtonObj->FindChildByName("Text")->GetComponentByTemplate<SpriteText>();
	stConfirm->SetColor(Color(255, 0, 0, 255));
	stConfirm->SetString("Confirm");
	vector2 v2 = confirmButtonObj->GetTransform()->GetLocalScale();
	stConfirm->GetOwner()->GetTransform()->SetScale(vector2(1 / v2.x, 1 / v2.y));

	AddRegisterObject(topTextObj);
	AddRegisterObject(cancelButtonObj);
	AddRegisterObject(confirmButtonObj);

	m_buttons = { cancelButton, confirmButton };
	m_buttonIdx = -1;
	m_initialized = true;
}

QuitConfirmation::~QuitConfirmation()
{
}

void QuitConfirmation::Update(float /*dt*/)
{
	m_buttonIdx = m_menu_input_manager.UpdateMenuNavigation(m_buttons, m_buttonIdx);
	m_menu_input_manager.CheckMenuButtonBack(GLFW_KEY_BACKSPACE, JOYSTICK_KEY::KEY_B, MAINMENU);
	m_menu_input_manager.CheckMenuButtonConfirm(GLFW_KEY_SPACE, JOYSTICK_KEY::KEY_A, m_buttons, m_buttonIdx);
}

void QuitConfirmation::Close()
{
	State::Close();
}
