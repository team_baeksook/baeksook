/***********************************************************************
File name : TileMap.h
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Tile.h"
#include "Graphic/Graphic.h"

class TileMap
{
private:
	const vector2 Start_Position{ 0,0 };	//Criteria axis
	std::vector<Tile*>Tile_Map_Physic;
	std::vector<Tile*>Tile_Map_Graphic;

	Graphic* graphic=nullptr;

	std::unordered_map<int, std::vector<vector2>> m_vertex_list;
	std::unordered_map<int, std::vector<vector2>> m_texture_list;

	void Settexturelist();

	Tile ImGui_Tile;
public:
    TileMap();
	~TileMap();

	void Load_TileMap(unsigned level_num);
	void Save_TileMap(unsigned level_num);

	//Find specific tile by using position only physic tile
	Tile* operator[](vector2 pos);
	//Find specific tile by using position regardless physic
	Tile* Find_Tile(vector2 pos);

	void Set_Graphic(Graphic*);
	Tile* Get_ImGui_Tile();
	
	void Add_Tile(Tile& tile, vector2 pos);
	void Delete_Tile(vector2 pos);

	void Init();
	void Update(float dt);
	void Close();
};
