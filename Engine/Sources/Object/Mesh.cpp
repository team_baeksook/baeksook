/***********************************************************************
	File name		: Mesh.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Mesh.h"
#include "Math/vector2.hpp"

void Mesh::MakeBox()
{
	m_vertices.push_back(vector2(-0.5f, -0.5f));
	m_vertices.push_back(vector2(-0.5f, 0.5f));
	m_vertices.push_back(vector2(0.5f, -0.5f));

	m_vertices.push_back(vector2(0.5f, -0.5f));
	m_vertices.push_back(vector2(-0.5f, 0.5f));
	m_vertices.push_back(vector2(0.5f, 0.5f));
    m_Rendertype = RENDER_TYPE::TRIANGLE;
}

void Mesh::MakeCircle()
{
    m_vertices.push_back(vector2(0, 0));
    for (int i = 1; i < 122; ++i)
    {
        float degree = (i - 1) * PI / 60.0f;
        m_vertices.push_back(vector2(cos(degree) / 2.0f, sin(degree) / 2.0f));
    }
    m_Rendertype = RENDER_TYPE::FAN;
}

void Mesh::MakeTriangle()
{
    m_vertices.push_back(vector2(0.0f, 0.5f));
    m_vertices.push_back(vector2(cos(To_DEGREE(30.0f)) * 0.5f, sin(To_DEGREE(30.0f)) * 0.5f));
    m_vertices.push_back(vector2(-cos(To_DEGREE(30.0f)) * 0.5f, sin(To_DEGREE(30.0f)) * 0.5f));
    m_Rendertype = RENDER_TYPE::TRIANGLE;
}

void Mesh::MakeRightTriangle()
{
    m_vertices.push_back(vector2(0.25f, 0.75f));
    m_vertices.push_back(vector2(0.25f, -0.25f));
    m_vertices.push_back(vector2(-0.75f, -0.25f));
    m_Rendertype = RENDER_TYPE::TRIANGLE;
}

void Mesh::MakeTexture()
{
    m_vertices.push_back(vector2(-0.5f, -0.5f));
    m_vertices.push_back(vector2(-0.5f, 0.5f));
    m_vertices.push_back(vector2(0.5f, -0.5f));

    m_vertices.push_back(vector2(0.5f, -0.5f));
    m_vertices.push_back(vector2(-0.5f, 0.5f));
    m_vertices.push_back(vector2(0.5f, 0.5f));
    m_Rendertype = RENDER_TYPE::TRIANGLE;
}

void Mesh::MakeByName(std::string name)
{
    if (name == "Rectangle")
    {
        MakeBox();
        return;
    }
    if (name == "Circle")
    {
        MakeCircle();
    }
    if (name == "Triangle")
    {
        MakeTriangle();
    }
    if (name == "RightTriangle")
    {
        MakeRightTriangle();
    }
}

float* Mesh::GetVerticesPointer()
{
    return &(m_vertices.at(0).x);
}

float* Mesh::MakeVerticesPointer()
{
	float* pointer;
	int size = static_cast<int>(m_vertices.size());
	pointer = new float[size * 2];
	for (int i = 0; i < size; ++i)
	{
		pointer[i * 2] = m_vertices.at(i).x;
		pointer[i * 2 + 1] = m_vertices.at(i).y;
	}

	return pointer;
}

std::vector<vector2>& Mesh::GetVertices()
{
    return m_vertices;
}

int Mesh::GetSize() const
{
    return (int)m_vertices.size();
}

RENDER_TYPE Mesh::GetRenderType() const
{
    return m_Rendertype;
}

void Mesh::SetCoordinate(std::vector<vector2> vertices)
{
    m_vertices.assign(vertices.begin(), vertices.end());
    //m_vertices = m_vertices;
}

void Mesh::SetRenderType(RENDER_TYPE type)
{
    m_Rendertype = type;
}

void Mesh::ClearVertices()
{
    m_vertices.clear();
    m_vertices.resize(0);
}
