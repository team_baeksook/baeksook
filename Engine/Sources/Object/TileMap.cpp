/***********************************************************************
	File name		: TileMap.cpp
	Project name	: WFPT
	Author			: SeongWook Shin

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "TileMap.h"
#include "Graphic/Texture.h"
#include <rapidjson/filereadstream.h>
#include <rapidjson/document.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>
#include <iostream>
#include "System/Imgui_app.h"
#include "System/ObjectManager.h"
#include "System/StateManager.h"
#include "Object/Object.h"
#include "System/Input.h"

TileMap::TileMap()
{
    Tile_Map_Physic.push_back(new Tile(OBSTACLE, 0, vector2(0, -10.f), 0));
}

void TileMap::Settexturelist()
{
	m_vertex_list.clear();
	m_texture_list.clear();

	for(auto& tile : Tile_Map_Physic)
	{
		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(-5.0f, -5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(0.0f, tile->m_UnitSize.y) + tile->Get_Tile_Position());
		
		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(-5.0f, 5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(0.0f, 0.0f) + tile->Get_Tile_Position());

		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(5.0f, -5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(tile->m_UnitSize.x, tile->m_UnitSize.y) + tile->Get_Tile_Position());

		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(5.0f, -5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(tile->m_UnitSize.x, tile->m_UnitSize.y) + tile->Get_Tile_Position());
		
		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(-5.0f, 5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(0.0f, 0.0f) + tile->Get_Tile_Position());
		
		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(5.0f, 5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(tile->m_UnitSize.x, 0.0f) + tile->Get_Tile_Position());
	}
	for(auto& tile : Tile_Map_Graphic)
	{
		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(-5.0f, -5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(0.0f, tile->m_UnitSize.y) + tile->Get_Tile_Position());
		
		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(-5.0f, 5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(0.0f, 0.0f) + tile->Get_Tile_Position());

		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(5.0f, -5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(tile->m_UnitSize.x, tile->m_UnitSize.y) + tile->Get_Tile_Position());

		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(5.0f, -5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(tile->m_UnitSize.x, tile->m_UnitSize.y) + tile->Get_Tile_Position());
		
		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(-5.0f, 5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(0.0f, 0.0f) + tile->Get_Tile_Position());
		
		m_vertex_list[tile->Tile_Map_Number].push_back(vector2(5.0f, 5.0f) + tile->Get_Real_Position());
		m_texture_list[tile->Tile_Map_Number].push_back(vector2(tile->m_UnitSize.x, 0.0f) + tile->Get_Tile_Position());
	}
}

TileMap::~TileMap()
{
	Close();
}

Tile* TileMap::operator[](vector2 pos)
{
	for(auto i:Tile_Map_Physic)
	{
		if ((i)->Real_Position == pos)
			return i;
	}
	return nullptr;
}

Tile* TileMap::Find_Tile(vector2 pos)
{
	Tile* result_tile = (*this)[pos]; //Only physic tile
	if ((*this)[pos] != nullptr)
	{
		return result_tile;
	}
	else
	{
		for (auto i : Tile_Map_Graphic)
		{
			if ((i)->Real_Position == pos)
				return i;
		}
		return nullptr;
	}
}

void TileMap::Load_TileMap(unsigned level_num)
{
	Close();
	errno_t err;
	FILE* fp;
	std::string filename = "data/";
	filename += std::to_string(level_num)+"level.json";
	err = fopen_s(&fp, filename.c_str(), "rb");
	if (err != 0)
		return;

	char readBuffer[65536] = { 0 };
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	rapidjson::Document d;
	d.ParseStream(is);

	const rapidjson::Value& comp = d["TileMap"];
	for (rapidjson::SizeType i = 0; i < comp.Size(); i++)
	{
		Tile* loaded_tile = new Tile;
		loaded_tile->Real_Position.x = comp[i].FindMember("Real_Position_x")->value.GetFloat();
		loaded_tile->Real_Position.y = comp[i].FindMember("Real_Position_y")->value.GetFloat();
		loaded_tile->Tile_Map_Name = comp[i].FindMember("Tile_Map_Name")->value.GetString();
		loaded_tile->Tile_Map_Number= graphic->GetTexture(loaded_tile->Tile_Map_Name)->GetID();
		loaded_tile->m_UnitSize.x = comp[i].FindMember("Tile_Map_UnitSize_x")->value.GetFloat();
		loaded_tile->m_UnitSize.y = comp[i].FindMember("Tile_Map_UnitSize_y")->value.GetFloat();
		loaded_tile->Tile_Map_Position.x = comp[i].FindMember("Tile_Map_Position_x")->value.GetFloat();
		loaded_tile->Tile_Map_Position.y = comp[i].FindMember("Tile_Map_Position_y")->value.GetFloat();
		loaded_tile->Type = static_cast<Tile_Type>(comp[i].FindMember("Tile_Type")->value.GetInt());

		if (static_cast<int>(loaded_tile->Type) < BACKGROUND)
		{
			Tile_Map_Physic.push_back(loaded_tile);
		}
		else
		{
			Tile_Map_Graphic.push_back(loaded_tile);
		}
	}
	fclose(fp);
	Settexturelist();
}

void TileMap::Save_TileMap(unsigned level_num)
{
	errno_t err;
	FILE* fp;
	std::string filename = "data/";
	filename += std::to_string(level_num) + "level.json";
	err = fopen_s(&fp, filename.c_str(), "wb"); // non-Windows use "r"

	char writeBuffer[65536];
	rapidjson::FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
	rapidjson::PrettyWriter<rapidjson::FileWriteStream> writer(os);
	writer.StartObject();
	writer.Key("TileMap");
	writer.StartArray();
	for (auto i = Tile_Map_Physic.begin(); i != Tile_Map_Physic.end(); ++i)
	{
		writer.StartObject();
		writer.Key("Real_Position_x");
		writer.Double((*i)->Real_Position.x);
		writer.Key("Real_Position_y");
		writer.Double((*i)->Real_Position.y);
		writer.Key("Tile_Map_Name");
		writer.String((*i)->Tile_Map_Name.c_str());
		writer.Key("Tile_Map_UnitSize_x");
		writer.Double((*i)->m_UnitSize.x);
		writer.Key("Tile_Map_UnitSize_y");
		writer.Double((*i)->m_UnitSize.y);
		writer.Key("Tile_Map_Position_x");
		writer.Double((*i)->Tile_Map_Position.x);
		writer.Key("Tile_Map_Position_y");
		writer.Double((*i)->Tile_Map_Position.y);
		writer.Key("Tile_Type");
		writer.Int(static_cast<int>((*i)->Type));

		writer.EndObject();
	}
	for (auto i = Tile_Map_Graphic.begin(); i != Tile_Map_Graphic.end(); ++i)
	{
		writer.StartObject();
		writer.Key("Real_Position_x");
		writer.Double((*i)->Real_Position.x);
		writer.Key("Real_Position_y");
		writer.Double((*i)->Real_Position.y);
		writer.Key("Tile_Map_Name");
		writer.String((*i)->Tile_Map_Name.c_str());
		writer.Key("Tile_Map_UnitSize_x");
		writer.Double((*i)->m_UnitSize.x);
		writer.Key("Tile_Map_UnitSize_y");
		writer.Double((*i)->m_UnitSize.y);
		writer.Key("Tile_Map_Position_x");
		writer.Double((*i)->Tile_Map_Position.x);
		writer.Key("Tile_Map_Position_y");
		writer.Double((*i)->Tile_Map_Position.y);
		writer.Key("Tile_Type");
		writer.Int(static_cast<int>((*i)->Type));
		writer.EndObject();
	}
	writer.EndArray();
	writer.EndObject();
	fclose(fp);
}

void TileMap::Set_Graphic(Graphic* graphic_ptr)
{
	graphic = graphic_ptr;
}

Tile * TileMap::Get_ImGui_Tile()
{
	return &ImGui_Tile;
}

void TileMap::Add_Tile(Tile& tile, vector2 pos)
{
	const auto tile_ptr = Find_Tile(pos);
	if (tile_ptr != nullptr)
	{
		Delete_Tile(pos);
	}
	if (tile.Type < BACKGROUND)//if it is physic tile
	{
		Tile_Map_Physic.push_back(new Tile(tile));	//make new tile
	}
	else					   //if it is graphic tile
	{
		Tile_Map_Graphic.push_back(new Tile(tile));	//make new tile
	}

	vector2 real_position = tile.Get_Real_Position();
	vector2 tile_position = tile.Get_Tile_Position();

	m_vertex_list[tile.Tile_Map_Number].push_back(vector2(-5.0f, -5.0f) + real_position);
	m_texture_list[tile.Tile_Map_Number].push_back(vector2(0.0f, tile.m_UnitSize.y) + tile_position);

	m_vertex_list[tile.Tile_Map_Number].push_back(vector2(-5.0f, 5.0f) + real_position);
	m_texture_list[tile.Tile_Map_Number].push_back(vector2(0.0f, 0.0f) + tile_position);

	m_vertex_list[tile.Tile_Map_Number].push_back(vector2(5.0f, -5.0f) + real_position);
	m_texture_list[tile.Tile_Map_Number].push_back(vector2(tile.m_UnitSize.x, tile.m_UnitSize.y) + tile_position);

	m_vertex_list[tile.Tile_Map_Number].push_back(vector2(5.0f, -5.0f) + real_position);
	m_texture_list[tile.Tile_Map_Number].push_back(vector2(tile.m_UnitSize.x, tile.m_UnitSize.y) + tile_position);

	m_vertex_list[tile.Tile_Map_Number].push_back(vector2(-5.0f, 5.0f) + real_position);
	m_texture_list[tile.Tile_Map_Number].push_back(vector2(0.0f, 0.0f) + tile_position);

	m_vertex_list[tile.Tile_Map_Number].push_back(vector2(5.0f, 5.0f) + real_position);
	m_texture_list[tile.Tile_Map_Number].push_back(vector2(tile.m_UnitSize.x, 0.0f) + tile_position);
}

//Not Efficient method
void TileMap::Delete_Tile(vector2 pos)
{
	const auto tile_ptr = Find_Tile(pos);
	if (tile_ptr == nullptr)
		return;
	else
	{
		if (tile_ptr->Type < BACKGROUND)//if physic tile
		{
			for (auto i = Tile_Map_Physic.begin(); i != Tile_Map_Physic.end(); ++i)
			{
				if ((*i)->Real_Position == pos)
				{
					Tile_Map_Physic.erase(i);
					break;
				}
			}
		}
		else //if graphic tile
		{
			for (auto i = Tile_Map_Graphic.begin(); i != Tile_Map_Graphic.end(); ++i)
			{
				if ((*i)->Real_Position == pos)
				{
					Tile_Map_Graphic.erase(i);
					break;
				}
			}
		}
		delete tile_ptr;
	}

	Settexturelist();
}

void TileMap::Init()
{
	IMGUIAPP->Set_Tile(&ImGui_Tile);
	ImGui_Tile.Set_Tile_Map_Number(STATE->Get_CurrentState()->GetGraphicSystem()->GetTexture("tile_map")->GetID());
}

void TileMap::Update(float /*dt*/)
{
	auto texture = m_texture_list.begin();
	for (auto& vertex : m_vertex_list)
	{
		DrawBehavior* draw = new DrawBehavior();
		draw->m_texture_id = vertex.first;
		draw->m_size = static_cast<int>(vertex.second.size()) * 2;
		draw->m_vertex.assign(vertex.second.begin(), vertex.second.end());
		draw->m_texture.assign((*texture).second.begin(), (*texture).second.end());
		draw->m_type = graphic_enum::DrawType::TILE;
		for (int i = 0; i < 4; ++i)
		{
			draw->m_color[i] = 1.0f;
		}

		graphic->m_drawlist.insert(std::make_pair(0.5f, draw));
		++texture;
	}

	if (IMGUIAPP->Get_Edit_Type() == TILE_MAP_EDIT)
	{
		vector2 pos = STATE->Get_CurrentState()->GetObjectManager()->Get_ObjectByName("Camera")->
							GetComponentByTemplate<Camera>()->GetLocalPosition(Input::Get_Mouse_Position());
		if (Input::Is_Pressed(GLFW_MOUSE_BUTTON_MIDDLE))
		{
			ImGui_Tile.Set_Real_Position(Round_Position(pos));
			Add_Tile(ImGui_Tile, Round_Position(pos));
		}
		else if (Input::Is_Pressed(GLFW_MOUSE_BUTTON_RIGHT))
		{
			Delete_Tile(Round_Position(pos));
		}
	}

}

void TileMap::Close()
{
	for (auto iter = Tile_Map_Physic.begin(); iter != Tile_Map_Physic.end();)
	{
		delete *iter;
		iter= Tile_Map_Physic.erase(iter);
	}
	for (auto iter = Tile_Map_Graphic.begin(); iter != Tile_Map_Graphic.end();)
	{
		delete *iter;
		iter = Tile_Map_Graphic.erase(iter);
	}

	Tile_Map_Physic.clear();
	Tile_Map_Graphic.clear();
	m_vertex_list.clear();
	m_texture_list.clear();
}