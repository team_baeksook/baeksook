/***********************************************************************
	File name		: Tile.h
	Project name	: WFPT
	Author			: SeongWook Shin

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <string>
#include "Math/vector2.hpp"
#include "Component/Collision.h"


enum Tile_Type
{
	OBSTACLE=0,
	ONEWAY=1,
	GHOST=2,
	BACKGROUND=3,
	EMPTY=4,
	NUMBEROFTYPE
};

class Tile
{
	friend class TileMap;
	friend class IMGUI_app;
private:
	Tile_Type Type = EMPTY;
	int Tile_Map_Number=0;
	std::string Tile_Map_Name= "tile_map";
	vector2 Real_Position{ 0,0 };
	vector2 Tile_Map_Position{ 0,0 };
	vector2 m_UnitSize = vector2(0.0416666679084301f, 0.0625f);
	//Collision*	m_collision_comp;
public:
	Tile()=default;
	Tile(Tile_Type type,int tile_map_number,vector2 real_position,vector2 tile_map_position):
			Type(type),Tile_Map_Number(tile_map_number),Real_Position(real_position),Tile_Map_Position(tile_map_position)
	{}

	void Update();

	void Set_Tile_Map_Number(int num);
	void Set_Real_Position(vector2 pos);
	
	Tile_Type Get_Type() const;
	vector2 Get_Real_Position();
	vector2 Get_Tile_Position();
};
