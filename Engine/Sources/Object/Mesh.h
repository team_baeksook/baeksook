/***********************************************************************
	File name		: Mesh.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Graphic/Graphic.h"

class Mesh
{
private:
    std::vector<vector2> m_vertices;
    RENDER_TYPE m_Rendertype;
public:
	friend class Collision;

    void MakeBox();
    void MakeCircle();
    void MakeTriangle();
    void MakeRightTriangle();
    void MakeTexture();

    void MakeByName(std::string name);

    float* GetVerticesPointer();
	float* MakeVerticesPointer();
	std::vector<vector2>& GetVertices();
    int GetSize() const;
    RENDER_TYPE GetRenderType() const;
    void SetCoordinate(std::vector<vector2> vertices);
    void SetRenderType(RENDER_TYPE type);
    void ClearVertices();
};
