/***********************************************************************
File name : Transform.h
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Math/MathLibrary.hpp"
#include <rapidjson/prettywriter.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/document.h>

class Object;

class Transform
{
    friend class Physics;
    friend class Collision;
	friend class ParticlePhysics;
	friend class Player;
private:
    vector2 m_Position;
    vector2 m_Scale;
    float m_Degree;

    vector2 m_previouspos;
    vector2 m_previousscale;
    float m_previousdegree;

	bool m_hierarchy = true;

    float m_depth = 0.0f;

    affine2d m_transformmat;

	Object* m_owner = nullptr;
public:
	Transform();
    Transform(Object* owner);
    void SetTransform(vector2 position, vector2 scale, float rotate);

    affine2d BuildTransformMatrix();
	affine2d BuildInverseTransformMatrix() const;

    void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&);
    void Deserialization(const rapidjson::Value &);
    void ImGui_Setting();

	void SetPosition(vector2 pos);
	void SetPositionX(float xvalue);
	void SetPositionY(float yvalue);
    void SetScale(vector2 scale);
    void SetDegree(float degree);

    float GetDepth() const;
    void SetDepth(float depth);

    vector2 GetWorldPosition();
	vector2* GetLocalPositionPtr();
    vector2 GetLocalPosition() const;
	void AddPosition(vector2 vec);

	vector2 GetWorldScale();
	vector2 GetLocalScale() const;
	void AddScale(vector2 vec);
	void MultiplyScale(vector2 scale);

    float GetWorldDegree();
    float GetLocalDegree() const;
	void AddDegree(float deg);

	void SetZero();

	void CalculateLocalTransform(Transform* parent);
	void CalculateWorldTransform(Transform* parent);

    float GetDiagonal() const;

	float* GetPointer();
	float* MakePointer();
};

