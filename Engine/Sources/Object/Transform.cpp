/***********************************************************************
File name : Transform.cpp
Project name : WFPT
Author : MinSuk Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Transform.h"
#include "IMGUI/imgui.h"
#include "Object.h"
#include <iostream>

Transform::Transform() : m_Position(vector2(0.0f, 0.0f)), m_Scale(vector2(1.0f, 1.0f)), m_Degree(0.0f)
{
	m_transformmat = Translate_Affine(m_Position) * Rotation_Affine_Degree(m_Degree) * Scale_Affine(m_Scale);
	m_previouspos = m_Position;
	m_previousscale = m_Scale;
	m_previousdegree = m_Degree;
}

Transform::Transform(Object* owner) : m_Position(vector2(0, 0)), m_Scale(vector2(1.0f, 1.0f)), m_Degree(0.0f), m_owner(owner)
{
    m_transformmat = Translate_Affine(m_Position) * Rotation_Affine_Degree(m_Degree) * Scale_Affine(m_Scale);
    m_previouspos = m_Position;
    m_previousscale = m_Scale;
    m_previousdegree = m_Degree;
}

void Transform::SetTransform(vector2 position, vector2 scale, float rotate)
{
    m_Position = position;
    m_Scale = scale;
    m_Degree = rotate;
}

affine2d Transform::BuildTransformMatrix()
{
    if((m_Position != m_previouspos) || (m_Scale != m_previousscale) || (m_Degree != m_previousdegree) || m_hierarchy)
    {
        m_transformmat = Translate_Affine(m_Position) * Rotation_Affine_Degree(m_Degree) * Scale_Affine(m_Scale);
        m_previouspos = m_Position;
        m_previousscale = m_Scale;
        m_previousdegree = m_Degree;
		m_hierarchy = false;
    }

	if(m_owner && m_owner->HasParent())
	{
		m_transformmat = m_owner->GetParent()->GetTransform()->BuildTransformMatrix() * m_transformmat;
		m_hierarchy = true;
	}

    return m_transformmat;
}

affine2d Transform::BuildInverseTransformMatrix() const
{
	if(m_owner && m_owner->HasParent())
	{
		return Scale_Affine(1.0f / m_Scale.x, 1.0f / m_Scale.y) *
			Rotation_Affine_Degree(-m_Degree) * Translate_Affine(-m_Position) * m_owner->GetParent()->GetTransform()->BuildInverseTransformMatrix();
	}

	return Scale_Affine(1.0f / m_Scale.x, 1.0f / m_Scale.y) *
		Rotation_Affine_Degree(-m_Degree) * Translate_Affine(-m_Position);
}

void Transform::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
    writer.StartObject();
    writer.Key("Position_x");
    writer.Double(m_Position.x);
    writer.Key("Position_y");
    writer.Double(m_Position.y);
    writer.Key("Scale_x");
    writer.Double(m_Scale.x);
    writer.Key("Scale_y");
    writer.Double(m_Scale.y);
    writer.Key("m_Degree");
    writer.Double(m_Degree);
    writer.Key("Depth");
    writer.Double(m_depth);
    writer.EndObject();
}

void Transform::Deserialization(const rapidjson::Value & object)
{
    m_Position.x = object.FindMember("Position_x")->value.GetFloat();
    m_Position.y = object.FindMember("Position_y")->value.GetFloat();
    m_Scale.x = object.FindMember("Scale_x")->value.GetFloat();
    m_Scale.y = object.FindMember("Scale_y")->value.GetFloat();
    m_Degree = object.FindMember("m_Degree")->value.GetFloat();
    m_depth = object.FindMember("Depth")->value.GetFloat();
}

void Transform::ImGui_Setting()
{
    if (ImGui::CollapsingHeader("Transform"))
    {
        ImGui::InputFloat("m_Position x", &m_Position.x);
        ImGui::InputFloat("m_Position y", &m_Position.y);
        ImGui::InputFloat("m_Scale x", &m_Scale.x);
        ImGui::InputFloat("m_Scale y", &m_Scale.y);
        ImGui::InputFloat("m_Degree", &m_Degree);
        ImGui::InputFloat("Depth", &m_depth);
    }
}

void Transform::SetPosition(vector2 pos)
{
	m_Position = pos;
}

void Transform::SetPositionX(float xvalue)
{
	m_Position.x = xvalue;
}

void Transform::SetPositionY(float yvalue)
{
	m_Position.y = yvalue;
}

void Transform::SetScale(vector2 scale)
{
    m_Scale = scale;
}

void Transform::SetDegree(float degree)
{
    m_Degree = degree;
}

float Transform::GetDepth() const
{
	if (m_owner && m_owner->HasParent())
	{
		return m_depth + m_owner->GetParent()->GetTransform()->GetDepth();
	}

    return m_depth;
}

void Transform::SetDepth(float depth)
{
    m_depth = depth;
}

vector2 Transform::GetWorldPosition()
{
	BuildTransformMatrix();

	if(m_owner && m_owner->HasParent())
	{
		return vector2(m_transformmat(2, 0), m_transformmat(2, 1));
	}

    return m_Position;
}

vector2* Transform::GetLocalPositionPtr()
{
	return &m_Position;
}

vector2 Transform::GetLocalPosition() const
{
	return m_Position;
}

void Transform::AddPosition(vector2 vec)
{
	m_Position += vec;
}

vector2 Transform::GetWorldScale()
{
	BuildTransformMatrix();

	if(m_owner->HasParent())
	{
		return vector2(m_owner->GetParent()->GetTransform()->GetWorldScale().x * m_Scale.x, m_owner->GetParent()->GetTransform()->GetWorldScale().y * m_Scale.y);
	}

	return m_Scale;
}

vector2 Transform::GetLocalScale() const
{
	return m_Scale;
}

void Transform::AddScale(vector2 vec)
{
	m_Scale += vec;
}

void Transform::MultiplyScale(vector2 scale)
{
	m_Scale.x *= scale.x;
	m_Scale.y *= scale.y;
}

float Transform::GetWorldDegree()
{
	BuildTransformMatrix();

	if(m_owner->HasParent())
	{
		return To_DEGREE(std::atan2(m_transformmat(0, 1), m_transformmat(0, 0)));
	}

	return m_Degree;
}

float Transform::GetLocalDegree() const
{
	return m_Degree;
}

void Transform::AddDegree(float deg)
{
	m_Degree += deg;
}

void Transform::SetZero()
{
	m_Position = vector2(0.0f, 0.0f);
	m_Scale = vector2(1.0f, 1.0f);
	m_Degree = 0.0f;
	m_transformmat = Translate_Affine(m_Position) * Rotation_Affine_Degree(m_Degree) * Scale_Affine(m_Scale);
	m_previouspos = m_Position;
	m_previousscale = m_Scale;
	m_previousdegree = m_Degree;
}

void Transform::CalculateLocalTransform(Transform* parent)
{
	affine2d transform = parent->BuildInverseTransformMatrix() * BuildTransformMatrix();

	m_Position.x = transform(2, 0);
	m_Position.y = transform(2, 1);
	m_Scale.x = m_Scale.x / parent->GetWorldScale().x;
	m_Scale.y = m_Scale.y / parent->GetWorldScale().y;
	m_Degree = To_DEGREE(std::atan2(transform(0, 1), transform(0, 0)));
	m_depth = m_depth - parent->GetDepth();
}

void Transform::CalculateWorldTransform(Transform* parent)
{
	BuildTransformMatrix();

	m_Position.x = m_transformmat(2, 0);
	m_Position.y = m_transformmat(2, 1);
	m_Scale.x = m_Scale.x * parent->GetWorldScale().x;
	m_Scale.y = m_Scale.y * parent->GetWorldScale().y;
	m_Degree = To_DEGREE(std::atan2(m_transformmat(0, 1), m_transformmat(0, 0)));
	m_depth = m_depth + parent->GetDepth();
}

float Transform::GetDiagonal() const
{
    return Magnitude(m_Scale);
}

float* Transform::GetPointer()
{
	BuildTransformMatrix();

	return &m_transformmat.column1.x;
}

float* Transform::MakePointer()
{
	BuildTransformMatrix();

	float* pt = new float[9];
	pt[0] = m_transformmat.column1.x;
	pt[1] = m_transformmat.column1.y;
	pt[2] = m_transformmat.column1.z;
	pt[3] = m_transformmat.column2.x;
	pt[4] = m_transformmat.column2.y;
	pt[5] = m_transformmat.column2.z;
	pt[6] = m_transformmat.column3.x;
	pt[7] = m_transformmat.column3.y;
	pt[8] = m_transformmat.column3.z;

	return pt;
}
