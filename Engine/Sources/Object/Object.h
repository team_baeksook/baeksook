/***********************************************************************
	File name		: Object.h
	Project name	: WFPT
	Author			: SeongWook Shin

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <vector>
#include "Transform.h"
#include "Component/Component.h"

class State;

class Object
{
	friend class IMGUI_app;
private:
    Transform* transform = nullptr;
	std::vector<Component*>component;
	std::string Name = "Unnamed";
    State * m_state;

	Object * m_parent = nullptr;
	std::vector<Object*> m_children;

	unsigned int m_ID = 0;

	bool m_ismortal = false;
	float m_lifetime = 0.0f;
	bool m_isdead = false;

	bool m_blink_when_dying = false;

	void EraseChild(Object* target);
	void AddChild(Object* target);
public:
    Object(State * state, std::string name = "Unnamed");
	Object(std::string name);
    ~Object();
    Transform* GetTransform();
    void SetTransform(Transform* temp_transform);
    template <typename COMPONENT>
    COMPONENT* GetComponentByTemplate() const;
	template <typename COMPONENT>
	COMPONENT* GetComponentByDynamicCasting() const;
    Component* Add_Component(Component*);
	Component* Add_Init_Component(Component* comp);

	template <typename COMPONENT>
	bool DeleteComponentByTemplate();

    void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&);
	void Deserialization(const rapidjson::Value& object, bool should_init = true);

    void Update(float dt);
    void Close(void);

    State * GetState() const;

    void ImGui_Setting();
    std::string GetName() const { return Name; };

	Object* GetParent() const;
	bool HasParent() const;
	bool HasChild() const;
	void AttatchToParent(Object * parent);
	std::vector<Object*>& GetChildren();
	void Detach();
	void Detach(bool ischild);
	Object* FindChildByName(const std::string& name);

	unsigned int GetID() const;
	void SetID(unsigned int id);

	void SetLifetime(float time);

	bool IsDead() const;
	void MakeDead();
};

template<typename COMPONENT>
COMPONENT * Object::GetComponentByTemplate() const
{
    for (auto i : component)
    {
        if (typeid(COMPONENT) == typeid(*i))
        {
            return dynamic_cast<COMPONENT*>(i);
        }
    }
    return nullptr;
}

//more slower version but for the derived class
template <typename COMPONENT>
COMPONENT* Object::GetComponentByDynamicCasting() const
{
	for (auto i : component)
	{
		if (COMPONENT* comp = dynamic_cast<COMPONENT*>(i); comp != nullptr)
		{
			return comp;
		}
	}
	return nullptr;
}

template <typename COMPONENT>
bool Object::DeleteComponentByTemplate()
{
	for(auto comp = component.begin(); comp != component.end(); ++comp)
	{
		if(typeid(COMPONENT) == typeid(*(*comp)))
		{
			(*comp)->Close();
			delete (*comp);
			component.erase(comp);
			return true;
		}
	}
	return false;
}
