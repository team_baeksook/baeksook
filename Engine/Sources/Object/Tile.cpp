/***********************************************************************
	File name		: Tile.cpp
	Project name	: WFPT
	Author			: SeongWook Shin

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Tile.h"

void Tile::Update()
{
	
}

void Tile::Set_Tile_Map_Number(int num)
{
	Tile_Map_Number = num;
}

void Tile::Set_Real_Position(vector2 pos)
{
	Real_Position = pos;
}

Tile_Type Tile::Get_Type() const
{
	return Type;
}

vector2 Tile::Get_Real_Position()
{
	return Real_Position;
}

vector2 Tile::Get_Tile_Position()
{
	return Tile_Map_Position;
}
