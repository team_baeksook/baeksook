/***********************************************************************
	File name		: Object.cpp
	Project name	: WFPT
	Author			: SeongWook Shin

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include <typeinfo>
#include "Object.h"
#include "IMGUI/imgui.h"
#include "Component/Physics.h"
#include "Component/Sprite.h"
#include "Component/SpriteText.h"
#include "System/Imgui_app.h"
#include "Component/Camera.h"
#include "System/State.h"
#include "System/ObjectManager.h"
#include "Component/ParticlePhysics.h"
#include "Component/Player.h"
#include "Component/Weapon/Bullet.h"
#include "Component/Weapon/Revolver.h"
#include "Component/Weapon/Rifle.h"
#include "Component/Weapon/Tomahawk.h"
#include "Component/Weapon/Shotgun.h"
#include "Component/Weapon/GuidedMissile.h"
#include "Component/Weapon/Bow.h"
#include "Component/Weapon/Grenade.h"
#include "Component/Weapon/Sniper.h"
#include "Component/Weapon/FlameThrower.h"
#include "Component/WeaponGenerator.h"
#include "Component/Weapon/Pistol.h"
#include "Component/Weapon/LMG.h"
#include "Component/Weapon/Laser.h"
#include "Component/Weapon/Magneto.h"
#include "Component/RedZone.h"
#include "Component/ClearPortal.h"

void Object::EraseChild(Object* target)
{
	for(auto child = m_children.begin(); child != m_children.end(); ++child)
	{
		if(*child == target)
		{
			m_children.erase(child);
			return;
		}
	}
}

void Object::AddChild(Object* target)
{
	m_children.push_back(target);
}

Object::Object(State* state, std::string name)
{
    transform = new Transform(this);
    m_state = state;
    Name = name;
}

Object::Object(std::string name)
{
	transform = nullptr;
	m_state = nullptr;
	Name = name;
}

Object::~Object()
{
	Detach(true);
	for(auto& child : m_children)
	{
		child->Detach();
	}
	m_children.clear();

    delete transform;
	transform = nullptr;

	for(auto& comp : component)
	{
		comp->Close();
		delete comp;
	}

	component.clear();
}

Transform* Object::GetTransform()
{
    return transform;
}

void Object::SetTransform(Transform* temp_transform)
{
    transform = temp_transform;
}

Component* Object::Add_Component(Component * comp)
{
	component.push_back(comp);

	return component.back();
}

Component* Object::Add_Init_Component(Component* comp)
{
	comp->Init();

	component.push_back(comp);

	return component.back();
}

void Object::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
    writer.StartObject();
    writer.Key("m_Name");
    writer.String(Name.c_str());
	writer.Key("m_lifetime");
	writer.Double(static_cast<double>(m_lifetime));
    writer.Key("Transform");
    GetTransform()->Serialization(writer);
	writer.Key("BlinkWhenDying");
	writer.Bool(m_blink_when_dying);
    writer.Key("Component");
    writer.StartArray();
    for (auto i : component)
    {
        i->Serialization(writer);
    }
    writer.EndArray();
    writer.EndObject();
}

void Object::Deserialization(const rapidjson::Value & object, bool should_init)
{
    Name = object.FindMember("m_Name")->value.GetString();

	if (object.FindMember("m_lifetime")->value.GetFloat()!=0)
	{
		SetLifetime(object.FindMember("m_lifetime")->value.GetFloat());
	}

	if(transform == nullptr)
	{
		transform = new Transform(this);
	}
    transform->Deserialization(object["Transform"]);
	m_blink_when_dying = object.FindMember("BlinkWhenDying")->value.GetBool();

	//TODO::Add component when you add component
    const rapidjson::Value& comp = object["Component"];
    for (rapidjson::SizeType i = 0; i < comp.Size(); i++)
    {
        switch (comp[i].FindMember("Kind")->value.GetInt())
        {
        case COMPONENT_KIND::SPRITE:
            //Add_Component(new Sprite(this, "Sprite"));
			component.push_back(new Sprite(this));
            break;
        case COMPONENT_KIND::PHYSICS:
            //Add_Component(new Physics(this, "Physics"));
			component.push_back(new Physics(this));
            break;
		case COMPONENT_KIND::SPRITE_TEXT:
			//Add_Component(new SpriteText(this, "SpriteText"));
			component.push_back(new SpriteText(this));
			break;
        case COMPONENT_KIND::CAMERA:
            //Add_Component(new Camera(this, "Camera"));
			component.push_back(new Camera(this));
            break;
		case COMPONENT_KIND::PARTICLEPHYSICS:
			//Add_Component(new ParticlePhysics(this, "ParticlePhysics"));
			component.push_back(new ParticlePhysics(this));
			break;
		case COMPONENT_KIND::COLLISION:
			//Add_Component(new Collision(this, "Collision"));
			component.push_back(new Collision(this));
			break;
		case COMPONENT_KIND::PLAYER:
			//Add_Component(new Player(this, "Player"));
			component.push_back(new Player(this));
			break;
		case COMPONENT_KIND::BULLET:
			//Add_Component(new Bullet(this, "Bullet"));
			component.push_back(new Bullet(this));
			break;
		case COMPONENT_KIND::REVOLVER:
			//Add_Component(new Revolver(this, "Revolver"));
			component.push_back(new Revolver(this));
			break;
		case COMPONENT_KIND::RIFLE:
			//Add_Component(new Rifle(this, "Rifle"));
			component.push_back(new Rifle(this));
			break;
		case COMPONENT_KIND::TOMAHAWK:
			//Add_Component(new Tomahawk(this, "Tomahawk"));
			component.push_back(new Tomahawk(this));
			break;
		case COMPONENT_KIND::PARTICLEGENERATOR :
			//Add_Component(new ParticleGenerator(this, "ParticleGenerator"));
			component.push_back(new ParticleGenerator(this));
			break;
		case COMPONENT_KIND::SHOTGUN:
			component.push_back(new Shotgun(this));
			break;
		case COMPONENT_KIND::GUIDEDMISSILE:
			component.push_back(new GuidedMissile(this));
			break;
		case COMPONENT_KIND::BOW:
			component.push_back(new Bow(this));
			break;
		case COMPONENT_KIND::GRENADE:
			component.push_back(new Grenade(this));
			break;
		case COMPONENT_KIND::SNIPER:
			component.push_back(new Sniper(this));
			break;
		case COMPONENT_KIND::FLAMETHROWER:
			component.push_back(new FlameThrower(this));
			break;
		case COMPONENT_KIND::WEAPONGENERATOR:
			component.push_back(new WeaponGenerator(this));
			break;
		case COMPONENT_KIND::REDZONE:
			component.push_back(new RedZone(this));
			break;
		case COMPONENT_KIND::LASER:
			component.push_back(new Laser(this));
			break;
		case COMPONENT_KIND::PISTOL:
			component.push_back(new Pistol(this));
			break;
		case COMPONENT_KIND::LMG:
			component.push_back(new LightMachineGun(this));
			break;
		case COMPONENT_KIND::MAGNETO:
			component.push_back(new Magneto(this));
			break;
		case COMPONENT_KIND::CLEARPORTAL:
			component.push_back(new ClearPortal(this));
			break;
        default:
            break;
        }
        //component[i]->Deserialization(comp[i]);
        component.back()->Deserialization(comp[i]);
		if(should_init)
		{
			component[i]->Init();
		}
    }
}

State* Object::GetState() const
{
    return m_state;
}

void Object::ImGui_Setting()
{
    static char temp_name[40];
    strcpy_s(temp_name, Name.c_str());
    ImGui::InputText("m_Name", temp_name, 20);
	if (ImGui::CollapsingHeader("Object Setting"))
	{	
		ImGui::Spacing();
		ImGui::InputFloat("m_lifetime", &m_lifetime);
		ImGui::Checkbox("Blink When Dying", &m_blink_when_dying);
	}
	ImGui::Spacing();
    transform->ImGui_Setting();
    ImGui::Spacing();
    for (unsigned int i = 0; i < component.size(); ++i)
    {
		if (ImGui::CollapsingHeader(component[i]->Get_Name().c_str()))
		{
			component[i]->ImGui_Setting();
			if (ImGui::Button(("Delete "+component[i]->Get_Name()).c_str()))
			{
				component.at(i)->Close();
				delete component.at(i);
				component.erase(component.begin() + i);
				i--;
			}
		}
        ImGui::Spacing();
    }
	Name = temp_name;
}

Object* Object::GetParent() const
{
	return m_parent;
}

bool Object::HasParent() const
{
	return m_parent != nullptr;
}

bool Object::HasChild() const
{
	return !m_children.empty();
}

void Object::AttatchToParent(Object* parent)
{
	if(parent == this)
	{
		return;
	}
	if(parent->GetParent() == this)
	{
		return;
	}
	this->Detach(true);

	transform->CalculateLocalTransform(parent->GetTransform());

	m_parent = parent;
	parent->AddChild(this);

	//if(m_ID > parent->GetID())
	//{
	//	this->m_state->GetObjectManager()->SwapObjectPosition(m_ID, parent->GetID());
	//}
}

std::vector<Object*>& Object::GetChildren()
{
	return m_children;
}

void Object::Detach()
{
	if (m_parent != nullptr)
	{
		transform->CalculateWorldTransform(m_parent->GetTransform());
		//m_parent->EraseChild(this);
		m_parent = nullptr;
	}
}

void Object::Detach(bool ischild)
{
	if (m_parent != nullptr)
	{
		transform->CalculateWorldTransform(m_parent->GetTransform());
		if (ischild)
		{
			m_parent->EraseChild(this);
		}
		m_parent = nullptr;
	}
}

Object* Object::FindChildByName(const std::string& name)
{
	for(auto& child : m_children)
	{
		if(child->GetName() == name)
		{
			return child;
		}
	}
	return nullptr;
}

unsigned int Object::GetID() const
{
	return m_ID;
}

void Object::SetID(unsigned int id)
{
	m_ID = id;
}

void Object::SetLifetime(float time)
{
	m_ismortal = true;
	m_lifetime = time;
}

bool Object::IsDead() const
{
	return m_isdead;
}

void Object::MakeDead()
{
	m_isdead = true;
	Detach(true);
}

void Object::Update(float dt)
{
	for(auto& child : m_children)
	{
		child->Update(dt);
	}

	if(m_ismortal)
	{
		m_lifetime -= dt;
		if(m_lifetime < 3.0f)
		{
			if (m_blink_when_dying)
			{
				if (Sprite* sprite_comp = dynamic_cast<Sprite*>(GetComponentByTemplate<Sprite>()); sprite_comp != nullptr)
				{
					if (static_cast<int>((m_lifetime * 10)) % 2 == 0)
					{
						sprite_comp->SetColorA(0.0f);
					}
					else
					{
						sprite_comp->SetColorA(1.0f);
					}
				}
			}
		}
		if(m_lifetime <= 0.0f)
		{
			m_isdead = true;
			Detach(true);
		}
	}

    for (auto i : component)
    {
		if (i->ShouldUpdate())
		{
			i->Update(dt);
		}
    }
}

void Object::Close()
{
	if (GetComponentByTemplate<Player>() != nullptr)
	{
		GetComponentByTemplate<Player>()->Close();
	}
	Detach(true);
	for (auto& child : m_children)
	{
		child->Detach();
	}
	m_children.clear();

	delete transform;
	transform = nullptr;

	for (auto& comp : component)
	{
		comp->Close();
		delete comp;
	}
	
	component.clear();
}
