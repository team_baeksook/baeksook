/***********************************************************************
	File name		: Shader.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Shader.h"
#include <GL/glew.h>
#include "System/Engine_Utility.h"
#include "System/Logger.h"
#include "Graphic.h"

void Shader::Use()
{
	if(m_isactive)
	{
		glUseProgram(m_shaderid);
	}
	m_isactive = true;
}

Shader::operator unsigned()
{
    return m_shaderid;
}

unsigned int Shader::Compile(const char * shader_text, int shader_type)
{
    GLuint shader_id = glCreateShader(shader_type);
    glShaderSource(shader_id, 1, &shader_text, NULL);
    glCompileShader(shader_id);

    GLint result;
    GLchar errorLog[512] = { 0 };
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
    if (!result)
    {
        glGetShaderInfoLog(shader_id, 512, NULL, errorLog);
        logger::log_handler.PrintLogOnConsoleAndFile(utility::MakeStringUsingFormat("Fail to compile shader source : {}", errorLog), spdlog::level::level_enum::err);
        glDeleteShader(shader_id);
    }

    char * line = nullptr;
    
    strtok_s(const_cast<char *>(shader_text), "\n", &line);

    while (true)
    {
        std::string temp = strtok_s(NULL, "\n", &line);
        if(temp.find("main()") != std::string::npos)
        {
            break;
        }
        if (size_t variable = temp.find("location = "); variable != std::string::npos)
        {
            variable += 11;
            int location = std::atoi(temp.substr(variable, 1).c_str());
            size_t variable_end = temp.find_last_of(";");
            size_t variable_start = temp.find_last_of(" ") + 1;
            m_loclist[temp.substr(variable_start, variable_end - variable_start)] = location;
            continue;
        }
    }

    return shader_id;
}

void Shader::Link(unsigned vertexid, unsigned fragmentid)
{
    m_shaderid = glCreateProgram();

    glAttachShader(m_shaderid, vertexid);
    glAttachShader(m_shaderid, fragmentid);
    glLinkProgram(m_shaderid);
    glDeleteShader(vertexid);
    glDeleteShader(fragmentid);

    GLint result;
    GLchar errorLog[512] = { 0 };

    glGetProgramiv(m_shaderid, GL_LINK_STATUS, &result);
    if (!result)
    {
      glGetProgramInfoLog(m_shaderid, 512, NULL, errorLog);
        logger::log_handler.PrintLogOnConsoleAndFile(utility::MakeStringUsingFormat("Fail to link shader program : {}", errorLog), spdlog::level::level_enum::err);
    }
}

//this function is for debugging the shader layout.
void Shader::Checking_for_shader_source()
{
    for (auto& i : m_loclist)
    {
        if (i.first == "inPosition")
        {
			if(i.second != static_cast<int>(SHADER_LOCATION::VERTEX_POSITION))
			{
				logger::log_handler.PrintLogOnConsoleAndFile(utility::MakeStringUsingFormat("The shader source's layout location is wrong! : Variable : {}", i.first), spdlog::level::level_enum::err);
			}
        }
		if(i.first == "inTexture_coordinate")
		{
			if (i.second != static_cast<int>(SHADER_LOCATION::TEXTURE_POSITION))
			{
				logger::log_handler.PrintLogOnConsoleAndFile(utility::MakeStringUsingFormat("The shader source's layout location is wrong! : Variable : {}", i.first), spdlog::level::level_enum::err);
			}
		}
    	if(i.first == "Transform")
		{
			if (i.second != static_cast<int>(SHADER_LOCATION::TRANSFORM))
			{
				logger::log_handler.PrintLogOnConsoleAndFile(utility::MakeStringUsingFormat("The shader source's layout location is wrong! : Variable : {}", i.first), spdlog::level::level_enum::err);
			}
		}
    	if(i.first == "Depth")
		{
			if (i.second != static_cast<int>(SHADER_LOCATION::DEPTH))
			{
				logger::log_handler.PrintLogOnConsoleAndFile(utility::MakeStringUsingFormat("The shader source's layout location is wrong! : Variable : {}", i.first), spdlog::level::level_enum::err);
			}
		}
    	if(i.first == "inColor")
		{
			if (i.second != static_cast<int>(SHADER_LOCATION::COLOR))
			{
				logger::log_handler.PrintLogOnConsoleAndFile(utility::MakeStringUsingFormat("The shader source's layout location is wrong! : Variable : {}", i.first), spdlog::level::level_enum::err);
			}
		}
    	if(i.first == "inTexture_to_sample")
		{
			if (i.second != static_cast<int>(SHADER_LOCATION::TEXTURE))
			{
				logger::log_handler.PrintLogOnConsoleAndFile(utility::MakeStringUsingFormat("The shader source's layout location is wrong! : Variable : {}", i.first), spdlog::level::level_enum::err);
			}
		}
    }

	m_loclist.clear();
}

Shader::Shader(const char* vertex_text, const char* fragment_text)
{
    unsigned int vertexid = Compile(vertex_text, GL_VERTEX_SHADER);
    unsigned int fragmentid = Compile(fragment_text, GL_FRAGMENT_SHADER);

    Link(vertexid, fragmentid);

    Checking_for_shader_source();
}
