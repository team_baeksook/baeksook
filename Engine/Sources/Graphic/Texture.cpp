/***********************************************************************
	File name		: Texture.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Texture.h"
#include "gl/glew.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#pragma warning(push)
#pragma warning(disable : 4505) // unreferenced local function has been removed
#pragma warning(disable : 4100) // unreferenced formal parameter
#include "stb/stb_image.h"
#pragma warning(pop)

#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STBI_MSC_SECURE_CRT
#include "stb/stb_image_write.h"
#include "gl/wglew.h"
#include "System/Logger.h"
#include "System/Engine_Utility.h"

Texture::Texture()
{
    m_texture_coordinate.push_back(vector2(0.0f, 1.0f));
    m_texture_coordinate.push_back(vector2(0.0f, 0.0f));
    m_texture_coordinate.push_back(vector2(1.0f, 1.0f));

    m_texture_coordinate.push_back(vector2(1.0f, 1.0f));
    m_texture_coordinate.push_back(vector2(0.0f, 0.0f));
    m_texture_coordinate.push_back(vector2(1.0f, 0.0f));
}

Texture::Texture(int w, int h) : Width(w), Height(h)
{
    m_texture_coordinate.push_back(vector2(0.0f, 1.0f));
    m_texture_coordinate.push_back(vector2(0.0f, 0.0f));
    m_texture_coordinate.push_back(vector2(1.0f, 1.0f));

    m_texture_coordinate.push_back(vector2(1.0f, 1.0f));
    m_texture_coordinate.push_back(vector2(0.0f, 0.0f));
    m_texture_coordinate.push_back(vector2(1.0f, 0.0f));
}

Texture::~Texture()
{
}

bool Texture::InitWithTexturePath(const std::string file_path, int frame_size)
{
    m_framesize = frame_size;
    bool result = LoadFromFile(file_path);
    GenTexture();
    return result;
}

bool Texture::LoadFromFile(const std::string file_path)
{
    m_Name = file_path.substr(file_path.find_last_of("/") + 1, file_path.size());
    m_Name = m_Name.substr(0, m_Name.size() - 4);
    data = stbi_load(file_path.c_str(), &Width, &Height, &Comp, STBI_rgb_alpha);
    if (!data)
    {
		logger::log_handler.PrintLogOnConsoleAndFile(utility::MakeStringUsingFormat("Cannot load file from {}", file_path), spdlog::level::level_enum::err);
        return false;
    }

    return true;
}

void Texture::ChangeSize(vector2 size)
{
    glBindTexture(GL_TEXTURE_2D, ID);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, static_cast<int>(size.x), static_cast<int>(size.y), 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

    Width = static_cast<int>(size.x);
    Height = static_cast<int>(size.y);
}

void Texture::SaveToFile(const std::string file_path)
{
	unsigned char * pixel = new unsigned char[Width * Height * 4];
	glReadPixels(0, 0, Width, Height, GL_RGBA, GL_UNSIGNED_BYTE, pixel);

	for (int i = 0; i < Width; ++i)
	{
		for(int j = 0; j < Height / 2; ++j)
		{
			for(int k = 0; k < 4; ++k)
			{
				Swap(pixel[((Height - 1 - j) * Width + i) * 4 + k], pixel[(j * Width + i) * 4 + k]);
			}
		}
	}

    stbi_write_png(file_path.c_str(), Width, Height, STBI_rgb_alpha, pixel, sizeof(unsigned char) * 4 * Width);

	delete []pixel;
}

void Texture::GenTexture()
{
    glGenTextures(1, &ID);
    glBindTexture(GL_TEXTURE_2D, ID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    if (data)
    {
        stbi_image_free(data);
		data = nullptr;
    }
}

void Texture::BindTexture(unsigned slot)
{
    int binded_ID = 0;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &binded_ID);
    if(binded_ID == static_cast<int>(ID))
    {
        return;
    }
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D, ID);
}

vector2 Texture::GetScale()
{
    return vector2((float)Width, (float)Height);
}

float* Texture::Get_Coordinate_Pointer(int current_frame, bool FlipX, bool FlipY)
{
	m_coordinate.clear();
    if(current_frame == -1)
    {
        return &(m_texture_coordinate.at(0).x);
    }
    int x_flip = (FlipX) ? -1 : 1;
    int y_flip = (FlipY) ? -1 : 1;
    for (unsigned int i = 0; i < m_texture_coordinate.size(); ++i)
    {
        float x_temp = m_texture_coordinate.at(i).x * x_flip - 0.5f * x_flip + 0.5f;
        m_coordinate.push_back(vector2(x_temp / m_framesize + static_cast<float>(current_frame) / m_framesize,
			m_texture_coordinate.at(i).y * y_flip - 0.5f * y_flip + 0.5f));
    }
    return &(m_coordinate.at(0).x);
}

float* Texture::Make_Coordinate_Pointer(int current_frame, bool FlipX, bool FlipY)
{
	float* pointer = new float[m_texture_coordinate.size() * 2];

	int x_flip = (FlipX) ? -1 : 1;
	int y_flip = (FlipY) ? -1 : 1;
	for (unsigned int i = 0; i < m_texture_coordinate.size(); ++i)
	{
		float x_temp = m_texture_coordinate.at(i).x * x_flip - 0.5f * x_flip + 0.5f;
		pointer[i * 2] = x_temp / m_framesize + static_cast<float>(current_frame) / m_framesize;
		pointer[i * 2 + 1] = m_texture_coordinate.at(i).y * y_flip - 0.5f * y_flip + 0.5f;
	}
	return pointer;
}

std::vector<vector2> Texture::CopyCoordinate(int current_frame, bool FlipX, bool FlipY)
{
	m_coordinate.clear();
	if (current_frame == -1)
	{
		return m_texture_coordinate;
	}
	int x_flip = (FlipX) ? -1 : 1;
	int y_flip = (FlipY) ? -1 : 1;
	for (unsigned int i = 0; i < m_texture_coordinate.size(); ++i)
	{
		float x_temp = m_texture_coordinate.at(i).x * x_flip - 0.5f * x_flip + 0.5f;
		m_coordinate.push_back(vector2(x_temp / m_framesize + static_cast<float>(current_frame) / m_framesize,
			m_texture_coordinate.at(i).y * y_flip - 0.5f * y_flip + 0.5f));
	}
	return m_coordinate;
}

void Texture::ClearCoordinate()
{
    m_texture_coordinate.clear();
    m_texture_coordinate.resize(0);
}

void Texture::SetCoordinate(std::vector<vector2> texture_coordinate)
{
    m_texture_coordinate.assign(texture_coordinate.begin(), texture_coordinate.end());
}

int Texture::GetFrameSize(int ) const
{
    return m_framesize;
}

float Texture::GetFrameSpeed(int ) const
{
    return m_framespeed;
}

void Texture::SetFrameSpeed(float framespeed)
{
    m_framespeed = framespeed;
}

void Texture::Close()
{
	ClearCoordinate();
	m_coordinate.clear();

	if (data != nullptr)
	{
		stbi_image_free(data);
		data = nullptr;
	}
}

void Texture::SetFrameSize(int frame_size)
{
    m_framesize = frame_size;
}

unsigned int Texture::GetID() const
{
    return ID;
}

std::string Texture::GetName() const
{
    return m_Name;
}
