/***********************************************************************
	File name		: Graphic.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Color.h"
#include "Shader.h"

#include "Math/vector2.hpp"

#include <unordered_map>

class Character_Sprite;
class affine2d;
class Camera;
class Sprite;
class SpriteText;
class Texture;
class Font;
class Mesh;

enum class RENDER_TYPE { FAN, STRIP, TRIANGLE, LINES, LINE_LOOP, LINE_STRIP, POLYGON };

enum class SHADER_PROGRAM_TYPE { DEBUG_SHADER = 0, STANDARD_SHADER, TEXTURE_SHADER, EFFECT_SHADER, POST_PROCESS_SHADER, MAX_SHADER };

enum class SHADER_LOCATION
{
	VERTEX_POSITION = 0, TEXTURE_POSITION = 1, PROJECTION = 2, 
	CAMERA = 3, TRANSFORM = 4, DEPTH = 5, COLOR = 6, TEXTURE = 7,
	HUD = 8
};

namespace graphic_enum
{
	enum DrawType
	{
		SOLID_SHAPE,
		SPRITE,
		TEXT,
		TEXTURE,
		SIMPLE_TEXTURE,
		CHARACTER,
		PARTICLE,
		TILE,
		EFFECT,
		MAX
	};
}

struct DrawBehavior
{
	int m_size;
	std::vector<vector2> m_vertex;
	std::vector<vector2> m_texture;
	float m_color[4];
	float m_transform[9];
	unsigned int m_texture_id;
	RENDER_TYPE m_rendertype;
	graphic_enum::DrawType m_type;
	bool m_hud = false;
	void close();
	~DrawBehavior();
};

class Graphic
{
public:
	Graphic();

    void Initialize();

	void DrawSolidShape(Mesh * mesh, Color col);
    void DrawSprite(Sprite * sprite);
    void DrawSpriteText(SpriteText * sprite_text);
	void DrawTexture(int texture_num, Color col = Color(255, 255, 255, 255), int size = 6, float* texture_coordinate = nullptr, float* vertex_coordinate = nullptr);
	void DrawSimpleTexture(int texture_id, Color col, float* transform = nullptr);
	void DrawCharacter(Sprite* sprite, Character_Sprite* character_sprite, int type);

    void DrawLine(vector2 start, vector2 end, Color col = Color(0, 0, 0, 255));
    void DrawRect(vector2 center, vector2 size, Color col = Color(0, 0, 0, 255));
    void DrawCircle(vector2 center, float radius, Color col = Color(0, 0, 0, 255));
    void DrawFilledCircle(vector2 center, float radius, Color col = Color(0, 0, 0, 255));
    void Draw_curvedline_bezier(Color col, int count, ...);
    void Draw_curvedline_bezier_with_thickness(Color col, float thick, int count, ...);
    void Draw_curveddotted_line_bezier_with_thickness(Color col, float thick, int count, ...);

	void DrawLaser(vector2 pos = vector2(0,0), float angle = 0.0f, bool fire = true);

	void StartParticle();
	void Draw_Particle(int texture_id, Color col, float* transform);

    void SendDebugData();

    void Draw(int num_vertex, RENDER_TYPE render_type, Color col);
    void Draw(int num_vertex, RENDER_TYPE render_type, float* col);

    void DrawPostProcess(float time);
    void SetFrameBuffer(Color col);

    void Close();

    void InitGLEW();

	void Update();
	void UpdateProjection(vector2 size);
	void UpdateCamera(affine2d& mat);

    unsigned int GetProgramID(SHADER_PROGRAM_TYPE shader_program);

    int GetUniformLocation(const char * string);

    void ChangeProgram(SHADER_PROGRAM_TYPE type);

    Texture * GetTexture(std::string key) const;

    Font * GetFont(std::string key) const;
	void InsertNewTexture(std::string name,Texture* texture);

    std::unordered_map<std::string, Texture *> GetTexture_Container() const;
    std::unordered_map<std::string, Font *> GetFont_Container() const;

	void Capture_Screenshot() const;

	void Toggle_confuse(bool flag);
	void Toggle_chaos(bool flag);
	void Toggle_shake(bool flag);

	std::multimap<float, DrawBehavior*, std::greater<float>> m_drawlist;
private:
	bool m_confuse;
	bool m_chaos;
	bool m_shake;

    std::unordered_map<std::string, Texture *> m_Textures;
    std::unordered_map<std::string, Font *> m_Fonts;

    void LoadAllTexture();
    void LoadAllFont();

	void InitUniformBuffer();
	void InitFramebuffer();

    void InitShader();

    unsigned int * m_vertexarrayobject;
    unsigned int * m_vertexbufferobject;

    std::vector<vector2> m_vertices;

    std::unordered_map<SHADER_PROGRAM_TYPE, Shader> m_shaderlist;
	SHADER_PROGRAM_TYPE m_currentprogram = SHADER_PROGRAM_TYPE::MAX_SHADER;

	unsigned int m_uniformbuffer = 0;
	unsigned int m_framebuffer = 0;
	unsigned int m_renderbuffer = 0;
	Texture * m_frame_buffer_texture = nullptr;
};