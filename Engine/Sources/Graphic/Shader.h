/***********************************************************************
	File name		: Shader.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <string>
#include <unordered_map>
#include <map>

enum class SHADER_PROGRAM_TYPE;

class Shader
{
private:
    int m_shaderid = -1;

	bool m_isactive = false;

    std::map<std::string, int> m_loclist;

    unsigned int Compile(const char * shader_text, int shader_type);
    void Link(unsigned int vertexid, unsigned int fragmentid);
    void Checking_for_shader_source();
public:
    Shader() {}
    Shader(const char * vertex_text, const char * fragment_text);
    void Use();

    operator unsigned int();
};

