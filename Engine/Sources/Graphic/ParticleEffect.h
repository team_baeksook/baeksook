/***********************************************************************
	File name		: ParticleEffect.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Math/vector2.hpp"

struct ParticleEffect
{
	ParticleEffect() = default;
	unsigned int texture_id = 14;
	std::string texture_name = "Red";
	vector2 scale_step = vector2(1.0f, 1.0f);
	vector2 pos_variation = vector2(0.0f, 0.0f);
	float diverge = 0.1f;
	float colorstep = 0.0f;
	float degreestep = 0.0f;
};