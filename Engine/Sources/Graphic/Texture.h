/***********************************************************************
	File name		: Texture.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <string>
#include <vector>
#include "Math/MathLibrary.hpp"
#include "Color.h"

class Texture
{
private:
    std::string m_Name = "";

    std::vector<vector2> m_coordinate;

    int Comp = 0;
    unsigned int ID = static_cast<unsigned int>(-1);
    int Width = 0;
    int Height = 0;

    unsigned char * data = nullptr;

    int m_framesize = 1;
    float m_framespeed = 5.0f;
protected:
    std::vector<vector2> m_texture_coordinate;
public:
    Texture();
    Texture(int w, int h);
	virtual ~Texture();
    bool InitWithTexturePath(const std::string file_path, int frame_size = 1);
    bool LoadFromFile(const std::string file_path);
    void ChangeSize(vector2 size);
    void SaveToFile(const std::string file_path);
    void GenTexture();
    virtual void BindTexture(unsigned slot = 0);
    vector2 GetScale();
    float* Get_Coordinate_Pointer(int current_frame = 0, bool FlipX = false, bool FlipY = false);
    float* Make_Coordinate_Pointer(int current_frame = 0, bool FlipX = false, bool FlipY = false);
    std::vector<vector2> CopyCoordinate(int current_frame = 0, bool FlipX = false, bool FlipY = false);
    void ClearCoordinate();
    void SetCoordinate(std::vector<vector2> texture_coordinate);
    virtual int GetFrameSize(int type) const;
    virtual float GetFrameSpeed(int type) const;
    void SetFrameSpeed(float framespeed);
	virtual void Close();

    void SetFrameSize(int frame_size);
    unsigned int GetID() const;

    std::string GetName() const;
};
