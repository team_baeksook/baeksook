/***********************************************************************
	File name		: Particle.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Color.h"
#include "Object/Transform.h"
#include "Math/Ease.h"

class Graphic;

class Particle
{
private:
	Transform* m_transform = nullptr;
	Color m_color = Color(255, 255, 255, 255);
	unsigned int m_texture_id;
	float m_duration = 0.0f;
	float m_innertime = 0.0f;
	bool m_isdead = false;
	bool m_isParticleEase = false;

	float m_colorstep = 1.0f;
	vector2 m_scalestep;

	vector2 m_velocity;

	float m_rotate = 0.0f;

	float m_timeForEase = 0.f;
	float m_EaseValue = 2.f;
public:
	Particle();
	Particle(Transform* transform, Color color, unsigned int id);
	Transform* GetTransform();
	void SetTransform(Transform* transform);
	Color GetColor() const;
	void SetColor(Color col);
	unsigned int GetID() const;
	void SetID(unsigned int id);
	bool IsDead() const;
	void SetDuration(float time);
	void Draw(float dt, Graphic* graphic);
	void Close();
	void Update(float dt);
	void Update(float dt, float begin, float end , float duration);
	void Setvelocity(vector2 vel);
	void SetColorStep(float step);
	void SetScaleStep(vector2 step);
	void SetEaseOut_Cubic(float time, float begin, float change, float duration);
	void SetParticleEasingSwitch(bool ease);
	void SetEaseValue(float value);
	void SetNew();
	void SetRoate(float rot);
};
