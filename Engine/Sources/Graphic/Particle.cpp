/***********************************************************************
	File name		: Particle.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Particle.h"
#include "Graphic.h"
#include "System\StateManager.h"

#include <iostream>

Particle::Particle()
{
	m_transform = new Transform();
}

Particle::Particle(Transform* transform, Color color, unsigned id) : m_transform(transform), m_color(color), m_texture_id(id) {}

Transform* Particle::GetTransform()
{
	return m_transform;
}

void Particle::SetTransform(Transform* transform)
{
	if (m_transform == transform)
	{
		return;
	}
	delete m_transform;
	m_transform = transform;
}

Color Particle::GetColor() const
{
	return m_color;
}

void Particle::SetColor(Color col)
{
	m_color = col;
}

unsigned Particle::GetID() const
{
	return m_texture_id;
}

void Particle::SetID(unsigned id)
{
	m_texture_id = id;
}

bool Particle::IsDead() const
{
	return m_isdead;
}

void Particle::SetDuration(float time)
{
	m_duration = time;
}

void Particle::Draw(float dt, Graphic* graphic)
{
	m_innertime += dt;
	if(m_innertime > m_duration)
	{
		m_isdead = true;
		return;
	}
	if(m_isParticleEase)
	{
		Update(m_innertime, 0, m_velocity.x, m_duration);
	}
	else
	{
		Update(dt);
	}

	m_transform->BuildTransformMatrix();
	float* transform = m_transform->GetPointer();
	DrawBehavior* draw_behavior = new DrawBehavior();
	float* color = m_color.GetColor_Float();
	for (int i = 0; i < 4; ++i)
	{
		draw_behavior->m_color[i] = color[i];
	}
	for (int i = 0; i < 9; ++i)
	{
		draw_behavior->m_transform[i] = transform[i];
	}
	draw_behavior->m_texture_id = m_texture_id;
	draw_behavior->m_type = graphic_enum::DrawType::PARTICLE;
	float depth = m_transform->GetDepth();
	graphic->m_drawlist.insert(std::make_pair(depth, draw_behavior));
}

void Particle::Update(float dt)
{
	m_transform->AddPosition(m_velocity * dt);
	m_color.color[3] -= dt * m_colorstep;
	m_transform->MultiplyScale(m_scalestep);
	m_transform->AddDegree(m_rotate);
}

void Particle::Update(float dt, float begin, float change, float duration)
{
	m_innertime += dt;
	m_EaseValue = Easing::easeOut_Cubic(m_innertime, begin, change, duration);
	//m_velocity.y = Easing::easeOut_Cubic(m_innertime, begin, change.y, duration); 
	m_transform->AddPosition({ m_EaseValue,0 });
	//m_transform->AddPosition(m_EaseValue);
	m_color.color[3] -= dt * m_colorstep;
	m_transform->MultiplyScale(m_scalestep);

	if(m_innertime >= duration)
	{
		m_innertime = 0.f;
	}
}

void Particle::Setvelocity(vector2 vel)
{
	m_velocity = vel;
}

void Particle::SetColorStep(float step)
{
	m_colorstep = step;
}

void Particle::SetScaleStep(vector2 step)
{
	m_scalestep = step;
}

void Particle::SetEaseOut_Cubic(float time, float begin, float change, float duration)
{
	m_velocity = Easing::easeOut_Cubic(time, begin, change, duration);
}

void Particle::SetParticleEasingSwitch(bool ease)
{
	m_isParticleEase = ease;
}

void Particle::SetEaseValue(float value)
{
	m_EaseValue = value;
}

void Particle::SetNew()
{
	m_innertime = 0.0f;
	m_isdead = false;
}

void Particle::SetRoate(float rot)
{
	m_rotate = rot;
}

void Particle::Close()
{
	delete m_transform;
	m_transform = nullptr;
}