﻿/***********************************************************************
	File name		: Graphic.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Graphic.h"

#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "System/Application.h"
#include <iostream>
#include "Object/Mesh.h"
#include "Object/Object.h"
#include "Texture.h"
#include "Component/Sprite.h"
#include "Component/SpriteText.h"
#include "System/Logger.h"
#include "System/Engine_Utility.h"
#include "Component/Camera.h"
#include "Character_sprite.h"

namespace shader_source
{
    const std::string Debug_vertex_shader_text = R"(
    #version 430
    
    layout(location = 0) in vec2 inPosition;
    
	layout(std140, binding = 0) uniform Proj {
		mat3 Projection;
	    mat3 Camera;
	};
	
    void main()
    {
        vec3 translatePos = Projection * Camera * vec3(inPosition, 1.0f);
        gl_Position = vec4(translatePos.xy, -0.8f, 1.0f);
    }
    )";
    const std::string Debug_fragment_shader_text = R"(
    #version 430
    
    out vec4 outColor;
    layout(location = 6) uniform vec4 inColor;

    void main()
    {
        if(inColor.a <= 0.0f)
        {
            discard;
        }
        outColor = inColor;
    }
    )";
    const std::string vertex_shader_text = R"(
    #version 430

    layout(location = 0) in vec2 inPosition;

	layout(std140, binding = 0) uniform Proj {
		mat3 Projection;
		mat3 Camera;
	};

    layout(location = 4) uniform mat3 Transform;
    layout(location = 5) uniform float Depth;

    void main()
    {
        vec3 translatePos = Transform * vec3(inPosition, 1.0f);
        vec3 resultPos = Projection * Camera * translatePos;

        gl_Position = vec4(resultPos.xy, Depth, 1.0f);
    }
    )";
    const std::string fragment_shader_text = R"(
    #version 430

    layout(location = 6) uniform vec4 inColor;

    out vec4 outputColor;

    void main()
    {
        if(inColor.a <= 0.0f)
        {
            discard;
        }
        outputColor = inColor;
    }
    )";
    const std::string texture_vertex_shader_text = R"(
    #version 430

    layout(location = 0) in vec2 inPosition;
    layout(location = 1) in vec2 inTexture_coordinate;

    out vec2 Texture_coordinate;

	layout(std140, binding = 0) uniform Proj {
		mat3 Projection;
		mat3 Camera;
	};

    layout(location = 4) uniform mat3 Transform;
    layout(location = 5) uniform float Depth;

	layout(location = 8) uniform bool HUD;

    void main()
    {
        vec3 translatePos = Transform * vec3(inPosition, 1.0f);

		vec3 resultPos;
		if(HUD)
		{
			resultPos = translatePos;
		}
		else
		{
			resultPos = Projection * Camera * translatePos;
		}

        gl_Position = vec4(resultPos.xy, Depth, 1.0f);
        Texture_coordinate = inTexture_coordinate;
    }
    )";
    const std::string texture_fragment_shader_text = R"(
    #version 430

    in vec2 Texture_coordinate;

    layout(location = 6) uniform vec4 inColor;
    layout(location = 7) uniform sampler2D inTexture_to_sample;

    out vec4 outputColor;

    void main()
    {
        vec4 texel = texture(inTexture_to_sample, Texture_coordinate);
        vec4 theColor;
        theColor = inColor * texel;
        if(theColor.a <= 0.0f)
        {
            discard;
        }

        outputColor = theColor;
    }
    )";
    const std::string postprocess_vertex_text = R"(
    #version 330

    layout(location = 0) in vec2 inPosition;
    layout(location = 1) in vec2 inTexture_coordinate;

    out vec2 Texture_coordinate;

    void main()
    {
        gl_Position = vec4(inPosition, 0.0f, 1.0f);
        Texture_coordinate = inTexture_coordinate;
    }
    )";
	/*const std::string postprocess_fragment_text = R"(
    #version 330

    in vec2 Texture_coordinate;

    uniform vec4 inColor;
    uniform sampler2D inTexture_to_sample;

    uniform vec2      offsets[9];
    uniform int       edge_kernel[9];
    uniform float     blur_kernel[9];

    out vec4 outputColor;

    uniform bool chaos;
    uniform bool confuse;
    uniform bool shake;

	uniform float time;

    void main()
    {
        float pixel = 512;
		float dx = time * (1.0 / pixel);
		float dy = time * (1.0 / pixel);
		vec2 coord = vec2(dx * floor(Texture_coordinate.x / dx), 
			dy * floor(Texture_coordinate.y / dy));
		outputColor = texture(inTexture_to_sample, coord);
    }
    )";*/
 //   const std::string postprocess_fragment_text = R"(
 //   #version 330

 //   in vec2 Texture_coordinate;

 //   uniform vec4 inColor;
 //   uniform sampler2D inTexture_to_sample;

 //   uniform vec2      offsets[9];
 //   uniform int       edge_kernel[9];
 //   uniform float     blur_kernel[9];

 //   out vec4 outputColor;

 //   uniform bool chaos;
 //   uniform bool confuse;
 //   uniform bool shake;

	//uniform float time;

 //   void main()
 //   {
 //       vec4 texel = texture(inTexture_to_sample, Texture_coordinate);
	//	vec4 col = texel;

	//	vec2 u_resolution = vec2(1920, 1080);

	//	vec2 st = gl_FragCoord.xy/u_resolution.xy;
	//	st.x *= u_resolution.x/u_resolution.y;

	//	float adjusted_time = (time > 0.5f) ? time - 0.5f : time + 0.5f;
 //       /*if((st.y > time && st.y < time + 0.02f) || (st.y > adjusted_time && st.y < adjusted_time + 0.02f))
	//	{
	//		vec3 sample[9];
 //           for(int i = 0; i < 9; i++)
	//		{
 //               sample[i] = vec3(texture(inTexture_to_sample, 
	//				Texture_coordinate.st + offsets[i]));
	//		}
 //           for(int i = 0; i < 9; i++)
	//		{
 //               outputColor += vec4(vec3(sample[i] * blur_kernel[i]), 0.0f);
	//		}
	//	}
	//	else
	//	{*/
	//		outputColor = col;
	//	//}
 //       outputColor.a = 1.0f;
 //   }
 //   )";
	const std::string postprocess_fragment_text = R"(
    #version 330

    in vec2 Texture_coordinate;

    uniform sampler2D inTexture_to_sample;

    uniform vec2      offsets[9];
    uniform int       edge_kernel[9];
    uniform float     blur_kernel[9];

    out vec4 outputColor;

    uniform bool chaos;
    uniform bool confuse;
    uniform bool shake;

    void main()
    {
        outputColor = vec4(0.0f);
        vec3 sample[9];
        if(chaos || shake)
            for(int i = 0; i < 9; i++)
                sample[i] = vec3(texture(inTexture_to_sample, Texture_coordinate.st + offsets[i]));

        if(chaos)
        {           
            for(int i = 0; i < 9; i++)
                outputColor += vec4(sample[i] * edge_kernel[i], 0.0f);
            outputColor.a = 1.0f;
        }
        else if(confuse)
        {
            outputColor = vec4(1.0 - texture(inTexture_to_sample, Texture_coordinate).rgb, 1.0);
        }
        else if(shake)
        {
            for(int i = 0; i < 9; i++)
			{
                outputColor += vec4(vec3(sample[i] * blur_kernel[i]), 0.0f);

			}
            outputColor.a = 1.0f;
        }
        else
        {
            outputColor =  texture(inTexture_to_sample, Texture_coordinate);
        }
    }
    )";

	const std::string effect_vertex_text = R"(
    #version 330

    layout(location = 0) in vec2 inPosition; 

    void main()
    {
        gl_Position = vec4(inPosition * 2, -0.5f, 1.0f);
    }
    )";
	const std::string effect_fragment_text = R"(
    #version 330

    out vec4 outputColor;

	uniform float time;
	uniform vec2 resolution;

	uniform vec2 pos;
	uniform float angle;
	uniform bool fire;

	float strand(vec2 uv, float hoffset, float hscale, float vscale, float t)
	{
		vec2 temp = uv - resolution / 2.0 - vec2(15.0, 0.0);
		if(fire)
		{
			if(uv.x < ((resolution.x / 2.0) + 15.0) && length(temp) > 30)
			{
				return 0;
			}
		}
		else
		{
			if(length(temp) > 20)
			{
				return 0;
			}
		}
		uv /= resolution;
		float g = .05 * 2000;
		float d = 1. - abs(uv.y - (sin(uv.x * hscale * 10. + t + hoffset)/5. * vscale + .5)) * 4000;
		return clamp(d, 0., 1.) + clamp(1.+d/g, 0., 1.) * 0.4;
	}

    void main()
    {
		outputColor =  vec4(0,0,0,0);

		vec2 st = gl_FragCoord.xy/resolution.xy;
		float t = time;
		float s =  1. + sin(time) * 5.;

		vec2 vert = gl_FragCoord.xy - pos - resolution / 2;
		vert *= mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
		vert += resolution / 2;

		outputColor += strand(vert, 0.234   +s, 1.0, 0.06, 10.0 * t) * vec4(1,0,0,1);
		outputColor += strand(vert, 0.645   +s, 1.5, 0.00,  8.3 * t) * vec4(0,1,0,1);
		outputColor += strand(vert, 1.735   +s, 1.3, 0.09,  8.0 * t) * vec4(0,0,1,1);
		outputColor += strand(vert, 0.9245  +s, 1.6, 0.04, 12.0 * t) * vec4(1,1,0,1);
		outputColor += strand(vert, 0.4234  +s, 1.9, 0.03, 14.0 * t) * vec4(0,1,1,1);
		outputColor += strand(vert, 0.14525 +s, 1.2, 0.08,  9.0 * t) * vec4(1,0,1,1);
    }
    )";
}

Graphic::Graphic()
{
	m_vertexarrayobject = new unsigned int[static_cast<int>(SHADER_PROGRAM_TYPE::MAX_SHADER)];
	m_vertexbufferobject = new unsigned int[static_cast<int>(SHADER_PROGRAM_TYPE::MAX_SHADER)];
}

void Graphic::Initialize()
{
    logger::log_handler.PrintLogOnConsole("Initialization graphic start");

    InitGLEW();

    glGenVertexArrays(static_cast<int>(SHADER_PROGRAM_TYPE::MAX_SHADER), m_vertexarrayobject);
    glGenBuffers(static_cast<int>(SHADER_PROGRAM_TYPE::MAX_SHADER), m_vertexbufferobject);

    InitShader();

    LoadAllTexture();
    LoadAllFont();

}

void Graphic::DrawSolidShape(Mesh* mesh, Color col)
{
	int size = mesh->GetSize();
	
	glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::STANDARD_SHADER)]);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(m_currentprogram)]);
	glBufferData(GL_ARRAY_BUFFER, size * 2 * sizeof(float), mesh->GetVerticesPointer(), GL_STATIC_DRAW);

	Draw(size, mesh->GetRenderType(), col);
}

void Graphic::DrawSprite(Sprite * sprite)
{
	sprite->GetTexture()->BindTexture(0);

    int size = sprite->GetMesh()->GetSize();

	glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(m_currentprogram)]);

	glBufferSubData(GL_ARRAY_BUFFER, 0, 2 * size * sizeof(float), sprite->GetMesh()->GetVerticesPointer());

	glBufferSubData(GL_ARRAY_BUFFER, 2 * size * sizeof(float), size * 2 * sizeof(float),
		sprite->GetTexture()->Get_Coordinate_Pointer(sprite->GetCurrentFrame(), sprite->GetFlipX(), sprite->GetFlipY()));

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(size * 2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	Draw(size, sprite->GetMesh()->GetRenderType(), sprite->GetColor());
}

void Graphic::DrawSpriteText(SpriteText* sprite_text)
{
    int size = sprite_text->GetMesh()->GetSize();
	glBindVertexArray(m_vertexarrayobject[static_cast<int>(m_currentprogram)]);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(m_currentprogram)]);

	glBufferData(GL_ARRAY_BUFFER, size * 4 * sizeof(float), nullptr, GL_DYNAMIC_DRAW);

	glBufferSubData(GL_ARRAY_BUFFER, 0, size * 2 * sizeof(float), sprite_text->GetMesh()->GetVerticesPointer());
	glBufferSubData(GL_ARRAY_BUFFER, 2 * size * sizeof(float), size * 2 * sizeof(float),
		sprite_text->GetTextureVerticesPointer());

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(size * 2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

    Draw(size, sprite_text->GetMesh()->GetRenderType(), sprite_text->GetColor());
}

void Graphic::DrawTexture(int texture_num, Color col, int size, float* vertex_coordinate, float* texture_coordinate)
{
	m_shaderlist.at(SHADER_PROGRAM_TYPE::TEXTURE_SHADER).Use();

	glUniform1i(static_cast<int>(SHADER_LOCATION::HUD), 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_num);

	glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

	glBufferData(GL_ARRAY_BUFFER, size * 4 * sizeof(float), nullptr, GL_DYNAMIC_DRAW);

	glBufferSubData(GL_ARRAY_BUFFER, 0, 2 * size * sizeof(float), vertex_coordinate);

	glBufferSubData(GL_ARRAY_BUFFER, 2 * size * sizeof(float), size * 2 * sizeof(float),
		texture_coordinate);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(size * 2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	affine2d a = Identity_Affine();

	glUniformMatrix3fv(static_cast<int>(SHADER_LOCATION::TRANSFORM), 1, GL_FALSE, &(a.column1.x));

	glUniform1f(static_cast<int>(SHADER_LOCATION::DEPTH), 0.5f);

	Draw(size, RENDER_TYPE::TRIANGLE, col);
}

void Graphic::DrawSimpleTexture(int texture_id, Color col, float* transform)
{
	int size = static_cast<int>(m_vertices.size());

	m_shaderlist.at(SHADER_PROGRAM_TYPE::TEXTURE_SHADER).Use();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	glUniform1i(static_cast<int>(SHADER_LOCATION::HUD), 0);

	glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

	glBufferData(GL_ARRAY_BUFFER, size * 2 * sizeof(float), &m_vertices.at(0), GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(6 * 2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glUniformMatrix3fv(static_cast<int>(SHADER_LOCATION::TRANSFORM), 1, GL_FALSE, transform);

	glUniform1f(static_cast<int>(SHADER_LOCATION::DEPTH), 0.5f);

	Draw(6, RENDER_TYPE::TRIANGLE, col);
}

void Graphic::DrawCharacter(Sprite* sprite, Character_Sprite* character_sprite, int type)
{
	Mesh* mesh = sprite->GetMesh();
	int size = mesh->GetSize();

	glActiveTexture(GL_TEXTURE0 + character_sprite->Getslot());
	glBindTexture(GL_TEXTURE_2D, character_sprite->GetID());

	glUniform1i(static_cast<int>(SHADER_LOCATION::TEXTURE), character_sprite->Getslot());

	glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(m_currentprogram)]);

	glBufferSubData(GL_ARRAY_BUFFER, 0, 2 * size * sizeof(float), mesh->GetVerticesPointer());

	glBufferSubData(GL_ARRAY_BUFFER, 2 * size * sizeof(float), size * 2 * sizeof(float),
		character_sprite->GetcharacterCooridinate(sprite->GetCurrentFrame(), type, sprite->GetFlipX(), sprite->GetFlipY()));

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(size * 2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	Draw(size, mesh->GetRenderType(), sprite->GetColor());

	glUniform1i(static_cast<int>(SHADER_LOCATION::TEXTURE), 0);
}

void Graphic::DrawLine(vector2 start, vector2 end, Color col)
{
    SendDebugData();

    float vertices[4] = { start.x, start.y, end.x, end.y};

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(SHADER_PROGRAM_TYPE::DEBUG_SHADER)]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

    Draw(2, RENDER_TYPE::LINES, col);
}

void Graphic::DrawRect(vector2 center, vector2 size, Color col)
{
    DrawLine(center + (size / 2), vector2(center.x + size.x / 2, center.y - size.y / 2), col);
    DrawLine(center + (size / 2), vector2(center.x - size.x / 2, center.y + size.y / 2), col);
    DrawLine(center - (size / 2), vector2(center.x + size.x / 2, center.y - size.y / 2), col);
    DrawLine(center - (size / 2), vector2(center.x - size.x / 2, center.y + size.y / 2), col);
}

void Graphic::DrawCircle(vector2 center, float radius, Color col)
{
    SendDebugData();

    float vertices[720];

    for (int i = 0; i < 360; ++i)
    {
        vertices[2 * i + 0] = center.x + radius * cos(To_RADIAN(static_cast<float>(i)));
        vertices[2 * i + 1] = center.y + radius * sin(To_RADIAN(static_cast<float>(i)));
    }

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(SHADER_PROGRAM_TYPE::DEBUG_SHADER)]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

    Draw(360, RENDER_TYPE::LINE_LOOP, col);
}

void Graphic::DrawFilledCircle(vector2 center, float radius, Color col)
{
	SendDebugData();

	float vertices[724];

	vertices[0] = center.x;
	vertices[1] = center.y;

	for (int i = 1; i < 362; ++i)
	{
		vertices[2 * i + 0] = center.x + radius * cos(To_RADIAN(static_cast<float>(i)));
		vertices[2 * i + 1] = center.y + radius * sin(To_RADIAN(static_cast<float>(i)));
	}

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(SHADER_PROGRAM_TYPE::DEBUG_SHADER)]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

	Draw(362, RENDER_TYPE::FAN, col);
}

void Graphic::Draw_curvedline_bezier(Color col, int count, ...)
{
    SendDebugData();

    float vertices[50];

	float x = 50.0f;

    for (int i = 0; i <= (x / 2.0f - 1); ++i)
    {
        vector2 result = vector2(0.0f, 0.0f);
        const float t = i / 24.0f;
        const float s = 1 - t;

        va_list arglist;
        va_start(arglist, count);
        for (int j = 0; j <= (count - 1); ++j)
        {
            vector2 a = va_arg(arglist, vector2);
            float fac = static_cast<float>(GetFactorial(count - 1)) / (GetFactorial(j) * GetFactorial(count - 1 - j));
			result += fac * std::pow(s, (count - 1 - j))*std::pow(t, j) * a;
        }
        va_end(arglist);

        vertices[2 * i + 0] = result.x;
        vertices[2 * i + 1] = result.y;
    }

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(SHADER_PROGRAM_TYPE::DEBUG_SHADER)]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

    Draw(25, RENDER_TYPE::LINE_STRIP, col);
}

void Graphic::Draw_curvedline_bezier_with_thickness(Color col, float thick, int count, ...)
{
	SendDebugData();

	float vertices[100];

	float x = 50.0f;

	for (int i = 0; i <= (x / 2.0f - 1); ++i)
	{
		vector2 result = vector2(0.0f, 0.0f);
		const float t = i / 24.0f;
		const float s = 1 - t;

		va_list arglist;
		va_start(arglist, count);
		for (int j = 0; j <= (count - 1); ++j)
		{
			vector2 a = va_arg(arglist, vector2);
			float fac = static_cast<float>(GetFactorial(count - 1)) / (GetFactorial(j) * GetFactorial(count - 1 - j));
			result += fac * std::pow(s, (count - 1 - j))*std::pow(t, j) * a;
		}
		va_end(arglist);

		if (i != 0)
		{
			vector2 end = vector2(vertices[4 * (i - 1) + 0], vertices[4 * (i - 1) + 1]);
			vector2 normal = Normalizing(Perpendicular(end - result));
			float half_thick = thick / 2.0f;
			vertices[4 * (i - 1) + 0] += (half_thick)*normal.x;
			vertices[4 * (i - 1) + 1] += (half_thick)*normal.y;
			vertices[4 * (i - 1) + 2] -= (half_thick)*normal.x;
			vertices[4 * (i - 1) + 3] -= (half_thick)*normal.y;
		}

		vertices[4 * i + 0] = result.x;
		vertices[4 * i + 1] = result.y;
		vertices[4 * i + 2] = result.x;
		vertices[4 * i + 3] = result.y;
	}

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(SHADER_PROGRAM_TYPE::DEBUG_SHADER)]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

	Draw(50, RENDER_TYPE::STRIP, col);
}

void Graphic::Draw_curveddotted_line_bezier_with_thickness(Color col, float thick, int count, ...)
{
	SendDebugData();

	float x = 50.0f;

	for (int i = 0; i <= (x / 2.0f - 1); ++i)
	{
		vector2 result = vector2(0.0f, 0.0f);
		const float t = i / 24.0f;
		const float s = 1 - t;

		va_list arglist;
		va_start(arglist, count);
		for (int j = 0; j <= (count - 1); ++j)
		{
			vector2 a = va_arg(arglist, vector2);
			float fac = static_cast<float>(GetFactorial(count - 1)) / (GetFactorial(j) * GetFactorial(count - 1 - j));
			result += fac * std::pow(s, (count - 1 - j))*std::pow(t, j) * a;
		}
		va_end(arglist);

		DrawFilledCircle(result, thick * 0.6f, col);
	}
}

void Graphic::DrawLaser(vector2 pos, float angle, bool fire)
{
	DrawBehavior* behavior = new DrawBehavior();
	behavior->m_type = graphic_enum::DrawType::EFFECT;
	behavior->m_color[0] = pos.x;
	behavior->m_color[1] = pos.y;
	behavior->m_color[2] = angle;
	behavior->m_color[3] = fire;
	m_drawlist.insert(std::make_pair(-0.5f, behavior));
}

void Graphic::StartParticle()
{
	int size = static_cast<int>(m_vertices.size());

	m_shaderlist.at(SHADER_PROGRAM_TYPE::TEXTURE_SHADER).Use();

	glUniform1i(static_cast<int>(SHADER_LOCATION::HUD), 0);

	glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

	glBufferData(GL_ARRAY_BUFFER, size * 2 * sizeof(float), &m_vertices.at(0), GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(6 * 2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
}

void Graphic::Draw_Particle(int texture_id, Color col, float* transform)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	glUniform1f(static_cast<int>(SHADER_LOCATION::DEPTH), 0.5f);

	glUniformMatrix3fv(static_cast<int>(SHADER_LOCATION::TRANSFORM), 1, GL_FALSE, transform);

	Draw(6, RENDER_TYPE::TRIANGLE, col);
}

void Graphic::SendDebugData()
{
    m_currentprogram = SHADER_PROGRAM_TYPE::DEBUG_SHADER;
	m_shaderlist[SHADER_PROGRAM_TYPE::DEBUG_SHADER].Use();

	glBindVertexArray(m_vertexarrayobject[static_cast<int>(m_currentprogram)]);
}

void Graphic::Draw(int num_vertex, RENDER_TYPE render_type, Color col)
{
    glUniform4fv(static_cast<int>(SHADER_LOCATION::COLOR), 1, col.GetColor_Float());

    switch (render_type)
    {
    case RENDER_TYPE::FAN:
        glDrawArrays(GL_TRIANGLE_FAN, 0, num_vertex);
        break;
    case RENDER_TYPE::STRIP:
        glDrawArrays(GL_TRIANGLE_STRIP, 0, num_vertex);
        break;
    case RENDER_TYPE::TRIANGLE:
        glDrawArrays(GL_TRIANGLES, 0, num_vertex);
        break;
    case RENDER_TYPE::LINES:
        glDrawArrays(GL_LINES, 0, num_vertex);
        break;
    case RENDER_TYPE::LINE_LOOP:
        glDrawArrays(GL_LINE_LOOP, 0, num_vertex);
        break;
    case RENDER_TYPE::LINE_STRIP:
        glDrawArrays(GL_LINE_STRIP, 0, num_vertex);
        break;
    default:
        break;
    }

	//glBindVertexArray(0);
}

void Graphic::Draw(int num_vertex, RENDER_TYPE render_type, float* col)
{
	glUniform4fv(static_cast<int>(SHADER_LOCATION::COLOR), 1, col);

	switch (render_type)
	{
	case RENDER_TYPE::FAN:
		glDrawArrays(GL_TRIANGLE_FAN, 0, num_vertex);
		break;
	case RENDER_TYPE::STRIP:
		glDrawArrays(GL_TRIANGLE_STRIP, 0, num_vertex);
		break;
	case RENDER_TYPE::TRIANGLE:
		glDrawArrays(GL_TRIANGLES, 0, num_vertex);
		break;
	case RENDER_TYPE::LINES:
		glDrawArrays(GL_LINES, 0, num_vertex);
		break;
	case RENDER_TYPE::LINE_LOOP:
		glDrawArrays(GL_LINE_LOOP, 0, num_vertex);
		break;
	case RENDER_TYPE::LINE_STRIP:
		glDrawArrays(GL_LINE_STRIP, 0, num_vertex);
		break;
	default:
		break;
	}

	//glBindVertexArray(0);
}

void Graphic::DrawPostProcess(float /*time*/)
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_currentprogram = SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER;
    m_shaderlist[SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER].Use();

	glBindVertexArray(m_vertexarrayobject[static_cast<int>(m_currentprogram)]);

    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D, m_frame_buffer_texture->GetID());

	static float time = 0.0f;
	time += 0.016f * 3.0f;
	//if (time > 20.0f)
	//{
	//	time -= 20.0f;
	//}
	//glUniform1fv(glGetUniformLocation(m_shaderlist.at(m_currentprogram), "time"), 1, &time);

    Draw(4, RENDER_TYPE::STRIP, Color(255, 255, 255, 255));
}

void Graphic::SetFrameBuffer(Color col)
{
	m_frame_buffer_texture->ChangeSize(APP->GetWindowSize());

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_frame_buffer_texture->GetID(), 0);

	glBindRenderbuffer(GL_RENDERBUFFER, m_renderbuffer);

	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,
		static_cast<int>(APP->GetWindowSize().x), static_cast<int>(APP->GetWindowSize().y));

    glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);

	glClearColor(col.color[0], col.color[1], col.color[2], col.color[3]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Graphic::Close()
{
    glDeleteProgram(m_shaderlist[SHADER_PROGRAM_TYPE::DEBUG_SHADER]);
    glDeleteProgram(m_shaderlist[SHADER_PROGRAM_TYPE::STANDARD_SHADER]);
    glDeleteProgram(m_shaderlist[SHADER_PROGRAM_TYPE::TEXTURE_SHADER]);
    glDeleteProgram(m_shaderlist[SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER]);
	glDeleteProgram(m_shaderlist[SHADER_PROGRAM_TYPE::EFFECT_SHADER]);

    m_shaderlist.clear();

	glDeleteVertexArrays(static_cast<unsigned int>(SHADER_PROGRAM_TYPE::MAX_SHADER), m_vertexarrayobject);
	glDeleteBuffers(static_cast<unsigned int>(SHADER_PROGRAM_TYPE::MAX_SHADER), m_vertexbufferobject);
	glDeleteBuffers(1, &m_uniformbuffer);
	glDeleteFramebuffers(1, &m_framebuffer);
	glDeleteRenderbuffers(1, &m_renderbuffer);

	delete []m_vertexarrayobject;
	delete []m_vertexbufferobject;

	delete m_frame_buffer_texture;

	m_vertices.clear();

	for(auto& texture : m_Textures)
	{
		if(texture.second != nullptr)
		{
			texture.second->Close();
		}
		delete texture.second;
		texture.second = nullptr;
	}
	m_Textures.clear();

	for(auto& font : m_Fonts)
	{
		delete font.second;
		font.second = nullptr;
	}
	m_Fonts.clear();

	for (auto draw : m_drawlist)
	{
		delete draw.second;
	}
	m_drawlist.clear();
	//for (int iter = graphic_enum::DrawType::SOLID_SHAPE; iter != graphic_enum::DrawType::MAX; ++iter)
	//{
	//	m_drawlist[iter].clear();
	//}
	//delete []m_drawlist;
}

void Graphic::InitGLEW()
{
    logger::log_handler.PrintLogOnConsole("Initialization GLEW start");

    glewExperimental = GL_TRUE;
    GLenum errorCode = glewInit();
    if (GLEW_OK != errorCode)
    {
        std::cerr << "Error: GLEW - " << glewGetErrorString(errorCode) << std::endl;
        glfwTerminate();
        std::exit(EXIT_FAILURE);
    }
    if (!GLEW_VERSION_3_3)
    {
        std::cerr << "Error: OpenGL 4.3 API is not available." << std::endl;
        glfwTerminate();
        std::exit(EXIT_FAILURE);
    }

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glBlendFunc(GL_ONE, GL_ONE);

	glEnable(GL_ALPHA_TEST);

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);

    logger::log_handler.PrintLogOnConsole("Initialization GLEW Complete");
}

void Graphic::Update()
{
	for (auto list : m_drawlist)
	{
		DrawBehavior* behavior = list.second;

		switch (behavior->m_type)
		{
		case graphic_enum::DrawType::SOLID_SHAPE:
		{
			ChangeProgram(SHADER_PROGRAM_TYPE::STANDARD_SHADER);

			glUniform1i(static_cast<int>(SHADER_LOCATION::HUD), behavior->m_hud);
			glUniformMatrix3fv(static_cast<int>(SHADER_LOCATION::TRANSFORM), 1, GL_FALSE, behavior->m_transform);
			glUniform1f(static_cast<int>(SHADER_LOCATION::DEPTH), list.first);

			int size = behavior->m_size;

			glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::STANDARD_SHADER)]);
			glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(m_currentprogram)]);
			glBufferData(GL_ARRAY_BUFFER, size * 2 * sizeof(float), behavior->m_vertex.data(), GL_STATIC_DRAW);

			Draw(size, behavior->m_rendertype, behavior->m_color);
			break;
		}
		case graphic_enum::DrawType::SPRITE:
		{
			ChangeProgram(SHADER_PROGRAM_TYPE::TEXTURE_SHADER);

			glUniform1i(static_cast<int>(SHADER_LOCATION::HUD), behavior->m_hud);
			glUniformMatrix3fv(static_cast<int>(SHADER_LOCATION::TRANSFORM), 1, GL_FALSE, behavior->m_transform);
			glUniform1f(static_cast<int>(SHADER_LOCATION::DEPTH), list.first);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, behavior->m_texture_id);

			glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);
			glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(m_currentprogram)]);
			glBufferSubData(GL_ARRAY_BUFFER, 0, 2 * 6 * sizeof(float), &m_vertices.at(0).x);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(6 * 2 * sizeof(GLfloat)));
			glEnableVertexAttribArray(1);
			glBufferSubData(GL_ARRAY_BUFFER, 2 * 6 * sizeof(float), 6 * 2 * sizeof(float), behavior->m_texture.data());

			Draw(6, behavior->m_rendertype, behavior->m_color);
			break;
		}
		case graphic_enum::DrawType::CHARACTER:
		{
			ChangeProgram(SHADER_PROGRAM_TYPE::TEXTURE_SHADER);

			glUniform1i(static_cast<int>(SHADER_LOCATION::HUD), behavior->m_hud);
			glUniformMatrix3fv(static_cast<int>(SHADER_LOCATION::TRANSFORM), 1, GL_FALSE, behavior->m_transform);
			glUniform1f(static_cast<int>(SHADER_LOCATION::DEPTH), list.first);
			glUniform1i(static_cast<int>(SHADER_LOCATION::TEXTURE), 0);

			glActiveTexture(GL_TEXTURE0);// +character_sprite->Getslot());
			glBindTexture(GL_TEXTURE_2D, behavior->m_texture_id);

			glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);
			glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(m_currentprogram)]);
			glBufferSubData(GL_ARRAY_BUFFER, 0, 2 * 6 * sizeof(float), &m_vertices.at(0).x);
			glBufferSubData(GL_ARRAY_BUFFER, 2 * 6 * sizeof(float), 6 * 2 * sizeof(float), behavior->m_texture.data());
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(6 * 2 * sizeof(GLfloat)));
			glEnableVertexAttribArray(1);

			Draw(6, behavior->m_rendertype, behavior->m_color);
			break;
		}
		case graphic_enum::DrawType::TEXT:
		{
			ChangeProgram(SHADER_PROGRAM_TYPE::TEXTURE_SHADER);

			int size = behavior->m_size;

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, behavior->m_texture_id);

			glUniform1i(static_cast<int>(SHADER_LOCATION::HUD), behavior->m_hud);
			glUniformMatrix3fv(static_cast<int>(SHADER_LOCATION::TRANSFORM), 1, GL_FALSE, behavior->m_transform);
			glUniform1f(static_cast<int>(SHADER_LOCATION::DEPTH), list.first);
			glUniform1i(static_cast<int>(SHADER_LOCATION::TEXTURE), 0);

			glBindVertexArray(m_vertexarrayobject[static_cast<int>(m_currentprogram)]);
			glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(m_currentprogram)]);

			glBufferData(GL_ARRAY_BUFFER, size * 4 * sizeof(float), nullptr, GL_DYNAMIC_DRAW);
			glBufferSubData(GL_ARRAY_BUFFER, 0, size * 2 * sizeof(float), behavior->m_vertex.data());
			glBufferSubData(GL_ARRAY_BUFFER, 2 * size * sizeof(float), size * 2 * sizeof(float),
				behavior->m_texture.data());

			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(size * 2 * sizeof(GLfloat)));
			glEnableVertexAttribArray(1);

			Draw(size, behavior->m_rendertype, behavior->m_color);
			break;
		}
		case graphic_enum::DrawType::PARTICLE:
		{
			StartParticle();
			
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, behavior->m_texture_id);

			glUniform1f(static_cast<int>(SHADER_LOCATION::DEPTH), list.first);

			glUniformMatrix3fv(static_cast<int>(SHADER_LOCATION::TRANSFORM), 1, GL_FALSE, behavior->m_transform);

			Draw(6, RENDER_TYPE::TRIANGLE, behavior->m_color);

			break;
		}
		case graphic_enum::DrawType::TILE:
		{
			m_shaderlist.at(SHADER_PROGRAM_TYPE::TEXTURE_SHADER).Use();

			glUniform1i(static_cast<int>(SHADER_LOCATION::HUD), 0);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, behavior->m_texture_id);

			glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

			glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(SHADER_PROGRAM_TYPE::TEXTURE_SHADER)]);

			int size = behavior->m_size;

			glBufferData(GL_ARRAY_BUFFER, size * 2 * sizeof(float), nullptr, GL_DYNAMIC_DRAW);

			glBufferSubData(GL_ARRAY_BUFFER, 0, size * sizeof(float), behavior->m_vertex.data());

			glBufferSubData(GL_ARRAY_BUFFER, size * sizeof(float), size * sizeof(float),
				behavior->m_texture.data());

			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(size * sizeof(GLfloat)));
			glEnableVertexAttribArray(1);

			affine2d identity = Identity_Affine();

			glUniformMatrix3fv(static_cast<int>(SHADER_LOCATION::TRANSFORM), 1, GL_FALSE, &(identity.column1.x));

			glUniform1f(static_cast<int>(SHADER_LOCATION::DEPTH), list.first);

			Draw(size, RENDER_TYPE::TRIANGLE, behavior->m_color);
			break;
		}
		case graphic_enum::DrawType::EFFECT:
		{
			m_currentprogram = SHADER_PROGRAM_TYPE::EFFECT_SHADER;
			m_shaderlist[SHADER_PROGRAM_TYPE::EFFECT_SHADER].Use();

			glBindVertexArray(m_vertexarrayobject[static_cast<int>(m_currentprogram)]);

			vector2 pos = vector2(behavior->m_color[0], behavior->m_color[1]);
			float angle = behavior->m_color[2];
			bool fire = behavior->m_color[3];

			static float time = 0.0f;
			time += 0.016f;

			glUniform1fv(glGetUniformLocation(m_shaderlist.at(m_currentprogram), "time"), 1, &time);

 			vector2 size = APP->GetWindowSize();
			glUniform2fv(glGetUniformLocation(m_shaderlist.at(m_currentprogram), "pos"), 1, &pos.x);

			glUniform2fv(glGetUniformLocation(m_shaderlist.at(m_currentprogram), "resolution"), 1, &size.x);

			float rad = -To_RADIAN(angle);
			glUniform1fv(glGetUniformLocation(m_shaderlist.at(m_currentprogram), "angle"), 1, &rad);

			glUniform1i(glGetUniformLocation(m_shaderlist.at(m_currentprogram), "fire"), fire);

			Draw(6, RENDER_TYPE::TRIANGLE, Color(255, 255, 255, 255));
			break;
		}
		}
		//behavior->close();
		delete behavior;
	}

	m_drawlist.clear();
}

void Graphic::UpdateProjection(vector2 size)
{
	affine2d projection = Scale_Affine(2.0f / size.x, 2.0f / size.y);

	glBindBuffer(GL_UNIFORM_BUFFER, m_uniformbuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, 3 * sizeof(float), &(projection.column1.x));
	glBufferSubData(GL_UNIFORM_BUFFER, 16, 3 * sizeof(float), &(projection.column2.x));
	glBufferSubData(GL_UNIFORM_BUFFER, 32, 3 * sizeof(float), &(projection.column3.x));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void Graphic::UpdateCamera(affine2d& mat)
{
	glBindBuffer(GL_UNIFORM_BUFFER, m_uniformbuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 48, 3 * sizeof(float), &(mat.column1.x));
	glBufferSubData(GL_UNIFORM_BUFFER, 48 + 16, 3 * sizeof(float), &(mat.column2.x));
	glBufferSubData(GL_UNIFORM_BUFFER, 48 + 32, 3 * sizeof(float), &(mat.column3.x));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

unsigned Graphic::GetProgramID(SHADER_PROGRAM_TYPE shader_program)
{
    return m_shaderlist[shader_program];
}

int Graphic::GetUniformLocation(const char* string)
{
    return glGetUniformLocation(m_shaderlist.at(m_currentprogram), string);
}

void Graphic::ChangeProgram(SHADER_PROGRAM_TYPE type)
{
    if(m_currentprogram == type)
    {
        return;
    }
    m_currentprogram = type;
    m_shaderlist[m_currentprogram].Use();
}


Texture* Graphic::GetTexture(std::string key) const
{
    if (m_Textures.find(key) == m_Textures.end())
    {
        return nullptr;
    }
    return m_Textures.at(key);
}

Font* Graphic::GetFont(std::string key) const
{
    if (m_Fonts.find(key) == m_Fonts.end())
    {
        return nullptr;
    }
    return m_Fonts.at(key);
}

void Graphic::InsertNewTexture(std::string name, Texture* texture)
{
	if (m_Textures.find(name) != m_Textures.end())
	{
		logger::log_handler.PrintLogOnConsoleAndFile(
			utility::MakeStringUsingFormat("Fail to insert the texture : {}", name), spdlog::level::err);
	}

	m_Textures[name] = texture;
}

std::unordered_map<std::string, Texture*> Graphic::GetTexture_Container() const
{
    return m_Textures;
}

std::unordered_map<std::string, Font*> Graphic::GetFont_Container() const
{
    return m_Fonts;
}

void Graphic::LoadAllTexture()
{
    logger::log_handler.PrintLogOnConsole("Loading the textures");

    m_Textures["Rectangle"] = NULL;
    m_Textures["Circle"] = NULL;
    m_Textures["Triangle"] = NULL;
    m_Textures["RightTriangle"] = NULL;

    errno_t err;
    FILE* fp;
    std::string filename = "data/texture.json";
    err = fopen_s(&fp, filename.c_str(), "rb");
    if (err != 0)
        return;

    char readBuffer[65536] = { 0 };
    rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

    rapidjson::Document d;
    d.ParseStream(is);

    const rapidjson::Value& comp = d["Texture"];
    for (rapidjson::SizeType i = 0; i < comp.Size(); i++)
    {
        Texture* temp = new Texture();
        int framesize = comp[i].FindMember("FrameSize")->value.GetInt();
        float framespeed = comp[i].FindMember("FrameSpeed")->value.GetFloat();
        if (!temp->InitWithTexturePath(comp[i].FindMember("Name")->value.GetString(), framesize))
        {
            logger::log_handler.PrintLogOnConsole(utility::MakeStringUsingFormat("Cannot load the texture from file : {}", comp[i].FindMember("Name")->value.GetString()), spdlog::level::level_enum::err);
        }
        temp->SetFrameSpeed(framespeed);

        m_Textures[temp->GetName()] = temp;
    }

	const rapidjson::Value& character_comp = d["Character"];

	if(character_comp.Size() > 25)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("Too many character!", spdlog::level::level_enum::err);
	}

	for (rapidjson::SizeType i = 0; i < character_comp.Size(); i++)
	{
		Character_Sprite* temp = new Character_Sprite();
		std::pair<int, float> info[static_cast<int>(PLAYER_STATUS::MAX)];
		std::string file_path = character_comp[i].FindMember("Name")->value.GetString();
		for(int j = 0; j < static_cast<int>(PLAYER_STATUS::MAX); ++j)
		{
			info[j].first = character_comp[i].FindMember("FrameSize")->value.GetArray()[j].GetInt();
			info[j].second = character_comp[i].FindMember("FrameSpeed")->value.GetArray()[j].GetFloat();
		}

		unsigned int id = temp->Init_character_sprite(info, file_path, i);

		glActiveTexture(GL_TEXTURE31 - i);
		glBindTexture(GL_TEXTURE_2D, id);

		m_Textures[temp->GetName()] = temp;
	}

    logger::log_handler.PrintLogOnConsole("Loading textures complete");
}

void Graphic::LoadAllFont()
{
    logger::log_handler.PrintLogOnConsole("Loading the fonts");

    errno_t err;
    FILE* fp;
    std::string filename = "data/font.json";
    err = fopen_s(&fp, filename.c_str(), "rb");
    if (err != 0)
        return;

    char readBuffer[65536] = { 0 };
    rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

    rapidjson::Document d;
    d.ParseStream(is);

    const rapidjson::Value& comp = d["Font"];
    for (rapidjson::SizeType i = 0; i < comp.Size(); i++)
    {
        Font * temp = new Font();
        if (!temp->LoadFromFile(comp[i].FindMember("Name")->value.GetString()))
        {
            logger::log_handler.PrintLogOnConsole(utility::MakeStringUsingFormat("Cannot load the font from file : {}", comp[i].FindMember("Name")->value.GetString()), spdlog::level::level_enum::err);
        }
        m_Fonts[temp->GetName()] = temp;
    }

    logger::log_handler.PrintLogOnConsole("Loading fonts complete");
}

void Graphic::InitUniformBuffer()
{
	for(auto i : m_shaderlist)
	{
		int uniform_loc = glGetUniformBlockIndex(i.second, "Proj");
		if(uniform_loc != -1)
		{
			glUniformBlockBinding(i.second, uniform_loc, 0);
		}
	}

	glGenBuffers(1, &m_uniformbuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, m_uniformbuffer);

	glBufferData(GL_UNIFORM_BUFFER, 48 * 2, NULL, GL_STATIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, m_uniformbuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void Graphic::InitFramebuffer()
{
	glGenFramebuffers(1, &m_framebuffer);

	glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);

	m_frame_buffer_texture = new Texture(static_cast<int>(APP->GetWindowSize().x), static_cast<int>(APP->GetWindowSize().y));

	m_frame_buffer_texture->GenTexture();

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_frame_buffer_texture->GetID(), 0);

	glGenRenderbuffers(1, &m_renderbuffer);

	glBindRenderbuffer(GL_RENDERBUFFER, m_renderbuffer);

	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,
		static_cast<int>(APP->GetWindowSize().x), static_cast<int>(APP->GetWindowSize().y));

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_renderbuffer);

	if (auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		status != GL_FRAMEBUFFER_COMPLETE)
	{
		logger::log_handler.PrintLogOnConsole("Cannot attatch to framebuffer", spdlog::level::level_enum::err);
	}

	m_shaderlist.at(SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER).Use();
	m_currentprogram = SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER;

	glUniform1i(glGetUniformLocation(m_shaderlist.at(SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER), "inTexture_to_sample"), 1);

	GLfloat offset = 1.0f / 200.0f;
	GLfloat offsets[9][2] = {
		{ -offset,  offset  },  // top-left
		{  0.0f,    offset  },  // top-center
		{  offset,  offset  },  // top-right
		{ -offset,  0.0f    },  // center-left
		{  0.0f,    0.0f    },  // center-center
		{  offset,  0.0f    },  // center - right
		{ -offset, -offset  },  // bottom-left
		{  0.0f,   -offset  },  // bottom-center
		{  offset, -offset  }   // bottom-right    
	};

	glUniform2fv(glGetUniformLocation(m_shaderlist.at(m_currentprogram), "offsets"), 9, (GLfloat*)offsets);
	GLint edge_kernel[9] = {
		-1, -1, -1,
		-1,  8, -1,
		-1, -1, -1
	};

	glUniform1iv(glGetUniformLocation(m_shaderlist.at(m_currentprogram), "edge_kernel"), 9, edge_kernel);
	GLfloat blur_kernel[9] = {
		1.0 / 16, 2.0 / 16, 1.0 / 16,
		2.0 / 16, 4.0 / 16, 2.0 / 16,
		1.0 / 16, 2.0 / 16, 1.0 / 16
	};

	glUniform1fv(glGetUniformLocation(m_shaderlist.at(m_currentprogram), "blur_kernel"), 9, blur_kernel);

	glBindVertexArray(m_vertexarrayobject[static_cast<int>(SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER)]);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(4 * 2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	float data[16] = { -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 
		0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f };
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 16, data);

	int loc = GetUniformLocation("confuse");

	glUniform1i(loc, 0);

	loc = GetUniformLocation("chaos");

	glUniform1i(loc, 0);

	loc = GetUniformLocation("shake");

	glUniform1i(loc, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Graphic::InitShader()
{
    logger::log_handler.PrintLogOnConsole("Initializing Shader.....");

    m_shaderlist.emplace(SHADER_PROGRAM_TYPE::DEBUG_SHADER, Shader(shader_source::Debug_vertex_shader_text.c_str(),
        shader_source::Debug_fragment_shader_text.c_str()));
    m_shaderlist.emplace(SHADER_PROGRAM_TYPE::STANDARD_SHADER, Shader(shader_source::vertex_shader_text.c_str(),
        shader_source::fragment_shader_text.c_str()));
    m_shaderlist.emplace(SHADER_PROGRAM_TYPE::TEXTURE_SHADER, Shader(shader_source::texture_vertex_shader_text.c_str(),
        shader_source::texture_fragment_shader_text.c_str()));
	m_shaderlist.emplace(SHADER_PROGRAM_TYPE::EFFECT_SHADER, Shader(shader_source::effect_vertex_text.c_str(),
		shader_source::effect_fragment_text.c_str()));
    m_shaderlist.emplace(SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER, Shader(shader_source::postprocess_vertex_text.c_str(),
        shader_source::postprocess_fragment_text.c_str()));

    logger::log_handler.PrintLogOnConsole("Shader compile complete");

	m_vertices.push_back(vector2(-0.5f, -0.5f));
	m_vertices.push_back(vector2(-0.5f, 0.5f));
	m_vertices.push_back(vector2(0.5f, -0.5f));
	m_vertices.push_back(vector2(0.5f, -0.5f));
	m_vertices.push_back(vector2(-0.5f, 0.5f));
	m_vertices.push_back(vector2(0.5f, 0.5f));

	m_vertices.push_back(vector2(0.0f, 1.0f));
	m_vertices.push_back(vector2(0.0f, 0.0f));
	m_vertices.push_back(vector2(1.0f, 1.0f));
	m_vertices.push_back(vector2(1.0f, 1.0f));
	m_vertices.push_back(vector2(0.0f, 0.0f));
	m_vertices.push_back(vector2(1.0f, 0.0f));

	for (auto i : m_shaderlist)
	{
		glUseProgram(i.second);

		glBindVertexArray(m_vertexarrayobject[static_cast<int>(i.first)]);

		glBindBuffer(GL_ARRAY_BUFFER, m_vertexbufferobject[static_cast<int>(i.first)]);
		glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * 2 * sizeof(float), &m_vertices.at(0), GL_STATIC_DRAW);

		int texture_loc = glGetUniformLocation(i.second, "inTexture_to_sample");
		if (texture_loc != -1)
		{
			glUniform1i(texture_loc, 0);

			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(6 * 2 * sizeof(GLfloat)));
			glEnableVertexAttribArray(1);
		}
		else
		{
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)0);
			glEnableVertexAttribArray(0);
		}

		glBindVertexArray(0);
	}

	logger::log_handler.PrintLogOnConsole("Shader initialization complete");

	InitUniformBuffer();
	InitFramebuffer();
}

void Graphic::Capture_Screenshot() const
{
	Texture * screen_shot = new Texture(static_cast<int>(APP->GetWindowSize().x), static_cast<int>(APP->GetWindowSize().y));
	screen_shot->SaveToFile("screen_shot.png");
	logger::log_handler.PrintLogOnConsoleAndFile("ScreenShot Capture");
	delete screen_shot;
}

void Graphic::Toggle_confuse(bool flag)
{
	m_currentprogram = SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER;
	m_shaderlist[SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER].Use();

	int loc = GetUniformLocation("confuse");

	glUniform1i(loc, flag);
}

void Graphic::Toggle_chaos(bool flag)
{
	m_currentprogram = SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER;
	m_shaderlist[SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER].Use();

	int loc = GetUniformLocation("chaos");

	glUniform1i(loc, flag);
}

void Graphic::Toggle_shake(bool flag)
{	
	m_currentprogram = SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER;
	m_shaderlist[SHADER_PROGRAM_TYPE::POST_PROCESS_SHADER].Use();

	int loc = GetUniformLocation("shake");

	glUniform1i(loc, flag);
}

void DrawBehavior::close()
{
	m_vertex.clear();
	m_texture.clear();
}

DrawBehavior::~DrawBehavior()
{
	m_vertex.clear();
	m_texture.clear();
}
