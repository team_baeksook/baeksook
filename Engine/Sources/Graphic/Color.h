/***********************************************************************
	File name		: Color.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once

class Color;

class HSV
{
public:
	unsigned char Hue = 0;
	float Saturation = 0;
	float Value = 0;
	HSV(unsigned char h = 0, float s = 0.0f, float v = 0.0f);
	Color ConvertRGB();


};

class Color
{
public:
    unsigned char Red = 0;
    unsigned char Green = 0;
    unsigned char Blue = 0;
    unsigned char Alpha = 255;
    Color(unsigned char r = 255, unsigned char g = 255, unsigned char b = 255, unsigned char a = 255);
    float color[4];
    float* GetColor_Float();
	float* MakeColor_Float();
	Color operator-(const Color& rhs);
	Color& operator=(const Color& rhs);
	void SetRGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	HSV ConvertHSV();
};

