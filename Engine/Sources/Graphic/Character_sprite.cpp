/***********************************************************************
	File name		: CharacterSprite.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include "Character_sprite.h"
#include "System/Logger.h"

unsigned Character_Sprite::Init_character_sprite(std::pair<int, float>* pair_ptr, std::string file_path, int slot)
{
	m_max_framesize = 0;
	for(int i = 0; i < static_cast<int>(PLAYER_STATUS::MAX); ++i)
	{
		if(m_max_framesize < pair_ptr[i].first)
		{
			m_max_framesize = pair_ptr[i].first;
		}
		m_animation_info[static_cast<PLAYER_STATUS>(i)] = pair_ptr[i];
	}

	LoadFromFile(file_path);

	if(m_animation_info.size() != static_cast<int>(PLAYER_STATUS::MAX))
	{
		logger::log_handler.PrintLogOnConsoleAndFile("the animation information is wrong", spdlog::level::level_enum::err);
	}

	GenTexture();

	m_texture_slot = 31 - slot;

	return GetID();
}

Character_Sprite::~Character_Sprite()
{
}

int Character_Sprite::GetFrameSize(int type) const
{
	return m_animation_info.at(static_cast<PLAYER_STATUS>(type)).first;
}

float Character_Sprite::GetFrameSpeed(int type) const
{
	return m_animation_info.at(static_cast<PLAYER_STATUS>(type)).second;
}

unsigned Character_Sprite::Getslot() const
{
	return m_texture_slot;
}

float* Character_Sprite::GetcharacterCooridinate(int current_frame, int type, bool flipx, bool flipy)
{
	m_character_coordinate.clear();
	int x_flip = (flipx) ? -1 : 1;
	int y_flip = (flipy) ? -1 : 1;
	for (unsigned int i = 0; i < m_texture_coordinate.size(); ++i)
	{
		float x_temp = m_texture_coordinate.at(i).x * x_flip - 0.5f * x_flip + 0.5f;
		float y_temp = m_texture_coordinate.at(i).y * y_flip - 0.5f * y_flip + 0.5f;
		m_character_coordinate.push_back(vector2((x_temp + current_frame) / static_cast<float>(m_max_framesize),
			((y_temp + static_cast<float>(type)) / static_cast<int>(PLAYER_STATUS::MAX))));
	}
	return &(m_character_coordinate.at(0).x);
}

float* Character_Sprite::MakecharacterCoordinate(int current_frame, int type, bool flipx, bool flipy)
{
	float* ptr = new float[m_texture_coordinate.size() * 2];

	int x_flip = (flipx) ? -1 : 1;
	int y_flip = (flipy) ? -1 : 1;
	for (unsigned int i = 0; i < m_texture_coordinate.size(); ++i)
	{
		float x_temp = m_texture_coordinate.at(i).x * x_flip - 0.5f * x_flip + 0.5f;
		float y_temp = m_texture_coordinate.at(i).y * y_flip - 0.5f * y_flip + 0.5f;
		ptr[i * 2] = (x_temp + current_frame) / static_cast<float>(m_max_framesize);
		ptr[i * 2 + 1] = (y_temp + static_cast<float>(type)) / static_cast<int>(PLAYER_STATUS::MAX);
	}

	return ptr;
}

std::vector<vector2> Character_Sprite::CopyCoordinate(int current_frame, int type, bool flipx, bool flipy)
{
	m_character_coordinate.clear();
	int x_flip = (flipx) ? -1 : 1;
	int y_flip = (flipy) ? -1 : 1;
	for (unsigned int i = 0; i < m_texture_coordinate.size(); ++i)
	{
		float x_temp = m_texture_coordinate.at(i).x * x_flip - 0.5f * x_flip + 0.5f;
		float y_temp = m_texture_coordinate.at(i).y * y_flip - 0.5f * y_flip + 0.5f;
		m_character_coordinate.push_back(vector2((x_temp + current_frame) / static_cast<float>(m_max_framesize),
			((y_temp + static_cast<float>(type)) / static_cast<int>(PLAYER_STATUS::MAX))));
	}
	return m_character_coordinate;
}

void Character_Sprite::Close()
{
	m_character_coordinate.clear();
	m_animation_info.clear();
	Texture::Close();
}
