/***********************************************************************
	File name		: Font.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include "Texture.h"

class Character
{
public:
    char character = NULL;
    float width = 0;
    float height = 0;
    float xoffset = 0;
    float yoffset = 0;
    float xadvance = 0;
    vector2 top_left = vector2(0.0f, 0.0f);
    vector2 bottom_right = vector2(0.0f, 0.0f);
};

class Font
{
public:
	~Font();

    Character GetCharacter(char letter) const;
    bool LoadFromFile(const std::string& file_path);

    Texture * GetTexture() const;
    float lineHeight = 0;
    int fontSize = 0;

    Texture * GetTexture();

    std::string GetName() const;
private:
    std::string m_Name = " ";
    Texture * m_Texture = NULL;

    std::unordered_map<char, Character> characters{};
    void SearchLineAndSetNumber(const std::string& line, std::string key, int& storage);
    void SearchLineAndSetNumber(const std::string& line, std::string key, float& storage);
    void SearchLineAndSetString(const std::string& line, std::string key, std::string& storage);
};