/***********************************************************************
	File name		: Font.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Font.h"
#include "fstream"
#include "System/Logger.h"
#include "System/Engine_Utility.h"

bool Font::LoadFromFile(const std::string& file_path)
{
    std::ifstream file(file_path);
    if(!file)
    {
        std::string error_string = utility::MakeStringUsingFormat("Cannot open font file : {}", file_path);
        logger::log_handler.PrintLogOnConsoleAndFile(
            error_string, spdlog::level::level_enum::err);
        return false;
    }

    std::string line;
    std::string variable;

    int imageWidth = 0;
    int imageHeight = 0;

    while(!file.eof())
    {
        file >> variable;
        std::getline(file, line);

        if (variable == "info")
        {
            SearchLineAndSetString(line, "face", m_Name);
            SearchLineAndSetNumber(line, "size", fontSize);
        }
        else if (variable == "common")
        {
            SearchLineAndSetNumber(line, "lineHeight", lineHeight);
            SearchLineAndSetNumber(line, "scaleW", imageWidth);
            SearchLineAndSetNumber(line, "scaleH", imageHeight);
        }
        else if (variable == "char")
        {
            Character desc;
            int character_to_int = 0;
            int xpos = 0;
            int ypos = 0;

            SearchLineAndSetNumber(line, "id", character_to_int);
            SearchLineAndSetNumber(line, "x", xpos);
            SearchLineAndSetNumber(line, "y", ypos);
            SearchLineAndSetNumber(line, "width", desc.width);
            SearchLineAndSetNumber(line, "height", desc.height);
            SearchLineAndSetNumber(line, "xoffset", desc.xoffset);
            SearchLineAndSetNumber(line, "yoffset", desc.yoffset);
            SearchLineAndSetNumber(line, "xadvance", desc.xadvance);

            desc.top_left = vector2(static_cast<float>(xpos) / imageWidth, static_cast<float>(ypos) / imageHeight);
            desc.bottom_right = vector2(static_cast<float>(xpos + desc.width * fontSize) / imageWidth, static_cast<float>(ypos + desc.height * fontSize) / imageHeight);

            desc.character = (char)character_to_int;
            characters.insert_or_assign(desc.character, desc);
        }
    }

    m_Texture = new Texture();
    std::string temp = file_path.substr(0, file_path.size() - 4);
    temp += ".png";
    m_Texture->InitWithTexturePath(temp);

    return true;
}

Texture* Font::GetTexture() const
{
    return m_Texture;
}

Font::~Font()
{
	delete m_Texture;
}

Character Font::GetCharacter(char letter) const
{
    if (characters.find(letter) == characters.end())
    {
        return Character();
    }   
    return characters.at(letter);
}

Texture* Font::GetTexture()
{
    return m_Texture;
}

std::string Font::GetName() const
{
    return m_Name;
}

void Font::SearchLineAndSetNumber(const std::string& line, std::string key, int& storage)
{
    key += "=";
    auto offset = line.find(key);
    if (offset != std::string::npos)
    {
        offset += key.length();
        auto end = line.find(" ", offset);
        std::string temp = line.substr(offset, end - offset);
        storage = std::stoi(temp);
    }
}

void Font::SearchLineAndSetNumber(const std::string& line, std::string key, float& storage)
{
    key += "=";
    auto offset = line.find(key);
    if (offset != std::string::npos)
    {
        offset += key.length();
        auto end = line.find(" ", offset);
        std::string temp = line.substr(offset, end - offset);
        storage = std::stof(temp) / fontSize;
    }
}

void Font::SearchLineAndSetString(const std::string& line, std::string key, std::string& storage)
{
    key += "=\"";
    auto offset = line.find(key);
    if (offset != std::string::npos)
    {
        offset += key.length();
        auto end = line.find("\"", offset);
        storage = line.substr(offset, end - offset);
    }
}
