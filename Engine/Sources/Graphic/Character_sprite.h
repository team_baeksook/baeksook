/***********************************************************************
	File name		: Character_sprite.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Graphic/Texture.h"
#include <map>

enum class PLAYER_STATUS { NORMAL = 0, WALKING = 1, DAMAGED = 2, JUMP = 3, ATTACK = 4,STUN=5, MAX };

class Character_Sprite : public Texture
{
private:
	std::map<PLAYER_STATUS, std::pair<int, float>> m_animation_info;
	int m_max_framesize = 0;
	unsigned int m_texture_slot;
	std::vector<vector2> m_character_coordinate;
public:
	unsigned int Init_character_sprite(std::pair<int, float>* pair_ptr, std::string file_path, int slot);
	~Character_Sprite() override;
	int GetFrameSize(int type) const override;
	float GetFrameSpeed(int type) const override;
	unsigned int Getslot() const;
	float* GetcharacterCooridinate(int current_frame, int type, bool flipx, bool flipy);
	float* MakecharacterCoordinate(int current_frame, int type, bool flipx, bool flipy);
	std::vector<vector2> CopyCoordinate(int current_frame, int type, bool flipx, bool flipy);
	void Close() override;
};
