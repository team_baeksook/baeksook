/***********************************************************************
	File name		: Color.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Color.h"
#include <cmath>

Color::Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a) : Red(r), Green(g), Blue(b), Alpha(a)
{
	color[0] = static_cast<float>(r / 255.0f);
	color[1] = static_cast<float>(g / 255.0f);
	color[2] = static_cast<float>(b / 255.0f);
	color[3] = static_cast<float>(a / 255.0f);
}

float* Color::GetColor_Float()
{
    //m_color[0] = (float)Red / 255.0f;
    //m_color[1] = (float)Green / 255.0f;
    //m_color[2] = (float)Blue / 255.0f;
    //m_color[3] = (float)Alpha / 255.0f;
    return &(color[0]);
}

float* Color::MakeColor_Float()
{
	float* result = new float[4];

	result[0] = color[0];
	result[1] = color[1];
	result[2] = color[2];
	result[3] = color[3];

	return result;
}

Color Color::operator-(const Color& rhs)
{
	return Color(Red - rhs.Red, Green - rhs.Green, Blue - rhs.Blue, Alpha - rhs.Alpha);
}

Color& Color::operator=(const Color & rhs)
{
	Red = rhs.Red;
	Green = rhs.Green;
	Blue = rhs.Blue;
	Alpha = rhs.Alpha;
	for (int i = 0; i < 4; ++i)
	{
		color[i] = rhs.color[i];
	}
	return *this;
}

void Color::SetRGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
	Red = r;
	Green = g;
	Blue = b;
	Alpha = a;
	color[0] = static_cast<float>(r / 255.0f);
	color[1] = static_cast<float>(g / 255.0f);
	color[2] = static_cast<float>(b / 255.0f);
	color[3] = static_cast<float>(a / 255.0f);
}

HSV Color::ConvertHSV()
{
	float R = (Red / 255.f);
	float G = (Green / 255.f);
	float B = (Blue / 255.f);
	float min;
	float max;

	if (R < G)
	{
		min = R;
		max = G;
	}
	else
	{
		min = G;
		max = R;
	}

	if (min > B)
	{
		min = B;
	}
	if (max < B)
	{
		max = B;
	}

	float diff = max - min;

	if (max == 0)
	{
		return HSV(0, 0, 0);
	}

	float H = 0.0f;
	float S = diff / max;
	float V = max;

	if (R == max)
	{
		H = (G - B) / diff;
	}
	else if (G == max)
	{
		H = 2 + (B - R) / diff;
	}
	else if (B == max)
	{
		H = 4 + (R - G) / diff;
	}

	H *= 60;

	if (H < 0)
	{
		H += 360;
	}

	unsigned char h = static_cast<unsigned char>(H);
	//for precision
	float s = std::roundf(S * 1000.0f) / 10.0f;
	float v = std::roundf(V * 1000.0f) / 10.0f;
	
	return HSV(h, s, v);
}

HSV::HSV(unsigned char h, float s, float v) : Hue(h), Saturation(s), Value(v) {}

Color HSV::ConvertRGB()
{
	float h = static_cast<float>(Hue);
	float s = Saturation / 100.0f;
	float v = Value / 100.0f;
	float adjusted_hue = std::floorf(h / 60.0f);
	int d = static_cast<int>(adjusted_hue) % 6;
	float f = (h / 60.0f) - adjusted_hue;
	float p = v * (1 - s);
	float q = v * (1 - f * s);
	float t = v * (1 - (1 - f) * s);
	float r = 0;
	float g = 0;
	float b = 0;
	
	switch(d)
	{
	case 0:
		r = v;
		g = t;
		b = p;
		break;
	case 1:
		r = q;
		g = v;
		b = p;
		break;
	case 2:
		r = p;
		g = v;
		b = t;
		break;
	case 3:
		r = p;
		g = q;
		b = v;
		break;
	case 4:
		r = t;
		g = p;
		b = v;
		break;
	case 5:
		r = v;
		g = p;
		b = q;
		break;
	}

	unsigned char red = static_cast<unsigned char>(r * 255.0f);
	unsigned char green = static_cast<unsigned char>(g * 255.0f);
	unsigned char blue = static_cast<unsigned char>(b * 255.0f);

	return Color(red, green, blue);
}
