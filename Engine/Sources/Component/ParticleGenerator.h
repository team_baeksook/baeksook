#pragma once
#include "Component.h"
#include "Graphic/Graphic.h"
#include "Math/vector2.hpp"

class Particle;

class ParticleGenerator : public Component
{
private:
	unsigned int m_texture_id;
	float m_duration = 3.0f;
	float m_duration_vary = 0.0f;

	vector2 m_size = vector2(10.0f, 10.0f);
	vector2 m_size_vary = vector2(1.0f, 1.0f);

	vector2 m_start_vel = vector2(1.0f, 1.0f);
	float m_vel_dir_vel = 0.0f;
	float m_vel_len_vel = 0.1f;

	std::string m_texture_name = "Red";

	Graphic* m_graphic_system = nullptr;

	bool m_pause = false;
	bool m_ease = false;

	float m_colorstep = 0.0f;
	vector2 m_scalestep = vector2(1.0f, 1.0f);

	Color m_color;

	float m_counter = 0.f;
	float m_gentime = 0.14f;
	float m_timeForEase = 0.f;
	//float m_endVelocityForEese = 1.0f;
	float m_easeValue = 1.0f;

	int m_NumberControl = 75;

	float m_rotvalue = 0.0f;
	float m_rot_vary = 0.0f;

	std::vector<Particle*> m_particles;
public:
	ParticleGenerator(Object* owner_object, const std::string& component_name = "ParticleGenerator");
	~ParticleGenerator() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;

	void SetPause(bool pause);
};
