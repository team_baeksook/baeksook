/***********************************************************************
	File name		: Clock.cpp
	Project name	: WFPT
	Author			: Min Suk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#include "Clock.h"
#include "Object/Object.h"
#include "System/Logger.h"
#include <iostream>
#include "Player.h"
#include "Component/SpriteText.h"
#include <IMGUI/imgui.h>

Clock::Clock(Object* owner_object, const std::string& component_name): Component(owner_object, component_name)
{
}

Clock::~Clock() {}

void Clock::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&)
{
}

void Clock::Deserialization(const rapidjson::Value&)
{
}

void Clock::ImGui_Setting()
{
	ImGui::Checkbox("Pause", &m_pause);
}

void Clock::Init()
{
	if(m_sprite_text_comp = m_owner->GetComponentByTemplate<SpriteText>(); m_sprite_text_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not hold the spritetext component", spdlog::level::err);
	}

	m_sprite_text_comp->SetAllign(TEXT_ALLIGN::ALLIGN_CENTER);
	m_sprite_text_comp->SetHud(true);
}

void Clock::Update(float dt)
{
	if(!m_sprite_text_comp)
	{
		return;
	}
	if(m_pause)
	{
		return;
	}

	m_innertime -= dt;

	int second = static_cast<int>(m_innertime);
	if(m_second != second)
	{
		std::string second_string = std::to_string(m_second);
		m_sprite_text_comp->SetString(second_string);
		m_second = second;
	}
}

void Clock::Close()
{
}

void Clock::TogglePause()
{
	m_pause = !m_pause;
}

int Clock::GetSecond() const
{
	return static_cast<int>(m_innertime);
}

void Clock::SetSecond(float sec)
{
	m_innertime = sec;
}
