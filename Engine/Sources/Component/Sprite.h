/***********************************************************************
	File name		: Sprite.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Graphic/Color.h"
#include "Object/Mesh.h"
#include "Component.h"
#include "Graphic/Texture.h"
#include "Graphic/Character_sprite.h"

class Object;

class Sprite : public Component
{
private:
	friend class Player;
	friend class Revolver;
	friend class Sniper;
	friend class Rifle;
	friend class Shotgun;
	friend class Bow;
	friend class FlameThrower;
	friend class Pistol;
	friend class LightMachineGun;

    Mesh * m_mesh = nullptr;

    SHADER_PROGRAM_TYPE m_shader_type;

    float m_innertime = 0.0f;

    Graphic * m_graphic_system = nullptr;

    Texture * m_texture = nullptr;

	PLAYER_STATUS* m_status = nullptr;
private:
	std::string * m_TextureName = new std::string("Rectangle");

    bool m_Visible = true;

    bool m_FlipX = false;
    bool m_FlipY = false;

    Color m_color;

    bool m_AnimationAcitve = false;
	bool m_oneloop_animation = false;

    int m_currentframe = 0;
	int m_type = 0;

	bool m_ishud = false;

	void HandleAmimation(float dt);
public:
    Sprite(Object * owner_object, std::string component_name = "Sprite");
	~Sprite() override;

    Color GetColor();
    void SetColor(Color temp_color);
	void SetColorR(float r);
	void SetColorG(float g);
	void SetColorB(float b);
	void SetColorA(float a);

    void SetMesh(Mesh * temp_mesh);
    Mesh * GetMesh();

    void SetGraphicSystem(Graphic * graphic);

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;

	void Init() override;
	void Update(float dt) override;
	void Close(void) override;

    int GetCurrentFrame() const;

    std::string GetTextureName() const;
    void SetTextureName(std::string texture_name);

    bool GetFlipX() const;
    bool GetFlipY() const;

	void SetFlipX(bool input);
	void SetFlipY(bool input);

	void TurnOnOffAnimation(bool input);
	void SetCurrentFrame(int frame);
    
    Texture * GetTexture() const;

	void SetType(int type);

	bool IsHud() const;
	void SetHud(bool hud);

	void SetAnimationActive(bool active);
};

