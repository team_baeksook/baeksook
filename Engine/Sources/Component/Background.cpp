#include <IMGUI/imgui.h>
#include <iostream>

#include "Background.h"
#include "Object/Object.h"
#include "System/State.h"
#include "System/Logger.h"
#include "Component/Sprite.h"

Background::Background(Object * owner_object, std::string component_name) : Component(owner_object, component_name)
{
}

void Background::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&)
{
}

void Background::Deserialization(const rapidjson::Value &)
{
}

void Background::ImGui_Setting()
{
	ImGui::InputFloat("Rate", &m_rate);
}

void Background::Init()
{
	m_initpos = m_owner->GetTransform()->GetWorldPosition().x;

	if (m_camera = m_owner->GetState()->GetCamera(0); m_camera == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("Fail to find the camera", spdlog::level::err);
	}

	if (m_sprite_comp = m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("the object does not have sprite component", spdlog::level::err);
	}

	Object* clone = new Object(m_owner->GetState(), m_owner->GetName());
	Sprite* sprite = dynamic_cast<Sprite*>(clone->Add_Component(new Sprite(clone)));
	sprite->SetTextureName(m_sprite_comp->GetTextureName());
	sprite->Init();
	sprite->SetFlipX(m_shouldflip);
	m_clone_trans = clone->GetTransform();
	m_clone_trans->SetPosition({ this->m_owner->GetTransform()->GetWorldScale().x, m_owner->GetTransform()->GetWorldPosition().y });
	m_clone_trans->SetScale(this->m_owner->GetTransform()->GetWorldScale());
	m_clone_trans->SetDepth(m_owner->GetTransform()->GetDepth());
	m_owner->GetState()->AddRegisterObject(clone);

	m_secondinitpos = this->m_owner->GetTransform()->GetWorldScale().x;
}

void Background::Update(float)
{
	vector2 camera_pos = m_camera->GetOwner()->GetTransform()->GetWorldPosition();
	float xpos = this->m_owner->GetTransform()->GetWorldScale().x;
	float pos = - camera_pos.x * m_rate;

	m_owner->GetTransform()->SetPositionX(m_firstinitpos + pos);
	m_clone_trans->SetPositionX(pos + m_secondinitpos);

	if (camera_pos.x >= (m_firstinitpos + pos + xpos))
	{
		m_firstinitpos += xpos * 2.0f;
	}
	if (camera_pos.x >= (m_secondinitpos + pos + xpos))
	{
		m_secondinitpos += xpos * 2.0f;
	}

	if (camera_pos.x <= (m_secondinitpos + pos - xpos))
	{
		m_secondinitpos -= xpos * 2.0f;
	}
	if (camera_pos.x <= (m_firstinitpos + pos - xpos))
	{
		m_firstinitpos -= xpos * 2.0f;
	}
}

void Background::Close(void)
{
}

void Background::SetRate(float rate)
{
	m_rate = rate;
}

void Background::Setshouldflip(bool flip)
{
	m_shouldflip = flip;
}
