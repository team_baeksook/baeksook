/***********************************************************************
	File name		: Collision.h
	Project name	: WFPT
	Author			: HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include "Object/Transform.h"
#include "Component/Sprite.h"
#include "Physics.h"

enum class COLLISION_GROUP : unsigned {
	BEGIN = __LINE__,
	BULLET = 2,		//0x00010
	PLAYER = 6,		//0x00110
	ITEM = 4,		//0x00100
	WALL = 10,		//0x01010
	TUTORIAL = 17,	//0x10001
	MAX = TUTORIAL + 1
};

enum class BOX_INDEX
{
	TOP_LEFT = 0,
	TOP_RIGHT = 1,
	BOT_RIGHT = 2,
	BOT_LEFT = 3
};

class Object;

namespace CollisionHelper
{
	std::string ConvertCollisionGroupID(COLLISION_GROUP collision_group);
	COLLISION_GROUP ConvertCollisionGroupString(std::string str);
	bool AABBandLineColision(Collision& box, vector2 start_point, vector2 end_point);
	bool AABBandwideLineCollision(const Collision& box, vector2 start_point, vector2 end_point, float width);
}

class Collision : public Component
{
public:
	Collision(Object* owner_object, const std::string& component_name = "Collision");

	bool IsAABB(Object* object) const;
	bool IsColliding(Object* obj);
	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer) override;
	void Deserialization(const rapidjson::Value& val) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;
	~Collision();

	auto GetCollisioinBox() { return m_collisionBox; }

	void SetCollisionBoxSize(vector2 size);
	vector2 GetCollisionBoxSize() const;

	Object* FindCollidingObjectByName(std::string find_name);
	bool IsInIgnorelist(Object* ptr);
	std::vector<Object*>& GetCollidingObjects();
	std::vector<Object*> GetIgnoreList();

	void AddIgnoreList(Object* obj);

	void SetCollisionGroup(COLLISION_GROUP group);
	COLLISION_GROUP GetCollisionGroup() const;

	vector2 GetCollisionBox_position(BOX_INDEX index = BOX_INDEX::TOP_LEFT) const;
	vector2 GetBoxSize() const;
	vector2 GetCollisionOffset();
	
private:
	vector2 m_collisionBox[4] = {
		vector2(-0.5f, 0.5f), vector2(0.5f, 0.5f),
		vector2(0.5f, -0.5f), vector2(-0.5f, -0.5f) };
	vector2 m_collisionsize = vector2(1.0f, 1.0f);
	vector2 m_collisionoffset = vector2(0.0f, 0.0f);
	
	std::vector<Object*> m_ignorelist;

	COLLISION_GROUP m_collision_group = COLLISION_GROUP::PLAYER;

	Graphic* m_graphic_system = nullptr;

	std::vector<Object*> m_collisionlist;

	void Display_collisionbox() const;

	vector2 GetSize() const;

	bool ShouldCheck(const COLLISION_GROUP target);

	bool IsThereTile(vector2 _position) const;
};
