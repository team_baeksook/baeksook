/***********************************************************************
File name		: WeaponGenerator.h
Project name	: WFPT
Author			: Hyun Seok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component.h"
#include "Graphic/Graphic.h"
#include "ParticleGenerator.h"

class WeaponGenerator : public Component
{
public:
	void MakeWeapon();
	WeaponGenerator(Object* owner_object, const std::string& component_name = "WeaponGenerator");
	~WeaponGenerator() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;

private:
	float m_innerTime = 0.f;
	float m_GenerateCoolDown = 3.f;
	bool m_shouldMakeWeapon = true;
	bool m_firstWeaponGen = true;

	unsigned int m_texture_id;

	Object* m_weapon = nullptr;
};
