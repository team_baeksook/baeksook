/***********************************************************************
File name		: WeaponGenerator.h
Project name	: WFPT
Author			: Hyun Seok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component.h"
#include "Graphic/Graphic.h"
#include "ParticleGenerator.h"
#include "System/SceneIdentifier.h"

class Target : public Component
{
public:
	Target(Object* owner_object, const std::string& component_name = "Target");
	~Target() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;

	bool Is_dead();
private:
	float m_innerTime = 0.f;

	Object* m_healthbar = nullptr;
	float m_healthpoint = 100.f;
};
