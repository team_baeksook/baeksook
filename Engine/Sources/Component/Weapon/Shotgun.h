/***********************************************************************
File name		: Shotgun.h
Project name	: WFPT
Author			: Hyunseok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component/Weapon/Gun.h"
#include "Component/Sprite.h"
#include "System/Audio.h"

class Shotgun : public Gun
{
private:
	Sprite* m_sprite_comp = nullptr;

	float m_innertime = 0.0f;
	float m_shotspeed = 1.0f;
	float m_bulletspeed = 362.0f;

	const int m_bulletPerShot = 7;
	const float m_highestBulletangle = 22.5f;

	const int m_max_magazine = 4;
	int m_magazine = m_max_magazine;
	bool m_should_reload = false;
	const float m_reloadtime = 3.0f;
	Audio* m_sound=nullptr;
public:
	Shotgun(Object* owner_object, const std::string& component_name = "Shotgun");
	~Shotgun() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Gun_Update(float dt) override;
	void Close() override;
	vector2 Fire(vector2 fire) override;
};

