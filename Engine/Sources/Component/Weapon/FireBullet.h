#pragma once
#include "Component/Component.h"
#include "Math/vector2.hpp"

class Collision;
class Physics;

class FireBullet : public Bullet
{
	friend class FlameThrower;
protected:
	enum Direction
	{
		LEFT = -1,
		RIGHT = 1
	};
	Collision* m_collision_comp = nullptr;
	Object* m_origin = nullptr;
	Physics* m_physics_comp = nullptr;

	bool m_IsCollidingWithWall = false;
	bool m_IsCollidingWithFloor = false;
	vector2 m_bulletSpeed;

	const int m_bulletPerShot = 9;

public:
	FireBullet(Object* owner_object, const std::string& component_name = "FireBullet");

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;
	void SetOrigin(Object* obj);
};