/***********************************************************************
	File name		: Bomb.h
	Project name	: WFPT
	Author			: 

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component/Component.h"

class Bomb : public Component
{
public:
	Bomb(Object* owner_object, const std::string& component_name = "Bomb"):Component(owner_object, component_name){}
	virtual ~Bomb() override {}

	virtual void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override = 0;
	virtual void Deserialization(const rapidjson::Value&) override = 0;
	virtual void ImGui_Setting() override = 0;
	virtual void Init() override = 0;
	virtual void Update(float dt) override = 0;
	virtual void Close() override = 0;
	virtual void Fire() = 0;
};