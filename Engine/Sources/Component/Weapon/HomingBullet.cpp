/***********************************************************************
	File name		: Bullet.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#include "HomingBullet.h"
#include "Object/Object.h"
#include "System/Logger.h"
#include "Component/ParticlePhysics.h"
#include "Component/Collision.h"
#include "Component/Player.h"
#include "System/State.h"

HomingBullet::HomingBullet(Object* owner_object, const std::string& component_name): Bullet(owner_object, component_name)
{
	kind = COMPONENT_KIND::BULLET;
}

HomingBullet::~HomingBullet() {}

void HomingBullet::Update(float dt)
{
	m_innertime += dt;
	if(m_innertime<m_refreshtime)
	{
		return;
	}
	else
	{
		m_innertime -= m_refreshtime;
	}
	vector2 velo;
	if (m_owner->GetComponentByTemplate<ParticlePhysics>() != nullptr)
	{
		velo = m_owner->GetComponentByTemplate<ParticlePhysics>()->getVelocity();
		float angle = To_DEGREE(atan2(velo.y, velo.x));
		m_owner->GetTransform()->SetDegree(angle);
	}

	vector2 direc_vector = *m_PlayerPositionPtr - m_owner->GetTransform()->GetWorldPosition();

	if(m_HeadingVector.x*direc_vector.y-m_HeadingVector.y*direc_vector.x>0)
	{
		m_HeadingVector = Rotation_Affine_Degree(5.f)*m_HeadingVector;
	}
	else
	{
		m_HeadingVector = Rotation_Affine_Degree(-5.f)*m_HeadingVector;
	}

	m_owner->GetComponentByTemplate<ParticlePhysics>()->setVelocity(m_HeadingVector*60.f);


	auto collidingobj = m_collision_comp->GetCollidingObjects();
	for(auto collision : collidingobj)
	{
		if(Player* player = collision->GetComponentByTemplate<Player>(); player != nullptr)
		{
			if(player->CanPlayerDamaged())
			{
				float dir = collision->GetTransform()->GetWorldPosition().x - m_owner->GetTransform()->GetWorldPosition().x;

				Player* origin_player = m_origin->GetComponentByTemplate<Player>();
				int current_health = player->GetHealth();
				int damage_taken = (m_damage > current_health) ? current_health : m_damage;
				origin_player->Add_Damage(damage_taken);

				if (player->DamagePlayer(m_damage, dir))
				{
					m_owner->GetState()->GetBadge()->AddKilllog(origin_player->GetPlayerNum(), player->GetPlayerNum());
					origin_player->IncrementKill();
				}
			}
		}
	}

	if(!m_collision_comp->GetCollidingObjects().empty())
	{
		this->m_owner->MakeDead();
		//put this component to bullet
		//when it collides with player occur event to player
	}
}

void HomingBullet::Close()
{
}

void HomingBullet::Set_PlayerPositionPtr(vector2* vector_ptr)
{
	m_PlayerPositionPtr = vector_ptr;
}

//(1,0) is 0 degree
void HomingBullet::Set_HeadingVector(float angle, float speed)
{
	vector3 directional_vector=Rotation_Affine_Degree(angle)*vector3(1.f, 0.f,0.f);
	directional_vector *= speed;
	m_HeadingVector = directional_vector;
}