/***********************************************************************
	File name		: Sniper.cpp
	Project name	: WFPT
	Author			: Seongwook Shin

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Sniper.h"
#include "Object/Object.h"
#include "Component/Sprite.h"
#include "Component/ParticlePhysics.h"
#include "System/State.h"
#include "System/Logger.h"
#include "System/StateManager.h"
#include "System/SoundManager.h"
#include "System/Imgui_app.h"
#include "Component/Collision.h"
#include "Bullet.h"
#include "Graphic/Particle.h"
#include <IMGUI/imgui.h>
#include <iostream>
#include "System/Application.h"

Sniper::Sniper(Object* owner_object, const std::string& component_name) : Gun(owner_object, component_name)
{
	kind = COMPONENT_KIND::SNIPER;
}

Sniper::~Sniper() {}

void Sniper::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Sniper::Deserialization(const rapidjson::Value&)
{
}

void Sniper::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::InputFloat("m_shotspeed", &m_shotspeed);
	ImGui::InputFloat("m_bulletspeed", &m_bulletspeed);
	ImGui::InputInt("m_magazine", &m_magazine);
	ImGui::Checkbox("m_reload", &m_should_reload);

	ImGui::InputFloat("m_max_range", &m_max_range);
	ImGui::ColorEdit4("m_lineColor", m_lineColor.GetColor_Float());

	if (m_sound != nullptr)
	{
		m_sound->ImGui_Setting();
	}
	ImGui::Unindent();
}

void Sniper::Init()
{
	kind = COMPONENT_KIND::SNIPER;
	name = "Sniper";

	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	m_sound = new Audio();
	m_sound->Load_Audio_From_File("asset/Sound/gun_fire3.wav");
	m_sound->Set_Volume(3.f);
	m_sound->Set_Name("Revolver_Sound");//Need Find sniper sound

	m_sprite_comp->m_AnimationAcitve = false;

	m_damage = WeaponInfo::Weapon_Damagelist.at(static_cast<WeaponInfo::WeaponList>(kind));
	m_magazine = WeaponInfo::Weapon_Magazine.at(static_cast<WeaponInfo::WeaponList>(kind));
}

void Sniper::Gun_Update(float dt)
{
	m_innertime += dt;
	if (m_player_ptr&&m_innertime < m_shotspeed)
	{
		float radian_current_angle = To_RADIAN(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);

		vector2 return_pos{};
		return_pos.x = (cos(radian_current_angle) - cos(radian_current_angle)*m_innertime / m_shotspeed)*8.f;
		return_pos.y = (sin(radian_current_angle) - sin(radian_current_angle)*m_innertime / m_shotspeed)*8.f;
		if (m_sprite_comp->GetFlipX() == false)
		{
			return_pos *= -1.f;
		}
		m_owner->GetTransform()->SetPosition(return_pos);
	}
	else if (m_player_ptr)
	{
		float radian_current_angle = To_RADIAN(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);

		m_angle_vector = { cos(radian_current_angle),sin(radian_current_angle) };
		m_line_end_point = m_owner->GetTransform()->GetWorldPosition() + m_angle_vector * m_max_range*100.f;
		APP->Get_Graphic()->DrawLine(m_owner->GetTransform()->GetWorldPosition() + m_angle_vector * 8.f, m_line_end_point, m_lineColor);
	}

	if (m_should_reload)
	{
		if (m_innertime > m_reloadtime)
		{
			m_should_reload = false;
		}
	}
}

void Sniper::Close()
{
	if (m_sound)
	{
		m_sound->Set_Dead(true);
	}
}

vector2 Sniper::Fire(vector2 fire)
{
	if (m_should_reload && Magnitude_Squared(fire) == 0.0f)
	{
		return vector2();
	}
	if (m_innertime < m_shotspeed && Magnitude_Squared(fire) == 0.0f)
	{
		return vector2();
	}
	m_sprite_comp->m_oneloop_animation = true;
	m_sound->Play_Audio(false);
	m_innertime = 0.0f;

	vector2 fire_dir = fire;
	vector2 bulletshell_dir = fire;
	if (Magnitude_Squared(fire) == 0.0f)
	{
		float fire_angle = m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle;
		fire_dir = GetVectorByDegree(fire_angle);
		bulletshell_dir = GetVectorByDegree(fire_angle + 180);
	}

	Object* temp = new Object(m_owner->GetState(), "temporary");
	temp->GetTransform()->SetScale(vector2(12.0f, 3.6f));
	//Need calibrate for initial position 
	temp->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition()); //initial correction position for bullet

	Sprite* sprite_comp = dynamic_cast<Sprite*>(temp->Add_Component(new Sprite(temp)));
	sprite_comp->SetTextureName("Revolver_Bullet");
	sprite_comp->SetColorA(.8f);
	sprite_comp->Init();
	ParticlePhysics* physics_comp = dynamic_cast<ParticlePhysics*>(temp->Add_Component(new ParticlePhysics(temp)));

	Object* Smoke = new Object(m_owner->GetState());
	LoadArchetypeinfo(Smoke, "Snipe_Particle");
	Smoke->GetTransform()->SetDepth(-0.91f);
	Smoke->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition() + vector2(0, 2.f));
	Smoke->Add_Component(new Physics(Smoke));
	Smoke->GetComponentByTemplate<Physics>()->SetGravityOn(false);
	Smoke->GetComponentByTemplate<Physics>()->Set_Ignore_Tile(true);
	Smoke->GetComponentByTemplate<Physics>()->SetMaxFallingSpeed(-3000.f);
	Smoke->GetComponentByTemplate<Physics>()->SetSpeed(vector2(fire_dir.x, fire_dir.y)*m_bulletspeed/1.8f);
	Smoke->GetComponentByTemplate<Physics>()->Init();
	m_owner->GetState()->AddRegisterObject(Smoke);

	Object* flash = new Object(m_owner->GetState(), "flash");
	flash->GetTransform()->SetScale(vector2(36.f, 36.f));
	flash->GetTransform()->SetDepth(-0.91f);

	Object* bulletshell = new Object(m_owner->GetState(), "bulletshell");
	bulletshell->GetTransform()->SetScale(vector2(5.f, 1.5f));
	bulletshell->GetTransform()->SetDepth(-0.91f);
	bulletshell->SetLifetime(5.f);
	Sprite* sprite_bulletshell = dynamic_cast<Sprite*>(bulletshell->Add_Component(new Sprite(bulletshell)));
	sprite_bulletshell->SetTextureName("bulletshell");
	sprite_bulletshell->SetColorA(1.f);
	sprite_bulletshell->Init();
	Physics* bulletshell_physics_comp = dynamic_cast<Physics*>(bulletshell->Add_Component(new Physics(bulletshell)));
	bulletshell_physics_comp->Init();
	Collision* bulletshell_collision_comp = dynamic_cast<Collision*>(bulletshell->Add_Component(new Collision(bulletshell)));
	bulletshell_collision_comp->SetCollisionGroup(COLLISION_GROUP::ITEM);
	bulletshell_collision_comp->Init();

	if (this->GetOwner()->GetComponentByTemplate<Sprite>()->GetFlipX())
	{
		//Need calibrate for initial position 
		flash->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition() - vector2(fire_dir.x, -fire_dir.y)*6.f);
		bulletshell->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition());
		bulletshell_physics_comp->SetSpeed(vector2(-bulletshell_dir.x, bulletshell_dir.y)*50.f);

		physics_comp->setVelocity(vector2(fire_dir.x, fire_dir.y)*m_bulletspeed);
		temp->GetTransform()->SetDegree(-Angle_Degree(fire_dir));
		m_owner->GetTransform()->SetPosition(-vector2(fire_dir.x*8.f, -fire_dir.y*8.f));
	}
	if (!this->GetOwner()->GetComponentByTemplate<Sprite>()->GetFlipX())
	{
		flash->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition() + vector2(fire_dir.x, fire_dir.y)*6.f);
		bulletshell->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition());
		bulletshell_physics_comp->SetSpeed(vector2(bulletshell_dir.x, bulletshell_dir.y)*50.f);

		physics_comp->setVelocity(vector2(fire_dir.x, fire_dir.y)*m_bulletspeed);
		temp->GetTransform()->SetDegree(Angle_Degree(fire_dir));
		m_owner->GetTransform()->SetPosition(vector2(-fire_dir.x*8.f, -fire_dir.y*8.f));
	}
	Sprite* sprite_flash = dynamic_cast<Sprite*>(flash->Add_Component(new Sprite(flash)));
	sprite_flash->SetTextureName("muzzleFlash");
	sprite_flash->SetColorA(.5f);
	sprite_flash->Init();
	flash->SetLifetime(0.1f);
	m_owner->GetState()->AddRegisterObject(flash);
	m_owner->GetState()->AddRegisterObject(bulletshell);

	sprite_comp->SetFlipX(m_sprite_comp->GetFlipX());
	sprite_bulletshell->SetFlipX(m_sprite_comp->GetFlipY());
	physics_comp->Init();
	temp->SetLifetime(2.0f);
	m_owner->GetState()->AddRegisterObject(temp);
	--m_magazine;

	//Judge Player dies or not
	auto object_list = STATE->Get_CurrentState()->GetPlayerlist();
	for (auto i = object_list.begin(); i != object_list.end(); ++i)
	{
		if (*i != m_player_ptr)
		{
			if (CollisionHelper::AABBandLineColision(*(*i)->GetComponentByTemplate<Collision>(), m_owner->GetTransform()->GetWorldPosition(), m_line_end_point))
			{
				Player* target_player = (*i)->GetComponentByTemplate<Player>();
				if (target_player->CanPlayerDamaged())
				{
					Player* origin_player = m_player_ptr->GetComponentByTemplate<Player>();
					m_owner->GetState()->GetBadge()->AddKilllog(origin_player->GetPlayerNum(), target_player->GetPlayerNum());
					origin_player->IncrementKill();
					float dir = -m_owner->GetTransform()->GetWorldPosition().x + (*i)->GetTransform()->GetWorldPosition().x;
					target_player->DamagePlayer(m_damage,dir);

					int current_health = target_player->GetHealth();
					int damage_taken = (m_damage > current_health) ? current_health : m_damage;
					origin_player->Add_Damage(damage_taken);
				}
			}
		}
	}


	if (m_magazine < 1)
	{
		m_owner->SetLifetime(0.0f);
		//m_magazine = m_max_magazine;
		//m_innertime = 0.0f;
		//m_should_reload = true;
		//Object* reload_text = new Object(this->GetOwner()->GetState());
		//reload_text->Add_Component(new SpriteText(reload_text));
		//reload_text->GetComponentByTemplate<SpriteText>()->Init();
		//reload_text->GetComponentByTemplate<SpriteText>()->SetString("Reload!");
		//reload_text->GetTransform()->SetScale(vector2(10.0f, 10.0f));
		//reload_text->SetLifetime(m_reloadtime);
		//reload_text->AttatchToParent(this->m_owner);
		//reload_text->GetTransform()->SetPosition(vector2(0.0f, 0.0f));
		//m_owner->GetState()->AddRegisterObject(reload_text);
	}

	return fire_dir;
}