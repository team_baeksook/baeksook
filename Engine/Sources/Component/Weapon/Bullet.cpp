/***********************************************************************
	File name		: Bullet.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#include "Bullet.h"
#include "Object/Object.h"
#include "System/Logger.h"
#include "Component/Collision.h"
#include "Component/ParticlePhysics.h"
#include <iostream>
#include "Component/Player.h"
#include "Object/Tile.h"
#include "Component/Physics.h"
#include "System/State.h"


Bullet::Bullet(Object* owner_object, const std::string& component_name): Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::BULLET;
}

Bullet::~Bullet() {}

void Bullet::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&)
{
}

void Bullet::Deserialization(const rapidjson::Value&)
{
}

void Bullet::ImGui_Setting()
{
}

void Bullet::Init()
{
	if(m_collision_comp = m_owner->GetComponentByTemplate<Collision>(); m_collision_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not hold the collision component", spdlog::level::err);
	}
	if (m_physics_comp = this->m_owner->GetComponentByTemplate<Physics>(); m_physics_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have physics component!", spdlog::level::level_enum::err);
	}
	if (m_owner->GetComponentByTemplate<ParticlePhysics>() != nullptr)
	{
		m_physics_comp->Gravity = 0.f;
	}
	m_collision_comp->AddIgnoreList(this->m_owner);
	m_innertime = 0.f;
}

void Bullet::Update(float dt)
{
	m_innertime += dt;
	vector2 velo;
	if (m_owner->GetComponentByTemplate<ParticlePhysics>() != nullptr)
	{
		m_physics_comp->Gravity = 0.f;
		velo = m_owner->GetComponentByTemplate<ParticlePhysics>()->getVelocity();
		float angle = To_DEGREE(atan2(velo.y,velo.x));
		m_owner->GetTransform()->SetDegree(angle);
	}

	m_bulletSpeed = m_physics_comp->GetSpeed();
	if(m_bulletSpeed >= 0.f)
	{
		m_IsCollidingWithWall = m_physics_comp->IsWallBlocked(m_bulletSpeed, RIGHT);
	}
	else
	{
		m_IsCollidingWithWall = m_physics_comp->IsWallBlocked(m_bulletSpeed, LEFT);
	}

	m_IsCollidingWithFloor = m_physics_comp->HasGround(m_bulletSpeed);

	if (m_NotDie)
	{
		return;
	}

	if (m_IsCollidingWithFloor)
	{
		//this->m_owner->MakeDead();
	}

	if(m_IsCollidingWithWall&&!m_IsNotDieAtWall&&m_innertime>0.032f)
	{
		this->m_owner->MakeDead();
	}
	
	auto collidingobj = m_collision_comp->GetCollidingObjects();
	for(auto collision : collidingobj)
	{
		if(Player* player = collision->GetComponentByTemplate<Player>(); player != nullptr)
		{
			if(player->CanPlayerDamaged())
			{
				float dir = collision->GetTransform()->GetWorldPosition().x - m_owner->GetTransform()->GetWorldPosition().x;

				if (Player* origin_player = m_origin->GetComponentByTemplate<Player>(); origin_player != nullptr)
				{
					int current_health = player->GetHealth();
					int damage_taken = (m_damage > current_health) ? current_health : m_damage;
					origin_player->Add_Damage(damage_taken);
					if (player->DamagePlayer(m_damage, dir))
					{
						m_owner->GetState()->GetBadge()->AddKilllog(origin_player->GetPlayerNum(), player->GetPlayerNum());
						origin_player->IncrementKill();
					}
				}
			}
		}
	}

	if(!m_collision_comp->GetCollidingObjects().empty())
	{
		this->m_owner->MakeDead();
		m_owner->GetComponentByTemplate<Sprite>()->SetColorA(0.f);
		//put this component to bullet
		//when it collides with player occur event to player
	}
}

void Bullet::Close()
{
}

void Bullet::SetOrigin(Object* obj)
{
	m_origin = obj;
	m_collision_comp->AddIgnoreList(obj);
}

void Bullet::SetOrigin(Object * obj, bool is_ignored)
{
	m_origin = obj;
	if (!is_ignored)
	{
		m_collision_comp->AddIgnoreList(obj);
	}
}

void Bullet::SetNotDieAtWall(bool TF)
{
	m_IsNotDieAtWall = TF;
}

void Bullet::SetDamage(int damage)
{
	m_damage = damage;
}

void Bullet::SetNotDie()
{
	m_NotDie = true;
}

bool Bullet::IsCollide() const
{
	return (m_IsCollidingWithFloor || m_IsCollidingWithWall);
}

Player* Bullet::GetCollidingplayer()
{
	auto collidingobj = m_collision_comp->GetCollidingObjects();
	for (auto collision : collidingobj)
	{
		if (Player* player = collision->GetComponentByTemplate<Player>(); player != nullptr)
		{
			return player;
		}
	}

	return nullptr;
}
