#include "FlameThrower.h"
#include "Object/Object.h"
#include "Component/Sprite.h"
#include "Component/ParticlePhysics.h"
#include "System/State.h"
#include "System/Logger.h"
#include "Component/SpriteText.h"
#include "Component/Collision.h"
#include "Bullet.h"
#include "Graphic/Particle.h"
#include <IMGUI/imgui.h>
#include <iostream>
#include "FireBullet.h"

FlameThrower::FlameThrower(Object* owner_object, const std::string& component_name) : Gun(owner_object, component_name)
{
	kind = COMPONENT_KIND::FLAMETHROWER;
}

void FlameThrower::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void FlameThrower::Deserialization(const rapidjson::Value&)
{
}

void FlameThrower::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::InputFloat("m_shotspeed", &m_shotspeed);
	ImGui::InputFloat("m_bulletspeed", &m_bulletspeed);
	ImGui::InputInt("m_magazine", &m_magazine);
	ImGui::Checkbox("m_reload", &m_should_reload);
	if (m_sound != nullptr)
	{
		m_sound->ImGui_Setting();
	}
	ImGui::Unindent();
}

void FlameThrower::Init()
{
	kind = COMPONENT_KIND::FLAMETHROWER;
	name = "FlameThrower";

	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	//m_sound = new Audio();
	//m_sound->Load_Audio_From_File("asset/Sound/gun_fire3.wav");
	//m_sound->Set_Volume(3.f);
	//m_sound->Set_Name("Revolver_Sound");

	m_sprite_comp->m_AnimationAcitve = false;
}

void FlameThrower::Gun_Update(float dt)
{
	m_innertime += dt;
	if (m_player_ptr&&m_innertime < m_shotspeed)
	{
		float radian_current_angle = To_RADIAN(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);
		vector2 return_pos{};
		return_pos.x = (cos(radian_current_angle) - cos(radian_current_angle)*m_innertime / m_shotspeed)*5.f;
		return_pos.y = -2.f - (sin(radian_current_angle) - sin(radian_current_angle)*m_innertime / m_shotspeed)*5.f;
		if (m_sprite_comp->GetFlipX() == false)
		{
			return_pos.x *= -1.f;
		}
		m_owner->GetTransform()->SetPosition(return_pos);
	}

	if (m_should_reload)
	{
		if (m_innertime > m_reloadtime)
		{
			m_should_reload = false;
		}
	}
}

void FlameThrower::Close()
{
	if (m_sound)
	{
		m_sound->Set_Dead(true);
	}
}

vector2 FlameThrower::Fire(vector2 fire)
{
	if (m_should_reload && Magnitude_Squared(fire) == 0.0f)
	{
		return vector2();
	}
	//m_sound.Play_Audio(false);
	m_innertime = 0.0f;
	m_origin = this->m_owner->GetParent()->GetParent();
	
	Object* bullet = new Object(m_owner->GetState(), "temporary");
	bullet->GetTransform()->SetScale(vector2(4.f, 4.f));
	bullet->GetTransform()->SetPosition(this->GetOwner()->GetTransform()->GetWorldPosition() + vector2(0, 2.f));  //initial correction position for bullet
	
	bullet->Add_Component(new Physics(bullet));
	bullet->GetComponentByTemplate<Physics>()->Init();

	bullet->Add_Component(new Collision(bullet));
	bullet->GetComponentByTemplate<Collision>()->SetCollisionGroup(COLLISION_GROUP::BULLET);

	bullet->Add_Component(new FireBullet(bullet));
	bullet->GetComponentByTemplate<FireBullet>()->Init();
	bullet->GetComponentByTemplate<FireBullet>()->SetOrigin(m_origin);
	bullet->GetComponentByTemplate<FireBullet>()->SetDamage(5.f);

	Sprite* sprite_comp = dynamic_cast<Sprite*>(bullet->Add_Component(new Sprite(bullet)));
	sprite_comp->SetTextureName("FlameBall");
	sprite_comp->Init();
	ParticlePhysics* particle_physics_comp = dynamic_cast<ParticlePhysics*>(bullet->Add_Component(new ParticlePhysics(bullet)));

	vector2 velo = fire;
	if (Magnitude_Squared(fire) == 0.0f)
	{
		velo.x = cos(To_RADIAN(m_origin->GetComponentByTemplate<Player>()->m_weapon_angle));
		velo.y = sin(To_RADIAN(m_origin->GetComponentByTemplate<Player>()->m_weapon_angle));
	}

	particle_physics_comp->setMass(200.0f);
	particle_physics_comp->setVelocity(velo*m_bulletspeed);
	particle_physics_comp->setAcceleration(0.0f, -3.f); //TODO::Gravity?
	particle_physics_comp->Init();

	bullet->SetLifetime(10.f);
	m_owner->GetState()->AddRegisterObject(bullet);

	--m_magazine;
	if (m_magazine < 1)
	{
		m_owner->SetLifetime(0.0f);
		return velo;
	}

	//Animation
	m_sprite_comp->m_oneloop_animation = false;
	m_sprite_comp->SetCurrentFrame(0);

	m_should_reload = true;

	return velo;
}
