/***********************************************************************
File name		: LMG.h
Project name	: WFPT
Author			: Hyun Seok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component/Component.h"
#include "Gun.h"
#include "Component/Sprite.h"
#include "System/Audio.h"

class LightMachineGun : public Gun
{
private:
	Sprite* m_sprite_comp = nullptr;
	Audio* m_sound = nullptr;

	float m_innertime = 0.0f;
	float m_shotspeed = 0.15f;
	float m_bulletspeed = 300.0f;

	const int m_max_magazine = 25;
	int m_magazine = m_max_magazine;
	bool m_should_reload = false;
	const float m_reloadtime = 3.0f;
public:
	LightMachineGun(Object* owner_object, const std::string& component_name = "LMG");
	~LightMachineGun() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Gun_Update(float dt) override;
	void Close() override;
	vector2 Fire(vector2 fire) override;
};

