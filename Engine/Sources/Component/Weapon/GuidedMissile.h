/***********************************************************************
File name		: Tomahawk.h
Project name	: WFPT
Author			: HyunSeok Kim, SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component/Weapon/Gun.h"
#include "Component/Sprite.h"
#include "System/Audio.h"

class GuidedMissile : public Gun
{
private:
	Sprite* m_sprite_comp = nullptr;

	float m_innertime = 0.0f;
	float m_shotspeed = 0.5f;
	float m_bulletspeed = 2.0f;

	const int m_max_magazine = 12;
	int m_magazine = m_max_magazine;
	bool m_should_reload = false;
	const float m_reloadtime = 3.0f;

	Collision* m_collision_comp = nullptr;

	Object* m_origin = nullptr;
	vector2* m_target_Player_pos=nullptr;

	Audio* m_sound;
public:
	GuidedMissile(Object* owner_object, const std::string& component_name = "GuidedMissile");
	~GuidedMissile() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Gun_Update(float dt) override;
	void Close() override;
	vector2 Fire(vector2 fire);
};
