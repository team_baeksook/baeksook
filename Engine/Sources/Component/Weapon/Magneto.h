#pragma once

#include "Gun.h"

class Sprite;

class Magneto : public Gun
{
public:
	Magneto(Object* owner_object, const std::string& component_name = "Magneto");
	~Magneto() override;

	virtual void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	virtual void Deserialization(const rapidjson::Value &) override;
	virtual void ImGui_Setting() override;
	virtual void Init() override;
	virtual void Update(float dt) override;
	virtual void Close() override;

	// Inherited via Gun
	virtual void Gun_Update(float) override;
	virtual vector2 Fire(vector2 fire = vector2()) override;
private:
	Sprite* m_sprite_comp = nullptr;
	Audio* m_sound = nullptr;

	Object* m_bullet = nullptr;

	int m_magazine = 0;
};