/***********************************************************************
	File name		: Revolver.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component/Component.h"
#include "Gun.h"
#include "Component/Sprite.h"
#include "System/Audio.h"

class Pistol : public Gun
{
private:
	Sprite* m_sprite_comp = nullptr;
	Audio* m_sound = nullptr;

	float m_innertime = 0.5f;
	float m_shotspeed = 0.5f;
	float m_bulletspeed = 360.0f;

	const int m_max_magazine = 6;
	int m_magazine = m_max_magazine;
	bool m_should_reload = false;
	const float m_reloadtime = 3.0f;
public:
	Pistol(Object* owner_object, const std::string& component_name = "Pistol");
	~Pistol() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Gun_Update(float dt) override;
	void Close() override;
	vector2 Fire(vector2 fire) override;
};

