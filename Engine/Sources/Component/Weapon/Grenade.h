/***********************************************************************
	File name		: Revolver.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Bomb.h"
#include "Component/Sprite.h"
#include "System/Audio.h"
#include "Gun.h"
#include "Component/Collision.h"
#include "Component/Physics.h"
#include "Component/ParticlePhysics.h"


class Grenade : public Gun
{
private:
	Sprite*		m_sprite_comp	= nullptr;
	Physics*	m_physics		= nullptr;
	Collision*	m_collision		= nullptr;
	Object*		m_origin		= nullptr;
	ParticlePhysics* physics_comp = nullptr;
	Audio*		m_sound			=nullptr;

	bool	m_isReadyToBlow = false;
	bool	m_isThrown		= false;
	bool	m_isOnGround	= false;
	float	m_countdown		= 1.0f;

public:
	Grenade(Object* owner_object, const std::string& component_name = "Grenade");
	~Grenade() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;
	vector2 Fire(vector2 fire) override;
	void Explode();
	void Gun_Update(float) override;
};




