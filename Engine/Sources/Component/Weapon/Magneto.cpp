#include "Magneto.h"
#include "Component/Sprite.h"
#include "System/Logger.h"
#include "Component/Weapon/Bullet.h"
#include "System/State.h"
#include "System/StateManager.h"
#include "System/Input.h"
#include <IMGUI/imgui.h>
#include "Component/ParticlePhysics.h"

Magneto::Magneto(Object * owner_object, const std::string & component_name) : Gun(owner_object, component_name)
{
	kind = COMPONENT_KIND::MAGNETO;
}

Magneto::~Magneto()
{
}

void Magneto::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Magneto::Deserialization(const rapidjson::Value &)
{
}

void Magneto::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::InputInt("m_magazine", &m_magazine);
	if (m_sound != nullptr)
	{
		m_sound->ImGui_Setting();
	}
	ImGui::Unindent();
}

void Magneto::Init()
{
	kind = COMPONENT_KIND::MAGNETO;
	name = "Magneto";
	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	m_sound = new Audio();
	m_sound->Load_Audio_From_File("asset/Sound/gun_fire.wav");
	m_sound->Set_Volume(0.8f);
	m_sound->Set_Name("Rifle_Sound");

	m_damage = WeaponInfo::Weapon_Damagelist.at(static_cast<WeaponInfo::WeaponList>(kind));
	m_magazine = WeaponInfo::Weapon_Magazine.at(static_cast<WeaponInfo::WeaponList>(kind));
}

void Magneto::Update(float dt)
{
	if (m_bullet)
	{
		vector2 diff;
		if (int playernum = m_player_ptr->GetComponentByTemplate<Player>()->GetPlayerNum(); playernum < 1)
		{
			vector2 pos = STATE->Get_CurrentState()->GetCamera(0)->GetLocalPosition(Input::Get_Mouse_Position());
			vector2 current_pos = m_bullet->GetTransform()->GetWorldPosition();
			diff = pos - current_pos;
		}
		else
		{
			diff = Input::Get_Right_Stick_Position(playernum);
		}

		Bullet* bullet_comp = m_bullet->GetComponentByTemplate<Bullet>();

		if (bullet_comp->IsCollide())
		{
			m_bullet->MakeDead();
			m_owner->MakeDead();
		}
		
		Player* player = bullet_comp->GetCollidingplayer();
		if(player != nullptr)
		{
			if (player->CanPlayerDamaged())
			{
				float dir = player->GetOwner()->GetTransform()->GetWorldPosition().x - m_bullet->GetTransform()->GetWorldPosition().x;

				if (Player* player_comp = m_player_ptr->GetComponentByTemplate<Player>(); player_comp != nullptr)
				{
					int current_health = player->GetHealth();
					int damage_taken = (m_damage > current_health) ? current_health : m_damage;
					player_comp->Add_Damage(damage_taken);

					if (player->DamagePlayer(m_damage, dir))
					{
						m_owner->GetState()->GetBadge()->AddKilllog(player_comp->GetPlayerNum(), player->GetPlayerNum());
						player_comp->IncrementKill();
					}
				}
			}
			m_bullet->MakeDead();
			m_owner->MakeDead();
		}

		m_bullet->GetComponentByDynamicCasting<ParticlePhysics>()->setVelocity(Normalizing(diff) * dt * 5000.0f);
	}
}

void Magneto::Close()
{

}

void Magneto::Gun_Update(float)
{

}

vector2 Magneto::Fire(vector2 /*fire*/)
{
	if (m_bullet == nullptr)
	{
		m_bullet = new Object(m_owner->GetState(), "Magnetobullet");
		m_bullet->GetTransform()->SetScale(vector2(5.0f, 2.5f));
		m_bullet->GetTransform()->SetPosition(this->GetOwner()->GetTransform()->GetWorldPosition() + vector2(0, 2.f));
		Physics* physics_comp = dynamic_cast<Physics*>(m_bullet->Add_Component(new Physics(m_bullet)));
		physics_comp->Init();
		physics_comp->SetGravityOn(false);
		m_bullet->Add_Component(new Collision(m_bullet));
		m_bullet->GetComponentByTemplate<Collision>()->SetCollisionGroup(COLLISION_GROUP::BULLET);
		m_bullet->GetComponentByTemplate<Collision>()->Init();
		Bullet* bulletcomp = dynamic_cast<Bullet*>(m_bullet->Add_Component(new Bullet(m_bullet)));
		bulletcomp->Init();
		bulletcomp->SetOrigin(m_player_ptr);
		bulletcomp->SetDamage(m_damage);
		bulletcomp->SetNotDie();
		Sprite* sprite_comp = dynamic_cast<Sprite*>(m_bullet->Add_Component(new Sprite(m_bullet)));
		sprite_comp->SetTextureName("Revolver_Bullet");
		sprite_comp->Init();
		m_bullet->Add_Component(new ParticlePhysics(m_bullet));
		m_owner->GetState()->AddRegisterObject(m_bullet);
	}

	return vector2();
}
