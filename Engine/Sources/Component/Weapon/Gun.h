/***********************************************************************
	File name		: Gun.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include "Component/Component.h"
#include "Object/Object.h"
#include "Component/Player.h"
#include "Weapon.h"

class Gun : public Weapon
{
public:
	Gun(Object* owner_object, const std::string& component_name = "Gun") : Weapon(owner_object, component_name) {}
	virtual ~Gun() override {};

	virtual void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override = 0;
	virtual void Deserialization(const rapidjson::Value&) override = 0;
	virtual void ImGui_Setting() override = 0;
	virtual void Init() override = 0;
	void Update(float dt) override
	{
		if (m_player_ptr)
		{
			Angle_Update();
		}
		Gun_Update(dt);
	};
	virtual void Gun_Update(float /*dt*/)=0;
	virtual void Close() override = 0;
	virtual vector2 Fire(vector2 fire = vector2()) = 0;

	void Angle_Update() const
	{
		//TODO::How can we apply the weapon's angle? How can we keep the gun component as pure_virtual?
		if (m_player_ptr)
		{
			m_owner->GetTransform()->SetDegree(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);
		}
	};
};

