/***********************************************************************
File name		: Tomahawk.h
Project name	: WFPT
Author			: HyunSeok Kim, SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component/Weapon/Gun.h"
#include "Component/Sprite.h"
#include "System/Audio.h"
#include "ChargeWeapon.h"

class Tomahawk : public ChargeWeapon
{
private:
	Sprite*		m_sprite_comp		= nullptr;
	Collision*	m_collision_comp	= nullptr;
	Object*		m_origin			= nullptr;
	Audio*		m_throwsound		= nullptr;

	float m_innertime = 0.0f;
	float m_shotspeed = 0.5f;
	float m_bulletspeed = 800.0f;
	float m_updateTime = 0.f;

	const float m_reloadtime = 3.0f;
	const int m_max_magazine = 6;
	int m_magazine = m_max_magazine;

	bool m_should_reload = false;
	bool m_isThrown = false;

	float m_scalevel = 80.f;
public:
	Tomahawk(Object* owner_object, const std::string& component_name = "Tomahawk");
	~Tomahawk() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void ChargeEnergy(float dt) override;
	void Close() override;
	bool Fire() override;
};
