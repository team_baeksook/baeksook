#include "Laser.h"

#include "System/StateManager.h"
#include "System/State.h"
#include "Component/Player.h"
#include "Object/Object.h"
#include "System/Logger.h"

#include <iostream>

Laser::Laser(Object* owner_object, const std::string& component_name): ChargeWeapon(owner_object, component_name)
{
	kind = COMPONENT_KIND::LASER;
}

Laser::~Laser() {}

void Laser::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Laser::Deserialization(const rapidjson::Value&)
{
}

void Laser::ImGui_Setting()
{
}

void Laser::Init()
{
	kind = COMPONENT_KIND::LASER;
	name = "Laser";
	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}

	m_damage = WeaponInfo::Weapon_Damagelist.at(static_cast<WeaponInfo::WeaponList>(kind));
	m_magazine = WeaponInfo::Weapon_Magazine.at(static_cast<WeaponInfo::WeaponList>(kind));
}

void Laser::Update(float dt)
{
	float deg = this->m_owner->GetTransform()->GetWorldDegree();
	vector2 localpos = m_owner->GetState()->GetCamera()->GetCameraPos(m_owner->GetTransform()->GetWorldPosition());

	if (m_ChargedEnergy != 0.0f || m_isfire)
	{
		m_owner->GetState()->GetGraphicSystem()->DrawLaser(localpos + 30.0f * GetVectorByDegree(deg) + 5.0f * GetVectorByDegree(deg + 90.0f), deg, m_isfire);
	}

	if (m_owner->HasParent())
	{
		this->m_owner->GetTransform()->SetDegree(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);
	}

	if (m_ChargedEnergy != 0.0f)
	{
		m_sprite_comp->SetTextureName("Unicorn_firing");
	}
	else
	{
		m_sprite_comp->SetTextureName("Uincorn_normal");
	}

	if (m_isfire)
	{
		auto object_list = STATE->Get_CurrentState()->GetPlayerlist();
		for (auto i = object_list.begin(); i != object_list.end(); ++i)
		{
			if (*i != m_player_ptr)
			{
				vector2 line_end_point = m_owner->GetTransform()->GetWorldPosition() + GetVectorByDegree(deg) * 2000.0f;
				if (CollisionHelper::AABBandwideLineCollision(*(*i)->GetComponentByTemplate<Collision>(), m_owner->GetTransform()->GetWorldPosition(), line_end_point, 2.f))
				{
					Player* player_comp = (*i)->GetComponentByTemplate<Player>();
					if (player_comp->CanPlayerDamaged())
					{
						Player* owner_player = m_player_ptr->GetComponentByTemplate<Player>();
						int current_health = player_comp->GetHealth();
						int damage_taken = (m_damage > current_health) ? current_health : m_damage;
						owner_player->Add_Damage(damage_taken);
						if (player_comp->DamagePlayer(m_damage, 0.0f))
						{
							m_owner->GetState()->GetBadge()->AddKilllog(owner_player->GetPlayerNum(), player_comp->GetPlayerNum());
							owner_player->IncrementKill();
						}
					}
				}
			}
		}
		m_fire_time += dt;
		if (m_fire_time > m_magazine)
		{
			m_fire_time = 0.0f;
			m_ChargedEnergy = 0.0f;
			m_isfire = false;
			m_owner->SetLifetime(0.0f);
		}
	}
}

void Laser::ChargeEnergy(float dt)
{
	m_ChargedEnergy += dt;

	if (m_ChargedEnergy > 2.f)
	{
		m_isfire = true;

		m_ChargedEnergy = 2.0f;
	}
}

void Laser::Close()
{
	
}

bool Laser::Fire()
{
	if (m_ChargedEnergy >= 2.f)
	{
		m_fire_time = 0.0f;
		m_ChargedEnergy = 0.0f;
		m_isfire = false;

		return true;
	}
	else
	{
		m_isfire = false;
		m_ChargedEnergy = 0.f;
	}
	return false;
}
