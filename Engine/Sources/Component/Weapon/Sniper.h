/***********************************************************************
	File name		: Revolver.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component/Component.h"
#include "Gun.h"
#include "Component/Sprite.h"
#include "System/Audio.h"

class Sniper : public Gun
{
private:
	Sprite* m_sprite_comp = nullptr;
	Audio* m_sound = nullptr;

	vector2 m_angle_vector;
	vector2 m_line_end_point;
	float m_max_range=200.f;
	Color m_lineColor=Color(255,0,0);

	float m_innertime = 2.0f;
	float m_shotspeed = 2.f;
	float m_bulletspeed = 3000.f;

	const int m_max_magazine = 200;
	int m_magazine = m_max_magazine;
	bool m_should_reload = false;
	const float m_reloadtime = 3.0f;
public:
	Sniper(Object* owner_object, const std::string& component_name = "Sniper");
	~Sniper() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Gun_Update(float dt) override;
	void Close() override;
	vector2 Fire(vector2 fire) override;
};