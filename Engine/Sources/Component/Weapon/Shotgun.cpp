/***********************************************************************
File name		: Tomahawk.cpp
Project name	: WFPT
Author			: HyunSeok Kim, SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
//#include "Shotgun.h"
#include <Component/Weapon/Shotgun.h>
#include "Object/Object.h"
#include "Component/Sprite.h"
#include "Component/ParticlePhysics.h"
#include "System/State.h"
#include "System/Logger.h"
#include "System/SoundManager.h"
#include "Component/Collision.h"
#include "Component/Weapon/Bullet.h"
#include <IMGUI/imgui.h>


Shotgun::Shotgun(class Object* owner_object, const std::string& component_name) : Gun(owner_object, component_name)
{
	kind = COMPONENT_KIND::SHOTGUN;
}

Shotgun::~Shotgun() {}

void Shotgun::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Shotgun::Deserialization(const rapidjson::Value&)
{
}

void Shotgun::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::InputFloat("m_shotspeed", &m_shotspeed);
	ImGui::InputFloat("m_bulletspeed", &m_bulletspeed);
	ImGui::InputInt("m_magazine", &m_magazine);
	ImGui::Checkbox("m_reload", &m_should_reload);
	m_sound->ImGui_Setting();
	ImGui::Unindent();
}

void Shotgun::Init()
{
	kind = COMPONENT_KIND::SHOTGUN;
	name = "Shotgun";
	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	m_sound = new Audio();
	m_sound->Load_Audio_From_File("asset/Sound/gun_fire.wav");
	m_sound->Set_Volume(0.8f);
	m_sound->Set_Name("Rifle_Sound");
	m_sprite_comp->m_AnimationAcitve = false;

	m_damage = WeaponInfo::Weapon_Damagelist.at(static_cast<WeaponInfo::WeaponList>(kind));
	m_magazine = WeaponInfo::Weapon_Magazine.at(static_cast<WeaponInfo::WeaponList>(kind));
}

void Shotgun::Gun_Update(float dt)
{
	m_innertime += dt;

	if (m_player_ptr && m_innertime < m_shotspeed)
	{
		float radian_current_angle = To_RADIAN(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);
		vector2 return_pos{}; 
		return_pos.x = (cos(radian_current_angle) - cos(radian_current_angle)*m_innertime / m_shotspeed)*3.f;
		return_pos.y = -2.f - (sin(radian_current_angle) - sin(radian_current_angle)*m_innertime / m_shotspeed)*3.f;
		if (m_sprite_comp->GetFlipX() == false)
		{
			return_pos.x *= -1.f;
		}
		m_owner->GetTransform()->SetPosition(return_pos);
	}
	if (m_should_reload)
	{
		if (m_innertime > m_reloadtime)
		{
			m_should_reload = false;
		}
	}

}

void Shotgun::Close()
{
	if (m_sound)
	{
		m_sound->Set_Dead(true);
	}
}

vector2 Shotgun::Fire(vector2 fire)
{
	if (m_should_reload && Magnitude_Squared(fire) == 0.0f)
	{
		return vector2();
	}
	if (m_innertime < m_shotspeed && Magnitude_Squared(fire) == 0.0f)
	{
		return vector2();
	}
	m_sprite_comp->m_oneloop_animation = true;
	m_sound->Play_Audio(false);
	m_innertime = 0.0f;

	vector2 fire_dir = fire;
	float fire_angle = Angle_Degree(fire);
	vector2 bulletshell_dir = fire;
	if (Magnitude_Squared(fire) == 0.0f)
	{
		fire_angle = m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle + GetRandomNumber(-3.f, 3.f);
		fire_dir = GetVectorByDegree(fire_angle);
		bulletshell_dir = GetVectorByDegree(fire_angle + 180);
	}

	Object* bulletshell = new Object(m_owner->GetState(), "bulletshell");
	bulletshell->GetTransform()->SetScale(vector2(4.f, 2.f));
	bulletshell->GetTransform()->SetDepth(-0.91f);
	bulletshell->SetLifetime(10.f);
	Sprite* sprite_bulletshell = dynamic_cast<Sprite*>(bulletshell->Add_Component(new Sprite(bulletshell)));
	sprite_bulletshell->SetTextureName("shotgun_bulletshell");
	sprite_bulletshell->SetColorA(1.f);
	sprite_bulletshell->Init();
	Physics* bulletshell_physics_comp = dynamic_cast<Physics*>(bulletshell->Add_Component(new Physics(bulletshell)));
	bulletshell_physics_comp->Init();
	Collision* bulletshell_collision_comp = dynamic_cast<Collision*>(bulletshell->Add_Component(new Collision(bulletshell)));
	bulletshell_collision_comp->SetCollisionGroup(COLLISION_GROUP::ITEM);
	bulletshell_collision_comp->Init();

	for (int i = 0; i < m_bulletPerShot; ++i)
	{
		float bullet_angle = fire_angle + m_highestBulletangle-i*(m_highestBulletangle*2/m_bulletPerShot);
		float radian_bullet_angle = To_RADIAN(bullet_angle);
		Object* bullet = new Object(m_owner->GetState(), "temporary");
		bullet->GetTransform()->SetScale(vector2(5.0f, 2.5f));
		bullet->GetTransform()->SetPosition(this->GetOwner()->GetTransform()->GetWorldPosition() + vector2(0, 2.f));  //initial correction position for bullet
		bullet->Add_Component(new Physics(bullet));
		bullet->GetComponentByTemplate<Physics>()->Init();
		bullet->Add_Component(new Collision(bullet));
		bullet->GetComponentByTemplate<Collision>()->Init();
		bullet->GetComponentByTemplate<Collision>()->SetCollisionGroup(COLLISION_GROUP::BULLET);
		
		Bullet* bullet_comp = dynamic_cast<Bullet*>(bullet->Add_Component(new Bullet(bullet)));
		bullet_comp->Init();
		bullet_comp->SetOrigin(m_player_ptr);
		bullet_comp->SetDamage(m_damage);

		Sprite* sprite_comp = dynamic_cast<Sprite*>(bullet->Add_Component(new Sprite(bullet)));
		sprite_comp->SetTextureName("Revolver_Bullet");
		sprite_comp->Init();
		ParticlePhysics* physics_comp = dynamic_cast<ParticlePhysics*>(bullet->Add_Component(new ParticlePhysics(bullet)));

		if (m_sprite_comp->GetFlipX())
		{
			//physics_comp->setVelocity(vector2(-m_bulletspeed, 0.3f - i * (0.6f / 5)));
			physics_comp->setVelocity(vector2(-cos(radian_bullet_angle),sin(radian_bullet_angle))*m_bulletspeed);
			bullet->GetTransform()->SetDegree(-bullet_angle);

			sprite_comp->SetFlipX(m_sprite_comp->GetFlipX());
		}
		else
		{
			//physics_comp->setVelocity(vector2(m_bulletspeed, 0.3f - i * (0.6f / 5)));
			physics_comp->setVelocity(vector2(cos(radian_bullet_angle), sin(radian_bullet_angle))*m_bulletspeed);
			bullet->GetTransform()->SetDegree(bullet_angle);

			sprite_comp->SetFlipX(m_sprite_comp->GetFlipX());
		}
		physics_comp->Init();
		bullet->SetLifetime(0.2f);
		m_owner->GetState()->AddRegisterObject(bullet);
	}

	Object* flash = new Object(m_owner->GetState(), "flash");
	flash->GetTransform()->SetScale(vector2(12.f, 12.f));
	flash->GetTransform()->SetDepth(-0.91f);

	if (m_sprite_comp->GetFlipX())
	{
		bulletshell->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition());
		bulletshell_physics_comp->SetSpeed(vector2(-bulletshell_dir.x, bulletshell_dir.y)*50.f);

		flash->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition() - vector2(fire_dir.x, -fire_dir.y)*6.f);
		m_owner->GetTransform()->SetPosition(vector2(fire_dir.x*3.f, -fire_dir.y*3.f - 2.f));
	}
	else
	{
		bulletshell->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition());
		bulletshell_physics_comp->SetSpeed(vector2(bulletshell_dir.x, bulletshell_dir.y)*50.f);


		flash->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition() + vector2(fire_dir.x, fire_dir.y)*6.f);
		m_owner->GetTransform()->SetPosition(vector2(fire_dir.x*3.f, -fire_dir.y*3.f - 2.f));
	}
	Sprite* sprite_flash = dynamic_cast<Sprite*>(flash->Add_Component(new Sprite(flash)));
	sprite_flash->SetTextureName("muzzleFlash");
	sprite_flash->SetColorA(.5f);
	sprite_flash->Init();
	sprite_bulletshell->SetFlipX(!m_sprite_comp->GetFlipY());
	m_owner->GetState()->AddRegisterObject(bulletshell);

	flash->SetLifetime(0.04f);
	m_owner->GetState()->AddRegisterObject(flash);

	--m_magazine;
	if (m_magazine < 1)
	{
		m_owner->SetLifetime(0.0f);
	}

	return fire_dir;
}
