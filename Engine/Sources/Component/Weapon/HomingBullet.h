/***********************************************************************
	File name		: Bullet.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Bullet.h"
#include "Math/vector2.hpp"
#include "Math/vector3.hpp"

class HomingBullet : public Bullet
{
private:
	vector2* m_PlayerPositionPtr = nullptr;
	vector3  m_HeadingVector;
	float m_innertime = 0.f;
	float m_refreshtime = 0.05f;
public:
	HomingBullet(Object* owner_object, const std::string& component_name = "HomingBullet");
	~HomingBullet() override;
		
	void Update(float dt) override;
	void Close() override;

	void Set_PlayerPositionPtr(vector2*);
	void Set_HeadingVector(float angle,float speed);
};
