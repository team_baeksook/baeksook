#pragma once
#include "Component/Component.h"
#include "Gun.h"
#include "Component/Sprite.h"
#include "System/Audio.h"

class FlameThrower : public Gun
{
private:
	Sprite* m_sprite_comp = nullptr;
	Audio* m_sound = nullptr;
	Object* m_origin = nullptr;

	float m_innertime = 1.0f;
	float m_shotspeed = 0.23f;
	float m_bulletspeed = 1.2f;

	const int m_max_magazine = 30;
	int m_magazine = m_max_magazine;
	bool m_should_reload = false;
	const float m_reloadtime = 0.1f;

	const float m_highestBulletangle = 22.5f;
	//const int m_bulletPerShot = 7;

public:
	FlameThrower(Object* owner_object, const std::string& component_name = "FlameThrower");

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override; 
	void Gun_Update(float dt) override;
	void Close() override;
	vector2 Fire(vector2 fire) override;
};
