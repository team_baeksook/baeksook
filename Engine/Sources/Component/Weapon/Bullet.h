/***********************************************************************
	File name		: Bullet.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component/Component.h"
#include "Math/vector2.hpp"

class Collision;
class Physics;
class Player;

class Bullet : public Component
{
	friend class FlameThrower;
protected:
	enum Direction
	{
		LEFT = -1,
		RIGHT = 1
	};
	Collision* m_collision_comp = nullptr;
	Object* m_origin = nullptr;
	Physics* m_physics_comp = nullptr;

	bool m_IsCollidingWithWall = false;
	bool m_IsCollidingWithFloor = false;
	bool m_IsNotDieAtWall = false;
	vector2 m_bulletSpeed;

	bool m_NotDie = false;

	int m_damage = 0;
	float m_innertime = 0.f;
public:
	Bullet(Object* owner_object, const std::string& component_name = "Bullet");
	~Bullet() override;

	virtual void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	virtual void Deserialization(const rapidjson::Value&) override;
	virtual void ImGui_Setting() override;
	virtual void Init() override;
	virtual void Update(float dt) override;
	virtual void Close() override;
	virtual void SetOrigin(Object* obj);
	virtual void SetOrigin(Object* obj,bool is_ignored);

	void SetNotDieAtWall(bool TF);
	void SetDamage(int damage);
	void SetNotDie();

	bool IsCollide() const;
	Player* GetCollidingplayer();
};
