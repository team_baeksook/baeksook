/***********************************************************************
File name		: Bow.cpp
Project name	: WFPT
Author			: HyunSeok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/

#include "Object/Object.h"
#include "Component/Sprite.h"
#include "Component/ParticlePhysics.h"
#include "System/State.h"
#include "System/Logger.h"
#include "Component/Collision.h"
#include "Component/Weapon/Bullet.h"
#include <IMGUI/imgui.h>
#include "Bow.h"
#include "System/Input.h"
#include "Component/Player.h"

#include "System/SoundManager.h"
#include "Component/Player.h"

Bow::Bow(class Object* owner_object, const std::string& component_name) : ChargeWeapon(owner_object, component_name)
{
	kind = COMPONENT_KIND::BOW;
}

Bow::~Bow() {}

void Bow::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Bow::Deserialization(const rapidjson::Value&)
{
}

void Bow::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::InputFloat("m_shotspeed", &m_shotspeed);
	ImGui::InputFloat("m_bulletspeed", &m_bulletspeed);
	ImGui::InputInt("m_magazine", &m_magazine);
	ImGui::Checkbox("m_reload", &m_should_reload);
	m_sound->ImGui_Setting();
	ImGui::Unindent();
}

void Bow::Init()
{
	kind = COMPONENT_KIND::BOW;
	name = "Bow";
	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	m_sound = new Audio();
	m_sound->Load_Audio_From_File("asset/Sound/axe_throw.wav");
	m_sound->Set_Volume(10.f);
	m_sound->Set_Name("Axe_Throw_Sound");
	m_sprite_comp->m_AnimationAcitve = false;

	m_damage = WeaponInfo::Weapon_Damagelist.at(static_cast<WeaponInfo::WeaponList>(kind));
	m_magazine = WeaponInfo::Weapon_Magazine.at(static_cast<WeaponInfo::WeaponList>(kind));
}

void Bow::Update(float dt)
{
	m_innertime += dt;

	if (m_owner->HasParent())
	{
		this->m_owner->GetTransform()->SetDegree(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);
	}

	if (m_owner->HasParent() && m_innertime < m_shotspeed)
	{
		vector2 return_pos{};
		return_pos.x = 0.6f - 0.6f*m_innertime / m_shotspeed;
		return_pos.y = -0.125f;
		if (m_sprite_comp->GetFlipX() == false)
		{
			return_pos.x *= -1.f;
		}
		m_owner->GetTransform()->SetPosition(return_pos);
	}

	if (m_should_reload)
	{
		if (m_innertime > m_reloadtime)
		{
			m_should_reload = false;
		}
	}

}

void Bow::Close()
{
	if (m_sound)
	{
		m_sound->Set_Dead(true);
	}
}

void Bow::ChargeEnergy(float dt)
{
	if(m_ChargedEnergy==0.f&&!m_sprite_comp->m_AnimationAcitve)
	{
		m_sprite_comp->m_oneloop_animation = true;
	}
	if(m_sprite_comp->m_currentframe==4)
	{
		m_sprite_comp->m_oneloop_animation = false;
	}
	if(m_ChargedEnergy>=1.5f)
	{
		m_ChargedEnergy = 1.5f;
		return;
	}
	m_ChargedEnergy += dt;
}

void Bow::SetTutorial(bool TF)
{
	m_isTutorial = TF;
	m_magazine = 30;
}


bool Bow::Fire()
{
	if(m_ChargedEnergy >= 0.032f)
	{	
		if (m_should_reload)
		{
			return false;
		}
		m_sound->Play_Audio(false);
		m_innertime = 0.0f;
		m_origin = this->m_owner->GetParent()->GetParent();
		Object* bullet = new Object(m_owner->GetState(), "temporary");
		bullet->GetTransform()->SetScale(vector2(15.f, 3.75f));
		bullet->GetTransform()->SetPosition(this->GetOwner()->GetTransform()->GetWorldPosition() + vector2(0, 2.f));  //initial correction position for bullet
		bullet->Add_Component(new Physics(bullet));
		bullet->GetComponentByTemplate<Physics>()->Init();
		bullet->Add_Component(new Collision(bullet));
		if(m_isTutorial)
		{
			bullet->GetComponentByTemplate<Collision>()->SetCollisionGroup(COLLISION_GROUP::TUTORIAL);
		}
		else
		{
			bullet->GetComponentByTemplate<Collision>()->SetCollisionGroup(COLLISION_GROUP::BULLET);
		}
		bullet->Add_Component(new Bullet(bullet));

		Bullet* bullet_comp = bullet->GetComponentByTemplate<Bullet>();
		bullet_comp->Init();
		bullet_comp->SetOrigin(m_origin);
		bullet_comp->SetDamage(static_cast<int>(m_damage * m_ChargedEnergy));

		Sprite* sprite_comp = dynamic_cast<Sprite*>(bullet->Add_Component(new Sprite(bullet)));
		sprite_comp->SetTextureName("Bow_Arrow");
		sprite_comp->Init();
		ParticlePhysics* physics_comp = dynamic_cast<ParticlePhysics*>(bullet->Add_Component(new ParticlePhysics(bullet)));

		vector2 velo;
		velo.x = cos(To_RADIAN(m_origin->GetComponentByTemplate<Player>()->m_weapon_angle));
		velo.y = sin(To_RADIAN(m_origin->GetComponentByTemplate<Player>()->m_weapon_angle));

		bullet->GetTransform()->SetPosition(this->GetOwner()->GetTransform()->GetWorldPosition() + velo*5.f);

		physics_comp->setMass(200.0f);
		physics_comp->setVelocity(velo*m_ChargedEnergy*m_bulletspeed);
		physics_comp->setAcceleration(0.0f, -3.f); //TODO::Gravity?

		physics_comp->Init();
		bullet->SetLifetime(10.f);
		m_owner->GetState()->AddRegisterObject(bullet);
		
		--m_magazine;
		if (m_magazine < 1)
		{
			m_owner->SetLifetime(0.0f);
			return true;
		}

		//Animation
		m_sprite_comp->m_oneloop_animation = false;
		m_sprite_comp->SetCurrentFrame(0);

		m_ChargedEnergy = 0.0f;
		return true;
	}

	m_ChargedEnergy = 0.f;
	return false;
}
