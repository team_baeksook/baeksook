#pragma once
#include "Component/Component.h"

namespace WeaponInfo
{
	enum WeaponList {
		WEAPON_START = __LINE__,
		WEAPON_REVOLVER			= COMPONENT_KIND::REVOLVER,
		WEAPON_RIFLE			= COMPONENT_KIND::RIFLE,
		WEAPON_TOMAHAWK			= COMPONENT_KIND::TOMAHAWK,
		WEAPON_SHOTGUN			= COMPONENT_KIND::SHOTGUN,
		WEAPON_BOW				= COMPONENT_KIND::BOW,
		WEAPON_GRENADE			= COMPONENT_KIND::GRENADE,
		WEAPON_GUIDEDMISSILE	= COMPONENT_KIND::GUIDEDMISSILE,
		WEAPON_SNIPER			= COMPONENT_KIND::SNIPER,
		WEAPON_FLAMETHROWER		= COMPONENT_KIND::FLAMETHROWER,
		WEAPON_LASER			= COMPONENT_KIND::LASER,
		WEAPON_PISTOL			= COMPONENT_KIND::PISTOL,
		WEAPON_LMG				= COMPONENT_KIND::LMG,
		WEAPON_MAGNETO			= COMPONENT_KIND::MAGNETO,
	};

	const std::map<WeaponList, int> Weapon_Damagelist = {
		std::pair<WeaponList, int>(WEAPON_REVOLVER, 25),
		std::pair<WeaponList, int>(WEAPON_RIFLE, 15),
		std::pair<WeaponList, int>(WEAPON_TOMAHAWK, 50),
		std::pair<WeaponList, int>(WEAPON_SHOTGUN, 17),
		std::pair<WeaponList, int>(WEAPON_BOW, 80),
		std::pair<WeaponList, int>(WEAPON_GRENADE, 10),
		std::pair<WeaponList, int>(WEAPON_GUIDEDMISSILE, 0),
		std::pair<WeaponList, int>(WEAPON_SNIPER, 100),
		std::pair<WeaponList, int>(WEAPON_FLAMETHROWER, 0),
		std::pair<WeaponList, int>(WEAPON_LASER, 2),
		std::pair<WeaponList, int>(WEAPON_PISTOL, 8.0f),
		std::pair<WeaponList, int>(WEAPON_LMG, 10.0f),
		std::pair<WeaponList, int>(WEAPON_MAGNETO, 80)
	};
	const std::map<WeaponList, int> Weapon_Magazine = {
		std::pair<WeaponList, int>(WEAPON_REVOLVER, 6),
		std::pair<WeaponList, int>(WEAPON_RIFLE, 25),
		std::pair<WeaponList, int>(WEAPON_TOMAHAWK, 1),
		std::pair<WeaponList, int>(WEAPON_SHOTGUN, 2),
		std::pair<WeaponList, int>(WEAPON_BOW, 10),
		std::pair<WeaponList, int>(WEAPON_GRENADE, 1),
		std::pair<WeaponList, int>(WEAPON_GUIDEDMISSILE, 6),
		std::pair<WeaponList, int>(WEAPON_SNIPER, 5),
		std::pair<WeaponList, int>(WEAPON_FLAMETHROWER, 30),
		std::pair<WeaponList, int>(WEAPON_LASER, 3),
		std::pair<WeaponList, int>(WEAPON_PISTOL, 15),
		std::pair<WeaponList, int>(WEAPON_LMG, 50),
		std::pair<WeaponList, int>(WEAPON_MAGNETO, 1)
	};
}

class Weapon : public Component
{
protected:
	Object* m_player_ptr = nullptr;
	bool	m_can_Pickup = true;
	bool	m_isthrown = false;

	int m_damage = 0;
public:
	Weapon(Object* owner_object, const std::string& component_name = "Weapon") : Component(owner_object, component_name) {}
	virtual ~Weapon() override {}

	virtual void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override = 0;
	virtual void Deserialization(const rapidjson::Value&) override = 0;
	virtual void ImGui_Setting() override = 0;
	virtual void Init() override = 0;
	virtual void Update(float dt) override = 0;
	virtual void Close() override = 0;

	void Set_Player_ptr(Object* ptr)
	{
		m_player_ptr = ptr;
	}

	bool Is_CanPickup()
	{
		if (this == nullptr)
		{
			return false;
		}
		return m_can_Pickup;
	}

	bool Is_Thrown()
	{
		return m_isthrown;
	}

	void Set_CanPickup(bool TF)
	{
		m_can_Pickup = TF;
	}

	void Set_Isthrown(bool TF)
	{
		m_isthrown = TF;
	}

	bool Is_Stun()
	{
		if (m_isthrown)
		{
			if (Magnitude(m_owner->GetComponentByTemplate<Physics>()->GetSpeed()) < 2.f)
			{
				m_isthrown = false;
				return false;
			}

			m_isthrown = false;
			m_owner->GetComponentByTemplate<Physics>()->SetSpeed(vector2(0, m_owner->GetComponentByTemplate<Physics>()->GetSpeed().y));
			return true;
		}
		else
		{
			return false;
		}
	}

	void SetDamage(int damage)
	{
		m_damage = damage;
	}
};