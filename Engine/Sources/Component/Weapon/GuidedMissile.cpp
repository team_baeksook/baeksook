/***********************************************************************
File name		: Tomahawk.cpp
Project name	: WFPT
Author			: HyunSeok Kim, SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "GuidedMissile.h"
#include "Object/Object.h"
#include "Component/Sprite.h"
#include "Component/ParticlePhysics.h"
#include "System/Logger.h"
#include "Component/SpriteText.h"
#include "Component/Collision.h"
#include "Graphic/Particle.h"
#include <GLFW/glfw3.h>
#include "System/Input.h"
#include "Component/Player.h"
#include "System/StateManager.h"
#include "System/SoundManager.h"
#include "System/Imgui_app.h"
#include "HomingBullet.h"
#include <IMGUI/imgui.h>

GuidedMissile::GuidedMissile(class Object* owner_object, const std::string& component_name): Gun(owner_object, component_name)
{
	kind = COMPONENT_KIND::GUIDEDMISSILE;
}

GuidedMissile::~GuidedMissile() {}

void GuidedMissile::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void GuidedMissile::Deserialization(const rapidjson::Value&)
{
}

void GuidedMissile::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::InputFloat("m_shotspeed", &m_shotspeed);
	ImGui::InputFloat("m_bulletspeed", &m_bulletspeed);
	ImGui::InputInt("m_magazine", &m_magazine);
	ImGui::Checkbox("m_reload", &m_should_reload);
	if (m_sound != nullptr)
	{
		m_sound->ImGui_Setting();
	}
	ImGui::Unindent();
}

void GuidedMissile::Init()
{
	kind = COMPONENT_KIND::GUIDEDMISSILE;
	name = "GuidedMissile";
	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	m_sound = new Audio();
	m_sound->Load_Audio_From_File("asset/Sound/axe_throw.wav");
	m_sound->Set_Volume(5.f);
	m_sound->Set_Name("Axe_Throw_Sound");
}

void GuidedMissile::Gun_Update(float dt)
{
	m_innertime += dt;
	//aim!
}

void GuidedMissile::Close()
{
	if (m_sound)
	{
		m_sound->Set_Dead(true);
	}
}

vector2 GuidedMissile::Fire(vector2 fire)
{
	if (m_should_reload && Magnitude_Squared(fire) == 0.0f)
	{
		return vector2();
	}
	if (m_innertime < m_shotspeed && Magnitude_Squared(fire) == 0.0f)
	{
		return vector2();
	}
	m_innertime = 0.0f;

	//Set target_player
	auto object_list = STATE->Get_CurrentState()->GetPlayerlist();
	float distance = 0.f;
	float minimum_distance = FLT_MAX;
	for (auto i = object_list.begin(); i != object_list.end(); ++i)
	{
		if (*i != m_player_ptr)
		{
			distance = Distance_Squared((*i)->GetTransform()->GetWorldPosition(), m_owner->GetTransform()->GetWorldPosition());
			if (distance < minimum_distance)
			{
				m_target_Player_pos = (*i)->GetTransform()->GetLocalPositionPtr();
				minimum_distance = distance;
			}
		}
	}

	float fire_angle = m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle + GetRandomNumber(-2.f, 2.f);

	Object* temp = new Object(m_owner->GetState(), "temporary");
	LoadArchetypeinfo(temp,"Guided_Bullet");
	temp->GetTransform()->SetScale(vector2(7.2f,3.6f));
	//Need calibrate for initial position 
	temp->GetTransform()->SetPosition(this->GetOwner()->GetTransform()->GetWorldPosition() + vector2(0, 2.f)); //initial correction position for bullet

	temp->Add_Component(new Physics(temp));
	temp->Add_Component(new Collision(temp));
	temp->GetComponentByTemplate<Collision>()->SetCollisionGroup(COLLISION_GROUP::BULLET);
	temp->GetComponentByTemplate<Collision>()->Init();
	temp->Add_Component(new HomingBullet(temp));
	temp->GetComponentByTemplate<HomingBullet>()->Init();
	temp->GetComponentByTemplate<HomingBullet>()->SetOrigin(m_player_ptr);
	temp->GetComponentByTemplate<HomingBullet>()->Set_PlayerPositionPtr(m_target_Player_pos);

	temp->GetComponentByTemplate<HomingBullet>()->SetDamage(20.f);
	if (this->GetOwner()->GetComponentByTemplate<Sprite>()->GetFlipX())
	{
		if (fire_angle == 0)
		{
			temp->GetComponentByTemplate<HomingBullet>()->Set_HeadingVector(180, m_bulletspeed);
		}
		else
		{
			temp->GetComponentByTemplate<HomingBullet>()->Set_HeadingVector(180-fire_angle, m_bulletspeed);
		}
	}
	else
	{
		temp->GetComponentByTemplate<HomingBullet>()->Set_HeadingVector(fire_angle, m_bulletspeed);
	}

	Sprite* sprite_comp = dynamic_cast<Sprite*>(temp->Add_Component(new Sprite(temp)));
	sprite_comp->SetTextureName("Revolver_Bullet");
	sprite_comp->SetColorA(.8f);
	sprite_comp->Init();
	ParticlePhysics* physics_comp = dynamic_cast<ParticlePhysics*>(temp->Add_Component(new ParticlePhysics(temp)));

	sprite_comp->SetFlipX(m_sprite_comp->GetFlipX());
	physics_comp->Init();
	temp->GetComponentByTemplate<Physics>()->SetGravityOn(false);
	temp->GetComponentByTemplate<Physics>()->Init();
	temp->SetLifetime(4.0f);
	m_owner->GetState()->AddRegisterObject(temp);

	--m_magazine;
	if (m_magazine < 1)
	{
		m_owner->SetLifetime(0.0f);
	}

	return vector2();
}
