/***********************************************************************
File name		: Tomahawk.cpp
Project name	: WFPT
Author			: HyunSeok Kim, SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Tomahawk.h"
#include "Object/Object.h"
#include "Component/Sprite.h"
#include "Component/ParticlePhysics.h"
#include "System/State.h"
#include "System/Logger.h"
#include "System/SoundManager.h"
#include "Component/SpriteText.h"
#include "Component/Collision.h"
#include "Graphic/Particle.h"
#include <GLFW/glfw3.h>
#include "System/Input.h"
#include "Component/Player.h"

#include <iostream>

Tomahawk::Tomahawk(class Object* owner_object, const std::string& component_name): ChargeWeapon(owner_object, component_name)
{
	kind = COMPONENT_KIND::TOMAHAWK;
}

Tomahawk::~Tomahawk() {}

void Tomahawk::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Tomahawk::Deserialization(const rapidjson::Value&)
{
}

void Tomahawk::ImGui_Setting()
{
}

void Tomahawk::Init()
{
	kind = COMPONENT_KIND::TOMAHAWK;
	name = "Tomahawk";
	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	m_throwsound = new Audio();
	m_throwsound->Load_Audio_From_File("asset/Sound/axe_throw.wav");
	m_throwsound->Set_Volume(3.f);
	m_throwsound->Set_Name("axe_throw_sound");

	m_damage = WeaponInfo::Weapon_Damagelist.at(static_cast<WeaponInfo::WeaponList>(kind));
	m_magazine = WeaponInfo::Weapon_Magazine.at(static_cast<WeaponInfo::WeaponList>(kind));
}

void Tomahawk::Update(float dt)
{
	if (m_isThrown)
	{
		//m_updateTime += dt;
		//if (m_updateTime >= 0.5f)
		//{
		//	m_updateTime = 0.5f;
		//}
		if (this->m_owner->GetComponentByTemplate<Sprite>()->GetFlipX())
		{
			this->m_owner->GetTransform()->AddDegree(600.0f*dt);
		}
		else
		{
			this->m_owner->GetTransform()->AddDegree(-600.0f*dt);
		}
		auto collidingobj = m_collision_comp->GetCollidingObjects();
		for (auto collision : collidingobj)
		{
			if (Player* player = collision->GetComponentByTemplate<Player>(); player != nullptr)
			{
				if (player->CanPlayerDamaged())
				{
					float dir = collision->GetTransform()->GetWorldPosition().x - m_owner->GetTransform()->GetWorldPosition().x;

					Player* origin_player = m_origin->GetComponentByTemplate<Player>();
					int current_health = player->GetHealth();
					int dmg = static_cast<int>(m_ChargedEnergy * m_damage);
					int damage_taken = (dmg > current_health) ? current_health : dmg;
					origin_player->Add_Damage(damage_taken);

					if (player->DamagePlayer(dmg, dir))
					{
						m_owner->GetState()->GetBadge()->AddKilllog(origin_player->GetPlayerNum(), player->GetPlayerNum());
						origin_player->IncrementKill();
					}
				}
			}
		}

		if (!m_collision_comp->GetCollidingObjects().empty())
		{
			this->m_owner->SetLifetime(0.f);
			//put this component to bullet
			//when it collides with player occur event to player
		}
		//m_updateTime = 0.f;
	}
	else if(m_owner->HasParent())
	{
		this->m_owner->GetTransform()->SetDegree(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);
	}
}

void Tomahawk::ChargeEnergy(float dt)
{
	m_ChargedEnergy += dt;

	if (m_ChargedEnergy > 2.f)
	{
		m_ChargedEnergy = 2.0f;
	}
	
	Graphic* graphic_system = m_owner->GetState()->GetGraphicSystem();

	vector2 velo = GetVectorByDegree(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);
	velo *= m_ChargedEnergy * m_scalevel;

	vector2 start = m_owner->GetTransform()->GetWorldPosition();
	float distance = 0.3f;
	vector2 mid = start + (0.5f + m_ChargedEnergy) * distance * velo * 0.5f;
	vector2 end = start + (0.5f + m_ChargedEnergy) * distance *
		vector2(velo.x, velo.y + 
		(0.5f + m_ChargedEnergy) * distance * 0.5f * PhysicsInfo::GLOBAL_GRAVITY);

	graphic_system->Draw_curveddotted_line_bezier_with_thickness(Color(255, 0, 0, 255), 
		m_ChargedEnergy * 0.5f + 1.0f, 3, start, mid, end);
}

void Tomahawk::Close()
{
	if (m_throwsound)
	{
		m_throwsound->Set_Dead(true);
	}
}

bool Tomahawk::Fire()
{
	if (m_ChargedEnergy >= 0.1f)
	{
		m_throwsound->Play_Audio(false);
		m_owner->Detach(true);
		m_isThrown = true;
		m_owner->GetTransform()->SetDepth(-1.f);
		m_collision_comp = dynamic_cast<Collision*>(m_owner->Add_Component(new Collision(m_owner)));
		m_collision_comp->SetCollisionGroup(COLLISION_GROUP::BULLET);
		m_collision_comp->AddIgnoreList(m_player_ptr);
		m_collision_comp->Init();
		m_origin = m_player_ptr;
		m_owner->SetLifetime(5.0f);
		m_owner->Add_Init_Component(new Physics(m_owner));
		vector2 velo;
		velo.x = cos(To_RADIAN(m_origin->GetComponentByTemplate<Player>()->m_weapon_angle));
		velo.y = sin(To_RADIAN(m_origin->GetComponentByTemplate<Player>()->m_weapon_angle));

		m_owner->GetComponentByTemplate<Physics>()->SetSpeed(velo*m_ChargedEnergy*m_scalevel);
		m_owner->GetComponentByTemplate<Physics>()->Set_Ignore_Tile(true);
		m_player_ptr = nullptr;
		//m_ChargedEnergy = 0.f;

		return true;
	}
	else
	{
		m_ChargedEnergy = 0.f;
	}
	return false;
}
