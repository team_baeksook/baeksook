#pragma once
#include "Weapon.h"

class ChargeWeapon : public Weapon
{
protected:
	float m_ChargedEnergy = 0.f;
public:
	ChargeWeapon(Object* owner_object, const std::string& component_name = "ChargeWeapon") : Weapon(owner_object, component_name) {}
	virtual ~ChargeWeapon() override {};

	virtual void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override = 0;
	virtual void Deserialization(const rapidjson::Value&) override = 0;
	virtual void ImGui_Setting() override = 0;
	virtual void Init() override = 0;
	virtual void Update(float dt) override = 0;
	virtual void Close() override = 0;
	virtual bool Fire() = 0;
	virtual void ChargeEnergy(float dt) = 0;

	float GetChargedEnergy() const
	{
		return m_ChargedEnergy;
	}
	void SetChargedEnergy(float energy)
	{
		ChargeEnergy(energy - m_ChargedEnergy);
		return;
	}
};