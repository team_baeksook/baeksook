/***********************************************************************
File name		: Rifle.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Rifle.h"
#include "Object/Object.h"
#include "Component/Sprite.h"
#include "Component/ParticlePhysics.h"
#include "System/State.h"
#include "System/Logger.h"
#include "System/SoundManager.h"

#include "Component/SpriteText.h"
#include "Component/Collision.h"
#include "Bullet.h"
#include "Graphic/Particle.h"
#include <IMGUI/imgui.h>

Rifle::Rifle(Object* owner_object, const std::string& component_name) : Gun(owner_object, component_name)
{
	kind = COMPONENT_KIND::RIFLE;
}

Rifle::~Rifle() {}

void Rifle::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Rifle::Deserialization(const rapidjson::Value&)
{
}

void Rifle::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::InputFloat("m_shotspeed", &m_shotspeed);
	ImGui::InputFloat("m_bulletspeed", &m_bulletspeed);
	ImGui::InputInt("m_magazine", &m_magazine);
	ImGui::Checkbox("m_reload", &m_should_reload);
	m_sound->ImGui_Setting();
	ImGui::Unindent();
}

void Rifle::Init()
{
	kind = COMPONENT_KIND::RIFLE;
	name = "Rifle";
	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	m_sound = new Audio();
	m_sound->Load_Audio_From_File("asset/Sound/gun_fire.wav");
	m_sound->Set_Volume(0.8f);
	m_sound->Set_Name("Rifle_Sound");
	m_sprite_comp->m_AnimationAcitve = false;

	m_damage = WeaponInfo::Weapon_Damagelist.at(static_cast<WeaponInfo::WeaponList>(kind));
	m_magazine = WeaponInfo::Weapon_Magazine.at(static_cast<WeaponInfo::WeaponList>(kind));
}

void Rifle::Gun_Update(float dt)
{
	m_innertime += dt;

	if (m_player_ptr && m_innertime < m_shotspeed)
	{
		float radian_current_angle = To_RADIAN(m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle);
		vector2 return_pos{};
		return_pos.x = (cos(radian_current_angle) - cos(radian_current_angle)*m_innertime / m_shotspeed)*3.f;
		return_pos.y = -2.f - (sin(radian_current_angle) - sin(radian_current_angle)*m_innertime / m_shotspeed)*3.f;
		if(m_sprite_comp->GetFlipX()==false)
		{
			return_pos.x *= -1.f;
		}
		m_owner->GetTransform()->SetPosition(return_pos);
	}
	if(m_should_reload)
	{
		if(m_innertime > m_reloadtime)
		{
			m_should_reload = false;
		}
	}

}

void Rifle::Close()
{
	if (m_sound)
	{
		m_sound->Set_Dead(true);
	}
}

vector2 Rifle::Fire(vector2 fire)
{
	if(m_should_reload && Magnitude_Squared(fire) == 0.0f)
	{
		return vector2();
	}
	if(m_innertime < m_shotspeed && Magnitude_Squared(fire) == 0.0f)
	{
		return vector2();
	}
	m_sprite_comp->m_oneloop_animation = true;
	m_sound->Play_Audio(false);
	m_innertime = 0.0f;

	vector2 fire_dir = fire;
	vector2 bulletshell_dir = fire;
	if (Magnitude_Squared(fire) == 0.0f)
	{
		float fire_angle = m_player_ptr->GetComponentByTemplate<Player>()->m_weapon_angle + GetRandomNumber(-3.f, 3.f);
		fire_dir = GetVectorByDegree(fire_angle);
		bulletshell_dir = GetVectorByDegree(fire_angle + 180);
	}

	Object* temp = new Object(m_owner->GetState(), "temporary");
	temp->GetTransform()->SetScale(vector2(5.0f, 2.5f));
	temp->GetTransform()->SetPosition(this->GetOwner()->GetTransform()->GetWorldPosition()+vector2(0,2.f));  //initial correction position for bullet
	temp->Add_Component(new Physics(temp));
	temp->GetComponentByTemplate<Physics>()->Init();
	temp->Add_Component(new Collision(temp));
	temp->GetComponentByTemplate<Collision>()->SetCollisionGroup(COLLISION_GROUP::BULLET);
	temp->GetComponentByTemplate<Collision>()->Init();
	Bullet* bulletcomp = dynamic_cast<Bullet*>(temp->Add_Component(new Bullet(temp)));
	bulletcomp->Init();
	bulletcomp->SetOrigin(m_player_ptr);
	bulletcomp->SetDamage(m_damage);

	Sprite* sprite_comp = dynamic_cast<Sprite*>(temp->Add_Component(new Sprite(temp)));
	sprite_comp->SetTextureName("Revolver_Bullet");
	sprite_comp->Init();
	ParticlePhysics* physics_comp = dynamic_cast<ParticlePhysics*>(temp->Add_Component(new ParticlePhysics(temp)));
	
	Object* flash = new Object(m_owner->GetState(), "flash");
	flash->GetTransform()->SetScale(vector2(12.f, 12.f));
	flash->GetTransform()->SetDepth(-0.91f);

	Object* bulletshell = new Object(m_owner->GetState(), "bulletshell");
	bulletshell->GetTransform()->SetScale(vector2(2.f, 1.f));
	bulletshell->GetTransform()->SetDepth(-0.91f);
	bulletshell->SetLifetime(5.f);
	Sprite* sprite_bulletshell = dynamic_cast<Sprite*>(bulletshell->Add_Component(new Sprite(bulletshell)));
	sprite_bulletshell->SetTextureName("bulletshell");
	sprite_bulletshell->SetColorA(1.f);
	sprite_bulletshell->Init();
	Physics* bulletshell_physics_comp = dynamic_cast<Physics*>(bulletshell->Add_Component(new Physics(bulletshell)));
	bulletshell_physics_comp->Init();
	Collision* bulletshell_collision_comp = dynamic_cast<Collision*>(bulletshell->Add_Component(new Collision(bulletshell)));
	bulletshell_collision_comp->SetCollisionGroup(COLLISION_GROUP::ITEM);
	bulletshell_collision_comp->Init();

	if (m_sprite_comp->GetFlipX())
	{
		flash->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition() - vector2(fire_dir.x, -fire_dir.y)*6.f);
		bulletshell->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition());
		bulletshell_physics_comp->SetSpeed(vector2(-bulletshell_dir.x, bulletshell_dir.y)*50.f);

		physics_comp->setVelocity(vector2(-fire_dir.x, fire_dir.y)*m_bulletspeed);
		temp->GetTransform()->SetDegree(-Angle_Degree(fire_dir));
		m_owner->GetTransform()->SetPosition(vector2(fire_dir.x*3.f, -fire_dir.y*3.f - 2.f));
	}
	else
	{
		flash->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition() + vector2(fire_dir.x, fire_dir.y)*6.f);
		bulletshell->GetTransform()->SetPosition(m_player_ptr->GetTransform()->GetWorldPosition());
		bulletshell_physics_comp->SetSpeed(vector2(bulletshell_dir.x, bulletshell_dir.y)*50.f);

		physics_comp->setVelocity(vector2(fire_dir.x, fire_dir.y)*m_bulletspeed);
		temp->GetTransform()->SetDegree(Angle_Degree(fire_dir));
		m_owner->GetTransform()->SetPosition(vector2(fire_dir.x*3.f, -fire_dir.y*3.f - 2.f));
	}
	Sprite* sprite_flash = dynamic_cast<Sprite*>(flash->Add_Component(new Sprite(flash)));
	sprite_flash->SetTextureName("SmallFlash");
	sprite_flash->SetColorA(.5f);
	sprite_comp->TurnOnOffAnimation(true);
	sprite_flash->Init();
	flash->SetLifetime(0.04f);
	m_owner->GetState()->AddRegisterObject(flash);
	m_owner->GetState()->AddRegisterObject(bulletshell);

	sprite_comp->SetFlipX(m_sprite_comp->GetFlipX());
	sprite_bulletshell->SetFlipX(m_sprite_comp->GetFlipY());
	physics_comp->Init();
	temp->SetLifetime(2.0f);
	m_owner->GetState()->AddRegisterObject(temp);
	--m_magazine;
	if(m_magazine < 1)
	{
		m_owner->SetLifetime(0.0f);
		//m_magazine = m_max_magazine;
		//m_innertime = 0.0f;
		//m_should_reload = true;
		//Object* reload_text = new Object(this->GetOwner()->GetState());
		//reload_text->Add_Component(new SpriteText(reload_text));
		//reload_text->GetComponentByTemplate<SpriteText>()->Init();
		//reload_text->GetComponentByTemplate<SpriteText>()->SetString("Reload!");
		//reload_text->GetTransform()->SetScale(vector2(10.0f, 10.0f));
		//reload_text->SetLifetime(m_reloadtime);
		//reload_text->AttatchToParent(this->m_owner);
		//reload_text->GetTransform()->SetPosition(vector2(0.0f, 0.0f));
		//m_owner->GetState()->AddRegisterObject(reload_text);
	}

	return fire_dir;
}
