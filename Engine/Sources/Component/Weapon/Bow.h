/***********************************************************************
File name		: Shotgun.h
Project name	: WFPT
Author			: Hyunseok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component/Sprite.h"
#include "System/Audio.h"
#include "ChargeWeapon.h"

class Bow : public ChargeWeapon
{
private:
	Sprite* m_sprite_comp = nullptr;

	float m_innertime = 0.0f;
	float m_shotspeed = 1.0f;
	float m_bulletspeed = 3.0f;

	const int m_max_magazine = 10;
	int m_magazine = m_max_magazine;
	bool m_should_reload = false;
	const float m_reloadtime = 3.0f;
	Audio* m_sound = nullptr;
	Object* m_origin = nullptr;
	bool m_readyToFire = false;
	float m_fireCountdown = 0.f;

	float m_fireRate = 1.0f;
	bool m_isTutorial = false;
public:
	Bow(Object* owner_object, const std::string& component_name = "Bow");
	~Bow() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;
	bool Fire() override;
	void ChargeEnergy(float dt) override;
	void SetTutorial(bool TF);
};

