#include "Grenade.h"
#include <IMGUI/imgui.h>
#include "System/Logger.h"
#include "Object/Object.h"
#include "Component/Player.h"
#include "Object/Tile.h"
#include "System/State.h"
#include "System/SoundManager.h"

#include <iostream>
#include "Bullet.h"

Grenade::Grenade(Object* owner_object, const std::string& component_name) : Gun(owner_object, component_name)
{
	kind = COMPONENT_KIND::GRENADE;
}

Grenade::~Grenade() {}

void Grenade::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Grenade::Deserialization(const rapidjson::Value&)
{
}

void Grenade::ImGui_Setting()
{
	ImGui::Indent();
	m_sound->ImGui_Setting();
	ImGui::Unindent();
}

void Grenade::Init()
{
	kind = COMPONENT_KIND::GRENADE;
	name = "Grenade";
	physics_comp = dynamic_cast<ParticlePhysics*>(m_owner->Add_Component(new ParticlePhysics(m_owner)));



	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	if(m_physics = this->m_owner->GetComponentByTemplate<Physics>(); m_physics == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have physics component!", spdlog::level::level_enum::err);
	}

	m_sound = new Audio();
	m_sound->Load_Audio_From_File("asset/Sound/gun_fire3.wav");
	m_sound->Set_Volume(3.f);
	m_sound->Set_Name("Revolver_Sound");
}

void Grenade::Update(float /*dt*/)
{
	if(m_isThrown)
	{
		if(this->m_owner->GetComponentByTemplate<Sprite>()->GetFlipX())
		{
			this->m_owner->GetTransform()->AddDegree(10.0f);
			//this->m_owner->GetTransform()->SetPosition(this->m_owner->GetTransform()->GetWorldPosition() -= vector2(1.6f, 0));
		}
		else
		{
			this->m_owner->GetTransform()->AddDegree(-10.0f);
			//this->m_owner->GetTransform()->SetPosition(this->m_owner->GetTransform()->GetWorldPosition() += vector2(1.6f, 0));
		}
		auto collidingObject = m_collision->GetCollidingObjects();
		//for(auto collision :collidingObject)
		//{
		//	/*if(Player* player = collision->GetComponentByTemplate<Player>(); player!=nullptr)
		//	{
		//		if(player->CanPlayerDamaged())
		//		{
		//			m_origin->GetComponentByTemplate<Player>()->IncrementScore();
		//			float direction = collision->GetTransform()->GetWorldPosition().x - m_owner->GetTransform()->GetWorldPosition().x;
		//			player->DamagePlayer(direction);
		//		}
		//	}*/
		//}
		if(!m_collision->GetCollidingObjects().empty())
		{
			this->m_owner->SetLifetime(0.f);
		}

		m_isOnGround = m_physics->GetIsOnGround();
		if (m_isOnGround)
		{
			physics_comp->setVelocity(vector2(0, 0));
			physics_comp->setAcceleration(vector2(0, 0));
			this->m_owner->GetTransform()->SetPosition(this->m_owner->GetTransform()->GetWorldPosition());
			Explode();
		}
	}
}

void Grenade::Explode()
{
	Object* flash = new Object(m_owner->GetState(), "flash");
	flash->GetTransform()->SetScale(vector2(36.f, 36.f));
	flash->GetTransform()->SetDepth(-0.91f);
	flash->GetTransform()->SetPosition(m_owner->GetTransform()->GetWorldPosition() + vector2(0.f, 18.0f));

	Sprite* sprite_flash = dynamic_cast<Sprite*>(flash->Add_Component(new Sprite(flash)));
	sprite_flash->SetTextureName("muzzleFlash");
	sprite_flash->SetColorA(.5f);
	sprite_flash->Init();
	flash->SetLifetime(0.04f);
	m_owner->GetState()->AddRegisterObject(flash);
}

void Grenade::Gun_Update(float)
{
}


vector2 Grenade::Fire(vector2 /*fire*/)
{
	this->m_owner->GetTransform()->AddPosition(vector2{ 0,0.5f });
	m_sound->Play_Audio(false);
	Object* parent = m_owner->GetParent()->GetParent();
	m_owner->Detach();
	m_isThrown = true;
	m_owner->GetTransform()->SetDepth(-1.f);
	m_physics = dynamic_cast<Physics*>(m_owner->Add_Init_Component(new Physics(m_owner)));
	m_collision = dynamic_cast<Collision*>(m_owner->Add_Init_Component(new Collision(m_owner)));
	m_collision->SetCollisionGroup(COLLISION_GROUP::BULLET);
	m_origin = parent;
	m_collision->AddIgnoreList(parent);
	m_owner->SetLifetime(3.0f);
	if (m_sprite_comp->GetFlipX())
	{
		physics_comp->setMass(200.0f);
		physics_comp->setVelocity(-2.5f, 1.5f);
		physics_comp->setAcceleration(0.0f, -2.f);

		m_sprite_comp->SetFlipX(m_sprite_comp->GetFlipX());
	}
	else
	{
		physics_comp->setMass(200.0f);
		physics_comp->setVelocity(2.5f, 1.5f);
		physics_comp->setAcceleration(0.0f, -2.f);

		m_sprite_comp->SetFlipX(m_sprite_comp->GetFlipX());
	}

	return vector2();
}

void Grenade::Close()
{
	if (m_sound)
	{
		m_sound->Set_Dead(true);
	}
}


