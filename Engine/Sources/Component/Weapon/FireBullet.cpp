/***********************************************************************
	File name		: FireBullet.cpp
	Project name	: WFPT
	Author			: Hyun Seok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#include "Bullet.h"
#include "Object/Object.h"
#include "System/Logger.h"
#include "Component/Collision.h"
#include "Component/ParticlePhysics.h"
#include <iostream>
#include "Component/Player.h"
#include "Object/Tile.h"
#include "Component/Physics.h"
#include "FireBullet.h"
#include "System/State.h"


FireBullet::FireBullet(Object* owner_object, const std::string& component_name) : Bullet(owner_object, component_name)
{
	kind = COMPONENT_KIND::BULLET;
}

void FireBullet::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&)
{
}

void FireBullet::Deserialization(const rapidjson::Value&)
{
}

void FireBullet::ImGui_Setting()
{
}

void FireBullet::Init()
{
	if (m_collision_comp = m_owner->GetComponentByTemplate<Collision>(); m_collision_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not hold the collision component", spdlog::level::err);
	}
	if (m_physics_comp = this->m_owner->GetComponentByTemplate<Physics>(); m_physics_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have physics component!", spdlog::level::level_enum::err);
	}
	m_physics_comp->Gravity = 0.f;
	m_collision_comp->AddIgnoreList(this->m_owner);
}

void FireBullet::Update(float /*dt*/)
{
	//vector2 velo = m_owner->GetComponentByTemplate<ParticlePhysics>()->getVelocity();

	//float angle = To_DEGREE(atan2(velo.y, velo.x));
	//m_owner->GetTransform()->SetDegree(angle);

	m_bulletSpeed = m_physics_comp->GetSpeed();
	if (m_bulletSpeed.x >= 0.f)
	{
		m_IsCollidingWithWall = m_physics_comp->IsWallBlocked(m_bulletSpeed, RIGHT);
	}
	else
	{
		m_IsCollidingWithWall = m_physics_comp->IsWallBlocked(m_bulletSpeed, LEFT);
	}

	m_IsCollidingWithFloor = m_physics_comp->HasGround(m_bulletSpeed);

	if (m_IsCollidingWithFloor)
	{
		std::cout << "Fire collide with Floor" << std::endl;

		for (int i = 0; i < m_bulletPerShot; ++i)
		{
			Object* newBullet = new Object(m_owner->GetState(), "BurnBabyBurn");
			newBullet->GetTransform()->SetScale(vector2(10.0f, 10.0f));
			newBullet->GetTransform()->SetDepth(-0.8f);
			newBullet->GetTransform()->SetPosition(m_owner->GetTransform()->GetWorldPosition()+vector2(0.f,5.f));

			newBullet->Add_Component(new Collision(newBullet));
			newBullet->GetComponentByTemplate<Collision>()->Init();
			newBullet->GetComponentByTemplate<Collision>()->SetCollisionGroup(COLLISION_GROUP::BULLET);

			//newBullet->Add_Component(new ParticlePhysics(newBullet));
			//newBullet->GetComponentByTemplate<ParticlePhysics>()->Init();

			newBullet->Add_Component(new Physics(newBullet));
			newBullet->GetComponentByTemplate<Physics>()->Init();
			newBullet->GetComponentByTemplate<Physics>()->m_shouldScaleOverTime = true;


			switch(i)
			{
			case 0:
				//newBullet->GetComponentByTemplate<Physics>()->SetIncreasingScaleValue(vector2(3.f,6.f));
				newBullet->GetComponentByTemplate<Physics>()->SetSpeed({ -20.f, 40.f });

				break;
			case 1:
				//newBullet->GetComponentByTemplate<Physics>()->SetIncreasingScaleValue(vector2(2.f, 4.f));
				newBullet->GetComponentByTemplate<Physics>()->SetSpeed({ -40.f, 30.f });
				break;
			case 2:
				//newBullet->GetComponentByTemplate<Physics>()->SetIncreasingScaleValue(vector2(1.5f, 3.f));
				newBullet->GetComponentByTemplate<Physics>()->SetSpeed({ -60.f, 20.f });
				break;
			case 3:
				//newBullet->GetComponentByTemplate<Physics>()->SetIncreasingScaleValue(vector2(3.f, 6.f));
				newBullet->GetComponentByTemplate<Physics>()->SetSpeed({ 20.f, 40.f });
				break;
			case 4:
				//newBullet->GetComponentByTemplate<Physics>()->SetIncreasingScaleValue(vector2(2.f, 4.f));
				newBullet->GetComponentByTemplate<Physics>()->SetSpeed({ 40.f, 30.f });
				break;
			case 5:
				//newBullet->GetComponentByTemplate<Physics>()->SetIncreasingScaleValue(vector2(1.5f, 3.f));
				newBullet->GetComponentByTemplate<Physics>()->SetSpeed({ 60.f, 20.f });
				break;
			case 6:
				//newBullet->GetComponentByTemplate<Physics>()->SetIncreasingScaleValue(vector2(1.f, 2.f));
				newBullet->GetComponentByTemplate<Physics>()->SetSpeed({ -80.f, -10.f });
				break;
			case 7:
				//newBullet->GetComponentByTemplate<Physics>()->SetIncreasingScaleValue(vector2(1.f, 2.f));
				newBullet->GetComponentByTemplate<Physics>()->SetSpeed({ 80.f, 10.f });
				break;
			case 8:
				//newBullet->GetComponentByTemplate<Physics>()->SetIncreasingScaleValue(vector2(5.f, 10.f));
				newBullet->GetComponentByTemplate<Physics>()->SetSpeed({ 0, 45.f });
				break;
			default:
				break;
			}

			//5newBullet->Add_Component(new Bullet(newBullet));
			//5newBullet->GetComponentByTemplate<Bullet>()->SetOrigin(newBullet);
			//5newBullet->GetComponentByTemplate<Bullet>()->Init();

			Bullet* bullet_comp = dynamic_cast<Bullet*>(newBullet->Add_Component(new Bullet(newBullet)));
			bullet_comp->Init();
			bullet_comp->SetOrigin(m_origin,true);
			bullet_comp->SetNotDieAtWall(true);
			bullet_comp->SetDamage(m_damage);

			Sprite* sprite_comp = dynamic_cast<Sprite*>(newBullet->Add_Component(new Sprite(newBullet)));
			sprite_comp->SetTextureName("Fire");
			sprite_comp->TurnOnOffAnimation(true);
			sprite_comp->Init();

			newBullet->SetLifetime(3.0f);
			this->m_owner->GetState()->AddRegisterObject(newBullet);
		}
		
		this->m_owner->MakeDead();
	}

	// if (m_IsCollidingWithWall)
	// {
		// std::cout << "collide with wall" << std::endl;
		// this->m_owner->MakeDead();
	// }

	// auto collidingobj = m_collision_comp->GetCollidingObjects();
	// for (auto collision : collidingobj)
	// {
		// if (Player* player = collision->GetComponentByTemplate<Player>(); player != nullptr)
		// {
			// if (player->CanPlayerDamaged())
			// {
				// float dir = collision->GetTransform()->GetWorldPosition().x - m_owner->GetTransform()->GetWorldPosition().x;

				// Player* origin_player = m_origin->GetComponentByTemplate<Player>();
				// int current_health = player->GetHealth();
				// int damage_taken = (m_damage > current_health) ? current_health : m_damage;
				// origin_player->Add_Damage(damage_taken);

				// if (player->DamagePlayer(m_damage, dir))
				// {
					// m_owner->GetState()->GetBadge()->AddKilllog(origin_player->GetPlayerNum(), player->GetPlayerNum());
					// origin_player->IncrementKill();
				// }
			// }
		// }
	// }

	// if (!m_collision_comp->GetCollidingObjects().empty())
	// {
		// this->m_owner->MakeDead();
		// //put this component to bullet
		// //when it collides with player occur event to player
	// }
// =======
	//if (m_IsCollidingWithWall)
	//{
	//	std::cout << "collide with wall" << std::endl;
	//	this->m_owner->MakeDead();
	//}

	//auto collidingobj = m_collision_comp->GetCollidingObjects();
	//for (auto collision : collidingobj)
	//{
	//	if (Player* player = collision->GetComponentByTemplate<Player>(); player != nullptr)
	//	{
	//		if (player->CanPlayerDamaged())
	//		{
	//			float dir = collision->GetTransform()->GetWorldPosition().x - m_owner->GetTransform()->GetWorldPosition().x;

	//			if (player->DamagePlayer(m_damage, dir))
	//			{
	//				m_origin->GetComponentByTemplate<Player>()->IncrementKill();
	//			}
	//		}
	//	}
	//}

	//if (!m_collision_comp->GetCollidingObjects().empty())
	//{
	//	this->m_owner->MakeDead();
	//	//put this component to bullet
	//	//when it collides with player occur event to player
	//}
}

void FireBullet::Close()
{
}

void FireBullet::SetOrigin(Object* obj)
{
	m_origin = obj;
	m_collision_comp->AddIgnoreList(obj);
}
