#include "Button.h"

Button::Button(Object* owner_object, std::string component_name) : Component(owner_object, component_name) 
{
	kind = COMPONENT_KIND::BUTTON;
}

Button::~Button()
{
}

void Button::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& /*value*/)
{
}

void Button::Deserialization(const rapidjson::Value& /*json*/)
{
}

void Button::ImGui_Setting()
{
}

void Button::Init()
{
	m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>();
	if (m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have Sprite component! Creating one..", spdlog::level::level_enum::warn);
		m_owner->Add_Component(new Sprite(m_owner));
		m_sprite_comp = m_owner->GetComponentByTemplate<Sprite>();
		m_sprite_comp->SetTextureName("Rectangle");
		m_sprite_comp->SetColor(Color(255, 255, 255, 255));
		m_sprite_comp->Init();
	}
	m_sprite_text_comp = this->m_owner->GetComponentByTemplate<SpriteText>();
	if (m_sprite_text_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have SpriteText component! Creating one..", spdlog::level::level_enum::warn);

		logger::log_handler.PrintLogOnConsole("is state null ?: " + std::to_string((m_owner->GetState() == nullptr)));
		Object* text = new Object(m_owner->GetState(), "Text");
		text->Add_Init_Component(new SpriteText(text));
		m_sprite_text_comp = text->GetComponentByTemplate<SpriteText>();
		m_sprite_text_comp->SetAllign(TEXT_ALLIGN::ALLIGN_CENTER);
		m_sprite_text_comp->SetColor(Color(0, 0, 0, 255));
		m_sprite_text_comp->SetString("");
		m_sprite_text_comp->SetFontSize(18);
		text->GetTransform()->SetPosition(vector2(0, 0));
		vector2 v = text->GetTransform()->GetLocalScale();
		logger::log_handler.PrintLogOnConsole("x: " + std::to_string(v.x) + " | y: " + std::to_string(v.y));
		text->AttatchToParent(m_owner);
		m_owner->GetState()->AddRegisterObject(text);
	}
	m_camera = this->m_owner->GetState()->GetCamera();
	if (m_camera == nullptr)
		logger::log_handler.PrintLogOnConsoleAndFile("The state does not have camera", spdlog::level::level_enum::err);

	m_original_color = m_sprite_comp->GetColor();
	HSV temp = m_original_color.ConvertHSV();
	temp.Value *= 0.7f;
	m_on_color = temp.ConvertRGB();
	temp.Value *= 0.7f;
	m_held_color = temp.ConvertRGB();
	temp.Value *= 0.7f;
	m_selected_color = temp.ConvertRGB();
}

void Button::Update(float /*dt*/)
{
	vector2 mouse_pos = Input::Get_Mouse_Position();
	vector2 local_pos = m_camera->GetLocalPosition(mouse_pos);
	vector2 cenpos = this->m_owner->GetTransform()->GetWorldPosition();
	vector2 scale = this->m_owner->GetTransform()->GetWorldScale();

	//left mouse button
	bool is_mouse_down = Input::Is_Pressed(GLFW_MOUSE_BUTTON_1);
	bool is_mouse_triggered = Input::Is_Triggered(GLFW_MOUSE_BUTTON_1);

	if (Rectangle_Point_CollisionCheck(local_pos, cenpos, scale))
	{
		if (!m_is_on)
		{
			m_is_on = true;
			if (m_callback_hover_enter)
				m_callback_hover_enter();
		}
	}
	else
		m_is_on = false;

	if (m_is_on)
	{
		if (is_mouse_triggered)
			m_is_held = true;
	}

	if (!is_mouse_down)
	{
		if (m_is_held && m_is_on && m_callback_click)
			m_callback_click();
		m_is_held = false;
	}

	UpdateColor();
}

void Button::Close()
{
}

void Button::UpdateColor()
{
	if (m_is_on)
	{
		if (m_is_held)
			m_sprite_comp->SetColor(m_held_color);
		else
			m_sprite_comp->SetColor(m_on_color);
	}
	else if (m_selected)
		m_sprite_comp->SetColor(m_selected_color);
	else
		m_sprite_comp->SetColor(m_original_color);
}

void Button::SetCallbackOnClick(std::function<void()> callback)
{
	m_callback_click = callback;
}

void Button::SetCallbackOnHoverEnter(std::function<void()> callback)
{
	m_callback_hover_enter = callback;
}

void Button::TriggerCallbackClick()
{
	if (m_callback_click)
		m_callback_click();
}

void Button::TriggerCallbackHoverEnter()
{
	if (m_callback_hover_enter)
		m_callback_hover_enter();
}

void Button::SetSelected(bool state)
{
	m_selected = state;
}

bool Button::IsSelected()
{
	return m_selected;
}
