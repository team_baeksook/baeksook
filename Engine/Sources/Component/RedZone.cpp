#include "RedZone.h"
#include "component\Sprite.h"
#include "Component/Collision.h"
#include "Component/Player.h"
#include "Object/Object.h"
#include "System/Logger.h"
#include "System/State.h"

#include <iostream>

RedZone::RedZone(Object* owner_object, const std::string& component_name) : Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::REDZONE;
}

RedZone::~RedZone()
{
}

void RedZone::Bombing(float dt)
{
	m_bombtime += dt;
	if (m_bombtime > 2.0f)
	{
		Object* bomb_effect = new Object(m_owner->GetState());
		Sprite* effect_sprite = dynamic_cast<Sprite*>(bomb_effect->Add_Init_Component(new Sprite(bomb_effect)));
		effect_sprite->SetTextureName("Circle");
		effect_sprite->SetColor(Color(255, 255, 255, 255));

		vector2 pos = this->m_owner->GetTransform()->GetWorldPosition();
		pos.x += GetRandomNumber<float>(-20.0f, 20.0f);
		pos.y += GetRandomNumber<float>(-20.0f, 20.0f);

		bomb_effect->GetTransform()->SetPosition(pos);
		bomb_effect->GetTransform()->SetScale(vector2(20.0f, 20.0f));
		bomb_effect->SetLifetime(1.0f);

		m_owner->GetState()->AddRegisterObject(bomb_effect);

		m_bombtime -= 2.0f;

		auto collidingobjs = m_collision_comp->GetCollidingObjects();
		for (auto obj : collidingobjs)
		{
			vector2 diff = pos - obj->GetTransform()->GetWorldPosition();
			std::cout << "the diff is " << Magnitude(diff) << std::endl;
			if (obj->GetTransform()->GetDiagonal() / 2.0f + 10.0f > Magnitude(diff))
			{
				if (Player* player = obj->GetComponentByTemplate<Player>(); player != nullptr)
				{
					if (player->CanPlayerDamaged())
					{
						player->DamagePlayer(30, -1.0f);
					}
				}
			}
		}
		m_innertime = 0.0f;
	}
}

void RedZone::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&)
{
}

void RedZone::Deserialization(const rapidjson::Value &)
{
}

void RedZone::ImGui_Setting()
{
}

void RedZone::Init()
{
	m_sprite_comp = dynamic_cast<Sprite*>(m_owner->Add_Init_Component(new Sprite(this->m_owner)));

	m_sprite_comp->SetTextureName("Circle");
	m_sprite_comp->SetColor(Color(255, 0, 0, 10));

	m_collision_comp = dynamic_cast<Collision*>(m_owner->Add_Init_Component(new Collision(this->m_owner)));
	m_collision_comp->SetCollisionGroup(COLLISION_GROUP::BULLET);

	m_owner->GetTransform()->SetScale(vector2(100, 100));
	m_owner->GetTransform()->SetDepth(-1.0f);
}

void RedZone::Update(float dt)
{
	m_innertime += dt;
	m_blinktime += dt;
	m_bombtime -= dt * 0.1f;

	m_sprite_comp->SetColorA(0.7f);
	if (m_blinktime > m_bombtime)
	{
		m_sprite_comp->SetColorA(0.0f);
		if (m_blinktime > m_bombtime * 2.0f)
		{
			m_blinktime = 0.0f;
		}
	}

	if (m_innertime > 10.0f)
	{
		for (auto obj : m_collision_comp->GetCollidingObjects())
		{
			if (Player* player = obj->GetComponentByTemplate<Player>(); player != nullptr)
			{
				if (player->CanPlayerDamaged())
				{
					player->DamagePlayer(100, -1.0f);
				}
			}
		}
		this->m_owner->SetLifetime(0.0f);
	}
}

void RedZone::Close(void)
{
}
