/***********************************************************************
File name		: WeaponGenerator.cpp
Project name	: WFPT
Author			: Hyun Seok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "WeaponGenerator.h"
#include "ParticleGenerator.h"
#include "Object/Object.h"
#include "System/State.h"
#include "Graphic/Particle.h"
#include <IMGUI/imgui.h>
#include "Graphic/Texture.h"
#include "System/Imgui_app.h"

WeaponGenerator::WeaponGenerator(Object* owner_object, const std::string& component_name) : Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::WEAPONGENERATOR;
}

WeaponGenerator::~WeaponGenerator()
{

}

void WeaponGenerator::Init()
{
}

void WeaponGenerator::MakeWeapon()
{
	Object* Item = new Object(m_owner->GetState(), "WeaponGenerator");
	float type = GetRandomNumber<float>(-1.0f, 18.0f);
	if (type < 0.5f)
	{
		LoadArchetypeinfo(Item, "Rifle");
	}
	else if (type < 2.0f)
	{
		LoadArchetypeinfo(Item, "Shotgun");
	}
	else if (type < 3.5f)
	{
		LoadArchetypeinfo(Item, "Sniper");
	}
	else if (type < 5.0f)
	{
		LoadArchetypeinfo(Item, "LMG");
	}
	else if (type < 7.0f)
	{
		LoadArchetypeinfo(Item, "Bow");
	}
	else if (type < 8.0f)
	{
		LoadArchetypeinfo(Item, "FlameThrower");
	}
	else if (type < 9.0f)
	{
		LoadArchetypeinfo(Item, "GuidedMissile");
	}
	else if (type < 11.0f)
	{
		LoadArchetypeinfo(Item, "Revolver");
	}
	else if (type < 14.0f)
	{
		LoadArchetypeinfo(Item, "Pistol");
	}
	else if (type < 17.6f)
	{
		LoadArchetypeinfo(Item, "Tomahawk");
	}
	else
	{
		LoadArchetypeinfo(Item, "Laser");
	}

	vector2 ownerLocation = m_owner->GetTransform()->GetWorldPosition();
	Item->GetTransform()->SetPosition(vector2(ownerLocation.x+3.f, ownerLocation.y + 2.5f));
	m_weapon = Item;

	m_owner->GetState()->AddRegisterObject(Item);
}

void WeaponGenerator::Update(float dt)
{
	m_innerTime += dt;
	if(m_firstWeaponGen)
	{
		MakeWeapon();
		m_firstWeaponGen = false;
	}
	else if(m_weapon == nullptr)
	{
		m_shouldMakeWeapon = true;
	}
	else if (m_weapon->HasParent())
	{
		m_shouldMakeWeapon = true;
		m_weapon = nullptr;
	}
	else
	{
		m_shouldMakeWeapon = false;
		m_innerTime = 0.f;
	}

	if(m_innerTime >= m_GenerateCoolDown && m_shouldMakeWeapon)
	{
		MakeWeapon();
		m_innerTime = 0.f;
	}
}

void WeaponGenerator::Close()
{

}

void WeaponGenerator::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void WeaponGenerator::Deserialization(const rapidjson::Value& /*value*/)
{

}

void WeaponGenerator::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::InputFloat("GenerateCoolDown", &m_GenerateCoolDown);
	ImGui::Unindent();
}






