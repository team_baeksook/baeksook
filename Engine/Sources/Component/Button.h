#pragma once
#include <iostream>
#include "Component.h"
#include "Graphic/Color.h"
#include "System/StateManager.h"
#include "System/SceneIdentifier.h"
#include "Sprite.h"
#include "System/Logger.h"
#include "Object/Object.h"
#include "System/Input.h"
#include "System/State.h"
#include "Camera.h"
#include "Component/SpriteText.h"
#include "System/Engine.h"

class Sprite;
class SpriteText;
class Camera;

class Button : public Component
{
private:
	Sprite* m_sprite_comp = nullptr;
	SpriteText* m_sprite_text_comp = nullptr;
	Camera* m_camera = nullptr;
	Color m_original_color;
	Color m_on_color;
	Color m_held_color;
	Color m_selected_color;
	bool m_is_on = false;
	bool m_is_held = false;
	bool m_selected = false;
	std::function<void()> m_callback_click;
	std::function<void()> m_callback_hover_enter;
public:
	Button(Object * owner_object, std::string component_name = "Button");
	~Button() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& json) override;
	void Deserialization(const rapidjson::Value& value) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;

	void UpdateColor();
	
	void SetCallbackOnClick(std::function<void()> callback);
	void SetCallbackOnHoverEnter(std::function<void()> callback);
	void TriggerCallbackClick();
	void TriggerCallbackHoverEnter();
	void SetSelected(bool state);
	bool IsSelected();
};
