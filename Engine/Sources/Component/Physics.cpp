/***********************************************************************
	File name		: Physics.cpp
	Project name	: WFPT
	Author			: HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#include "Physics.h"
#include "IMGUI/imgui.h"
#include "System/Input.h"
#include "Object/Object.h"
#include "System/ObjectManager.h"
#include "Sprite.h"
#include <iostream>
#include "System/Logger.h"
#include "System/Engine_Utility.h"
#include "Object/TileMap.h"

Physics::Physics(Object * owner_object, std::string component_name)
	: Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::PHYSICS;
	IsOnGround = true;
}

Physics::~Physics() {}

void Physics::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Physics::Deserialization(const rapidjson::Value &)
{
}

void Physics::ImGui_Setting()
{
	//ImGui::Checkbox("Gravity On/Off", &IsGravityOn);
	//ImGui::Checkbox("Ghost On/Off", &IsGhostOn);
	//ImGui::Spacing(); ImGui::Separator(); ImGui::Spacing();
	//ImGui::InputFloat("WalkSpeed", &WalkSpeed);
	//ImGui::InputFloat("JumpSpeed", &JumpSpeed);
	ImGui::InputFloat("MaxFallingSpeed", &MaxFallingSpeed);
	//ImGui::InputFloat2("Position", &Position.x);
	ImGui::InputFloat("Gravity", &Gravity);
	//ImGui::InputFloat2("Speed", &Speed.x);
}

void Physics::Init()
{
	name = "Physics";
	kind = COMPONENT_KIND::PHYSICS;
	Set_TileMap(m_owner->GetState()->GetTile());
}

bool Physics::IsObstacle(vector2 _position) const
{
	return (*Tile_Map)[Round_Position(_position)]->Get_Type() == OBSTACLE;
}

bool Physics::IsGround(vector2 _position) const
{
	if ((*Tile_Map)[Round_Position(_position)]->Get_Type() == OBSTACLE)
	{
		return true;
	}
	if((*Tile_Map)[Round_Position(_position)]->Get_Type() == ONEWAY)
	{
		if(_position.y > (Round_Position(_position).y + 3.0f))
		{
			return true;
		}
	}
	return false;
}

bool Physics::IsOneway(vector2 _position) const
{
	return (*Tile_Map)[Round_Position(_position)]->Get_Type() == ONEWAY;
}

bool Physics::IsEmpty(vector2 _position) const
{
	return (*Tile_Map)[Round_Position(_position)]->Get_Type() == EMPTY;
}

bool Physics::IsThereTile(vector2 _position) const
{
	return (*Tile_Map)[Round_Position(_position)] != nullptr;
}

bool Physics::HasGround(vector2 _Speed)
{

	const vector2 bottomLeft = m_collision_comp->GetCollisionBox_position(BOX_INDEX::BOT_LEFT);
	const vector2 bottomRight = m_collision_comp->GetCollisionBox_position(BOX_INDEX::BOT_RIGHT);

	const vector2 start = bottomLeft + vector2(0, _Speed.y);
	const vector2 end = bottomRight + vector2(0, _Speed.y);

	for(vector2 iteration = start; Round_Position(iteration).x <= Round_Position(end).x; iteration.x += 10.0f)
	{
		if (IsThereTile(iteration))
		{
			if (IsGround(iteration))
			{
				//m_how_much_push_up = Round_Position(iter).y + 5.0f + m_collision_comp->GetBoxSize().y * 0.5f - m_collision_comp->GetCollisionOffset().y + 0.05f;
				return true;
			}
		}
	}

	//no collision
	return false;
}

bool Physics::HasGround(vector2 _Speed, float dt)
{

	const vector2 bottomLeft = m_collision_comp->GetCollisionBox_position(BOX_INDEX::BOT_LEFT);
	const vector2 bottomRight = m_collision_comp->GetCollisionBox_position(BOX_INDEX::BOT_RIGHT);

	const vector2 start = bottomLeft + vector2(0, _Speed.y *dt);
	const vector2 end = bottomRight + vector2(0, _Speed.y * dt);

	for (vector2 iter = start; Round_Position(iter).x <= Round_Position(end).x; iter.x += 10.0f)
	{
		if (IsThereTile(iter))
		{
			if (IsGround(iter))
			{
				//m_how_much_push_up = (Round_Position(iter).y + 5.0f + m_collision_comp->GetBoxSize().y * 0.5f - m_collision_comp->GetCollisionOffset().y + 0.05f);
				return true;
			}
		}
	}

	//no collision
	return false;
}

bool Physics::HasGround_OneWay()
{
	vector2 bottomLeft = m_collision_comp->GetCollisionBox_position(BOX_INDEX::BOT_LEFT);
	vector2 bottomRight = m_collision_comp->GetCollisionBox_position(BOX_INDEX::BOT_RIGHT);

	vector2 start = bottomLeft + vector2(0, Speed.y - 0.1f);
	vector2 end = bottomRight + vector2(0, Speed.y - 0.1f);

	for (vector2 iter = start; Round_Position(iter).x <= Round_Position(end).x; iter.x += 10.0f)
	{
		if (IsThereTile(iter))
		{
			if (IsOneway(iter))
			{
				return true;
			}
		}
	}

	return false;
}

bool Physics::IsCeilingBlocked(vector2 _Speed) const
{
	//if (IsGhostOn)
	//{
	//	return false;
	//}

	vector2 topLeft = m_collision_comp->GetCollisionBox_position(BOX_INDEX::TOP_LEFT);
	vector2 topRight = m_collision_comp->GetCollisionBox_position(BOX_INDEX::TOP_RIGHT);

	if (IsThereTile(topLeft + vector2(0, _Speed.y)))
	{
		if (IsObstacle(topLeft + vector2(0, _Speed.y)))
			return true;
	}
	if (IsThereTile(topRight + vector2(0, _Speed.y)))
	{
		if (IsObstacle(topRight + vector2(0, _Speed.y)))
			return true;
	}
	//can jump through ceiling
	return false;
}

bool Physics::IsWallBlocked(vector2 _Speed, int Direction) const
{
	//if (IsGhostOn)
	//{
	//	return false;
	//}

	if (Direction == RIGHT)
	{
		vector2 botRight = m_collision_comp->GetCollisionBox_position(BOX_INDEX::BOT_RIGHT) + vector2(_Speed.x, 0);
		vector2 topRight = m_collision_comp->GetCollisionBox_position(BOX_INDEX::TOP_RIGHT) + vector2(_Speed.x, 0);

		if (IsThereTile(topRight))
		{
			if (IsObstacle(topRight))
				return true;
		}
		if (IsThereTile(botRight))
		{
			if (IsObstacle(botRight))
				return true;
		}
		//can go through wall
		//vector2 topRight = m_collision_comp->GetCollisionBox_position(BOX_INDEX::TOP_RIGHT);
		//vector2 bottomRight = m_collision_comp->GetCollisionBox_position(BOX_INDEX::BOT_RIGHT);
		vector2 start = topRight + vector2(0, _Speed.y);
		vector2 end = botRight + vector2(0, _Speed.y);

		for (vector2 iter = start; iter.y > end.y; iter.y -= 10.0f)
		{
			if (IsThereTile(iter))
			{
				if (IsObstacle(iter))
				{
					//m_how_much_push_up = Round_Position(iter).y + 5.0f + m_collision_comp->GetBoxSize().y * 0.5f - m_collision_comp->GetCollisionOffset().y;
					return true;
				}
			}
		}

		return false;
	}
	if (Direction == LEFT)
	{
		vector2 topLeft = m_collision_comp->GetCollisionBox_position(BOX_INDEX::TOP_LEFT) + vector2(_Speed.x, 0);
		vector2 botLeft = m_collision_comp->GetCollisionBox_position(BOX_INDEX::BOT_LEFT) + vector2(_Speed.x, 0);

		if (IsThereTile(topLeft))
		{
			if (IsObstacle(topLeft))
				return true;
		}
		if (IsThereTile(botLeft))
		{
			if (IsObstacle(botLeft))
				return true;
		}
		//can go through wall
		vector2 start = topLeft + vector2(0, _Speed.y);
		vector2 end = botLeft + vector2(0, _Speed.y);

		for (vector2 iter = start; iter.y > end.y; iter.y -= 10.0f)
		{
			if (IsThereTile(iter))
			{
				if (IsObstacle(iter))
				{
					return true;
				}
			}
		}
		return false;
	}
	return false;
}

void Physics::SetSpeed(vector2 speed)
{
	Speed = speed;
}

void Physics::SetMaxFallingSpeed(float y)
{
	MaxFallingSpeed = y;
}

void Physics::SetGravityOn(bool gravity)
{
	IsGravityOn = gravity;
}

float Physics::GetHowMuchPush() const
{
	//return round(m_how_much_push_up*100000.0f)/100000.0f;
	return 0;
}

bool Physics::GetIsOnGround() const
{
	return IsOnGround;
}

void Physics::SetIsOnGround(bool isground)
{
	IsOnGround = isground;
}

vector2 Physics::GetSpeed() const
{
	return Speed;
}

bool Physics::IsOnOnWay() const
{
	return m_IsOnOnWay;
}

float Physics::GetGravity() const
{
	return Gravity;
}

//bool Physics::IsitOnGround() const
//{
//	return IsOnGround;
//}
//
//void Physics::SetSpeed(vector2 speed)
//{
//	Speed = speed;
//}
//
//void Physics::SetGravityOn(bool gravity)
//{
//	IsGravityOn = gravity;
//}

void Physics::Update(float dt)
{
	if(m_owner->HasParent())
	{
		return;
	}
	m_updateTime += dt;
	if (m_updateTime >= 0.5f)
	{
		m_updateTime = 0.5f;
	}

	if(m_shouldScaleOverTime)
	{
		m_owner->GetTransform()->SetScale(m_owner->GetTransform()->GetWorldScale() + m_scaleValue * m_updateTime);
		m_owner->GetTransform()->SetPosition(m_owner->GetTransform()->GetWorldPosition() +vector2(0.f, 0.5f* m_scaleValue.y * m_updateTime));
	}

	vector2 pos = m_owner->GetTransform()->GetWorldPosition();

	//Gravity = Gravity * dt;

	if (IsGravityOn || !IsOnGround)
	{
		Speed.y += Gravity * m_updateTime;
		Speed.y = (Speed.y > MaxFallingSpeed) ? Speed.y : MaxFallingSpeed;
	}

	if(m_IsIgnoreTile)
	{
		pos += Speed * m_updateTime;
		m_owner->GetTransform()->SetPosition(pos);
		m_updateTime = 0.f;

		return;
	}

	if (m_collision_comp != nullptr)
	{
		if (Speed.y < 0)
		{
			if (HasGround(Speed*m_updateTime) && !IsOnGround)
			{
				int playerYPosition = static_cast<int>(Round_Position(m_owner->GetTransform()->GetWorldPosition()+ Speed * m_updateTime).y);

				int proper_Y_position = static_cast<int>(m_owner->GetTransform()->GetWorldScale().y * m_collision_comp->GetCollisionBoxSize().y / 2.f)-5;

				if (playerYPosition % 10 != proper_Y_position)
				{
					playerYPosition += proper_Y_position - playerYPosition % 10;
				}
				if (abs(playerYPosition - pos.y) > 5.f)
				{
					playerYPosition += 10;
				}

				//m_owner->GetTransform()->SetPosition(m_owner->GetTransform()->GetWorldPosition() + vector2(0.f, 0.35* m_scaleValue.y * dt));
				pos = vector2(m_owner->GetTransform()->GetWorldPosition().x,static_cast<float>(playerYPosition));
				m_owner->GetTransform()->SetPosition(pos);
				Speed.y = 0;
				m_IsOnOnWay = HasGround_OneWay();
				IsOnGround = true;
			}
			else if(!HasGround(Speed*m_updateTime)&&IsOnGround)
			{
				IsOnGround = false;
			}
			if(IsOnGround)
			{
				Speed.y = 0;
				Speed.x *= 0.9f;
				m_IsOnOnWay = HasGround_OneWay();
			}
		}
		if (Speed.y > 0)
		{
			if (IsCeilingBlocked(Speed*m_updateTime))
			{
				Speed.y = 0;
			}
		}
		if (Speed.x > 0)
		{
			if (IsWallBlocked(Speed*m_updateTime, RIGHT))
			{
				Speed.x = 0;
			}
		}
		if (Speed.x < 0)
		{
			if (IsWallBlocked(Speed*m_updateTime, LEFT))
			{
				Speed.x = 0;
			}
		}
	}
	else
	{
		m_collision_comp = m_owner->GetComponentByTemplate<Collision>();
	}

	pos += Speed * m_updateTime;

	m_owner->GetTransform()->SetPosition(pos);
	m_updateTime = 0.f;
}

void Physics::Close(void)
{
}

void Physics::Set_TileMap(TileMap* tile)
{
	Tile_Map = tile;
}

void Physics::Set_Ignore_Tile(bool Boolean)
{
	m_IsIgnoreTile = Boolean;
}

//void Physics::KnockBackDemo()
//{
//	float x1 = 1;
//	float y1 = 0.5;//sin(-30.0) * 2;
//
//	if (Input::Is_Pressed(GLFW_KEY_LEFT_SHIFT))
//	{
//		Position.x = Position.x + x1;
//		Position.y = Position.y + y1;
//	}
//}

void Physics::SetIncreasingScaleValue(vector2 scaleValue)
{
	m_scaleValue = scaleValue;
}
