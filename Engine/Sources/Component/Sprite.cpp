/***********************************************************************
	File name		: Sprite.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Sprite.h"
#include <GL/glew.h>
#include "Object/Object.h"
#include "IMGUI/imgui.h"
#include "System/State.h"
#include "System/Input.h"
#include <iostream>
#include "Graphic/Character_sprite.h"

void Sprite::HandleAmimation(float dt)
{
	if ((m_oneloop_animation || m_AnimationAcitve) && m_texture != NULL)
    {
        m_innertime += dt;
        if (m_innertime >= 1.0f / m_texture->GetFrameSpeed(m_type))
        {
            ++m_currentframe;
            if (m_currentframe >= m_texture->GetFrameSize(m_type))
            {
				m_oneloop_animation = false;
                m_currentframe = 0;
            }
            m_innertime -= 1.0f / m_texture->GetFrameSpeed(m_type);
        }
    }
}

Sprite::Sprite(Object* owner_object, std::string component_name) : Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::SPRITE;
}

Sprite::~Sprite()
{
}

Color Sprite::GetColor()
{
    return m_color;
}

void Sprite::SetColor(Color temp_color)
{
    m_color = temp_color;
}

void Sprite::SetColorR(float r)
{
	m_color.color[0] = r;
}

void Sprite::SetColorG(float g)
{
	m_color.color[1] = g;
}

void Sprite::SetColorB(float b)
{
	m_color.color[2] = b;
}

void Sprite::SetColorA(float a)
{
	m_color.color[3] = a;
}

void Sprite::SetMesh(Mesh * temp_mesh)
{
    m_mesh = temp_mesh;
}

Mesh* Sprite::GetMesh()
{
    return m_mesh;
}

void Sprite::SetGraphicSystem(Graphic* graphic)
{
    m_graphic_system = graphic;
}

void Sprite::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
    writer.StartObject();
    writer.Key("Kind");
    writer.Int(kind);
    writer.Key("Name");
    writer.String(name.c_str());
    writer.Key("m_Visible");
    writer.Bool(m_Visible);
    writer.Key("TextureName");
    writer.String(m_TextureName->c_str());
    writer.Key("Red");
    writer.Int(static_cast<int>(m_color.color[0] * 255));
    writer.Key("Green");
    writer.Int(static_cast<int>(m_color.color[1] * 255));
    writer.Key("Blue");
    writer.Int(static_cast<int>(m_color.color[2] * 255));
    writer.Key("Alpha");
    writer.Int(static_cast<int>(m_color.color[3] * 255));
    writer.Key("m_FlipX");
    writer.Bool(m_FlipX);
    writer.Key("m_FlipY");
    writer.Bool(m_FlipY);
    writer.Key("AnimationActive");
    writer.Bool(m_AnimationAcitve);
    writer.Key("CurrentFrame");
    writer.Int(m_currentframe);
	writer.Key("HUD");
	writer.Bool(m_ishud);
    writer.EndObject();
}

void Sprite::Deserialization(const rapidjson::Value & val)
{
    m_Visible = val.FindMember("m_Visible")->value.GetBool();
    *m_TextureName = val.FindMember("TextureName")->value.GetString();
    m_color.color[0] = static_cast<float>(val.FindMember("Red")->value.GetInt()) / 255.f;
    m_color.color[1] = static_cast<float>(val.FindMember("Green")->value.GetInt()) / 255.f;
    m_color.color[2] = static_cast<float>(val.FindMember("Blue")->value.GetInt()) / 255.f;
    m_color.color[3] = static_cast<float>(val.FindMember("Alpha")->value.GetInt()) / 255.f;
    m_FlipX = val.FindMember("m_FlipX")->value.GetBool();
    m_FlipY = val.FindMember("m_FlipY")->value.GetBool();
    m_AnimationAcitve = val.FindMember("AnimationActive")->value.GetBool();
    m_currentframe = val.FindMember("CurrentFrame")->value.GetInt();
	m_ishud = val.FindMember("HUD")->value.GetBool();
}

void Sprite::ImGui_Setting()
{
	if (m_graphic_system == nullptr)
	{
		m_graphic_system = APP->Get_Graphic();
	}
    ImGui::Checkbox("m_Visible", &m_Visible);
    ImGui::ColorEdit4("Color", m_color.GetColor_Float());
    ImGui::Checkbox("m_FlipX", &m_FlipX);
    ImGui::Checkbox("m_FlipY", &m_FlipY);
    ImGui::Checkbox("AnimationActive", &m_AnimationAcitve);

    auto texture_list = m_graphic_system->GetTexture_Container();

    if (ImGui::BeginCombo("Spritesource", m_TextureName->c_str(), ImGuiComboFlags_None))
    {
        for (auto i : texture_list)
        {
            bool is_selected = (*m_TextureName == i.first);
            if (ImGui::Selectable(i.first.c_str(), is_selected))
            {
                SetTextureName(i.first);
                if (i.second == NULL)
                {
                    m_shader_type = SHADER_PROGRAM_TYPE::STANDARD_SHADER;
                    m_mesh->ClearVertices();
                    m_mesh->MakeByName(i.first);
                }
                else
                {
                    m_shader_type = SHADER_PROGRAM_TYPE::TEXTURE_SHADER;
                    m_mesh->ClearVertices();
                    m_mesh->MakeTexture();
                }
            }
            if (is_selected)
            {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }

	bool hud_temp = m_ishud;
	ImGui::Checkbox("HUD", &hud_temp);
	SetHud(hud_temp);
}

void Sprite::Init()
{
    m_graphic_system = APP->Get_Graphic();
    name = "Sprite";
    kind = COMPONENT_KIND::SPRITE;

    auto texture_list = m_graphic_system->GetTexture_Container();

    m_mesh = new Mesh();

    m_texture = m_graphic_system->GetTexture(*m_TextureName);
    if (!m_texture)
    {
        m_shader_type = SHADER_PROGRAM_TYPE::STANDARD_SHADER;
        m_mesh->MakeByName(*m_TextureName);
    }
    else
    {
        m_shader_type = SHADER_PROGRAM_TYPE::TEXTURE_SHADER;
        m_mesh->MakeTexture();
    }
}

void Sprite::Update(float dt)
{
    if (!m_Visible)
    {
        return;
    }

	if(m_owner->HasParent())
	{
		////2th sang-sok for player->hand->weapon
		//if (m_owner->GetParent()->HasParent())
		//{
		//	if (m_owner->GetParent()->GetParent()->GetComponentByTemplate<Sprite>())
		//	{
		//		if (m_FlipX != m_owner->GetParent()->GetParent()->GetComponentByTemplate<Sprite>()->GetFlipX())
		//		{
		//			//TODO:: Temporary solution for gun recoil
		//			this->m_FlipX = m_owner->GetParent()->GetParent()->GetComponentByTemplate<Sprite>()->GetFlipX();
		//			vector2 temp_pos = m_owner->GetTransform()->GetLocalPosition();
		//			temp_pos.x *= -1.f;
		//			m_owner->GetTransform()->SetPosition(temp_pos);
		//		}
		//	}
		//}
	}

	HandleAmimation(dt);

	float depth = this->m_owner->GetTransform()->GetDepth();

	if (depth > 1.0f || depth < -1.0f)
	{
		return;
	}

	//if (!m_ishud && !m_owner->GetState()->GetCamera(0)->IsDisplayed(*m_owner->GetTransform()))
	//{
	//	std::cout << "the name  : " << m_owner->GetName() << "disappear!" << std::endl;
	//	return;
	//}

	if(Character_Sprite* character_texture = dynamic_cast<Character_Sprite*>(m_texture); character_texture != nullptr)
	{
		DrawBehavior* draw = new DrawBehavior();
		float* pointer = m_color.GetColor_Float();
		for (int i = 0; i < 4; ++i)
		{
			draw->m_color[i] = pointer[i];
		}
		draw->m_hud = m_ishud;
		draw->m_size = m_mesh->GetSize();
		auto vertices = character_texture->CopyCoordinate(m_currentframe, m_type, m_FlipX, m_FlipY);
		draw->m_texture.assign(vertices.begin(), vertices.end());
		vertices.clear();
		draw->m_texture_id = character_texture->GetID();    
		vertices = m_mesh->GetVertices();
		draw->m_vertex.assign(vertices.begin(), vertices.end());
		vertices.clear();
		pointer = this->m_owner->GetTransform()->GetPointer();
		for (int i = 0; i < 9; ++i)
		{
			draw->m_transform[i] = pointer[i];
		}
		draw->m_rendertype = m_mesh->GetRenderType();
		draw->m_type = graphic_enum::DrawType::CHARACTER;
		m_graphic_system->m_drawlist.insert(std::make_pair(depth, draw));
		//m_graphic_system->m_drawlist[graphic_enum::DrawType::CHARACTER].insert(std::make_pair(depth, draw));
		return;
	}
	if(m_texture)
	{
		DrawBehavior* draw = new DrawBehavior();
		float* pointer = m_color.GetColor_Float();
		for (int i = 0; i < 4; ++i)
		{
			draw->m_color[i] = pointer[i];
		}
		draw->m_hud = m_ishud;
		draw->m_size = m_mesh->GetSize();
		auto vertices = m_texture->CopyCoordinate(m_currentframe, m_FlipX, m_FlipY);
		draw->m_texture.assign(vertices.begin(), vertices.end());
		vertices.clear();
		draw->m_texture_id = m_texture->GetID();
		vertices = m_mesh->GetVertices();
		draw->m_vertex.assign(vertices.begin(), vertices.end());
		vertices.clear();
		pointer = this->m_owner->GetTransform()->GetPointer();
		for (int i = 0; i < 9; ++i)
		{
			draw->m_transform[i] = pointer[i];
		}
		draw->m_rendertype = m_mesh->GetRenderType();
		draw->m_type = graphic_enum::DrawType::SPRITE;
		m_graphic_system->m_drawlist.insert(std::make_pair(depth, draw));
		//m_graphic_system->m_drawlist[graphic_enum::DrawType::SPRITE].insert(std::make_pair(depth, draw));
	}
	else
	{
		DrawBehavior* draw = new DrawBehavior();
		float* pointer = m_color.GetColor_Float();
		for (int i = 0; i < 4; ++i)
		{
			draw->m_color[i] = pointer[i];
		}
		draw->m_hud = m_ishud;
		draw->m_size = m_mesh->GetSize();
		auto vertices = m_mesh->GetVertices();
		draw->m_vertex.assign(vertices.begin(), vertices.end());
		vertices.clear();
		pointer = this->m_owner->GetTransform()->GetPointer();
		for (int i = 0; i < 9; ++i)
		{
			draw->m_transform[i] = pointer[i];
		}
		draw->m_rendertype = m_mesh->GetRenderType();
		draw->m_type = graphic_enum::DrawType::SOLID_SHAPE;
		m_graphic_system->m_drawlist.insert(std::make_pair(depth, draw));
		//m_graphic_system->m_drawlist[graphic_enum::DrawType::SOLID_SHAPE].insert(std::make_pair(depth, draw));
	}
}

void Sprite::Close(void)
{
    delete m_mesh;
	m_mesh = nullptr;

	delete m_TextureName;
}

int Sprite::GetCurrentFrame() const
{
    return m_currentframe;
}

std::string Sprite::GetTextureName() const
{
    return *m_TextureName;
}

void Sprite::SetTextureName(std::string texture_name)
{
    if (texture_name == *m_TextureName)
    {
        return;
    }
	m_currentframe = 0;
    *m_TextureName = texture_name;
	m_innertime = 0.0f;
    if (m_graphic_system)
    {
        m_texture = m_graphic_system->GetTexture(*m_TextureName);
		m_mesh->ClearVertices();
		if (!m_texture)
		{
			m_shader_type = SHADER_PROGRAM_TYPE::STANDARD_SHADER;
			m_mesh->MakeByName(*m_TextureName);
		}
		else
		{
			m_shader_type = SHADER_PROGRAM_TYPE::TEXTURE_SHADER;
			m_mesh->MakeTexture();
		}
    }
}

bool Sprite::GetFlipX() const
{
    return m_FlipX;
}

bool Sprite::GetFlipY() const
{
    return m_FlipY;
}

void Sprite::SetFlipX(bool input)
{
    m_FlipX = input;
}

void Sprite::SetFlipY(bool input)
{
    m_FlipY = input;
}

void Sprite::TurnOnOffAnimation(bool input)
{
	m_AnimationAcitve = input;
}

void Sprite::SetCurrentFrame(int frame)
{
	m_currentframe = frame;
}

Texture* Sprite::GetTexture() const
{
    return m_texture;
}

void Sprite::SetType(int type)
{
	if(m_type == type)
	{
		return;
	}
	m_type = type;
	m_currentframe = 0;
}

bool Sprite::IsHud() const
{
	return m_ishud;
}

void Sprite::SetHud(bool hud)
{
	m_ishud = hud;
}

void Sprite::SetAnimationActive(bool active)
{
	m_AnimationAcitve = active;
}
