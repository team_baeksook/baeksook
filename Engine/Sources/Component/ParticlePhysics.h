/***********************************************************************
	File name		: ParticlePhysics.h
	Project name	: WFPT
	Author			: HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include "Math/vector2.hpp"
#include "Component/Component.h"

class ParticlePhysics : public Component
{
public:
	//ParticlePhysics(Object * owner_object, const std::string& component_name);
	//void initialize();
	ParticlePhysics(Object* owner_object, const std::string& component_name = "ParticlePhysics");
	~ParticlePhysics() override;

	void Init() override;
	void Update(float dt) override;
	void Close() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer) override;
	void Deserialization(const rapidjson::Value& val) override;
	void ImGui_Setting() override;

	void integrate(float duration);

	void setMass(const float _mass);
	float getMass()const;

	void setInvereseMass(const float _inverseMass);
	float getInverseMass()const;
	bool hasFiniteMass()const;

	void setDamping(const float _damping);
	float getDamping()const;

	void setPosition(const vector2 &_position);
	void setPosition(const float x, const float y);
	void getPosition(vector2* position);
	vector2 getPosition() const;

	void setVelocity(const vector2 &_velocity);
	void setVelocity(const float _x, const float _y);
	void setVelocity(const vector3 &_velocity);
	void getVelocity(vector2 *velocity);
	vector2 getVelocity() const;

	void AddVelocity(const vector2& vel);

	void setAcceleration(const vector2 &acceleration);
	void setAcceleration(const float x, const float y);
	void getAcceleration(vector2* acceleration) const;
	vector2 getAcceleration() const;

	void clearAccumulator();

	void FireDemo(float dt);
private:
	float inversemass;
	float damping;
	vector2& position;
	vector2 velocity;
	vector2 forceAccumulated;
	vector2 acceleration;
};
