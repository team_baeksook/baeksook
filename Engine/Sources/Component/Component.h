/***********************************************************************
	File name		: Component.h
	Project name	: WFPT
	Author			: SeongWook Shin

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include "rapidjson/prettywriter.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"
#include <vector>

enum COMPONENT_KIND
{
	NOT_EXIST = -1,
	SPRITE = 0,
	PHYSICS = 1,
	SPRITE_TEXT = 2,
	CAMERA = 3,
	PARTICLEPHYSICS = 4,
	COLLISION = 5,
	PLAYER = 6,
	BULLET = 7,
	REVOLVER = 8,
	RIFLE = 9,
	TOMAHAWK = 10,
	PARTICLEGENERATOR = 11,
	SHOTGUN = 12,
	BOW = 13,
	GRENADE = 14,
	GUIDEDMISSILE=15,
	BUTTON = 16,
	SNIPER = 17,
	FLAMETHROWER = 18,
	WEAPONGENERATOR = 19,
	REDZONE = 20,
	LASER = 21,
	PISTOL=22,
	LMG=23,
	MAGNETO = 24,
	CLEARPORTAL=25,
	END_OF_KIND
};

class Object;

class Component
{
public:
    virtual void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) = 0;
    virtual void Deserialization(const rapidjson::Value&) = 0;

    virtual void ImGui_Setting() = 0;

    Component(Object* owner_object, std::string component_name) : m_owner(owner_object), name(component_name) {}
	virtual ~Component() {}
    virtual void Init() = 0;
    virtual void Update(float dt) = 0;
    virtual void Close(void) = 0;

    std::string Get_Name() { return name; }
    COMPONENT_KIND Get_Kind() { return kind; }

	Object* GetOwner() const { return m_owner; }

	bool ShouldUpdate() const {	return m_shouldupdate; }
	void SetUpdate(bool TF) { m_shouldupdate = TF; }
protected:
    Object* m_owner = nullptr;
    std::string name;

    COMPONENT_KIND kind = NOT_EXIST;
private:
	bool m_shouldupdate = true;
};
