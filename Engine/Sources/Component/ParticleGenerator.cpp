#include "ParticleGenerator.h"
#include "Object/Object.h"
#include <iostream>
#include "System/State.h"
#include "Graphic/Particle.h"
#include <IMGUI/imgui.h>
#include "Graphic/Texture.h"
#include "Math/Ease.h"

ParticleGenerator::ParticleGenerator(Object* owner_object, const std::string& component_name): Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::PARTICLEGENERATOR;
}

ParticleGenerator::~ParticleGenerator() {}

void ParticleGenerator::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.Key("Duration");
	writer.Double(m_duration);
	writer.Key("Duration_vary");
	writer.Double(m_duration_vary);
	writer.Key("Size_x");
	writer.Double(m_size.x);
	writer.Key("Size_y");
	writer.Double(m_size.y);
	writer.Key("Size_vary_x");
	writer.Double(m_size_vary.x);
	writer.Key("Size_vary_y");
	writer.Double(m_size_vary.y);
	writer.Key("Start_velocity_X");
	writer.Double(m_start_vel.x);
	writer.Key("Start_velocity_Y");
	writer.Double(m_start_vel.y);
	writer.Key("Velocity_direction_vary");
	writer.Double(m_vel_dir_vel);
	writer.Key("Velocity_length_vary");
	writer.Double(m_vel_len_vel);
	writer.Key("Texture_Name");
	writer.String(m_texture_name.c_str());
	writer.Key("Pause");
	writer.Bool(m_pause);
	writer.Key("Colorstep");
	writer.Double(m_colorstep);
	writer.Key("ScaleStep_x");
	writer.Double(m_scalestep.x);
	writer.Key("ScaleStep_y");
	writer.Double(m_scalestep.y);
	writer.Key("Red");
	writer.Int(static_cast<int>(m_color.color[0] * 255));
	writer.Key("Green");
	writer.Int(static_cast<int>(m_color.color[1] * 255));
	writer.Key("Blue");
	writer.Int(static_cast<int>(m_color.color[2] * 255));
	writer.Key("Alpha");
	writer.Int(static_cast<int>(m_color.color[3] * 255));
	writer.EndObject();
}

void ParticleGenerator::Deserialization(const rapidjson::Value& val)
{
	m_duration = val.FindMember("Duration")->value.GetFloat();
	m_duration_vary = val.FindMember("Duration_vary")->value.GetFloat();

 	m_size.x = val.FindMember("Size_x")->value.GetFloat();
	m_size.y = val.FindMember("Size_y")->value.GetFloat();
	m_size_vary.x = val.FindMember("Size_vary_x")->value.GetFloat();
	m_size_vary.y = val.FindMember("Size_vary_y")->value.GetFloat();

	m_start_vel.x = val.FindMember("Start_velocity_X")->value.GetFloat();
	m_start_vel.y = val.FindMember("Start_velocity_Y")->value.GetFloat();

	m_vel_dir_vel = val.FindMember("Velocity_direction_vary")->value.GetFloat();
	m_vel_len_vel = val.FindMember("Velocity_length_vary")->value.GetFloat();

	m_texture_name = val.FindMember("Texture_Name")->value.GetString();
	m_pause = val.FindMember("Pause")->value.GetBool();

	m_colorstep = val.FindMember("Colorstep")->value.GetFloat();

	m_scalestep.x = val.FindMember("ScaleStep_x")->value.GetFloat();
	m_scalestep.y = val.FindMember("ScaleStep_y")->value.GetFloat();

	m_color.color[0] = static_cast<float>(val.FindMember("Red")->value.GetInt()) / 255.f;
	m_color.color[1] = static_cast<float>(val.FindMember("Green")->value.GetInt()) / 255.f;
	m_color.color[2] = static_cast<float>(val.FindMember("Blue")->value.GetInt()) / 255.f;
	m_color.color[3] = static_cast<float>(val.FindMember("Alpha")->value.GetInt()) / 255.f;
}

void ParticleGenerator::ImGui_Setting()
{
	if (ImGui::BeginCombo("Texture", m_texture_name.c_str(), ImGuiComboFlags_None))
	{
		auto texture_list = m_graphic_system->GetTexture_Container();
		for (auto i : texture_list)
		{
			if (i.second == nullptr)
			{
				continue;
			}
			bool is_selected = (m_texture_name == i.first);
			if (ImGui::Selectable(i.first.c_str(), is_selected))
			{
				m_texture_name = i.first;
				m_texture_id = i.second->GetID();
			}
			if (is_selected)
			{
				ImGui::SetItemDefaultFocus();
			}
		}
		ImGui::EndCombo();
	}
	ImGui::InputInt("NumberControl", &m_NumberControl);

	ImGui::InputFloat("Duration", &m_duration);
	ImGui::InputFloat("Duration_vary", &m_duration_vary);

	ImGui::InputFloat2("Size", &m_size.x);
	ImGui::InputFloat2("Size_vary", &m_size_vary.x);

	ImGui::InputFloat2("Start_velocity", &m_start_vel.x);
	ImGui::InputFloat("Velocity_dir_vary", &m_vel_dir_vel);
	ImGui::InputFloat("Velocity_length_vary", &m_vel_len_vel);

	//ImGui::InputFloat4("VelocityEaseOut", &)
	//ImGui::InputFloat("EndVelocity", &m_endVelocityForEese);
	ImGui::InputFloat("Ease Value", &m_easeValue);
	ImGui::Checkbox("Eese Switch", &m_ease);

	ImGui::InputFloat("Rotate Value", &m_rotvalue);
	ImGui::InputFloat("Rotate_vary", &m_rot_vary);

	ImGui::ColorEdit4("Color", m_color.GetColor_Float());

	ImGui::InputFloat("Colorstep", &m_colorstep);
	ImGui::InputFloat2("Scale step", &m_scalestep.x);

	ImGui::Checkbox("Pause", &m_pause);
}

void ParticleGenerator::Init()
{
	m_graphic_system = this->m_owner->GetState()->GetGraphicSystem();
	m_texture_id = m_graphic_system->GetTexture(m_texture_name)->GetID();
}

void ParticleGenerator::Update(float dt)
{
	if (!m_pause)
	{
		m_counter += dt;
	}

	for (auto particle : m_particles)
	{
		particle->Draw(dt, m_graphic_system);
		if (particle->IsDead() && !m_pause)
		{
			float duration = GetRandomNumber(-m_duration_vary, m_duration_vary);
			particle->SetDuration(duration + m_duration);
			Transform* transform = particle->GetTransform();
			Transform* owner_transform = m_owner->GetTransform();
			transform->SetPosition(owner_transform->GetWorldPosition());
			transform->SetDepth(owner_transform->GetDepth());

			vector2 size = vector2(GetRandomNumber(-m_size_vary.x, m_size_vary.x),
				GetRandomNumber(-m_size_vary.y, m_size_vary.y));
			transform->SetScale(size + m_size);
			particle->SetTransform(transform);

			float rotate = GetRandomNumber(-m_rot_vary, m_rot_vary);
			rotate += m_rotvalue;
			particle->SetRoate(rotate);

			float dir = GetRandomNumber(-m_vel_dir_vel, m_vel_dir_vel);
			matrix2 rot = Rotation_Matrix_Degree(dir);
			vector2 vel = rot * m_start_vel;
			float scalar = GetRandomNumber(1 - m_vel_len_vel, 1 + m_vel_len_vel);
			vel *= scalar;

			if(!m_ease)
			{
				particle->SetParticleEasingSwitch(false);
				particle->Setvelocity(vel);	
			}
			if(m_ease)
			{
				particle->SetParticleEasingSwitch(true);
				particle->Setvelocity(vel);
			}

			particle->SetColor(m_color);

			particle->SetColorStep(m_colorstep);
			particle->SetScaleStep(m_scalestep);

			particle->SetNew();
		}
	}

	if (m_counter > m_gentime)
	{
		if (!m_pause && m_particles.size() < m_NumberControl)
		{
			Particle* particle = new Particle();
			particle->SetID(m_texture_id);

			float duration = GetRandomNumber(-m_duration_vary, m_duration_vary);
			particle->SetDuration(duration + m_duration);
			Transform* transform = new Transform();
			Transform* owner_transform = m_owner->GetTransform();
			transform->SetPosition(owner_transform->GetWorldPosition());
			transform->SetDepth(owner_transform->GetDepth());

			vector2 size = vector2(GetRandomNumber(-m_size_vary.x, m_size_vary.x),
				GetRandomNumber(-m_size_vary.y, m_size_vary.y));
			transform->SetScale(size + m_size);
			particle->SetTransform(transform);

			float rotate = GetRandomNumber(-m_rot_vary, m_rot_vary);
			rotate += m_rotvalue;
			particle->SetRoate(rotate);

			float dir = GetRandomNumber(-m_vel_dir_vel, m_vel_dir_vel);
			matrix2 rot = Rotation_Matrix_Degree(dir);
			vector2 vel = rot * m_start_vel;
			float scalar = GetRandomNumber(1 - m_vel_len_vel, 1 + m_vel_len_vel);
			vel *= scalar;

			if (!m_ease)
			{
				particle->SetParticleEasingSwitch(false);
				particle->Setvelocity(vel);
			}
			if (m_ease)
			{
				particle->SetParticleEasingSwitch(true);
				particle->Setvelocity(vel);
			}

			particle->SetColor(m_color);

			particle->SetColorStep(m_colorstep);
			particle->SetScaleStep(m_scalestep);

			m_particles.push_back(particle);
		}
	}

	if(m_timeForEase >= m_duration)
	{
		m_timeForEase = 0.f;
	}
}

void ParticleGenerator::Close()
{
	for (auto& particle : m_particles)
	{
		particle->Close();
		delete particle;
	}
	m_particles.clear();
}

void ParticleGenerator::SetPause(bool pause)
{
	m_pause = pause;
}
