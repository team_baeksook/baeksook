/***********************************************************************
	File name		: Collision.cpp
	Project name	: WFPT
	Author			: HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#include "Object/Object.h"
#include "Collision.h"
#include "Component/Sprite.h"
#include "Component/Camera.h"
#include "System/State.h"
#include "System/ObjectManager.h"
#include "System/Logger.h"
#include "System/Engine_Utility.h"
#include <IMGUI/imgui.h>
#include <iostream>
#include "System/Imgui_app.h"

/*Helper Functions*/

namespace
{
	float projectedPoint(vector2 point, vector2 slope)
	{
		return (point.x * slope.x) + (point.y * slope.y);
	}
	vector2 slope(vector2 P1, vector2 P2)
	{
		return vector2((P2.x - P1.x), (P2.y - P1.y));
	}
}

std::string CollisionHelper::ConvertCollisionGroupID(COLLISION_GROUP collision_group)
{
	switch (collision_group)
	{
	case COLLISION_GROUP::TUTORIAL:
		return "Tutorial";
	case COLLISION_GROUP::BULLET:
		return "Bullet";
	case COLLISION_GROUP::PLAYER:
		return "Player";
	case COLLISION_GROUP::ITEM:
		return "Item";
	case COLLISION_GROUP::WALL:
		return "Wall";
	case COLLISION_GROUP::MAX:
		return "Max";
	default:
		logger::log_handler.PrintLogOnConsoleAndFile(
			utility::MakeStringUsingFormat("The Collision Group {} does not exist!", static_cast<unsigned int>(collision_group)),
			spdlog::level::level_enum::err);
	}

	return "ERROR";
}

COLLISION_GROUP CollisionHelper::ConvertCollisionGroupString(std::string str)
{
	if (str == "Tutorial")
	{
		return COLLISION_GROUP::TUTORIAL;
	}
	if(str == "Bullet")
	{
		return COLLISION_GROUP::BULLET;
	}
	if(str == "Player")
	{
		return COLLISION_GROUP::PLAYER;
	}
	if(str == "Item")
	{
		return COLLISION_GROUP::ITEM;
	}
	if(str == "Wall")
	{
		return COLLISION_GROUP::WALL;
	}
	if (str == "Max")
	{
		return COLLISION_GROUP::MAX;
	}

	logger::log_handler.PrintLogOnConsoleAndFile(
		utility::MakeStringUsingFormat("The string {} is not a collision group!", str),
		spdlog::level::level_enum::err);
	return COLLISION_GROUP::MAX;
}

bool CollisionHelper::AABBandLineColision(Collision & box, vector2 start_point, vector2 end_point)
{
	//up left down right 
	vector2 left_top = box.GetCollisionBox_position(BOX_INDEX::TOP_LEFT);
	vector2 right_top = box.GetCollisionBox_position(BOX_INDEX::TOP_RIGHT);
	vector2 right_bot = box.GetCollisionBox_position(BOX_INDEX::BOT_RIGHT);
	vector2 left_bot = box.GetCollisionBox_position(BOX_INDEX::BOT_LEFT);

	//up edge case left_top->right top
	float denominator = ((end_point.x - start_point.x) * (right_top.y - left_top.y)) - ((end_point.y - start_point.y) * (right_top.x - left_top.x));
	float numerator1 = ((start_point.y - left_top.y) * (right_top.x - left_top.x)) - ((start_point.x - left_top.x) * (right_top.y - left_top.y));
	float numerator2 = ((start_point.y - left_top.y) * (end_point.x - start_point.x)) - ((start_point.x - left_top.x) * (end_point.y - start_point.y));

	// Detect coincident lines
	if (denominator == 0)
		return numerator1 == 0 && numerator2 == 0;

	float r = numerator1 / denominator;
	float s = numerator2 / denominator;

	if ((r >= 0 && r <= 1) && (s >= 0 && s <= 1))
	{
		return true;
	}

	//right edge case right_top->right bot
	denominator = ((end_point.x - start_point.x) * (right_bot.y - right_top.y)) - ((end_point.y - start_point.y) * (right_bot.x - right_top.x));
	numerator1 = ((start_point.y - right_top.y) * (right_bot.y - right_top.x)) - ((start_point.x - right_top.x) * (right_bot.y - right_top.y));
	numerator2 = ((start_point.y - right_top.y) * (end_point.x - start_point.x)) - ((start_point.x - right_top.x) * (end_point.y - start_point.y));

	// Detect coincident lines (has a problem, read below)
	if (denominator == 0)
		return numerator1 == 0 && numerator2 == 0;

	r = numerator1 / denominator;
	s = numerator2 / denominator;

	if ((r >= 0 && r <= 1) && (s >= 0 && s <= 1))
	{
		return true;
	}

	//down edge case right_bot->left_bot
	denominator = ((end_point.x - start_point.x) * (left_bot.y - right_bot.y)) - ((end_point.y - start_point.y) * (left_bot.x - right_bot.x));
	numerator1 = ((start_point.y - right_bot.y) * (left_bot.x - right_bot.x)) - ((start_point.x - right_bot.x) * (left_bot.y - right_bot.y));
	numerator2 = ((start_point.y - right_bot.y) * (end_point.x - start_point.x)) - ((start_point.x - right_bot.x) * (end_point.y - start_point.y));

	// Detect coincident lines (has a problem, read below)
	if (denominator == 0)
		return numerator1 == 0 && numerator2 == 0;

	r = numerator1 / denominator;
	s = numerator2 / denominator;

	if ((r >= 0 && r <= 1) && (s >= 0 && s <= 1))
	{
		return true;
	}

	//left edge case left_bot->left top
	denominator = ((end_point.x - start_point.x) * (left_top.y - left_bot.y)) - ((end_point.y - start_point.y) * (left_top.x - left_bot.x));
	numerator1 = ((start_point.y - left_bot.y) * (left_top.x - left_bot.x)) - ((start_point.x - left_bot.x) * (left_top.y - left_bot.y));
	numerator2 = ((start_point.y - left_bot.y) * (end_point.x - start_point.x)) - ((start_point.x - left_bot.x) * (end_point.y - start_point.y));

	// Detect coincident lines (has a problem, read below)
	if (denominator == 0)
		return numerator1 == 0 && numerator2 == 0;

	r = numerator1 / denominator;
	s = numerator2 / denominator;

	if ((r >= 0 && r <= 1) && (s >= 0 && s <= 1))
	{
		return true;
	}
	return false;
}

bool CollisionHelper::AABBandwideLineCollision(const Collision & box, vector2 start_point, vector2 end_point, float width)
{
	float dist = Distance_between_line_rect(start_point, end_point, box.GetCollisionBox_position(BOX_INDEX::BOT_LEFT), 
		box.GetCollisionBox_position(BOX_INDEX::BOT_RIGHT), box.GetCollisionBox_position(BOX_INDEX::TOP_LEFT),
		box.GetCollisionBox_position(BOX_INDEX::TOP_RIGHT));

	return width > dist;
}

Collision::Collision(Object* owner_object, const std::string& component_name): Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::COLLISION;
} 

/*SAT Collision Test*/
bool Collision::IsColliding(Object* obj)
{
	//TODO
	//1. Get edge
	//2. find its normal vector
	//3. project each vertex onn edge's normal vector
	//4. project vertex min&max and see if they overlap
	//5. if all edge overlap, than it collides


	affine2d trans2 = this->m_owner->GetTransform()->BuildTransformMatrix();
	affine2d trans1 = obj->GetTransform()->BuildTransformMatrix();

	vector2 vertices1[4] = {
	vector2(-0.5f, 0.5f), vector2(0.5f, 0.5f),
	vector2(0.5f, -0.5f), vector2(-0.5f, -0.5f) };
	vector2 vertices2[4] = {
		vector2(-0.5f, 0.5f), vector2(0.5f, 0.5f),
		vector2(0.5f, -0.5f), vector2(-0.5f, -0.5f) };

	for(unsigned int i=0; i < 4; ++i)
	{
		vertices1[i] = (trans1 * vector3(vertices1[i], 1.0f)).GetVector2();
		vertices2[i] = (trans2 * vector3(vertices2[i], 1.0f)).GetVector2();
	}
	/*Which m_vertices are corners //Please DO NOT erase this for future reference
	 *	1/4***********5    10
	 *	*  *		  *		32
	 *	*      *	  *
	 *	*          *  *
	 *	0*************2/3
	 *	v1(1) v1(5)
	 *	v1(0) v1(2)
	 */

	auto box1_point = vertices1;
	auto box2_point= vertices2;
	
	float box1_min = 0;
	float box1_max = 0;
	float box2_min = 0;
	float box2_max = 0;

	vector2 normal;
	vector2 lineSlope;

	for (int i = 0; i < 8; i++)
	{
		box1_min = 0;
		box1_max = 0;
		box2_min = 0;
		box2_max = 0;

		if (i < 4)
		{
			int q = i + 1;
			if (q >= 4)
				q = 0;

			lineSlope = slope(box1_point[i], box1_point[q]);
			normal = vector2(-lineSlope.y, lineSlope.x);
		}
		else
		{
			int q = i - 4 + 1;
			if (q >= 4)
				q = 0;
			lineSlope = slope(box2_point[i - 4], box2_point[q]);
			normal = vector2(-lineSlope.y, lineSlope.x);
		}

		box1_max = projectedPoint(box1_point[0], normal);
		box1_min = projectedPoint(box1_point[0], normal);

		box2_max = projectedPoint(box2_point[0], normal);
		box2_min = projectedPoint(box2_point[0], normal);

		for (int p = 0; p < 8; p++)
		{
			if (p < 4)
			{
				if (projectedPoint(box1_point[p], normal) > box1_max)
				{
					box1_max = projectedPoint(box1_point[p], normal);
				}

				else if (projectedPoint(box1_point[p], normal) < box1_min)
				{
					box1_min = projectedPoint(box1_point[p], normal);
				}
			}

			else
			{
				if (projectedPoint(box2_point[p - 4], normal) > box2_max)
				{
					box2_max = projectedPoint(box2_point[p - 4], normal);
				}

				else if (projectedPoint(box2_point[p - 4], normal) < box2_min)
				{
					box2_min = projectedPoint(box2_point[p - 4], normal);
				}
			}
		}

		if (box2_min > box1_max || box1_min > box2_max)
		{
			

			return false;
		}

	}
	//If there is collision


	return true;
}

void Collision::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.Key("Size_x");//It's half size of x
	writer.Double(m_collisionsize.x);
	writer.Key("Size_y");//It's half size of x
	writer.Double(m_collisionsize.y);
	writer.Key("Offset_x");//It's half size of x
	writer.Double(m_collisionoffset.x);
	writer.Key("Offset_y");//It's half size of x
	writer.Double(m_collisionoffset.y);
	writer.Key("Collision_Group");
	writer.String(CollisionHelper::ConvertCollisionGroupID(m_collision_group).c_str());
	writer.EndObject();
}

void Collision::Deserialization(const rapidjson::Value& val)
{
	m_collisionsize.x = val.FindMember("Size_x")->value.GetFloat();
	m_collisionsize.y = val.FindMember("Size_y")->value.GetFloat();

	m_collisionoffset.x = val.FindMember("Offset_x")->value.GetFloat();
	m_collisionoffset.y = val.FindMember("Offset_y")->value.GetFloat();

	m_collision_group = CollisionHelper::ConvertCollisionGroupString(val.FindMember("Collision_Group")->value.GetString());

	SetCollisionBoxSize(m_collisionsize);
}

void Collision::ImGui_Setting()
{
	vector2 size = m_collisionsize;
	ImGui::InputFloat2("CollisionBoxSize", &size.x);
	SetCollisionBoxSize(size);
	ImGui::InputFloat2("CollisionBoxOffset", &m_collisionoffset.x);
	ImGui::InputInt("CollisionGroup", (int*)&m_collision_group);
}

void Collision::Init()
{
	name = "Collision";
	kind = COMPONENT_KIND::COLLISION;
	m_graphic_system = APP->Get_Graphic();
}

void Collision::Update(float /*dt*/)
{
	m_collisionlist.clear();

	auto& objcontainer = this->m_owner->GetState()->GetObjectManager()->GetContainer();
	for(auto& obj : objcontainer)
	{
		Object* object = obj.second.get();
		Collision* collision = object->GetComponentByTemplate<Collision>();
		if(collision == nullptr)
		{
			continue;
		}
		if(this->m_owner->GetID() == object->GetID())
		{
			continue;
		}
 		COLLISION_GROUP group = collision->m_collision_group;

		if(!ShouldCheck(group))
		{
			continue;
		}

		//if it is a ignore list
		if (std::find_if(m_ignorelist.begin(), m_ignorelist.end(), [&](Object* obj)
		{
			if (obj->GetID() == object->GetID())
			{
				return true;
			}
			return false;
		}) != m_ignorelist.end())
		{
			continue;
		}
		if(this->IsAABB(object))
		{
			m_collisionlist.push_back(object);
		}
	}

	if(IMGUIAPP->GetIsCollisionDraw())
	{
		if(m_graphic_system)
		{
			Display_collisionbox();
		}
	}
}

void Collision::Close()
{
	m_collisionlist.clear();
	m_ignorelist.clear();
	m_graphic_system = nullptr;
}

Collision::~Collision()
{
	m_collisionlist.clear();
	m_ignorelist.clear();
	m_graphic_system = nullptr;
}

void Collision::SetCollisionBoxSize(vector2 size)
{
	m_collisionsize = size;
	vector2 half_size = size / 2;
	m_collisionBox[0] = vector2(-half_size.x, half_size.y);
	m_collisionBox[1] = vector2(half_size.x, half_size.y);
	m_collisionBox[2] = vector2(half_size.x, -half_size.y);
	m_collisionBox[3] = vector2(-half_size.x, -half_size.y);
}

vector2 Collision::GetCollisionBoxSize() const
{
	return m_collisionsize;
}

Object* Collision::FindCollidingObjectByName(std::string find_name)
{
	for(auto& object : m_collisionlist)
	{
		if(object->GetName() == find_name)
		{
			return object;
		}
	}
	return nullptr;
}

bool Collision::IsInIgnorelist(Object* ptr)
{
	for (auto& i : m_ignorelist)
	{
		if (i == ptr)
		{
			return true;
		}
	}
	return false;
}

std::vector<Object*>& Collision::GetCollidingObjects()
{
	return m_collisionlist;
}

std::vector<Object*> Collision::GetIgnoreList()
{
	return m_ignorelist;
}

void Collision::AddIgnoreList(Object* obj)
{
	m_ignorelist.push_back(obj);
}

void Collision::SetCollisionGroup(COLLISION_GROUP group)
{
	m_collision_group = group;
}

COLLISION_GROUP Collision::GetCollisionGroup() const
{
	return m_collision_group;
}

vector2 Collision::GetCollisionBox_position(BOX_INDEX index) const
{
	vector2 center = m_owner->GetTransform()->GetWorldPosition() + m_collisionoffset;
	vector2 scale = m_owner->GetTransform()->GetWorldScale();
	vector2 tempbox = m_collisionBox[static_cast<int>(index)].Scale_the_vector(scale);
	return tempbox + center;
}

vector2 Collision::GetBoxSize() const
{
	return m_collisionsize.Scale_the_vector(m_owner->GetTransform()->GetWorldScale());
}

vector2 Collision::GetCollisionOffset()
{
	return m_collisionoffset;
}

void Collision::Display_collisionbox() const
{
	vector2 center = m_owner->GetTransform()->GetWorldPosition() + m_collisionoffset;
	vector2 scale = m_owner->GetTransform()->GetWorldScale();
	vector2 tempbox[4];
	for(int i = 0; i <4 ; ++i)
	{
		tempbox[i] = m_collisionBox[i].Scale_the_vector(scale);
	}
	for (int i = 0; i < 4; ++i)
	{
		int next = (i == 3) ? 0 : i + 1;
		m_graphic_system->DrawLine(tempbox[i] + center, tempbox[next] + center, Color(255, 0, 0, 255));
	}
}

vector2 Collision::GetSize() const
{
	return m_collisionsize;
}

bool Collision::ShouldCheck(const COLLISION_GROUP target)
{
	int left_int = static_cast<int>(m_collision_group);
	int right_int = static_cast<int>(target);

	if (left_int == right_int)
	{
		if (((left_int & right_int) & 1) == 0)
		{
			return false;
		}
	}

	left_int = left_int >> 1;
	right_int = right_int >> 1;

	int result = left_int & right_int;

	if (result == 0)
	{
		return false;
	}

	return true;
}

/*
 * check if distance between two objects is smaller than sum of their width/height
 */
bool Collision::IsAABB(Object* object) const
{
	if (this->m_owner->GetComponentByTemplate<Camera>() != nullptr
		|| object->GetComponentByTemplate<Camera>() != nullptr)
		return false;

	Collision* targetcollision = object->GetComponentByTemplate<Collision>();

	if(targetcollision == nullptr)
	{
		return false;
	}

	vector2 mypos = m_owner->GetTransform()->GetWorldPosition() + m_collisionoffset;
	vector2 targetpos = object->GetTransform()->GetWorldPosition() + targetcollision->m_collisionoffset;
	vector2 diff = mypos - targetpos;

	if( abs(diff.x) > 0.5*(this->m_owner->GetTransform()->m_Scale.x * m_collisionsize.x + 
		object->GetTransform()->m_Scale.x * targetcollision->GetSize().x) )
	{
		return false;
	}
	if (abs(diff.y) > 0.5*(this->m_owner->GetTransform()->m_Scale.y * m_collisionsize.y + 
		object->GetTransform()->m_Scale.y * targetcollision->GetSize().y))
	{
		return false;
	}

	return true;
}

