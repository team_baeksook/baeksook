/***********************************************************************
	File name		: Player.cpp
	Project name	: WFPT
	Author			: MinSuk Kim, HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#include "Player.h"
#include "Object/Object.h"
#include "Sprite.h"
#include "System/Logger.h"
#include "System/State.h"
#include "System/Input.h"
#include "Physics.h"
#include "ParticlePhysics.h"
#include "Weapon/Gun.h"
#include "System/ObjectManager.h"
#include "Math/Ease.h"
#include <IMGUI/imgui.h>
#include <iostream>
#include "Weapon/Bullet.h"
#include "Component/ParticleGenerator.h"
#include "Weapon/ChargeWeapon.h"
#include "Weapon/Sniper.h"
#include "Math/Ease.h"
#include "Component/SpriteText.h"

//for Vibration
#include <windows.h>
#include <XInput.h>     // XInput API
#pragma comment(lib, "XInput.lib")

void Player::SetZeroKey()
{
	Jump = false;
	DoubleJump = false;
	Right = false;
	Down = false;
	Left = false;
	Up = false;
	m_fire = false;
}

void Player::Respawn(float /*dt*/)
{
	//if (m_respawnsound == false)
	//{
	//	m_respawnsound = true;
	//	m_respawn.Play_Audio(false);
	//	m_playerhud->GetComponentByTemplate<Sprite>()->SetTextureName(m_hudpngbasename);
	//}
}

void Player::Respawn_Player()
{
	float xpos = GetRandomNumber(-100.0f, 100.0f);
	this->m_owner->GetTransform()->SetPosition(vector2(xpos, 200.0f));
	Speed = vector2(0.f, 0.f);
	m_current_status = PLAYER_STATUS::NORMAL;
	m_particle_comp->SetPause(false);
	auto children = m_hand->GetChildren();
	for (auto child : children)
	{
        //TODO::Sibal..
        if (child->GetName() == "Crosshair")
        {
            continue;
        }
		child->Detach(true);
		child->MakeDead();
	}

	m_event_manager.AddEvent(&Player::Respawn_Event);
	m_respawnsound=false;
}

bool Player::Respawn_Event(float t)
{
	m_healthbar->GetTransform()->SetScale(vector2(0.8f * (m_healthpoint / 100.0f), 0.1f));
	
	if (m_respawnsound == false)
	{
		CurrentState = STAND;
		m_physics_comp->SetSpeed(0.f);
		m_respawnsound = true;
		m_respawn->Play_Audio(false);
		m_playerhud->GetComponentByTemplate<Sprite>()->SetTextureName(m_hudpngbasename);
	}
	m_isblink = true;


	if (t < 1.5f)
	{
		SetZeroKey();
	}

	if (t > 2.0f)
	{
		//m_isinvincible = true;
		m_event_manager.AddEvent(&Player::Invincible_Event);

		m_particle_comp->SetPause(true);
		return true;
	}

	return false;
}

bool Player::Revive_Event(float /*t*/)
{
	float xpos = GetRandomNumber(-100.0f, 100.0f);
	this->m_owner->GetTransform()->SetPosition(vector2(xpos, 200.0f));
	Speed = vector2(0.f, 0.f);
	m_current_status = PLAYER_STATUS::NORMAL;
	m_particle_comp->SetPause(false);
	auto children = m_hand->GetChildren();
	for (auto child : children)
	{
		//TODO::Sibal..
		if (child->GetName() == "Crosshair")
		{
			continue;
		}
		child->Detach(true);
		child->MakeDead();
	}

	m_respawnsound = false;

	return true;
}

bool Player::Invincible_Event(float t)
{
	m_isblink = true;
	if (static_cast<int>(t * 10) % 2 == 0)
	{
		m_sprite_comp->SetColorA(0.0f);
	}
	else
	{
		m_sprite_comp->SetColorA(1.0f);
	}
	if (t > 3.0f)
	{
		m_sprite_comp->SetColorA(1.0f);
		m_isDamaged = false;
		m_isblink = false;
		return true;
	}

	return false;
}

bool Player::KnockBack_Event(float t)
{
	SetZeroKey();

	vector2 Position = m_owner->GetTransform()->GetWorldPosition();

	Speed = { 0,-1 };

	if (IsOnGround)
	{
		if (t > 0.1f + m_knockback_duration)
		{
			m_initialY = Position.y;
			m_bounceheight *= m_knockback_restitution;
			m_knockback_duration = t;
		}
	}

	m_owner->GetTransform()->SetPosition(
		{ Easing::easeNone_Linear(t, m_initialX, m_knockback_dir * m_knockback_xpos, m_maxknockbacktime),
		Easing::ease_Bounce_animation(t - m_knockback_duration, m_initialY, m_bounceheight, 0.5f) }
	);

	//Vibration
	if(t>0.5f&&m_isvib)
	{
		if (m_playerNum >= 0)
		{
			Vibration(0.f, m_playerNum);
		}
		m_isvib = false;
	}

	if (t > m_maxknockbacktime)
	{
		shouldKnockBack = false;
		Speed = { 0,0 };

		this->m_owner->GetTransform()->SetDegree(0.0f);
		vector2 size = m_collision_comp->GetCollisionBoxSize();
		size.SwapXY();
		m_collision_comp->SetCollisionBoxSize(size);
		m_knockback_duration = 0.0f;

		return true;
	}

	return false;
}

bool Player::Stun_Event(float t)
{
	m_isstun = true;
	SetZeroKey();
	if (t > 1.0f||m_isdead)
	{
		m_isstun = false;
		m_sprite_comp->m_oneloop_animation = false;
		return true;
	}

	return false;
}

bool Player::Finish_Event(float /*dt*/)
{
	SetZeroKey();

	m_current_status = PLAYER_STATUS::ATTACK;
	m_sprite_comp->m_oneloop_animation = true;
	m_owner->GetTransform()->SetDepth(0.0f);
	return false;
}

bool Player::Damage_Event(float t)
{
	//m_sprite_comp->SetColorG(0.0f);
	//m_sprite_comp->SetColorB(0.0f);

	if (static_cast<int>(t * 10) % 2 == 0)
	{
		m_sprite_comp->SetColorA(0.0f);
	}
	else
	{
		m_sprite_comp->SetColorA(1.0f);
	}
	if (t > 0.5f)
	{
		m_sprite_comp->SetColorA(1.0f);
		return true;
	}


	if (t > 0.5f)
	{
		//m_sprite_comp->SetColorG(1.0f);
		//m_sprite_comp->SetColorB(1.0f);
		return true;
	}

	return false;
}

bool Player::Pick(Object * obj)
{
	//TODO::If you change Pickup method, Change it!
	if (!obj->GetComponentByDynamicCasting<Weapon>()->Is_CanPickup())
	{
		return false;
	}

	if (obj->GetComponentByDynamicCasting<Weapon>())
	{
		m_itempickup->Play_Audio(false);
		if (obj->GetComponentByDynamicCasting<Sniper>())
		{
			obj->GetTransform()->SetPosition(m_owner->GetTransform()->GetWorldPosition());
		}
		else
		{
			obj->GetTransform()->SetPosition(m_owner->GetTransform()->GetWorldPosition() - vector2(0.f, 2.f));
		}
		//time limit after picking up gun
		obj->SetLifetime(45.f);
		obj->AttatchToParent(this->m_hand);
		obj->GetComponentByDynamicCasting<Weapon>()->Set_Player_ptr(m_owner);
		obj->GetComponentByDynamicCasting<Weapon>()->Set_CanPickup(false);
		if (Sprite* sprite_comp = obj->GetComponentByTemplate<Sprite>(); sprite_comp != nullptr)
		{
			sprite_comp->SetColorA(1.0f);
		}
		if (!obj->DeleteComponentByTemplate<Collision>())
		{
			std::cout << "fail to delete collision component" << std::endl;
		}
		if (!obj->DeleteComponentByTemplate<Physics>())
		{
			std::cout << "fail to delete physics component" << std::endl;
		}
		return true;
	}

	return false;
}

void Player::SetInput(bool jump, bool doublejump, bool right, bool left, bool up)
{
	Jump = jump;
	DoubleJump = doublejump;
	Right = right;
	Left = left;
	Up = up;
}

float Player::GetChargedGage() const
{
	auto container = m_hand->GetChildren();
	if (container.size() == 1)
	{
		return 0.0f;
	}
	if (auto weapon = (std::find_if(container.begin(), container.end(), [](Object* obj)
	{
		if (obj->GetComponentByDynamicCasting<ChargeWeapon>() != nullptr)
		{
			return true;
		}
		return false;
	})); weapon != container.end())
	{
		return (*weapon)->GetComponentByDynamicCasting<ChargeWeapon>()->GetChargedEnergy();
	}
	return 0.0f;
}

void Player::SetChargedGage(float gage)
{
	auto container = m_hand->GetChildren();
	if (container.size() == 1)
	{
		return;
	}
	if (auto weapon = (std::find_if(container.begin(), container.end(), [](Object* obj)
	{
		if (obj->GetComponentByDynamicCasting<ChargeWeapon>() != nullptr)
		{
			return true;
		}
		return false;
	})); weapon != container.end())
	{
		(*weapon)->GetComponentByDynamicCasting<ChargeWeapon>()->SetChargedEnergy(gage);
	}
}

void Player::SetWeaponAngle(float angle)
{
	m_weapon_angle = angle;
}

unsigned int Player::GetPlayerNum() const
{
	return m_playerNum;
}

std::string Player::GetCharacterName() const
{
	return m_character_name;
}

void Player::Add_Damage(unsigned int dmg)
{
	m_playerinfo.m_dmg += dmg;
}

unsigned int Player::GetHealth() const
{
	return m_healthpoint;
}

int Player::GetDamage() const
{
	return m_playerinfo.m_dmg;
}

PlayerInfo Player::GetInfo() const
{
	return m_playerinfo;
}

Player::Player(Object* owner_object, const std::string& component_name)
	: Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::PLAYER;
	m_knockback_dir = LEFT;
}

Player::~Player() {}

void Player::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.Key("m_playerNum");
	writer.Int(m_playerNum);
	writer.Key("CharacterName");
	writer.String(m_character_name.c_str());
	writer.EndObject();
}

void Player::Deserialization(const rapidjson::Value& value)
{
	m_playerNum = value.FindMember("m_playerNum")->value.GetInt();
	m_character_name = value.FindMember("CharacterName")->value.GetString();
}

void Player::ImGui_Setting()
{
	ImGui::InputFloat("scaleX", &scale_step.x);
	ImGui::InputFloat("scaleY", &scale_step.y);
	ImGui::InputFloat("posX", &pos_variation.x);
	ImGui::InputFloat("posY", &pos_variation.y);
	ImGui::InputFloat("diverge", &diverge);
	ImGui::InputFloat("color", &colorstep);
	ImGui::Spacing(); ImGui::Separator(); ImGui::Spacing();
	ImGui::InputFloat("SpeedX", &Speed.x);
	ImGui::InputFloat("SpeedY", &Speed.y);
	ImGui::InputFloat("JumpSpeed", &JumpSpeed);
}

void Player::Init()
{
	name = "Player";
	kind = COMPONENT_KIND::PLAYER;
	if (m_sprite_comp = this->m_owner->GetComponentByTemplate<Sprite>(); m_sprite_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have sprite component!", spdlog::level::level_enum::err);
	}
	if (m_physics_comp = this->m_owner->GetComponentByTemplate<Physics>(); m_physics_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have physics component!", spdlog::level::level_enum::err);
	}
	if (m_collision_comp = this->m_owner->GetComponentByTemplate<Collision>(); m_collision_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have collision component!", spdlog::level::level_enum::err);
	}
	if (m_particle_comp = this->m_owner->GetComponentByTemplate<ParticleGenerator>(); m_particle_comp == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have particlegenerator component!", spdlog::level::level_enum::err);
	}

	m_graphic_system = m_sprite_comp->m_graphic_system;
	m_sprite_comp->SetTextureName(m_character_name);

	m_hudpngbasename = m_character_name + "_Face";
	m_playerhud = new Object(m_owner->GetState(), "Player_HUD");
	m_playerhud->Add_Component(new Sprite(m_playerhud));
	Sprite* sprite = m_playerhud->GetComponentByTemplate<Sprite>();
	sprite->SetTextureName(m_hudpngbasename);
	sprite->Init();
	sprite->SetHud(true);

	//Initial Player HUD position
	if (m_playerNum == 0)
	{
		m_playerhud->GetTransform()->SetPosition(vector2(-0.9f, -0.9f));
		std::cout << "player 0" << std::endl;
	}
	else if (m_playerNum == 1)
	{
		m_playerhud->GetTransform()->SetPosition(vector2(-0.3f, -0.9f));
		sprite->SetFlipX(false);
		std::cout << "player 1" << std::endl;
	}
	else if (m_playerNum == 2)
	{
		m_playerhud->GetTransform()->SetPosition(vector2(0.3f, -0.9f));
		sprite->SetFlipX(true);
		std::cout << "player 2" << std::endl;
	}
	else if (m_playerNum == 3)
	{
		m_playerhud->GetTransform()->SetPosition(vector2(0.9f, -0.9f));
		sprite->SetFlipX(true);
		std::cout << "player 3" << std::endl;
	}
	m_playerhud->GetTransform()->SetScale(vector2(0.09f, 0.16f));
	m_owner->GetState()->AddRegisterObject(m_playerhud);

	m_respawn = new Audio();
	m_respawn->Load_Audio_From_File("asset/Sound/player_respawn.wav");
	m_respawn->Set_Volume(5.f);
	m_respawn->Set_Name("Respawn_Sound");
	m_diesound = new Audio();
	m_diesound->Load_Audio_From_File("asset/Sound/player_die.wav");
	m_diesound->Set_Volume(5.f);
	m_diesound->Set_Name("Die_Sound");
	m_jumpsound = new Audio();
	m_jumpsound->Load_Audio_From_File("asset/Sound/player_jump.wav");
	m_jumpsound->Set_Volume(10.f);
	m_jumpsound->Set_Name("Jump_Sound");
	m_landingsound = new Audio();
	m_landingsound->Load_Audio_From_File("asset/Sound/player_landing.mp3");
	m_landingsound->Set_Volume(1.f);
	m_landingsound->Set_Name("Landing_Sound");
	m_itempickup = new Audio();
	m_itempickup->Load_Audio_From_File("asset/Sound/item_pickup.wav");
	m_itempickup->Set_Volume(5.f);
	m_itempickup->Set_Name("Item_PickupSound");

	m_owner->GetState()->AddPlayer(m_owner);
	Make_Hand();

    m_CrossHair = new Object(m_owner->GetState(), "Crosshair");
    m_CrossHair->Add_Component(new Sprite(m_CrossHair));
    m_CrossHair->AttatchToParent(m_hand);
    m_CrossHair->GetTransform()->SetPosition(vector2(0, 0));
    m_CrossHair->GetTransform()->SetScale({ 10.f, 10.f });
    m_owner->GetState()->AddRegisterObject(m_CrossHair);
    Sprite* crosshairSprite = m_CrossHair->GetComponentByTemplate<Sprite>();
    crosshairSprite->SetTextureName("crosshair");
    crosshairSprite->Init();

	Object* indicator = new Object(m_owner->GetState(), "Indicator");
	indicator->Add_Init_Component(new SpriteText(indicator));
	indicator->AttatchToParent(m_owner);
	SpriteText* indicator_text = indicator->GetComponentByTemplate<SpriteText>();
	indicator_text->SetAllign(TEXT_ALLIGN::ALLIGN_CENTER);
	indicator_text->SetColor(Color(255, 0, 0, 255));
	indicator_text->SetFontSize(8);
	indicator_text->SetString("P" + std::to_string(m_playerNum + 1));
	indicator->GetTransform()->SetPosition(vector2(0.0f, 0.7f));
	m_owner->GetState()->AddRegisterObject(indicator);

	m_healthbar = new Object(m_owner->GetState(), "HealthBar");
	m_healthbar->Add_Init_Component(new Sprite(m_healthbar));
	m_healthbar->AttatchToParent(m_owner);
	Sprite* healthbar_sprite = m_healthbar->GetComponentByTemplate<Sprite>();
	healthbar_sprite->SetTextureName("Rectangle");
	healthbar_sprite->SetColor(Color(255, 0, 0, 255));
	Transform* healthbar_transform = m_healthbar->GetTransform();
	healthbar_transform->SetPosition(vector2(0.0f, 0.55f));
	healthbar_transform->SetScale(vector2(0.8f, 0.1f));
	m_owner->GetState()->AddRegisterObject(m_healthbar);

	m_event_manager.SetOwner(this);
}

void Player::Make_Hand()
{
	m_hand = new Object(m_owner->GetState(), "hand" + std::to_string(m_playerNum));
	m_hand->AttatchToParent(m_owner);
	m_hand->GetTransform()->SetPosition(vector2(0, 0));
	m_owner->GetState()->AddRegisterObject(m_hand);
}

void Player::Update(float dt)
{
	m_head_dir = (m_sprite_comp->GetFlipX() == true) ? LEFT : RIGHT;

	//if (m_isinvincible)
	//{
	//	m_isinvincible = false;
	//	//Invincible_Event(dt);
	//}
	m_event_manager.Update(dt);
	UpdateKeyEvent(dt);
	UpdateSprite(dt);
	UpdatePhysics(dt);

	if (m_isdead)
	{
		this->m_owner->GetTransform()->SetDegree(m_head_dir * 90.0f);
		vector2 size = m_collision_comp->GetCollisionBoxSize();
		size.SwapXY();
		m_collision_comp->SetCollisionBoxSize(size);
		
		Die();
		
		m_isdead = false;
		m_isvib = true;
		if (m_playerNum >= 0)
		{
			Vibration(1.f, m_playerNum);
		}
		return;
	}
	if(!m_CanDash)
	{
		m_dashCoolDown -= dt;
		if(m_dashCoolDown <= 0)
		{
			m_CanDash = true;
			m_dashCoolDown = 2.f;
		}
	}

	if (Magnitude_Squared(this->m_owner->GetTransform()->GetWorldPosition()) > 100000.0f && CanPlayerDamaged())
	{
		++m_playerinfo.m_death;
		Die(false);
		//Respawn_Player();
	}

	float xLength = cos(To_RADIAN(m_weapon_angle))*15.f;
	float yLength = sin(To_RADIAN(m_weapon_angle))*15.f;

	m_CrossHair->GetTransform()->SetPosition({ xLength, yLength });
}

void Player::Close()
{
	if (m_hand == nullptr)
	{
		return;
	}
	//for (auto& child : m_hand->GetChildren())
	//{
	//	child->Detach();
	//}

	if (m_diesound)
	{
		m_diesound->Set_Dead(true);
	}
	if (m_respawn)
	{
		m_respawn->Set_Dead(true);
	}
	if (m_jumpsound)
	{
		m_jumpsound->Set_Dead(true);
	}
	if (m_landingsound)
	{
		m_landingsound->Set_Dead(true);
	}
	if (m_itempickup)
	{
		m_itempickup->Set_Dead(true);
	}

	m_event_manager.Close();
}

//
void Player::Vibration(float intensity,int player_index)
{
	XINPUT_VIBRATION Vibration;

	memset(&Vibration, 0, sizeof(XINPUT_VIBRATION));

	WORD leftVib = (WORD)(intensity*65535.0f);
	WORD rightVib = (WORD)(intensity*65535.0f);

	// Set the Vibration Values
	Vibration.wLeftMotorSpeed = leftVib;
	Vibration.wRightMotorSpeed = rightVib;
	// Vibrate the controller
	XInputSetState(player_index, &Vibration);
}

int Player::Pick_Weapon()
{
	if (m_hand->GetChildren().size() == 1)
	{
		auto collisionlist = m_collision_comp->GetCollidingObjects();
		for (auto& obj : collisionlist)
		{
			if (obj->HasParent())
			{
				continue;
			}

			auto container = m_owner->GetChildren();
			if (auto weapon = (std::find_if(container.begin(), container.end(), [](Object* obj)
			{
				if (obj->GetComponentByDynamicCasting<Weapon>() != nullptr)
				{
					return true;
				}
				return false;
			})); weapon != container.end())
			{
				continue;
			}
			
			if (Pick(obj))
			{
				return obj->GetID();
			}
		}
	}
	return -1;
}

void Player::Throw_Weapon()
{
	auto container = m_hand->GetChildren();
	if (auto weapon = (std::find_if(container.begin(), container.end(), [](Object* obj)
	{
		if (obj->GetComponentByDynamicCasting<Weapon>() != nullptr)
		{
			return true;
		}
		return false;
	})); weapon != container.end())
	{
		(*weapon)->Detach(true);
		(*weapon)->SetLifetime(15.f);
		(*weapon)->GetComponentByDynamicCasting<Weapon>()->Set_Player_ptr(nullptr);
		(*weapon)->GetComponentByDynamicCasting<Weapon>()->Set_CanPickup(true);

		(*weapon)->GetComponentByDynamicCasting<Sprite>()->SetColorA(1.0f);
		(*weapon)->Add_Init_Component(new Physics(*weapon));
		(*weapon)->GetComponentByTemplate<Physics>()->SetSpeed(vector2(cos(To_RADIAN(m_weapon_angle)), sin(To_RADIAN(m_weapon_angle)))*3.f);
		(*weapon)->Add_Init_Component(new Collision(*weapon));
		(*weapon)->GetComponentByTemplate<Collision>()->SetCollisionGroup(COLLISION_GROUP::BULLET);

		(*weapon)->GetTransform()->SetDegree(0);
		if (abs(m_weapon_angle) > 90)
		{
			(*weapon)->GetComponentByTemplate<Sprite>()->SetFlipY(true);
			(*weapon)->GetTransform()->SetDegree(180);
		}
	}
}

void Player::SetWalkSpeed(float scale)
{
	WalkSpeed = Default_WalkSpeed * scale;
}

void Player::SetPlayerNum(int num)
{
	m_playerNum = num;

	if (m_playerNum == 0)
	{
		m_playerhud->GetTransform()->SetPosition(vector2(-0.9f, -0.9f));
	}
	else if (m_playerNum == 1)
	{
		m_playerhud->GetTransform()->SetPosition(vector2(-0.3f, -0.9f));
	}
	else if (m_playerNum == 2)
	{
		m_playerhud->GetTransform()->SetPosition(vector2(0.3f, -0.9f));
	}
	else if (m_playerNum == 3)
	{
		m_playerhud->GetTransform()->SetPosition(vector2(0.9f, -0.9f));
	}

	if (Object* indicator = m_owner->FindChildByName("Indicator"); indicator != nullptr)
	{
		indicator->GetComponentByTemplate<SpriteText>()->SetString("P" + std::to_string(m_playerNum + 1));
	}
}

bool Player::DamagePlayer(int damage, float dir)
{
	vector2 pos = m_owner->GetTransform()->GetWorldPosition();
	if (dir != 0.0f)
	{
		m_owner->GetTransform()->SetPosition(vector2(pos.x, pos.y + 2.0f));
	}
	m_healthpoint -= damage;

	m_event_manager.AddEvent(&Player::Damage_Event);

	if (m_healthpoint < 0)
	{
		m_healthpoint = 0;
	}

	m_healthbar->GetTransform()->SetScale(vector2(0.8f * (m_healthpoint / 100.0f), 0.1f));

	if (m_healthpoint > 0)
	{
		return false;
	}
	++m_playerinfo.m_death;

	m_healthpoint = 100;

	m_knockback_dir = (dir < 0) ? LEFT : RIGHT;

	m_initialX = pos.x;
	m_initialY = pos.y;
	m_bounceheight = m_maxbounceheight;
	shouldKnockBack = true;
	m_smallknockbackTime = 0.0f;
	m_isDamaged = true;

	//TODO::If you add health system. you should have to add condition for this sound
	m_deathsound = false;
	//m_graphic_system->Toggle_shake(true);
	m_owner->GetState()->GetCamera(0)->ShakeCamera();

	return true;
}

void Player::SetCharacterName(std::string character_name)
{
	m_character_name = character_name;
	if (m_sprite_comp)
	{
		m_sprite_comp->SetTextureName(character_name);
	}
}

void Player::UpdateSprite(float /*dt*/)
{
	m_sprite_comp->SetTextureName(m_character_name);

	m_sprite_comp->SetType(static_cast<int>(m_current_status));

	if (!m_sprite_comp->m_oneloop_animation)
	{
		m_current_status = PLAYER_STATUS::NORMAL;
		//m_sprite_comp->m_AnimationAcitve = t;
	}

	if(m_isstun&& !m_sprite_comp->m_oneloop_animation)
	{
		m_current_status = PLAYER_STATUS::STUN;
		m_sprite_comp->m_oneloop_animation = true;
		m_sprite_time = 0.0f;
	}

	if (Input::Is_Triggered(CONTROL_SETTING::KEY_ATTACK) && !m_sprite_comp->m_oneloop_animation)
	{
		m_current_status = PLAYER_STATUS::ATTACK;
		m_sprite_comp->m_oneloop_animation = true;
		m_sprite_time = 0.0f;

		return;
	}

	if (m_isDamaged && !m_sprite_comp->m_oneloop_animation)
	{
		if (m_deathsound == false)
		{
			m_playerhud->GetComponentByTemplate<Sprite>()->SetTextureName(m_hudpngbasename + "_damaged");
			m_diesound->Play_Audio(false);
			m_deathsound = true;
		}
		m_current_status = PLAYER_STATUS::DAMAGED;
		m_sprite_comp->m_oneloop_animation = true;
		m_inner_time = 0.0f;
		m_isblink = true;
		m_isDamaged = false;
		m_isdead = true;
		return;
	}

	if (m_isstun && !m_sprite_comp->m_oneloop_animation)
	{
		m_current_status = PLAYER_STATUS::STUN;
		m_sprite_comp->m_oneloop_animation = true;
		m_inner_time = 0.0f;
		return;
	}

	if (CurrentState == JUMP && !m_sprite_comp->m_oneloop_animation)
	{
		m_current_status = PLAYER_STATUS::JUMP;
		return;
	}

	if ((Left || Right) && !m_sprite_comp->m_oneloop_animation)
	{
		m_current_status = PLAYER_STATUS::WALKING;
		m_sprite_comp->m_AnimationAcitve = true;
		return;
	}
}

void Player::UpdateKeyEvent(float dt)
{
  //  if (m_playerNum == 0)
  //  {
        //vector2 pos = m_owner->GetState()->GetCamera(0)->GetLocalPosition(Input::Get_Mouse_Position()) - m_owner->GetTransform()->GetWorldPosition();
        //m_weapon_angle = To_DEGREE(atan2(pos.y, pos.x));
  //  }
  //  else if (m_playerNum >= 1)
  //  {
		//if(!Input::Is_Joystick_Exist(m_playerNum))
		//{
		//	return;
		//}
  //      //TODO::Magic Numberrrrrr! 
  //      vector2 pos = Input::Get_Right_Stick_Position(m_playerNum);
  //      if (abs(pos.x) <= 0.5f && abs(pos.y) <= 0.5f)
  //      {
  //          //??keep previous angle
  //          //since recoil of analogue stick? 
  //      }
  //      else
  //      {
  //          m_weapon_angle = To_DEGREE(atan2(pos.y, pos.x));
  //      }
  //  }

	if (m_hand->GetChildren().size()!=1)
	{
		if (abs(m_weapon_angle) > 90)
		{
			m_hand->GetChildren()[1]->GetComponentByTemplate<Sprite>()->SetFlipY(true);
            m_sprite_comp->SetFlipX(true);
		}
		else
		{
			m_hand->GetChildren()[1]->GetComponentByTemplate<Sprite>()->SetFlipY(false);
            m_sprite_comp->SetFlipX(false);
        }
	}
    else if (m_hand->GetChildren().size() == 1) // No weapon situation
    {
        if (abs(m_weapon_angle) > 90)
        {
            m_sprite_comp->SetFlipX(true);
        }
        else
        {
            m_sprite_comp->SetFlipX(false);
        }
    }

	if (Input::Is_Triggered(CONTROL_SETTING::KEY_DASH, m_playerNum))
	{
		//vector2 pos = m_owner->GetState()->GetCamera(0)->GetLocalPosition(Input::Get_Mouse_Position()) - m_owner->GetTransform()->GetWorldPosition();
		//m_physics_comp->GetSpeed() += pos;
		//Speed += pos;
		//m_physics_comp->SetSpeed(pos);
		Dash = true;
        if (m_playerNum == 0)
        {
            m_DashDirection = m_owner->GetState()->GetCamera(0)->GetLocalPosition(Input::Get_Mouse_Position()) - m_owner->GetTransform()->GetWorldPosition();
        }
        else if (m_playerNum == 1)
        {
            m_DashDirection = Input::Get_Right_Stick_Position(0);
        }
        m_DashDirection = Normalizing(m_DashDirection);
	}

	if (Down)
	{
		Pick_Weapon();
	}

	auto container = m_hand->GetChildren();
	if (container.size() == 1)	//only contains cross_hair
	{
		return;
	}
	if (auto weapon = (std::find_if(container.begin(), container.end(), [](Object* obj)
	{
		if (obj->GetComponentByDynamicCasting<Weapon>() != nullptr)
		{
			return true;
		}
		return false;
	})); weapon != container.end())
	{
		if (m_fire)
		{
			if (ChargeWeapon* charged_weapon = (*weapon)->GetComponentByDynamicCasting<ChargeWeapon>();
				charged_weapon != nullptr)
			{
				charged_weapon->ChargeEnergy(dt);
			}
			if (Gun* gun = (*weapon)->GetComponentByDynamicCasting<Gun>();
				gun != nullptr)
			{
				gun->Fire();
			}
		}
		else
		{
			if (ChargeWeapon* charged_weapon = (*weapon)->GetComponentByDynamicCasting<ChargeWeapon>();
				charged_weapon != nullptr)
			{
				charged_weapon->Fire();
			}
		}
	}
}

void Player::UpdatePhysics(float dt)
{
	if (!m_physics_comp)
	{
		return;
	}
	m_movementTime += dt;

	if (m_movementTime >= 0.5f)
	{
		m_movementTime = 0.5f;
	}

	IsOnGround = m_physics_comp->GetIsOnGround();

	vector2 Position = this->m_owner->GetTransform()->GetWorldPosition();

	Speed = m_physics_comp->GetSpeed();

	//Switch Character State
	switch (CurrentState)
	{
	case CharacterState::STAND:
	{
		m_movementTime = 0;
		CanDoubleJump = true;
		Speed = { 0,0 };
		if (Dash&&m_CanDash)
		{
			CurrentState = CharacterState::DASH;
			break;
		}
		if (Right || Left)
		{
			CurrentState = CharacterState::WALK;
			break;
		}
		if (!Down&&Jump)
		{
			IsOnGround = false;
			Speed.y = JumpSpeed;
			CurrentState = CharacterState::JUMP;
			m_jumpsound->Play_Audio(false);
			break;
		}
		if (Down&&Jump)
		{
			if (m_physics_comp->IsOnOnWay())
			{
				CurrentState = CharacterState::JUMP;
				Is_Collide_Bot = true;
				IsOnGround = false;
				this->m_owner->GetTransform()->AddPosition(vector2(0.0f, -2.f));
				m_landingsound->Play_Audio(false);
				break;
			}
		}
		if (!IsOnGround)
		{
			CurrentState = CharacterState::JUMP;
			break;
		}
	}
	break;
	case CharacterState::WALK:
	{
		CanDoubleJump = true;
		if (Dash&&m_CanDash)
		{
			CurrentState = CharacterState::DASH;
			break;
		}
		if (Right)
		{
			if (m_physics_comp->IsWallBlocked(Speed*dt, RIGHT))
			{
				Speed.x = 0;
				m_movementTime = 0;
				CurrentState = CharacterState::STAND;
				break;
			}
		}
		if (Left)
		{
			if (m_physics_comp->IsWallBlocked(Speed*dt, LEFT))
			{
				Speed.x = 0;
				m_movementTime = 0;
				CurrentState = CharacterState::STAND;
				break;
			}
		}

		if (!Down&&Jump)
		{
			Speed.y = JumpSpeed;
			IsOnGround = false;
			CurrentState = CharacterState::JUMP;
			m_jumpsound->Play_Audio(false);
			break;
		}
		if (Down&&Jump)
		{
			if (m_physics_comp->IsOnOnWay())
			{
				CurrentState = CharacterState::JUMP;
				Is_Collide_Bot = true;
				IsOnGround = false;
				this->m_owner->GetTransform()->AddPosition(vector2(0.0f, -2.f));
				m_landingsound->Play_Audio(false);
				break;
			}
		}
		if (!IsOnGround)
		{
			CurrentState = CharacterState::JUMP;
			break;
		}
		if (Right && Left)//if both and right is pressed, stop moving
		{
			CurrentState = CharacterState::STAND;
			m_movementTime = 0;
			Speed = { 0,0 };
			break;
		}
		else if (Right)
		{	
			if(Speed.x < WalkSpeed)
			{
				Speed.x = Easing::easeOut_Cubic(m_movementTime, 0, WalkSpeed, 0.5f);
				if (m_physics_comp->IsWallBlocked(Speed*dt, RIGHT))
				{
					Speed.x = 0;
					m_movementTime = 0;
					CurrentState = CharacterState::STAND;
					break;
				}
			}
			else if (Speed.x >= WalkSpeed)
			{
				Speed.x = WalkSpeed;
			}
		}
		else if (Left)
		{
			if (Speed.x > -WalkSpeed)
			{
				Speed.x = -(Easing::easeOut_Cubic(m_movementTime, 0, WalkSpeed, 0.5f));
				if (m_physics_comp->IsWallBlocked(Speed*dt, LEFT))
				{
					Speed.x = 0;
					m_movementTime = 0;
					CurrentState = CharacterState::STAND;
					break;
				}
			}
			else if (Speed.x <= -WalkSpeed)
			{
				Speed.x = -WalkSpeed;
			}
		}


		if (!Right && !Left)
		{
			CurrentState = CharacterState::STAND;
			Speed = { 0,0 };
			m_movementTime = 0;
			break;
		}
	}
	break;
	case CharacterState::JUMP:
	{
		//Speed.y += Gravity * dt;
		//Speed.y = (Speed.y < MaxFallingSpeed) ? Speed.y : MaxFallingSpeed;
		if (Dash&&m_CanDash)
		{
			CurrentState = CharacterState::DASH;
			break;
		}
		if (!Jump&&CanDoubleJump && Speed.y > 0)//jump cancel 
		{
			Speed.y = (Speed.y < MinJumpSpeed) ? Speed.y : MinJumpSpeed;
		}
		else if (!Jump && !CanDoubleJump && Speed.y > 0)//double jump cancel 
		{
			Speed.y = (Speed.y < MinJumpSpeed) ? Speed.y : MinJumpSpeed;
		}

		if (CanDoubleJump && DoubleJump)
		{
			Speed.y = JumpSpeed;
			m_jumpsound->Play_Audio(false);
			CanDoubleJump = false;
		}

		if (Speed.y > 0)
		{
			if (m_physics_comp->IsCeilingBlocked(Speed*dt))
			{
				Speed.y = 0;
			}

		}

		if (!Right && !Left)
		{
			Speed.x = 0;
			m_movementTime = 0;
		}
		else if (Right)
		{
			Speed.x = WalkSpeed;
			if (m_physics_comp->IsWallBlocked(Speed*dt, RIGHT))
			{
				m_movementTime = 0.f;
				Speed.x = 0;
			}
			//m_owner->GetComponentByTemplate<Sprite>()->SetFlipX(false);

		}
		else if (Left)
		{
			Speed.x = -WalkSpeed;
			if (m_physics_comp->IsWallBlocked(Speed*dt, LEFT))
			{
				m_movementTime = 0.f;
				Speed.x = 0;
			}
			//m_owner->GetComponentByTemplate<Sprite>()->SetFlipX(true);
		}

		if (IsOnGround)
		{
			m_landingsound->Play_Audio(false);
			if (!Right && !Left)
			{
				CurrentState = CharacterState::STAND;
				Speed = { 0,0 };
				break;
			}
			else
			{
				CurrentState = CharacterState::WALK;
				Speed.y = 0;
				break;
			}
		}
	}
	break;
	case CharacterState::DASH:
	{
		m_dashinnertime += dt;

		if (m_dashinnertime <= dt * 2)
		{
			break;
		}
		m_dashinnertime -= 2 * dt;

		if (m_dashframe < 4)
		{
			//vector2 pos = m_owner->GetState()->GetCamera(0)->GetLocalPosition(Input::Get_Mouse_Position()) - m_owner->GetTransform()->GetWorldPosition();
			Speed = m_DashDirection * 3.f;
			++m_dashframe;
			break;
		}

		if (m_physics_comp->IsOnGround)
		{
			CurrentState = CharacterState::STAND;
		}
		if (!m_physics_comp->IsOnGround)
		{
			CurrentState = CharacterState::JUMP;
		}
		Dash = false;
		m_dashframe = 0;
		m_dashinnertime = 0.f;
		Speed = 0;
		m_CanDash = false;
		break;
	}
	break;
	}
	
	m_physics_comp->SetIsOnGround(IsOnGround);
	m_physics_comp->SetSpeed(Speed);


	if (CanPlayerDamaged())
	{
		//check Stun!
		auto collisionlist = m_collision_comp->GetCollidingObjects();
		for (auto& obj : collisionlist)
		{
			if (obj->HasParent() || obj->GetComponentByDynamicCasting<Collision>()->IsInIgnorelist(m_owner))
			{
				continue;
			}

			if (obj->GetComponentByDynamicCasting<Weapon>() == nullptr)
			{
				continue;
			}

			if (!obj->GetComponentByDynamicCasting<Weapon>()->Is_Thrown())
			{
				continue;
			}

			if (obj->GetComponentByDynamicCasting<Weapon>()->Is_Stun())
			{
				m_itempickup->Play_Audio(false); //TODO::need stun sound

				m_event_manager.AddEvent(&Player::Stun_Event);

				break;
			}
		}
	}
}

void Player::IncrementKill()
{
	++m_playerinfo.m_kill;
}

bool Player::CanPlayerDamaged() const
{
	if (m_isDamaged)
	{
		return false;
	}
	if (m_isblink)
	{
		return false;
	}
	if (m_current_status == PLAYER_STATUS::DAMAGED)
	{
		return false;
	}
	return true;
}

void Player::EndEvent()
{
	m_current_status = PLAYER_STATUS::ATTACK;
	m_sprite_comp->m_oneloop_animation = true;
}

void Player::SendEndSignal()
{
	m_event_manager.AddEvent(&Player::Finish_Event);
	//m_event_queue.push(&Player::Finish_Event);
}

void Player::Die(bool hurt)
{
	Event<Player>* e = nullptr;
	if (hurt)
	{
		e = m_event_manager.AddEvent(&Player::KnockBack_Event);
		e = e->SetNext(&Player::Revive_Event);
		e->SetNext(&Player::Respawn_Event);
		e->SetNext(&Player::Invincible_Event);
	}
	else
	{
		e = m_event_manager.AddEvent(&Player::Revive_Event);
		e->SetNext(&Player::Respawn_Event);
		e->SetNext(&Player::Invincible_Event);
	}
}

vector2 Player::Fire(vector2 fire)
{
	auto container = m_hand->GetChildren();
	if (container.size() == 1)	//only contains cross_hair
	{
		return vector2();
	}
	if (auto weapon = (std::find_if(container.begin(), container.end(), [](Object* obj)
	{
		if (obj->GetComponentByDynamicCasting<Weapon>() != nullptr)
		{
			return true;
		}
		return false;
	})); weapon != container.end())
	{
		if ((*weapon)->GetComponentByDynamicCasting<Gun>() != nullptr)
		{
			return (*weapon)->GetComponentByDynamicCasting<Gun>()->Fire(fire);
		}
	}

	return vector2();
}

bool Player::FireCharged()
{
	auto container = m_hand->GetChildren();
	if (container.size() == 1)	//only contains cross_hair
	{
		return false;
	}
	if (auto weapon = (std::find_if(container.begin(), container.end(), [](Object* obj)
	{
		if (obj->GetComponentByDynamicCasting<ChargeWeapon>() != nullptr)
		{
			return true;
		}
		return false;
	})); weapon != container.end())
	{
		(*weapon)->GetComponentByDynamicCasting<ChargeWeapon>()->Fire();
		return true;
	}
	return false;
}

unsigned Player::GetKill() const
{
	return m_playerinfo.m_kill;
}

unsigned int Player::GetDeath() const
{
	return m_playerinfo.m_death;
}


