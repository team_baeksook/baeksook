/***********************************************************************
File name		: WeaponGenerator.h
Project name	: WFPT
Author			: Hyun Seok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component.h"
#include "Graphic/Graphic.h"
#include "ParticleGenerator.h"
#include "System/SceneIdentifier.h"

class ClearPortal : public Component
{
public:
	ClearPortal(Object* owner_object, const std::string& component_name = "ClearPortal");
	~ClearPortal() override;

	void Set_NextState(SceneIdentifier state);
	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;

private:
	float m_innerTime = 0.f;

	unsigned int m_texture_id;

	Graphic* m_graphic_system = nullptr;
	std::string m_texture_name = "Red";
	ParticleGenerator* m_particle_comp = nullptr;
	SceneIdentifier next_state = SceneIdentifier::MAINMENU;

};
