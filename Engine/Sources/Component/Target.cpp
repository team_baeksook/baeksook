/***********************************************************************
File name		: WeaponGenerator.cpp
Project name	: WFPT
Author			: Hyun Seok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Target.h"
#include "Object/Object.h"
#include "System/State.h"
#include "System/StateManager.h"
#include "Component/Collision.h"
#include <IMGUI/imgui.h>
#include "System/Imgui_app.h"

Target::Target(Object* owner_object, const std::string& component_name) : Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::CLEARPORTAL;
}

Target::~Target()
{

}

void Target::Init()
{
	m_healthbar = new Object(m_owner->GetState(), "HealthBar");
	m_healthbar->Add_Init_Component(new Sprite(m_healthbar));
	m_healthbar->AttatchToParent(m_owner);
	Sprite* healthbar_sprite = m_healthbar->GetComponentByTemplate<Sprite>();
	healthbar_sprite->SetTextureName("Rectangle");
	healthbar_sprite->SetColor(Color(255, 0, 0, 255));
	Transform* healthbar_transform = m_healthbar->GetTransform();
	healthbar_transform->SetPosition(vector2(0.0f, 0.55f));
	healthbar_transform->SetScale(vector2(0.8f, 0.1f));
	m_owner->GetState()->AddRegisterObject(m_healthbar);
}

void Target::Update(float dt)
{
	m_innerTime += dt;
	m_healthbar->GetTransform()->SetScale(vector2(0.8f * (m_healthpoint / 100.0f), 0.1f));

	if (!m_owner->GetComponentByTemplate<Collision>()->GetCollidingObjects().empty())
	{
		m_healthpoint -= 50.f;
	}
}

void Target::Close()
{
}

bool Target::Is_dead()
{
	return m_healthpoint>0.f?false:true;
}

void Target::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void Target::Deserialization(const rapidjson::Value& /*value*/)
{

}

void Target::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::Unindent();
}