/***********************************************************************
	File name		: ParticlePhysics.cpp
	Project name	: WFPT
	Author			: HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#include "IMGUI/imgui.h"
#include "System/Input.h"
#include "Object/Object.h"
#include "System/ObjectManager.h"
#include <iostream>
#include "System/Logger.h"
#include "ParticlePhysics.h"
#include <cassert>


//ParticlePhysics::ParticlePhysics(Object * owner_object, std::string component_name)
//	: Component(owner_object, component_name), position(m_owner->GetTransform()->m_Position) {}

ParticlePhysics::ParticlePhysics(Object* owner_object, const std::string& component_name) 
					: Component(owner_object, component_name), position(m_owner->GetTransform()->m_Position)
{
	kind = COMPONENT_KIND::PARTICLEPHYSICS;
}

ParticlePhysics::~ParticlePhysics() {}

void ParticlePhysics::Init()
{
	
}
void ParticlePhysics::Close()
{
	
}
void ParticlePhysics::Update(float dt)
{
	FireDemo(dt);
}

void ParticlePhysics::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.Key("Damping");
	writer.Double(damping);
	writer.EndObject();
}

void ParticlePhysics::Deserialization(const rapidjson::Value& val)
{
	damping = val.FindMember("Damping")->value.GetFloat();
}

void ParticlePhysics::ImGui_Setting()
{

}


void ParticlePhysics::integrate(float duration)
{
	if (inversemass <= 0.0f)
		return;
	assert(duration > 0.0);

	position += { (velocity.x), ( velocity.y) };

	vector2 resultingAccel = acceleration;

	//update linear velocity from acceleration
	velocity += {(duration*resultingAccel.x), (duration*resultingAccel.y)};

	//impose drag
	velocity *= pow(damping, duration);
}

void ParticlePhysics::setMass(const float mass)
{
	assert(mass != 0);
	ParticlePhysics::inversemass = ((float)1.0) / mass;
}

float ParticlePhysics::getMass()const
{
	if (inversemass == 0)
		return 10000.0f;//change must be changed later. should implement infinity.
	else
		return (1.0f) / inversemass;
}

void ParticlePhysics::setInvereseMass(const float _inverseMass)
{
	inversemass = _inverseMass;
}

float ParticlePhysics::getInverseMass() const
{
	return inversemass;
}

bool ParticlePhysics::hasFiniteMass() const
{
	return inversemass >= 0.0f;
}

void ParticlePhysics::setDamping(const float _damping)
{
	damping = _damping;
}

float ParticlePhysics::getDamping() const
{
	return damping;
}

void ParticlePhysics::setPosition(const vector2 &_position)
{
	position = _position;
}

void ParticlePhysics::setPosition(const float _x, const float _y)
{
	position.x = _x;
	position.y = _y;
}

void ParticlePhysics::getPosition(vector2 *_position)
{
	*_position = position;
}


vector2 ParticlePhysics::getPosition() const
{
	return position;
}

void ParticlePhysics::setVelocity(const vector2 &_velocity)
{
	velocity = _velocity;
}

void ParticlePhysics::setVelocity(const float _x, const float _y)
{
	velocity.x = _x;
	velocity.y = _y;
}

void ParticlePhysics::setVelocity(const vector3 & _velocity)
{
	velocity.x = _velocity.x;
	velocity.y = _velocity.y;
}

void ParticlePhysics::getVelocity(vector2 *_velocity)
{
	*_velocity = velocity;
}

vector2  ParticlePhysics::getVelocity() const
{
	return velocity;
}

void ParticlePhysics::AddVelocity(const vector2 & vel)
{
	velocity += vel;
}

void ParticlePhysics::setAcceleration(const vector2 &_acceleration)
{
	acceleration = _acceleration;
}

void ParticlePhysics::setAcceleration(const float _x, const float _y)
{
	acceleration.x = _x;
	acceleration.y = _y;
}

void ParticlePhysics::getAcceleration(vector2 *_acceleration) const
{
	*_acceleration = acceleration;
}

vector2 ParticlePhysics::getAcceleration() const
{
	return acceleration;
}

void ParticlePhysics::clearAccumulator()
{
	forceAccumulated = { 0,0 };
}

void ParticlePhysics::FireDemo(float dt)
{
	//setMass(1.0f);
	//setVelocity(10.0f, 0.0f);
	//setAcceleration(0, 0.0f);
	setDamping(0.99f);

	integrate(dt);

	position += getVelocity()*dt;
	//bullet.transform.SetTranslation({ bullet.x, bullet.y });
}
