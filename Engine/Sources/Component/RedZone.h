#pragma once
#include "Component.h"

class Sprite;
class Collision;

class RedZone : public Component
{
private:
	Sprite* m_sprite_comp = nullptr;
	Collision* m_collision_comp = nullptr;
	float m_innertime = 0.0f;
	float m_bombtime = 1.0f;
	float m_blinktime = 0.0f;
public:
	RedZone(Object* owner_object, const std::string& component_name = "RedZone");
	~RedZone() override;

	void Bombing(float dt);

	virtual void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	virtual void Deserialization(const rapidjson::Value &) override;
	virtual void ImGui_Setting() override;
	virtual void Init() override;
	virtual void Update(float dt) override;
	virtual void Close(void) override;
};
