/***********************************************************************
	File name		: Physics.h
	Project name	: WFPT
	Author			: HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include "Component.h"
#include "Math/vector2.hpp"
//#include "Object/TileMap.h"
#include <queue>

class Collision;
class TileMap;

namespace PhysicsInfo
{
	const float GLOBAL_GRAVITY = -180.0f;
}

class Physics :public Component
{
public:
	friend class Player;
	friend class Bullet;
	friend class FireBullet;
	Physics(Object * owner_object, std::string component_name = "Physics");
	~Physics() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;

	void Init()				override;
	void Update(float dt)	override;
	void Close(void)		override;

	void Set_TileMap(TileMap*);
	void Set_Ignore_Tile(bool);

	bool IsObstacle(vector2 _position)	const;
	bool IsGround(vector2 _position)	const;
	bool IsOneway(vector2 _position)	const;
	bool IsEmpty(vector2 _position)		const;
	bool IsThereTile(vector2 _position) const;

	bool HasGround(vector2 _speed);
	bool HasGround(vector2 _speed, float dt);
	bool HasGround_OneWay();

	bool IsCeilingBlocked(vector2 _speed)					const;
	bool IsWallBlocked(vector2 _speed, int direction)		const;

	void SetIncreasingScaleValue(vector2 scaleValue);
	bool m_shouldScaleOverTime = false;
	vector2 m_scaleValue;

	void SetSpeed(vector2 speed);
	void SetMaxFallingSpeed(float y);
	void SetGravityOn(bool gravity);

	enum Direction
	{
		LEFT = -1,
		RIGHT = 1
	};

	float GetHowMuchPush() const;
	bool GetIsOnGround() const;
	void SetIsOnGround(bool isground);

	vector2 GetSpeed() const;
	bool IsOnOnWay() const;

	float GetGravity() const;
private:	
	TileMap* Tile_Map;

	float Gravity = PhysicsInfo::GLOBAL_GRAVITY;
	float MaxFallingSpeed = -110.0f;
	vector2 Speed;
	bool IsGravityOn = true;
	bool IsOnGround = false;
	bool m_IsOnOnWay = false;

	bool m_IsIgnoreTile = false;

	float m_how_much_push_up;

	float m_innertime;

	float m_updateTime = 0.f;

	Collision* m_collision_comp = nullptr;
};
