/***********************************************************************
	File name		: Camera.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include "Component.h"
#include "Math/vector2.hpp"
#include "Math/affine2d.hpp"

class Transform;
class Graphic;

//this component should be one per each state
class Camera : public Component
{
private:
    Graphic * m_GraphicSystem = nullptr;
    float m_CameraSpeed = 300.0f;

    bool m_isShake = false;
    float radius = 0.0f;
    float randomAngle = 0.0f;

    bool m_ismove = false;
    affine2d m_transformmatrix;

    void UpdateTransformmat();

	std::vector<Object*> m_playerlist;

	void Follow_Players(float dt);

	vector2 m_camerapos_before_shake;
private:
	bool m_IsFollow = true;

	vector2 m_minfollowsize = vector2(-100.0f, 30.0f);
	vector2 m_maxfollowsize = vector2(100.0f, 120.0f);

	float m_minscalerate = 4.0f;
	float m_maxscalerate = 6.0f;
    float m_ScaleRate = 4.0f;
    vector2 m_DisplaySize = vector2(1.0f, 1.0f);
    vector2 m_DisplayPosition = vector2(0.0f, 0.0f);
public:
    Camera(Object* owner_object, const std::string& component_name = "Camera");
	~Camera() override;

    void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
    void Deserialization(const rapidjson::Value&) override;
    void ImGui_Setting() override;
    void Init() override;
    void Update(float dt) override;
    void Close() override;

    void RegisterCamera();

    void SetCameraCoordinate() const;

    vector2 GetLocalPosition(vector2 vector);
	vector2 GetCameraPos(vector2 vector);

    bool IsDisplayed(Transform transform) const;

    float GetCameraDisplayDiagonal() const;
	void ShakeCamera();

	void SetFollow(bool follow);
	float GetScaleRate() const;
};
