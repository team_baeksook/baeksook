#pragma once
#include "Component.h"

class Transform;
class Camera;
class Sprite;

class Background : public Component
{
private:
	Camera* m_camera = nullptr;
	float m_initpos = 0.0f;
	Sprite* m_sprite_comp = nullptr;

	Transform* m_clone_trans = nullptr;

	float m_rate = 0.0f;
	float m_firstinitpos = 0.0f;
	float m_secondinitpos = 0.0f;

	bool m_shouldflip = true;
public:
	Background(Object* owner_object, std::string component_name = "Background");
	virtual void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	virtual void Deserialization(const rapidjson::Value &) override;
	virtual void ImGui_Setting() override;
	virtual void Init() override;
	virtual void Update(float dt) override;
	virtual void Close(void) override;

	void SetRate(float rate);

	void Setshouldflip(bool flip);
};