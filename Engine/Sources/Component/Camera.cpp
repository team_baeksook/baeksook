/***********************************************************************
	File name		: Camera.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#include "Camera.h"
#include "Object/Object.h"
#include "System/State.h"
#include <GL/glew.h>
#include "System/Input.h"
#include "IMGUI/imgui.h"
#include <iostream>

void Camera::UpdateTransformmat()
{
    m_transformmatrix = Scale_Affine(m_ScaleRate, m_ScaleRate) *
        Rotation_Affine_Degree(-m_owner->GetTransform()->GetWorldDegree()) *
        Translate_Affine(-this->m_owner->GetTransform()->GetWorldPosition());

	m_GraphicSystem->UpdateCamera(m_transformmatrix);
}

void Camera::Follow_Players(float dt)
{
	vector2 min = m_playerlist.at(0)->GetTransform()->GetWorldPosition();
	vector2 max = min;
	for (auto& player : m_playerlist)
	{
		vector2 pos = player->GetTransform()->GetWorldPosition();
		if (min.x > pos.x)
		{
			min.x = pos.x;
		}
		if (max.x < pos.x)
		{
			max.x = pos.x;
		}
		if (min.y > pos.y)
		{
			min.y = pos.y;
		}
		if (max.y < pos.y)
		{
			max.y = pos.y;
		}
	}

	vector2 center = (min + max) / 2.0f;

	/*if(center.x < m_minfollowsize.x)
	{
		center.x = m_minfollowsize.x;
	}
	if (center.x > m_maxfollowsize.x)
	{
		center.x = m_maxfollowsize.x;
	}*/
	if (center.y < m_minfollowsize.y)
	{
		center.y = m_minfollowsize.y;
	}
	if (center.y > m_maxfollowsize.y)
	{
		center.y = m_maxfollowsize.y;
	}

	vector2 camerapos = this->m_owner->GetTransform()->GetWorldPosition();
	vector2 difference = center - camerapos;

	this->m_owner->GetTransform()->AddPosition(difference * dt);

	vector2 windowsize = APP->GetWindowSize();

	max = GetRightTopPoint(3, min, max, camerapos);
	min = GetLeftBottomPoint(3, min, max, camerapos);

	vector2 diff = max - min;

	float range = std::max(diff.x, diff.y) +100.0f;

	this->m_ScaleRate = windowsize.x / (range);

	if (m_ScaleRate <= m_minscalerate)
	{
		m_ScaleRate = m_minscalerate;
	}
	if (m_ScaleRate >= m_maxscalerate)
	{
		m_ScaleRate = m_maxscalerate;
	}
}

Camera::Camera(Object* owner_object, const std::string& component_name) : Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::CAMERA;
}

Camera::~Camera() {}

void Camera::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
    writer.StartObject();
    writer.Key("Kind");
    writer.Int(kind);
    writer.Key("Name");
    writer.String(name.c_str());
    writer.Key("Scale rate");
    writer.Double(m_ScaleRate);
    writer.Key("Display size x");
    writer.Double(m_DisplaySize.x);
    writer.Key("Display size y");
    writer.Double(m_DisplaySize.y);
    writer.Key("Display Position x");
    writer.Double(m_DisplayPosition.x);
    writer.Key("Display Position y");
    writer.Double(m_DisplayPosition.y);
    writer.EndObject();
}

void Camera::Deserialization(const rapidjson::Value& val)
{
    m_ScaleRate = (float)val.FindMember("Scale rate")->value.GetDouble();
    m_DisplaySize.x = (float)val.FindMember("Display size x")->value.GetDouble();
    m_DisplaySize.y = (float)val.FindMember("Display size y")->value.GetDouble();
    m_DisplayPosition.x = (float)val.FindMember("Display Position x")->value.GetDouble();
    m_DisplayPosition.y = (float)val.FindMember("Display Position y")->value.GetDouble();
}

void Camera::ImGui_Setting()
{
    ImGui::SliderFloat2("DisplaySize", &m_DisplaySize.x, 0.0f, 1.0f);
    ImGui::SliderFloat("ScaleRate", &m_ScaleRate, 0.1f, 3.0f);
    if (1.0f - m_DisplaySize.x < m_DisplayPosition.x)
    {
        m_DisplayPosition.x = 1.0f - m_DisplaySize.x;
    }
    if (1.0f - m_DisplaySize.y < m_DisplayPosition.y)
    {
        m_DisplayPosition.y = 1.0f - m_DisplaySize.y;
    }
    ImGui::SliderFloat("DisplayPosition x", &m_DisplayPosition.x, 0.0f, 1.0f - m_DisplaySize.x);
    ImGui::SliderFloat("DisplayPosition y", &m_DisplayPosition.y, 0.0f, 1.0f - m_DisplaySize.y);
	ImGui::Checkbox("Follow", &m_IsFollow);

	ImGui::InputFloat2("Min", &m_minfollowsize.x);
	ImGui::InputFloat2("Max", &m_maxfollowsize.x);
	ImGui::InputFloat("minscale", &m_minscalerate);
	ImGui::InputFloat("maxscale", &m_maxscalerate);

	m_ismove = true;
}

void Camera::Init()
{
    name = "Camera";
    kind = COMPONENT_KIND::CAMERA;
    RegisterCamera();
    m_GraphicSystem = m_owner->GetState()->GetGraphicSystem();

    UpdateTransformmat();
}

void Camera::Update(float dt)
{
	if (m_isShake)
	{
	    radius *= 0.9f;
	    randomAngle += static_cast<float>(GetRandomNumber<double>(120, 240));
	    vector2 offset = vector2(sin(To_RADIAN(randomAngle)), cos(To_RADIAN(randomAngle))) * radius;
	    m_owner->GetTransform()->SetPosition(m_camerapos_before_shake + offset);
	    if (radius <= 0.01f)
	    {
			m_owner->GetTransform()->SetPosition(m_camerapos_before_shake);
	        m_isShake = false;
	    }
	}

	float tempscroll = Input::GetScrollInfo();

	if (tempscroll != 0.0f)
	{
		m_ismove = true;
	}

	//m_ScaleRate += tempscroll * 0.1f;

	if (m_IsFollow)
	{
		m_playerlist = m_owner->GetState()->GetPlayerlist();
		if (!m_playerlist.empty())
		{
			Follow_Players(dt);
		}
	}

	UpdateTransformmat();
}

void Camera::Close()
{

}

void Camera::RegisterCamera()
{
    this->m_owner->GetState()->AddCamera(this);
}

void Camera::SetCameraCoordinate() const
{
    //vector2 WindowSize = APP->GetWindowSize();
    //glViewport(static_cast<int>(m_DisplayPosition.x * WindowSize.x), static_cast<int>(m_DisplayPosition.y * WindowSize.x),
    //    static_cast<int>(m_DisplaySize.x * WindowSize.x), static_cast<int>(m_DisplaySize.y * WindowSize.y));

    //int loc = static_cast<int>(SHADER_LOCATION::CAMERA);

    //if (loc != -1)
    //{
    //    glUniformMatrix3fv(loc, 1, GL_FALSE, &(m_transformmatrix.column1.x));
    //}
}

vector2 Camera::GetLocalPosition(vector2 vector)
{
    vector2 screensize = APP->GetWindowSize();
    vector.x += (0.5f - (m_DisplayPosition.x + m_DisplaySize.x / 2.0f)) * screensize.x;
    vector.y += (0.5f - (m_DisplayPosition.y + m_DisplaySize.y / 2.0f)) * screensize.y;

    affine2d transform_matrix = Translate_Affine(this->m_owner->GetTransform()->GetWorldPosition()) * Rotation_Affine_Degree(m_owner->GetTransform()->GetWorldDegree()) * Scale_Affine(1 / m_ScaleRate, 1 / m_ScaleRate);

    vector3 result = transform_matrix * vector3(vector.x, vector.y, 1.0f);

    return result.GetVector2();
}

vector2 Camera::GetCameraPos(vector2 vector)
{
	vector3 result = m_transformmatrix * vector3(vector, 1.0f);
	return result.GetVector2();;
}

bool Camera::IsDisplayed(Transform transform) const
{
    const float length = Magnitude(m_owner->GetTransform()->GetWorldPosition() - transform.GetWorldPosition());
    const float radius_sum = GetCameraDisplayDiagonal() + transform.GetDiagonal();
    return length <= radius_sum;
}

float Camera::GetCameraDisplayDiagonal() const
{
    vector2 windowsize = APP->GetWindowSize();
    vector2 real_displayedsize(windowsize.x * m_DisplaySize.x, windowsize.y * m_DisplaySize.y);

    return Magnitude(real_displayedsize) / (2.0f * m_ScaleRate);
}

void Camera::ShakeCamera()
{
	if (m_isShake == false)
	{
		radius = 8.0f;
		randomAngle = static_cast<float>(GetRandomNumber<double>(0, 359));
		vector2 offset = vector2(sin(To_RADIAN(randomAngle)), cos(To_RADIAN(randomAngle))) * radius;

		m_camerapos_before_shake = m_owner->GetTransform()->GetWorldPosition();

		m_owner->GetTransform()->SetPosition(m_camerapos_before_shake + offset);

		m_isShake = true;
	}
}

void Camera::SetFollow(bool follow)
{
	m_IsFollow = follow;
}

float Camera::GetScaleRate() const
{
	return m_ScaleRate;
}
