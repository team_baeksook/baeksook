/***********************************************************************
	File name		: SpriteText.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "SpriteText.h"
#include "Object/Object.h"
#include "System/State.h"
#include <GL/glew.h>
#include "Object/Mesh.h"
#include "IMGUI/imgui.h"

SpriteText::SpriteText(Object* owner_object, std::string component_name) : Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::SPRITE_TEXT;
}

SpriteText::~SpriteText() {}

Color SpriteText::GetColor()
{
    return m_color;
}

void SpriteText::SetColor(Color temp_color)
{
    m_color = temp_color;
}

void SpriteText::SetMesh(Mesh* temp_mesh)
{
    m_mesh = temp_mesh;
}

Mesh* SpriteText::GetMesh()
{
    return m_mesh;
}

void SpriteText::SetFontSize(float size)
{
	m_fontSize = size;
	std::string temp = m_Text;
	m_Text.clear();
	SetString(temp);
}

void SpriteText::SetGraphicSystem(Graphic* graphic)
{
    m_graphic_system = graphic;
}

void SpriteText::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>& writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.Key("fontSize");
	writer.Double(m_fontSize);
	writer.Key("text");
	writer.String(m_Text.c_str());
	writer.Key("TextAllignment");
	if(m_TextAllignment==TEXT_ALLIGN::ALLIGN_CENTER)
	{ 
		writer.Int(0);
	}
	else if (m_TextAllignment == TEXT_ALLIGN::ALLIGN_LEFT)
	{
		writer.Int(-1);
	}
	else if (m_TextAllignment == TEXT_ALLIGN::ALLIGN_RIGHT)
	{
		writer.Int(1);
	}
	writer.EndObject();
}

void SpriteText::Deserialization(const rapidjson::Value& val)
{
	m_fontSize=val.FindMember("fontSize")->value.GetFloat();
	m_Text = val.FindMember("text")->value.GetString();
	strcpy_s(Text_ImGui, m_Text.c_str());
	m_Text = "";
	int val_allignment = val.FindMember("TextAllignment")->value.GetInt();

	if (val_allignment==0)
	{
		m_TextAllignment = TEXT_ALLIGN::ALLIGN_CENTER;
	}
	else if (val_allignment==-1)
	{
		m_TextAllignment = TEXT_ALLIGN::ALLIGN_LEFT;
	}
	else if (val_allignment == 1)
	{
		m_TextAllignment = TEXT_ALLIGN::ALLIGN_RIGHT;
	}
}

void SpriteText::ImGui_Setting()
{
    ImGui::Checkbox("Visible", &m_Visible);
    if(ImGui::InputFloat("FontSize", &m_fontSize))
    {
		std::string temp = m_Text;
		m_Text.clear();
        SetString(temp);
    }
	strcpy_s(Text_ImGui, m_Text.c_str());
	ImGui::InputText("Text", Text_ImGui, 80);
    SetString(Text_ImGui);
    for (int i = 0; i < 3; i++)
    {
        if (i != 0)
        {
            ImGui::SameLine();
        }
        if (ImGui::RadioButton(m_allign_name[i].c_str(), m_TextAllignment == static_cast<TEXT_ALLIGN>(i)))
        {
            m_TextAllignment = static_cast<TEXT_ALLIGN>(i);
			std::string temp = m_Text;
        	m_Text = " ";
            SetString(temp);
        }
    }

    auto font_list = m_graphic_system->GetFont_Container();
    if (ImGui::BeginCombo("Font", m_fontName.c_str(), ImGuiComboFlags_None))
    {
        for (auto i : font_list)
        {
            bool is_selected = (m_fontName == i.first);
            if (ImGui::Selectable(i.first.c_str(), is_selected))
            {
                m_fontName = i.first;
                m_Font = i.second;
                SetString(m_Text);
            }
            if (is_selected)
            {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }

	bool hud_temp = m_ishud;
	ImGui::Checkbox("HUD", &hud_temp);
	SetHud(hud_temp);

	ImGui::ColorEdit4("Color", m_color.GetColor_Float());
}

void SpriteText::Init()
{
    m_graphic_system = m_owner->GetState()->GetGraphicSystem();
    name = "SpriteText";
    kind = COMPONENT_KIND::SPRITE_TEXT;

    m_shader_type = SHADER_PROGRAM_TYPE::TEXTURE_SHADER;

    m_Font = m_graphic_system->GetFont(m_fontName);

    m_mesh = new Mesh();
    m_mesh->MakeTexture();

    SetString(Text_ImGui);
}

void SpriteText::Update(float /*dt*/)
{
    if(m_temp_texture_coordinates.empty())
    {
        return;
    }

    if (!m_Visible)
    {
        return;
    }

	float depth = this->m_owner->GetTransform()->GetDepth();

	if (depth > 1.0f || depth < -1.0f)
	{
		return;
	}

	DrawBehavior* draw = new DrawBehavior();
	float* pointer = m_color.GetColor_Float();
	for (int i = 0; i < 4; ++i)
	{
		draw->m_color[i] = pointer[i];
	}
	draw->m_hud = m_ishud;
	draw->m_size = m_mesh->GetSize();
	draw->m_vertex = m_mesh->GetVertices();
	pointer = this->m_owner->GetTransform()->GetPointer();
	for (int i = 0; i < 9; ++i)
	{
		draw->m_transform[i] = pointer[i];
	}
	draw->m_rendertype = m_mesh->GetRenderType();
	draw->m_texture = m_temp_texture_coordinates;
	draw->m_texture_id = m_Font->GetTexture()->GetID();
	draw->m_type = graphic_enum::DrawType::TEXT;
	m_graphic_system->m_drawlist.insert(std::make_pair(depth, draw));
}

void SpriteText::Close()
{
    delete m_mesh;
}

void SpriteText::SetString(std::string string)
{
	if(m_Text == string)
	{
		return;
	}
    m_mesh->ClearVertices();
    m_Font->GetTexture()->ClearCoordinate();
    m_mesh->SetRenderType(RENDER_TYPE::TRIANGLE);
    m_temp_mesh_coordinates.clear();
    m_temp_texture_coordinates.clear();
    m_Text = string;
    vector2 position = vector2(0.0f, -m_fontSize * m_Font->lineHeight / 2);
    std::string line_str;
    for (auto i = string.begin(); i != string.end(); ++i)
    {
        if (*i == '\n')
        {
            DealLine(line_str, position);
            position.y -= m_Font->lineHeight * m_fontSize;
            position.x = 0;
            continue;
        }
        if(*i == '\\' && i != string.end() - 1)
        {
            if(*(i+1)=='n')
            {
                DealLine(line_str, position);
                position.y -= m_Font->lineHeight * m_fontSize;
                position.x = 0;
                i += 1;
                continue;
            }
        }
        line_str.push_back(*i);
        Character temp = m_Font->GetCharacter(*i);
        position.x -= m_fontSize * static_cast<int>(m_TextAllignment) * temp.xadvance / 2.0f;
    }
    DealLine(line_str, position);
    m_mesh->SetCoordinate(m_temp_mesh_coordinates);
}

Font* SpriteText::GetFont() const
{
    return m_Font;
}

bool SpriteText::IsHud() const
{
	return m_ishud;
}

void SpriteText::SetHud(bool hud)
{
	m_ishud = hud;
}

void SpriteText::SetAllign(TEXT_ALLIGN allign)
{
	m_TextAllignment = allign;
}

float* SpriteText::GetTextureVerticesPointer()
{
	if(m_temp_texture_coordinates.empty())
	{
		return nullptr;
	}
	return &m_temp_texture_coordinates.at(0).x;
}

std::string SpriteText::GetString() const
{
	return m_Text;
}

void SpriteText::CharToTexture(vector2 pos, Character character)
{
    if(character.character == NULL)
    {
        return;
    }

    float left = pos.x + character.xoffset * m_fontSize;
    float right = left + character.width * m_fontSize;
    float bottom = pos.y - character.yoffset * m_fontSize - character.height * m_fontSize + m_Font->lineHeight * m_fontSize;
    float top = bottom + character.height * m_fontSize;

    m_temp_mesh_coordinates.push_back(vector2(left, top));
    m_temp_texture_coordinates.push_back(character.top_left);
    m_temp_mesh_coordinates.push_back(vector2(left, bottom));
    m_temp_texture_coordinates.push_back(vector2(character.top_left.x, character.bottom_right.y));
    m_temp_mesh_coordinates.push_back(vector2(right, top));
    m_temp_texture_coordinates.push_back(vector2(character.bottom_right.x, character.top_left.y));

    m_temp_mesh_coordinates.push_back(vector2(right, top));
    m_temp_texture_coordinates.push_back(vector2(character.bottom_right.x, character.top_left.y));
    m_temp_mesh_coordinates.push_back(vector2(left, bottom));
    m_temp_texture_coordinates.push_back(vector2(character.top_left.x, character.bottom_right.y));
    m_temp_mesh_coordinates.push_back(vector2(right, bottom));
    m_temp_texture_coordinates.push_back(character.bottom_right);
}

void SpriteText::DealLine(std::string& str, vector2& pos)
{
    for (auto i : str)
    {
        Character temp = m_Font->GetCharacter(i);
        CharToTexture(pos, temp);
        pos.x += temp.xadvance * m_fontSize;
    }
    str.clear();
}
