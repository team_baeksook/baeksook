/***********************************************************************
	File name		: SpriteText.h
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component.h"
#include "Graphic/Graphic.h"
#include "Graphic/Color.h"
#include "Graphic/Font.h"

enum class TEXT_ALLIGN { ALLIGN_LEFT = 0, ALLIGN_CENTER = 1, ALLIGN_RIGHT = 2 };

class SpriteText : public Component
{
private:
    Color m_color;
    SHADER_PROGRAM_TYPE m_shader_type;

    Font * m_Font = nullptr;

    Mesh * m_mesh = nullptr;

    std::vector<vector2> m_temp_mesh_coordinates;
    std::vector<vector2> m_temp_texture_coordinates;

    void CharToTexture(vector2 pos, Character character);
    void DealLine(std::string& str, vector2& pos);
    Graphic * m_graphic_system = nullptr;

    const std::string m_allign_name[3] = { "Left", "Center", "Right" };
private:
    float m_fontSize = 1.0f;

    std::string m_fontName = "Press Start 2P";

    bool m_Visible = true;

    std::string m_Text = "";
	char Text_ImGui[80];

    TEXT_ALLIGN m_TextAllignment = TEXT_ALLIGN::ALLIGN_LEFT;

	bool m_ishud = false;
public:
    SpriteText(Object * owner_object, std::string component_name = "SpriteText");
	~SpriteText() override;

    Color GetColor();
    void SetColor(Color temp_color);
    void SetMesh(Mesh * temp_mesh);
    Mesh * GetMesh();
	void SetFontSize(float size);

    void SetGraphicSystem(Graphic * graphic);

    void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
    void Deserialization(const rapidjson::Value&) override;
    void ImGui_Setting() override;

    void Init() override;
    void Update(float dt) override;
    void Close(void) override;

    void SetString(std::string string);

    Font * GetFont() const;

	bool IsHud() const;
	void SetHud(bool hud);

	void SetAllign(TEXT_ALLIGN allign);

	float* GetTextureVerticesPointer();
	std::string GetString() const;
};

