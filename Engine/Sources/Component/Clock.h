/***********************************************************************
	File name		: Clock.h
	Project name	: WFPT
	Author			: Kim Minsuk

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include "Component.h"

class SpriteText;

class Clock : public Component
{
private:
	SpriteText* m_sprite_text_comp = nullptr;
	int m_timelimit = 120;

	float m_innertime = static_cast<float>(m_timelimit);
	int m_second = m_timelimit;
	bool m_pause = false;
private:
public:
	Clock(Object* owner_object, const std::string& component_name = "Clock");
	~Clock() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;
	void Init() override;
	void Update(float dt) override;
	void Close() override;

	void TogglePause();

	int GetSecond() const;
	void SetSecond(float sec);
};
