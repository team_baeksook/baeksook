/***********************************************************************
	File name		: Player.h
	Project name	: WFPT
	Author			: MinSuk Kim, HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Component.h"
#include <unordered_map>
#include "Graphic/Character_sprite.h"
#include "Graphic/Graphic.h"
#include "Collision.h"
#include "System/Audio.h"
#include "ParticleGenerator.h"
#include <queue>

#include "System/EventManager.h"

class Sprite;
class Physics;

enum CharacterState
{
	STAND,
	WALK,
	JUMP,
	DASH
};

enum Direction
{
	LEFT = -1,
	RIGHT = 1
};

struct PlayerInfo
{
	unsigned int m_kill = 0;
	unsigned int m_death = 0;
	int m_dmg = 0;
};


class Player : public Component
{
	//changed this
	friend class World0;
	
	friend class User;
	friend class Weapon;
	friend class Gun;
	friend class Revolver;
	friend class Rifle;
	friend class Shotgun;
	friend class Tomahawk;
	friend class GuidedMissile;
	friend class Bow;
	friend class FlameThrower;
	friend class Sniper;
	friend class Pistol;
	friend class LightMachineGun;
	friend class Laser;
private:
	Character_Sprite* m_character_sprit = nullptr;
	std::string m_character_name = "Chicken_Character";

	PLAYER_STATUS m_current_status = PLAYER_STATUS::NORMAL;

	Graphic*	m_graphic_system	= nullptr;
	Sprite*		m_sprite_comp		= nullptr;
	Physics*	m_physics_comp		= nullptr;
	Collision*	m_collision_comp	= nullptr;
	ParticleGenerator* m_particle_comp = nullptr;

	float m_sprite_time = 0.0f;

	void UpdateSprite(float dt);
	void UpdateKeyEvent(float dt);
	void UpdatePhysics(float dt);

	vector2 scale_step = vector2(1.0f, 1.0f);
	vector2 pos_variation = vector2(0.0f, 0.0f);
	float diverge = 0.1f;
	float colorstep = 0.0f;

	unsigned int m_playerNum = 0;

	float m_inner_time = 0.0f;

	vector2 Speed;

	float JumpSpeed = 125.f;
	float MinJumpSpeed = 2.f;
	const float Default_WalkSpeed = 90.0f;
	float WalkSpeed = 90.0f;

	bool IsGhostOn = false;
	bool IsGravityOn = true;

	bool CanDoubleJump = true;
	bool Is_Collide_Bot = false;

	bool IsOnGround = false;

	bool shouldKnockBack = false;

	Direction m_head_dir = LEFT;
	CharacterState CurrentState = STAND;

	bool Jump = false, Right = false, Down = false, Left = false, Up = false, Dash = false;
	bool DoubleJump = false;
	bool m_fire = false;
	bool m_isDamaged = false;

	float m_weapon_angle = 0;

	int m_dashframe = 0;
	float m_dashinnertime = 0.f;
	float m_dashCoolDown = 2.f;//IMPORTANT: if you change this value, change the value in the update() as well
	bool m_CanDash = true;

	vector2 m_DashDirection = { 0.f,0.f };
	Object* m_hand = nullptr;
	Object* m_CrossHair = nullptr;

	bool m_isdead = false;
	bool m_isstun = false;
	bool m_deathsound = false;
	float m_death_time = 1.0f;
	bool m_respawnsound = false;

	void SetZeroKey();
	void Respawn(float dt);

	bool m_should_respawn = false;

	float m_initialY = 0;
	float m_initialX = 0;

	float m_knockback_xpos = 30.0f;
	float m_smallknockbackTime = 0.0f;
	const float m_maxbounceheight = 10.0f;
	float m_bounceheight = m_maxbounceheight;
	float m_knockback_restitution = 0.6f;
	float m_knockback_duration = 0.0f;

	const float m_maxknockbacktime = 3.0f;

	Audio* m_respawn;
	Audio* m_diesound;
	Audio* m_jumpsound;
	Audio* m_landingsound;
	Audio* m_itempickup;

	PlayerInfo m_playerinfo;

	bool m_isinvincible = false;
	bool m_isblink = false;
	bool m_isvib = false;

	float m_blink_time = 0.0f;
	float m_movementTime = 0.f;

	Direction m_knockback_dir;

	Object* m_playerhud = nullptr;
	std::string m_hudpngbasename = "";

	void Respawn_Player();

	bool Respawn_Event(float t);
	bool Revive_Event(float t);
	bool Invincible_Event(float t);
	bool KnockBack_Event(float t);
	bool Stun_Event(float t);
	bool Finish_Event(float t);
	bool Damage_Event(float t);

	EventManager<Player> m_event_manager;

	int m_healthpoint = 100;
	Object* m_healthbar = nullptr;
public:
	Player(Object* owner_object, const std::string& component_name = "Player");
	~Player() override;

	void Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&) override;
	void Deserialization(const rapidjson::Value&) override;
	void ImGui_Setting() override;

	void Init() override;
	void Update(float dt) override;
	void Make_Hand();
	void Close() override;

	void Vibration(float intensity, int player_index);

	void SetPlayerNum(int num);

	bool DamagePlayer(int damage = 0, float dir = -1.0f);

	void SetCharacterName(std::string character_name);

	void IncrementKill();
	unsigned int GetKill() const;
	unsigned int GetDeath() const;

	bool CanPlayerDamaged() const;
	void EndEvent();

	void SendEndSignal();

	void Die(bool hurt = true);
	vector2 Fire(vector2 fire = vector2());
	bool FireCharged();
	int Pick_Weapon();

	void Throw_Weapon();

	void SetWalkSpeed(float scale);

	bool Pick(Object* obj);
	void SetInput(bool jump, bool doublejump, bool right, bool left, bool up);
	float GetChargedGage() const;
	void SetChargedGage(float gage);
	void SetWeaponAngle(float angle);

	unsigned int GetPlayerNum() const;
	std::string GetCharacterName() const;
	void Add_Damage(unsigned int dmg);
	unsigned int GetHealth() const;
	int GetDamage() const;

	PlayerInfo GetInfo() const;
};