/***********************************************************************
File name		: WeaponGenerator.cpp
Project name	: WFPT
Author			: Hyun Seok Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "ClearPortal.h"
#include "ParticleGenerator.h"
#include "Object/Object.h"
#include "System/State.h"
#include "Graphic/Particle.h"
#include "System/StateManager.h"
#include "Component/Collision.h"
#include <IMGUI/imgui.h>
#include "Graphic/Texture.h"
#include "System/Imgui_app.h"

ClearPortal::ClearPortal(Object* owner_object, const std::string& component_name) : Component(owner_object, component_name)
{
	kind = COMPONENT_KIND::CLEARPORTAL;
}

ClearPortal::~ClearPortal()
{

}

void ClearPortal::Init()
{
}

void ClearPortal::Update(float dt)
{
	m_innerTime += dt;
	
	auto player_num = STATE->Get_CurrentState()->GetPlayerlist().size();
	auto collide_num = m_owner->GetComponentByTemplate<Collision>()->GetCollidingObjects().size();

	if (player_num == collide_num)
	{
		printf("SibalSibal\n");
		STATE->Change_State(next_state);
	}
}

void ClearPortal::Close()
{

}

void ClearPortal::Set_NextState(SceneIdentifier state)
{
	next_state = state;
}

void ClearPortal::Serialization(rapidjson::PrettyWriter<rapidjson::FileWriteStream>&writer)
{
	writer.StartObject();
	writer.Key("Kind");
	writer.Int(kind);
	writer.Key("Name");
	writer.String(name.c_str());
	writer.EndObject();
}

void ClearPortal::Deserialization(const rapidjson::Value& /*value*/)
{

}

void ClearPortal::ImGui_Setting()
{
	ImGui::Indent();
	ImGui::Unindent();
}