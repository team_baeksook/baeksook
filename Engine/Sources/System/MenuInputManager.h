/***********************************************************************
File name		: MenuInputManager.h
Project name	: WFPT
Author			: Maxime Cauvin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
#include <iostream>
#include "Component/Button.h"
#include "System/JoyStick.h"
#include "System/Imgui_app.h"

class MenuInputManager
{
public:
	MenuInputManager();
	~MenuInputManager();

	int UpdateMenuNavigation(std::vector<Button *> buttons, int currentIdx);
	bool CheckMenuButtonBack(int key, JOYSTICK_KEY controllerKey, SceneIdentifier prevStateId);
	bool CheckMenuButtonConfirm(int key, JOYSTICK_KEY controllerKey, std::vector<Button *> buttons, int currentIdx);
};

