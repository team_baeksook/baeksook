/***********************************************************************
File name : Logger.cpp
Project name : WFPT
Author : MinSuk Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Logger.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "fstream"

namespace logger
{
    Logger log_handler;
}

Logger::Logger()
{
    spdlog::drop_all();
    m_logger_file = spdlog::rotating_logger_mt("logfile", "log/log.txt", 50000, 3);
    m_logger_file->set_pattern("[%Y-%m-%d %T] [%l] %v");
    m_logger_console = spdlog::stdout_color_st("logconsole");
    m_logger_console->set_pattern("[%Y-%m-%d %T] [%^%l%$] %v");
    m_logger_console->set_level(spdlog::level::level_enum::debug);
}

Logger::~Logger()
{
    spdlog::shutdown();
}

void Logger::PrintLogOnConsole(std::string message, spdlog::level::level_enum level)
{
    switch(level)
    {
    case spdlog::level::level_enum::info:
        m_logger_console->info(message);
        return;
    case spdlog::level::level_enum::warn:
        m_logger_console->warn(message);
        return;
    case spdlog::level::level_enum::err:
        m_logger_console->error(message);
        return;
    default:
        return;
    }
}

void Logger::PrintLogOnFile(std::string message, spdlog::level::level_enum level)
{
    switch (level)
    {
    case spdlog::level::level_enum::debug:
        m_logger_file->debug(message);
        return;
    case spdlog::level::level_enum::info:
        m_logger_file->info(message);
        return;
    case spdlog::level::level_enum::warn:
        m_logger_file->warn(message);
        return;
    case spdlog::level::level_enum::err:
        m_logger_file->error(message);
        return;
    default:
        return;
    }
}

void Logger::PrintLogOnConsoleAndFile(std::string message, spdlog::level::level_enum level)
{
    PrintLogOnConsole(message, level);
    PrintLogOnFile(message, level);
}
