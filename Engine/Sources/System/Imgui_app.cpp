/***********************************************************************
File name : Engine_Utility.h
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Imgui_app.h"

#include "IMGUI\imgui.h"
#include "IMGUI\imgui_impl_glfw.h"
#include "IMGUI\imgui_impl_opengl3.h"

#include "Input.h"
#include "Object/Object.h"
#include "Component/Physics.h"
#include "Component/Sprite.h"
#include "Component/SpriteText.h"
#include <iostream>
#include "Engine.h"
#include "StateManager.h"
#include "ObjectManager.h"
#include "Component/Camera.h"
#include "SoundManager.h"
#include <Rapidjson/filereadstream.h>
#include <Rapidjson/document.h>
#include "Component/ParticlePhysics.h"
#include "Math/vector2.hpp"
#include "Component/Weapon/Bullet.h"
#include "Component/Weapon/Revolver.h"
#include "Component/Player.h"
#include "Component/Weapon/Rifle.h"
#include "Component/Weapon/Tomahawk.h"
#include "Component/ParticleGenerator.h"
#include <Component/Weapon/Shotgun.h>
#include "Component/Weapon/GuidedMissile.h"
#include "Component/Weapon/Bow.h"
#include "Component/Weapon/Grenade.h"
#include "Component/Weapon/Sniper.h"
#include "Component/Weapon/FlameThrower.h"
#include "Component/WeaponGenerator.h"
#include "Component/ClearPortal.h"
#include "Component/Weapon/LMG.h"
#include "Component/Weapon/Pistol.h"
#include "Component/RedZone.h"
#include "Component/Weapon/Laser.h"
#include "Component/Weapon/Magneto.h"

IMGUI_app * IMGUIAPP = nullptr;
std::vector<Object*>Archetype_Container;

IMGUI_app::IMGUI_app()
{
	if (IMGUIAPP != nullptr)
	{
		std::cout << "Error: IMGUIAPP is already exist\n";
		return;
	}
	IMGUIAPP = this;
    Current_Edit_Type = ARCHETYPE_EDIT;
}

void IMGUI_app::Initialize()
{
	window = APP->Get_Window();
	glsl_version = "#version 150";
	gamedebugging_active = false;

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO &io = ImGui::GetIO();
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard | ImGuiConfigFlags_NavEnableGamepad;
	io.BackendFlags |= ImGuiBackendFlags_HasGamepad;

	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);

	// Setup style
	ImGui::StyleColorsDark();

	GameDebuggingInitialize();
	GlobalSettingsInitialize();
}

void IMGUI_app::Update(float)
{
    if (Input::Is_Triggered(GLFW_KEY_Y))
        gamedebugging_active = !gamedebugging_active;
	if (Input::Is_Triggered(GLFW_KEY_O))
	{
		globalsettings_active = !globalsettings_active;
		ImGui::SetWindowCollapsed("Settings", !globalsettings_active, ImGuiSetCond_Always);
		if (globalsettings_active)
			ImGui::SetWindowFocus("Settings");
	}

	if (!gamedebugging_active)
	{
		if (Current_Dragging_Object != nullptr)
		{
			if (Current_Dragging_Object->GetComponentByTemplate<Sprite>())
				Current_Dragging_Object->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 255, 255, 255));
			Current_Dragging_Object = nullptr;
		}
	}
	else
	{
		//TODO::Find a New Condition for Current_Object
		if (Current_Object == nullptr)
		{
			Current_Object = m_currentstate->GetObjectManager()->GetContainer().begin()->second.get();
			return;
		}
	}

	//io.NavInputs[ImGuiNavInput_DpadDown] = 1;


	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	if (gamedebugging_active)
		GameDebuggingUpdate();
	if (globalsettings_active)
		GlobalSettingsUpdate();
	// Rendering
	ImGui::Render();
	glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
	glfwMakeContextCurrent(window);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

}

void IMGUI_app::Close()
{
	errno_t err;
	FILE* fp;
	std::string filename = "data/tileinfo.json";
	err = fopen_s(&fp, filename.c_str(), "wb"); // non-Windows use "r"

	if (err != 0)
		return;

	char writeBuffer[65536];
	rapidjson::FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
	rapidjson::PrettyWriter<rapidjson::FileWriteStream> writer(os);
	writer.StartObject();
	writer.Key("TileInfo");
	writer.StartArray();
	writer.StartObject();
	writer.Key("TileNumber");
	writer.Int((m_max_tile_number));
	writer.EndObject();
	writer.EndArray();
	writer.EndObject();
	fclose(fp);

	for(auto iter = Archetype_Container.begin(); iter != Archetype_Container.end();)
	{
		(*iter)->Close();
		delete (*iter);
		iter = Archetype_Container.erase(iter);
	}
	Archetype_Container.clear();

	ImGui::DestroyContext();
}

IMGUI_app::~IMGUI_app()
{
}

void IMGUI_app::GameDebuggingInitialize()
{
	// Setup Component list vector
	// TODO:: If someone add Component, add this!
	component_list.emplace_back("Sprite");
	component_list.emplace_back("Physics");
	component_list.emplace_back("SpriteText");
	component_list.emplace_back("Camera");
	component_list.emplace_back("ParticlePhysics");
	component_list.emplace_back("Collision");
	component_list.emplace_back("Player");
	component_list.emplace_back("Bullet");
	component_list.emplace_back("Revolver");
	component_list.emplace_back("Rifle");
	component_list.emplace_back("Tomahawk");
	component_list.emplace_back("ParticleGenerator");
	component_list.emplace_back("Shotgun");
	component_list.emplace_back("Bow");
	component_list.emplace_back("Grenade");
	component_list.emplace_back("GuidedMissile");
	component_list.emplace_back("Button");
	component_list.emplace_back("Sniper");
	component_list.emplace_back("FlameThrower");
	component_list.emplace_back("WeaponGenerator");
	component_list.emplace_back("RedZone");
	component_list.emplace_back("Laser");
	component_list.emplace_back("Pistol");
	component_list.emplace_back("LightMachineGun");
	component_list.emplace_back("Magneto");
	component_list.emplace_back("ClearPortal");
	tile_type_list.emplace_back("Obstacle");
	tile_type_list.emplace_back("Oneway");
	tile_type_list.emplace_back("Ghost");
	tile_type_list.emplace_back("BackGround");
	tile_type_list.emplace_back("Empty");
	Load_Archetype();
	Current_Archetype = Archetype_Container[0];

	errno_t err;
	FILE* fp;
	std::string filename = "data/tileinfo.json";
	err = fopen_s(&fp, filename.c_str(), "rb");
	if (err != 0)
		return;

	char readBuffer[65536] = { 0 };
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	rapidjson::Document d;
	d.ParseStream(is);

	const rapidjson::Value& tile = d["TileInfo"];
	m_max_tile_number = tile[0].FindMember("TileNumber")->value.GetInt();

	fclose(fp);
}

void IMGUI_app::GameDebuggingUpdate()
{
	if (m_currentstate&&Current_Edit_Type == OBJECT_EDIT)
	{
		ShowObjmanager();
	}
	else if (Current_Edit_Type == ARCHETYPE_EDIT)
	{
		ShowArchetypemanager();
	}
	if (Current_Dragging_Object != nullptr&&Current_Edit_Type != ARCHETYPE_EDIT)
	{
		Current_Dragging_Object->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 255, 255, 255));
		Current_Dragging_Object = nullptr;
	}

	ImGui::Begin("Game Debugging");

	ImGui::RadioButton("Object Edit", &Current_Edit_Type, OBJECT_EDIT);
	ImGui::SameLine();
	ImGui::RadioButton("Tile Map Edit", &Current_Edit_Type, TILE_MAP_EDIT);
	ImGui::SameLine();
	ImGui::RadioButton("Archetype Edit", &Current_Edit_Type, ARCHETYPE_EDIT);

	switch (Current_Edit_Type)
	{
	case OBJECT_EDIT:
		Object_Edit();
		break;
	case TILE_MAP_EDIT:
		Tile_Map_Edit();
		break;
	case ARCHETYPE_EDIT:
		Archetype_Edit();
		break;
	default:
		break;
	}
	ImGui::NewLine();
	ImGui::NewLine();
	ENGINE->BackGroundMusic->ImGui_Setting();

	ImGui::End();
}

void IMGUI_app::GlobalSettingsInitialize()
{
	screen_resolution = { std::make_pair<std::string, std::vector<std::string>>("4:3",{ "640x480", "800x600", "960x720", "1024x768", "1280x960", "1400x1050", "1440x1080", "1600x1200" }),
		std::make_pair<std::string, std::vector<std::string>>("16:9",{ "1024x576", "1152x648", "1280x720", "1366x768", "1600x900", "1920x1080", "2560x1440", "3840x2160" }),
		std::make_pair<std::string, std::vector<std::string>>("16:10",{ "1280x800", "1440x900", "1680x1050", "1920x1200", "2560x1600" }) };
	screenratio_current = nullptr;
	resolution_current = nullptr;
	general_audio = 60;
	sfx_audio = 80;
	music_audio = 80;
}

void IMGUI_app::GlobalSettingsUpdate()
{
	ImGui::Begin("Settings", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
	ImGui::SetWindowSize("Settings", ImVec2(430, 280), ImGuiCond_Once);
	ImGui::SetWindowPos("Settings", ImVec2(ImGui::GetIO().DisplaySize.x - ImGui::GetWindowSize().x, 0), ImGuiCond_Always);
	//ImGui::SetWindowCollapsed("Settings", true, ImGuiCond_Once);
	ImGui::Text("Graphics");
	if (screenratio_current == nullptr)
	{
		screenratio_current = screen_resolution[0].first.c_str();
		screenratio_currentIndex = 0;
	}
	if (ImGui::BeginCombo("Ratio resolution", screenratio_current))
	{
		for (int i = 0; i < screen_resolution.size(); ++i)
		{
			const char *ratio = screen_resolution[i].first.c_str();
			bool is_selected = (screenratio_current == ratio);

			if (ImGui::Selectable(ratio, is_selected))
			{
				screenratio_current = ratio;
				screenratio_currentIndex = i;
				resolution_current = screen_resolution[i].second[0].c_str();
			}
			if (is_selected)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndCombo();
	}
	if (resolution_current == nullptr)
	{
		resolution_current = screen_resolution[0].second[0].c_str();
		resolution_currentIndex = 0;
	}
	if (ImGui::BeginCombo("Screen resolution", resolution_current))
	{
		for (int i = 0; i < screen_resolution[screenratio_currentIndex].second.size(); ++i)
		{
			const char *resolution = screen_resolution[screenratio_currentIndex].second[i].c_str();
			bool is_selected = (resolution_current == resolution);

			if (ImGui::Selectable(resolution, is_selected))
				resolution_current = resolution;
			if (is_selected)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndCombo();
	}
	ImGui::NewLine();
	ImGui::Text("Audio");
	ImGui::SliderInt("General", &general_audio, 0, 100);
	ImGui::SliderInt("SFX", &sfx_audio, 0, 100);
	ImGui::SliderInt("Music", &music_audio, 0, 100);

	ImGui::NewLine();
	ImGui::Separator();
	ImGui::NewLine();

	if (ImGui::Button("Save", ImVec2(60, 30))) {}

	ImGui::End();
}

void IMGUI_app::Set_Object(Object * object)
{
	Current_Object = object;
}

void IMGUI_app::Set_Tile(Tile* tile)
{
	Current_Tile_Info = tile;
}

void IMGUI_app::Set_TileMap(TileMap* tile_map)
{
	Current_Tile_Map = tile_map;
}

int IMGUI_app::Get_Edit_Type() const
{
	return Current_Edit_Type;
}

bool IMGUI_app::GetIsCollisionDraw()
{
	return Is_Collision_draw;
}


void IMGUI_app::Object_Edit()
{
	if(ImGui::Button("Save This to Archetype"))
	{
		for(auto i=Archetype_Container.begin();i!=Archetype_Container.end();++i)
		{
			//if object name is already exist
			if((*i)->GetName()==Current_Object->GetName())
			{
				//Do not save
				return;
			}
		}
		int OBJ_ID = Current_Object->GetID();
		int ARC_ID = Archetype_Container.back()->GetID() + 1;
		Current_Object->SetID(ARC_ID);
		Archetype_Container.push_back(Current_Object);
		Save_Archetype();
		Current_Object->SetID(OBJ_ID);
		Archetype_Container.back() = new Object(m_currentstate, Current_Object->GetName());		
		LoadArchetypeinfo(Archetype_Container.back(), ARC_ID);
	}

	Current_Object->ImGui_Setting();

	if (ImGui::Button("Delete this Object"))
	{
		STATE->Get_CurrentState()->GetObjectManager()->Delete_Object(Current_Object);
		Current_Object = nullptr;
	}

	ImGui::Combo("", &Current_Component_Number, vector_getter,
		&component_list, static_cast<int>(component_list.size()));
	ImGui::SameLine();

	// TODO:: If add new component fill this
	if (ImGui::Button("Add Component"))
	{
		switch (Current_Component_Number)
		{
		case SPRITE:
			if (Current_Object->GetComponentByTemplate<Sprite>() == nullptr)
			{
				Current_Object->Add_Component(new Sprite(Current_Object, "Sprite"));
				Current_Object->GetComponentByTemplate<Sprite>()->SetMesh(new Mesh());
				Current_Object->GetComponentByTemplate<Sprite>()->GetMesh()->MakeTexture();
				Current_Object->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 0, 0, 255));
				Current_Object->GetComponentByTemplate<Sprite>()->Init();
			}
			break;
		case PHYSICS:
			if (Current_Object->GetComponentByTemplate<Physics>() == nullptr)
			{
				Current_Object->Add_Component(new Physics(Current_Object, "Physics"));
				Current_Object->GetComponentByTemplate<Physics>()->Init();
			}
			break;
		case SPRITE_TEXT:
			if (Current_Object->GetComponentByTemplate<SpriteText>() == nullptr)
			{
				Current_Object->Add_Component(new SpriteText(Current_Object, "SpriteText"));
				Current_Object->GetComponentByTemplate<SpriteText>()->Init();
				Current_Object->GetComponentByTemplate<SpriteText>()->SetColor(Color(255, 0, 0, 255));
				Current_Object->GetComponentByTemplate<SpriteText>()->SetString("Hello");
			}
			break;
		case CAMERA:
			if (Current_Object->GetComponentByTemplate<Camera>() == nullptr)
			{
				Current_Object->Add_Component(new Camera(Current_Object, "Camera"));
				Current_Object->GetComponentByTemplate<Camera>()->Init();
			}
			break;
		case PARTICLEPHYSICS:
			if (Current_Object->GetComponentByTemplate<ParticlePhysics>() == nullptr)
			{
				Current_Object->Add_Component(new ParticlePhysics(Current_Object, "ParticlePhysics"));
				Current_Object->GetComponentByTemplate<ParticlePhysics>()->Init();
			}
			break;
		case COLLISION:
			if (Current_Object->GetComponentByTemplate<Collision>() == nullptr)
			{
				Current_Object->Add_Component(new Collision(Current_Object));
				Current_Object->GetComponentByTemplate<Collision>()->Init();
			}
			break;
		case PLAYER:
			if (Current_Object->GetComponentByTemplate<Player>() == nullptr)
			{
				Current_Object->Add_Component(new Player(Current_Object));
				Current_Object->GetComponentByTemplate<Player>()->Init();
			}
			break;
		case BULLET:
			if (Current_Object->GetComponentByTemplate<Bullet>() == nullptr)
			{
				Current_Object->Add_Component(new Bullet(Current_Object));
				Current_Object->GetComponentByTemplate<Bullet>()->Init();
			}
			break;
		case REVOLVER:
			if (Current_Object->GetComponentByTemplate<Revolver>() == nullptr)
			{
				Current_Object->Add_Component(new Revolver(Current_Object));
				Current_Object->GetComponentByTemplate<Revolver>()->Init();
			}
			break;
		case RIFLE:
			if (Current_Object->GetComponentByTemplate<Rifle>() == nullptr)
			{
				Current_Object->Add_Component(new Rifle(Current_Object));
				Current_Object->GetComponentByTemplate<Rifle>()->Init();
			}
			break;
		case TOMAHAWK:
			if (Current_Object->GetComponentByTemplate<Tomahawk>() == nullptr)
			{
				Current_Object->Add_Component(new Tomahawk(Current_Object));
				Current_Object->GetComponentByTemplate<Tomahawk>()->Init();
			}
			break;
		case PARTICLEGENERATOR:
			if (Current_Object->GetComponentByTemplate<ParticleGenerator>() == nullptr)
			{
				Current_Object->Add_Init_Component(new ParticleGenerator(Current_Object));
			}
			break;
		case SHOTGUN:
			if(Current_Object->GetComponentByTemplate<Shotgun>()==nullptr)
			{
				Current_Object->Add_Init_Component(new Shotgun(Current_Object));
			}
			break;
		case GUIDEDMISSILE:
			if (Current_Object->GetComponentByTemplate<GuidedMissile>() == nullptr)
			{
				Current_Object->Add_Init_Component(new GuidedMissile(Current_Object));
			}
			break;
		case BOW:
			if (Current_Object->GetComponentByTemplate<Bow>() == nullptr)
			{
				Current_Object->Add_Init_Component(new Bow(Current_Object));
			}
			break;
		case GRENADE:
			if (Current_Object->GetComponentByTemplate<Grenade>() == nullptr)
			{
				Current_Object->Add_Init_Component(new Grenade(Current_Object));
			}
			break;
		case SNIPER:
			if (Current_Object->GetComponentByTemplate<Sniper>() == nullptr)
			{
				Current_Object->Add_Init_Component(new Sniper(Current_Object));
			}
			break;
		case FLAMETHROWER:
			if (Current_Object->GetComponentByTemplate<FlameThrower>() == nullptr)
			{
				Current_Object->Add_Init_Component(new FlameThrower(Current_Object));
			}
			break;
		case WEAPONGENERATOR:
			if (Current_Object->GetComponentByTemplate<WeaponGenerator>() == nullptr)
			{
				Current_Object->Add_Init_Component(new WeaponGenerator(Current_Object));
			}
			break;
		case REDZONE:
			if (Current_Object->GetComponentByTemplate<RedZone>() == nullptr)
			{
				Current_Object->Add_Init_Component(new RedZone(Current_Object));
			}
			break;
		case LASER:
			if (Current_Object->GetComponentByTemplate<Laser>() == nullptr)
			{
				Current_Object->Add_Init_Component(new Laser(Current_Object));
			}
			break;
		case PISTOL:
			if (Current_Object->GetComponentByTemplate<Pistol>() == nullptr)
			{
				Current_Object->Add_Init_Component(new Pistol(Current_Object));
			}
			break;
		case LMG:
			if (Current_Object->GetComponentByTemplate<LightMachineGun>() == nullptr)
			{
				Current_Object->Add_Init_Component(new LightMachineGun(Current_Object));
			}
			break;
		case MAGNETO:
			if (Current_Object->GetComponentByTemplate<Magneto>() == nullptr)
			{
				Current_Object->Add_Init_Component(new Magneto(Current_Object));
			}
			break;
		case CLEARPORTAL:
			if (Current_Object->GetComponentByTemplate<ClearPortal>() == nullptr)
			{
				Current_Object->Add_Init_Component(new ClearPortal(Current_Object));
			}
			break;
		default:
			break;
		}
	}
}

void IMGUI_app::Tile_Map_Edit()
{
	ImGui::NewLine();

	if(ImGui::BeginCombo("TileLevel", ("TileMap" + std::to_string(m_current_tile_num)).c_str()))
	{
		for (unsigned i = 0; i < m_max_tile_number; ++i)
		{
			bool is_selected = (m_current_tile_num == i);
			if (ImGui::Selectable(("TileMap" + std::to_string(i)).c_str(), is_selected))
			{
				m_current_tile_num = i;
			}
			if (is_selected)
			{
				ImGui::SetItemDefaultFocus();
			}
		}
		ImGui::EndCombo();
	}

	if(ImGui::Button("Add new TileMap"))
	{
		++m_max_tile_number;

	}

	if (ImGui::Button("Save Current TileMap"))
	{
		//TODO:State number set
		Current_Tile_Map->Save_TileMap(m_current_tile_num);
	}
	ImGui::SameLine();
	if (ImGui::Button("Load TileMap"))
	{
		//TODO:State number set
		Current_Tile_Map->Load_TileMap(m_current_tile_num);
		m_currentstate->SetTileMap(m_current_tile_num);
	}

	//TODO:: Need Tile Map select
	unsigned int texture_id = Current_Object->GetState()->GetGraphicSystem()->GetTexture("tile_map")->GetID();
	ImTextureID Tile_ID = (void*)(intptr_t)texture_id;


	if(ImGui::Combo("", &Current_Tile_Type_Number, vector_getter,
		&tile_type_list, static_cast<int>(tile_type_list.size())))
	{
		Current_Tile_Info->Type = static_cast<Tile_Type>(Current_Tile_Type_Number);
	}

	ImGui::Image(Tile_ID, ImVec2(30.f, 30.f), 
		ImVec2(Current_Tile_Info->Tile_Map_Position.x, Current_Tile_Info->Tile_Map_Position.y),
		ImVec2(Current_Tile_Info->Tile_Map_Position.x + 1 / 24.f, Current_Tile_Info->Tile_Map_Position.y + 1 / 16.f));

	for(int y=0;y<16;++y)
	{
		for(int x=0;x<24;++x)
		{
			if (ImGui::ImageButton(Tile_ID, ImVec2(16.f, 16.f),x,y, ImVec2(x / 24.f, y / 16.f), ImVec2((x + 1) / 24.f, (y + 1) / 16.f)))
			{
				Current_Tile_Info->Set_Tile_Map_Number(texture_id);
				Current_Tile_Info->Tile_Map_Position = vector2(static_cast<float>(x / 24.0f), static_cast<float>(y / 16.0f));
				Current_Tile_Info->m_UnitSize = vector2(1.0f / 24.0f, 1.0f / 16.0f);
			}
			ImGui::SameLine();
		}
		ImGui::NewLine();
	}
}

void IMGUI_app::Archetype_Edit()
{
	ImGui::NewLine();
	if (ImGui::Button("Save all Archetype to json"))
	{
		Save_Archetype();
	}
	if(Current_Archetype==nullptr)
	{
		return;
	}
	Current_Archetype->ImGui_Setting();


	if (ImGui::Button("Delete this Archetype"))
	{
		Archetype_Container.erase(std::find(Archetype_Container.begin(), Archetype_Container.end(), Current_Archetype));
		delete Current_Archetype;
		Current_Archetype = nullptr;
		return;
	}

	ImGui::Combo("", &Current_Component_Number, vector_getter,
		&component_list, static_cast<int>(component_list.size()));
	ImGui::SameLine();

	// TODO:: If add new component fill this
	if (ImGui::Button("Add Component"))
	{
		switch (Current_Component_Number)
		{
		case SPRITE:
			if (Current_Archetype->GetComponentByTemplate<Sprite>() == nullptr)
			{
				Current_Archetype->Add_Component(new Sprite(Current_Archetype, "Sprite"));
				Current_Archetype->GetComponentByTemplate<Sprite>()->SetMesh(new Mesh());
				Current_Archetype->GetComponentByTemplate<Sprite>()->GetMesh()->MakeTexture();
				Current_Archetype->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 0, 0, 255));
				Current_Archetype->GetComponentByTemplate<Sprite>()->Init();
			}
			break;
		case PHYSICS:
			if (Current_Archetype->GetComponentByTemplate<Physics>() == nullptr)
			{
				Current_Archetype->Add_Component(new Physics(Current_Archetype, "Physics"));
				Current_Archetype->GetComponentByTemplate<Physics>()->Init();
			}
			break;
		case SPRITE_TEXT:
			if (Current_Archetype->GetComponentByTemplate<SpriteText>() == nullptr)
			{
				Current_Archetype->Add_Component(new SpriteText(Current_Archetype, "SpriteText"));
				Current_Archetype->GetComponentByTemplate<SpriteText>()->Init();
				Current_Archetype->GetComponentByTemplate<SpriteText>()->SetColor(Color(255, 0, 0, 255));
				Current_Archetype->GetComponentByTemplate<SpriteText>()->SetString("Hello");
			}
			break;
		case CAMERA:
			if (Current_Archetype->GetComponentByTemplate<Camera>() == nullptr)
			{
				Current_Archetype->Add_Component(new Camera(Current_Archetype, "Camera"));
				Current_Archetype->GetComponentByTemplate<Camera>()->Init();
			}
			break;
		case PARTICLEPHYSICS:
			if (Current_Archetype->GetComponentByTemplate<ParticlePhysics>() == nullptr)
			{
				Current_Archetype->Add_Component(new ParticlePhysics(Current_Archetype, "ParticlePhysics"));
				Current_Archetype->GetComponentByTemplate<ParticlePhysics>()->Init();
			}
			break;
		case COLLISION:
			if (Current_Archetype->GetComponentByTemplate<Collision>() == nullptr)
			{
				Current_Archetype->Add_Component(new Collision(Current_Archetype));
				Current_Archetype->GetComponentByTemplate<Collision>()->Init();
			}
			break;
		case PLAYER:
			if (Current_Archetype->GetComponentByTemplate<Player>() == nullptr)
			{
				Current_Archetype->Add_Component(new Player(Current_Archetype));
				Current_Archetype->GetComponentByTemplate<Player>()->Init();
			}
			break;
		case BULLET:
			if (Current_Archetype->GetComponentByTemplate<Bullet>() == nullptr)
			{
				Current_Archetype->Add_Component(new Bullet(Current_Archetype));
				Current_Archetype->GetComponentByTemplate<Bullet>()->Init();
			}
			break;
		case REVOLVER:
			if (Current_Archetype->GetComponentByTemplate<Revolver>() == nullptr)
			{
				Current_Archetype->Add_Component(new Revolver(Current_Archetype));
				Current_Archetype->GetComponentByTemplate<Revolver>()->Init();
			}
			break;
		case RIFLE:
			if (Current_Archetype->GetComponentByTemplate<Rifle>() == nullptr)
			{
				Current_Archetype->Add_Component(new Rifle(Current_Archetype));
				Current_Archetype->GetComponentByTemplate<Rifle>()->Init();
			}
			break;
		case TOMAHAWK:
			if (Current_Archetype->GetComponentByTemplate<Tomahawk>() == nullptr)
			{
				Current_Archetype->Add_Component(new Tomahawk(Current_Archetype));
				Current_Archetype->GetComponentByTemplate<Tomahawk>()->Init();
			}
			break;
		case PARTICLEGENERATOR:
			if (Current_Archetype->GetComponentByTemplate<ParticleGenerator>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new ParticleGenerator(Current_Archetype));
			}
			break;
		case SHOTGUN:
			if (Current_Archetype->GetComponentByTemplate<Shotgun>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new Shotgun(Current_Archetype));
			}
			break;
		case BOW:
			if (Current_Archetype->GetComponentByTemplate<Bow>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new Bow(Current_Archetype));
			}
			break;
		case GRENADE:
			if (Current_Archetype->GetComponentByTemplate<Grenade>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new Grenade(Current_Archetype));
			}
			break;
		case SNIPER:
			if (Current_Archetype->GetComponentByTemplate<Sniper>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new Sniper(Current_Archetype));
			}
			break;
		case FLAMETHROWER:
			if (Current_Archetype->GetComponentByTemplate<FlameThrower>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new FlameThrower(Current_Archetype));
			}
			break;
		case WEAPONGENERATOR:
			if (Current_Archetype->GetComponentByTemplate<WeaponGenerator>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new WeaponGenerator(Current_Archetype));
			}
			break;
		case REDZONE:
			if (Current_Archetype->GetComponentByTemplate<RedZone>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new RedZone(Current_Archetype));
			}
			break;
		case LASER:
			if (Current_Archetype->GetComponentByTemplate<Laser>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new Laser(Current_Archetype));
			}
			break;
		case PISTOL:
			if (Current_Archetype->GetComponentByTemplate<Pistol>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new Pistol(Current_Archetype));
			}
			break;
		case LMG:
			if (Current_Archetype->GetComponentByTemplate<LightMachineGun>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new LightMachineGun(Current_Archetype));
			}
			break;
		case MAGNETO:
			if (Current_Archetype->GetComponentByTemplate<Magneto>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new Magneto(Current_Archetype));
			}
			break;
		case CLEARPORTAL:
			if (Current_Archetype->GetComponentByTemplate<ClearPortal>() == nullptr)
			{
				Current_Archetype->Add_Init_Component(new ClearPortal(Current_Archetype));
			}
			break;
		default:
			break;
		}
	}
}

void IMGUI_app::RegisterCurrentState(State* state)
{
	if(m_currentstate != state)
	{
		m_currentstate = state;
	}
}

Object* IMGUI_app::GetObject() const
{
	return Current_Object;
}

bool IMGUI_app::IsDebuggingActive() const
{
	return gamedebugging_active;
}

bool IMGUI_app::IsGlobalSettingsActive() const
{
	return globalsettings_active;
}

void IMGUI_app::ShowObjmanager()
{
	ImGui::Begin("ObjectManager");

	ImGui::Checkbox("Draw_Collision_Box", &Is_Collision_draw);

	ImGui::Text("Object list");

	auto& container = m_currentstate->GetObjectManager()->GetContainer();
	for (auto& obj : container)
	{
		if (obj.second->HasParent())
		{
			continue;
		}

		ListObject(obj.second.get());
	}

	ImGui::End();
}

void IMGUI_app::ShowArchetypemanager()
{
	ImGui::Begin("Archetype manager");

	ImGui::Text("Archetype list");

	for (auto& obj : Archetype_Container)
	{
		if (obj->HasParent())
		{
			continue;
		}

		ListArchetype(obj);
	}

	ImGui::End();
}

void IMGUI_app::ListObject(Object* obj)
{
	unsigned int id = obj->GetID();

	std::string text = obj->GetName() + "###" + std::to_string(id);

	bool isSame = Current_Object->GetID() == obj->GetID();

	if (isSame)
	{
		ImGui::PushID(0);
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.59f, 0.78f, 0.26f, 0.40f));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.59f, 0.78f, 0.26f, 1.00f));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.53f, 0.78f, 0.06f, 1.00f));
	}

	if(ImGui::Button(text.c_str(), ImVec2(60, 20)))
	{
		Current_Object = obj;
	}

	if (isSame)
	{
		ImGui::PopStyleColor(3);
		ImGui::PopID();
	}

	if(ImGui::IsItemClicked(1) && ImGui::IsItemHovered())
	{
		obj->Detach();
	}

	if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
	{
		ImGui::SetDragDropPayload("Object", &id, sizeof(unsigned int));

		ImGui::Text(obj->GetName().c_str());

		ImGui::EndDragDropSource();
	}
	if (ImGui::BeginDragDropTarget())
	{
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("Object"))
		{
			unsigned int temp = *(unsigned int *)(payload->Data);

			m_currentstate->GetObjectManager()->GetContainer().at(temp)->AttatchToParent(obj);
			if(temp>id)
			{
				m_currentstate->GetObjectManager()->SwapObjectPosition(temp, id);
			}
		}
		ImGui::EndDragDropTarget();
	}

	if (obj->HasChild())
	{
		ImGui::Indent();
		for (auto& child : obj->GetChildren())
		{
			ListObject(child);
		}
		ImGui::Unindent();
	}
}

void IMGUI_app::ListArchetype(Object* obj)
{
	int id = obj->GetID();
	std::string text = obj->GetName() + "###" + std::to_string(id);
	if (ImGui::Button(text.c_str(), ImVec2(120, 20)))
	{
		Current_Archetype = obj;
	}

	if (ImGui::IsItemClicked(1) && ImGui::IsItemHovered())
	{
		Current_Dragging_Object = new Object(m_currentstate,Current_Archetype->GetName());
		LoadArchetypeinfo(Current_Dragging_Object, id);
		m_currentstate->GetObjectManager()->Add_Object(Current_Dragging_Object);
	}
	if (Current_Dragging_Object&&Input::Is_Pressed(GLFW_MOUSE_BUTTON_RIGHT))
	{
		Current_Dragging_Object->GetTransform()->SetPosition(m_currentstate->GetCamera()->GetLocalPosition(Input::Get_Mouse_Position()));

		if (Current_Dragging_Object->GetComponentByTemplate<Sprite>())
		{
			Current_Dragging_Object->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 255, 255, 125));
		}
	}
	else if(Current_Dragging_Object&&Input::Is_Released(GLFW_MOUSE_BUTTON_RIGHT))
	{
		if (Current_Dragging_Object->GetComponentByTemplate<Sprite>())
		{
			Current_Dragging_Object->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 255, 255, 255));
		}
		Current_Dragging_Object = nullptr;
	}
}

void IMGUI_app::Load_Archetype()
{
	errno_t err;
	FILE* fp;
	std::string filename = "data/archetype.json";
	err = fopen_s(&fp, filename.c_str(), "rb");
	if (err != 0)
		return;

	char readBuffer[65536] = { 0 };
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	rapidjson::Document d;
	d.ParseStream(is);

	const rapidjson::Value& obj = d["Object"];
	for (rapidjson::SizeType i = 0; i < obj.Size(); i++)
	{
		Archetype_Container.emplace_back(new Object(nullptr, obj[i].FindMember("m_Name")->value.GetString()));
		Archetype_Container.back()->Deserialization(obj[i], false);
		Archetype_Container.back()->SetID(i);
	}
	SOUND->Clear_AudioList();
	fclose(fp);
}

void IMGUI_app::Save_Archetype()
{
	errno_t err;
	FILE* fp;
	std::string filename = "data/archetype.json";
	err = fopen_s(&fp, filename.c_str(), "wb"); // non-Windows use "r"

	if (err != 0)
		return;

	char writeBuffer[65536];
	rapidjson::FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
	rapidjson::PrettyWriter<rapidjson::FileWriteStream> writer(os);
	writer.StartObject();
	writer.Key("Object");
	writer.StartArray();
	for (auto i = Archetype_Container.begin(); i != Archetype_Container.end(); ++i)
	{
		(*i)->Serialization(writer);
	}
	writer.EndArray();
	writer.EndObject();
	fclose(fp);
}

//TODO:: change num to ..enum? name?
void LoadArchetypeinfo(Object* object, int num)
{
	errno_t err;
	FILE* fp;
	std::string filename = "data/archetype.json";
	err = fopen_s(&fp, filename.c_str(), "rb");
	if (err != 0)
		return;

	char readBuffer[65536] = { 0 };
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	rapidjson::Document d;
	d.ParseStream(is);

	const rapidjson::Value& obj = d["Object"];
	object->Deserialization(obj[num]);

	object->SetID(num);

	fclose(fp);
}

void LoadArchetypeinfo(Object* object, std::string name)
{
	errno_t err;
	FILE* fp;
	std::string filename = "data/archetype.json";
	err = fopen_s(&fp, filename.c_str(), "rb");
	if (err != 0)
		return;

	char readBuffer[65536] = { 0 };
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	rapidjson::Document d;
	d.ParseStream(is);

	const rapidjson::Value& obj = d["Object"];

	int num = 0;

	for(auto& archetype : Archetype_Container)
	{
		if(archetype->GetName() == name)
		{
			break;
		}
		++num;
	}

	object->Deserialization(obj[num]);

	object->SetID(num);

	fclose(fp);
}