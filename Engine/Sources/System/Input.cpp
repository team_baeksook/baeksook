/***********************************************************************
File name : Input.cpp
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Input.h"
#include "JoyStick.h"
#include <iostream>
#include "Logger.h"
#include "Engine_Utility.h"
#include "User.h"
#include "StateManager.h"

std::bitset<GLFW_KEY_LAST> Input::k_pressed;
std::bitset<GLFW_KEY_LAST> Input::k_triggered;
vector2 Input::m_pos;
float Input::m_scroll;
std::map<CONTROL_SETTING, int> Input::m_key_binding;
std::vector<JoyStick*> Input::m_joystick;

void Input::Initialize()
{
	k_pressed.reset();
	k_triggered.reset();
	m_pos = vector2(0,0);

	m_key_binding[CONTROL_SETTING::KEY_UP] = GLFW_KEY_W;
	m_key_binding[CONTROL_SETTING::KEY_DOWN] = GLFW_KEY_S;
	m_key_binding[CONTROL_SETTING::KEY_LEFT] = GLFW_KEY_A;
	m_key_binding[CONTROL_SETTING::KEY_RIGHT] = GLFW_KEY_D;
	m_key_binding[CONTROL_SETTING::KEY_JUMP] = GLFW_KEY_W;
    m_key_binding[CONTROL_SETTING::KEY_JUMP_ALT] = GLFW_KEY_SPACE;
    m_key_binding[CONTROL_SETTING::KEY_FIRE] = GLFW_MOUSE_BUTTON_LEFT;
    m_key_binding[CONTROL_SETTING::KEY_DASH] = GLFW_MOUSE_BUTTON_RIGHT;
	m_key_binding[CONTROL_SETTING::KEY_THROW] = GLFW_KEY_E;

	m_key_binding[CONTROL_SETTING::KEY_ATTACK] = GLFW_KEY_L;
	m_key_binding[CONTROL_SETTING::KEY_PICK] = GLFW_KEY_S;

    //input test for joystick
	int index;
	for(index = GLFW_JOYSTICK_1; index < GLFW_JOYSTICK_LAST; ++index)
	{
		if(glfwJoystickPresent(index) == GLFW_TRUE)
		{
			m_joystick.push_back(new JoyStick(index, index));
			STATE->m_users.push_back(new User(index));
			STATE->m_users.back()->Initialize();
		}
		else
		{
			break;
		}
	}

	logger::log_handler.PrintLogOnConsole(utility::MakeStringUsingFormat("The {} controllers are connected : ", index));

	for(int i = 0; i < index; ++i)
	{
		logger::log_handler.PrintLogOnConsole(utility::MakeStringUsingFormat("{} connected",
			glfwGetJoystickName(i)));
	}
}

void Input::Set_Keyboard(KEY_STATE state, int key_num)
{
	switch (state)
	{
	case KEY_UP:
		k_pressed[key_num] = false;
		break;
	case KEY_TRIGGERED:
		k_triggered[key_num] = true;
		k_pressed[key_num] = true;
		break;
	case KEY_DOWN:
		k_pressed[key_num] = true;
		break;
	}
}

bool Input::Is_Pressed(CONTROL_SETTING key_num, int playerNum)
{
	if(playerNum >= 0)
	{
		for(auto& ref:m_joystick)
		{
			if(int pNum=ref->Get_PlayerNum();pNum==playerNum)
			{
				return ref->is_pressed(key_num);
			}
		}
		return false;
	}

	CONTROL_SETTING KEY = static_cast<CONTROL_SETTING>(static_cast<int>(key_num) + playerNum);
	return k_pressed[m_key_binding.at(KEY)];
}

bool Input::Is_Triggered(CONTROL_SETTING key_num, int playerNum)
{
	if (playerNum >= 0)
	{
		for (auto& ref : m_joystick)
		{
			if (int pNum = ref->Get_PlayerNum(); pNum == playerNum)
			{
				return ref->is_triggered(key_num);
			}
		}
		return false;
	}

	CONTROL_SETTING KEY = static_cast<CONTROL_SETTING>(static_cast<int>(key_num) + playerNum);
	return k_triggered[m_key_binding.at(KEY)];
}

bool Input::Is_Released(CONTROL_SETTING key_num, int playerNum)
{
	if (playerNum >= 0)
	{
		for (auto& ref : m_joystick)
		{
			if (int pNum = ref->Get_PlayerNum(); pNum == playerNum)
			{
				return ref->is_released(key_num);
			}
		}
		return false;
	}

	CONTROL_SETTING KEY = static_cast<CONTROL_SETTING>(static_cast<int>(key_num) + playerNum);
	return !k_pressed[m_key_binding.at(KEY)];
}

bool Input::Is_Pressed(int key_num)
{
	return k_pressed[key_num];
}

bool Input::Is_Triggered(int key_num)
{
	return k_triggered[key_num];
}

bool Input::Is_Released(int key_num)
{
	return !k_pressed[key_num];
}

void Input::Reset()
{
	k_triggered.reset();
    m_scroll = 0;
}

void Input::ResetAll()
{
	k_pressed.reset();
	k_triggered.reset();
	m_scroll = 0;
}

void Input::Update()
{
	Reset();
	for(auto joystick : m_joystick)
	{
		joystick->Update();
	}
}

void Input::Set_Mouse_Position(float x, float y)
{
	m_pos.x = x-APP->Width/2.f;
	m_pos.y = -y+APP->Height/2.f;
}

vector2 Input::Get_Mouse_Position()
{
	return m_pos;
}

std::vector<JoyStick*> Input::Get_Joystick()
{
	return m_joystick;
}

bool Input::Is_Joystick_Exist(int player_num)
{
	for (auto i = m_joystick.begin(); i != m_joystick.end(); ++i)
	{
		if ((*i)->Get_PlayerNum() == player_num)
		{
			return true;
		}
	}
	return false;
}

void Input::Delete_Specific_Joystick(int index)
{
	bool is_delete = false;
	for(auto i=m_joystick.begin();i!=m_joystick.end();++i)
	{
		if((*i)->Get_JoystickId()==index)
		{
			i=m_joystick.erase(i);
			is_delete = true;
			break;
		}
	}
	if (is_delete)
	{
		std::vector<User*>& user_vector = STATE->m_users;
		for (auto user = user_vector.begin(); user != user_vector.end(); ++user)
		{
			if (static_cast<int>((*user)->GetPlayerNum()) == index + 1)
			{
				(*user)->Close();
				delete (*user);
				user_vector.erase(user);
				return;
			}
		}
	}

	//fail to delete joystick
	logger::log_handler.PrintLogOnConsole(utility::MakeStringUsingFormat("The {} index controller fails to delete: ", index));
}

void Input::Add_Joystick(int index)
{
	m_joystick.push_back(new JoyStick(index, index));

	User* new_user = new User(index);
	STATE->m_users.push_back(new_user);
	new_user->Initialize();
	new_user->ChangeCurrentState(STATE->Get_CurrentState());
}

vector2 Input::Get_Left_Stick_Position(int playerNum)
{
	if (playerNum >= 0)
	{
		for (auto& ref : m_joystick)
		{
			if (int pNum = ref->Get_PlayerNum(); pNum == playerNum)
			{
				return ref->Getlstick();
			}
		}
	}
	return 0;//error!
}

vector2 Input::Get_Right_Stick_Position(int playerNum)
{
	if (playerNum >= 0)
	{
		for (auto& ref : m_joystick)
		{
			if (int pNum = ref->Get_PlayerNum(); pNum == playerNum)
			{
				return ref->Getrstick();
			}
		}
	}
	return 0;//error!
}

void Input::Set_Mouse(KEY_STATE state, int key_num)
{
	switch (state)
	{
	case KEY_UP:
		k_pressed[key_num] = false;
		break;
	case KEY_TRIGGERED:
		if (k_pressed[key_num] == false)
			k_triggered[key_num] = true;
		k_pressed[key_num] = true;
		break;
	}
}

void Input::SetScroll(float amount)
{
    m_scroll = amount;
}

float Input::GetScrollInfo()
{
    return m_scroll;
}

void Input::Close()
{
	for (auto joy : m_joystick)
	{
		joy->Close();
		delete joy;
	}
	m_joystick.clear();
	m_key_binding.clear();
}

bool Input::IsProceed()
{
	bool result = false;
	for (auto joy : m_joystick)
	{
		if (joy->is_triggered(JOYSTICK_KEY::KEY_A))
		{
			result = true;
		}
	}
	if (Input::Is_Triggered(GLFW_KEY_SPACE))
	{
		result = true;
	}
	return result;
}