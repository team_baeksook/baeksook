/***********************************************************************
File name		: SceneIdentifier.h
Project name	: WFPT
Author			: Maxime Cauvin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/

#pragma once
enum SceneIdentifier : int {
	UNDEFINED = -1,
	SPLASHSCREEN = 0,
	GAMELOGO = 1,
	MAINMENU = 2,
	SELECTGAMEMODE = 3,
	QUITCONFIRMATION = 4,
	CHARACTERSELECTION = 5,
	MAPSELECTION = 6,
	GAMESCREEN = 7,
	TUTORIAL = 8,
	TUTORIAL2 = 9
};