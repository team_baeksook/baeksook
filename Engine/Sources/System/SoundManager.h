/***********************************************************************
File name		: SoundManager.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "System.h"
#include "FMOD/fmod.hpp"
#include "FMOD/fmod_errors.h"
#include <vector>
#include "Audio.h"
#include "Logger.h"

class SoundManager:public System
{
public:
	void Initialize() override;
	void Update(float dt) override;
    void Close() override;
	~SoundManager() =default;

	void Load_All_Sound();
	void Create_Sound(FMOD::Sound** pSound, const char* filepath);
	void Play_Sound(FMOD::Sound* pSound, FMOD::Channel ** pChannel, bool loop = false);

	void Add_AudiotoList(Audio* ptr)
	{
		audio_list.emplace_back(ptr);
	};
	void Delete_AudioinList(Audio* ptr)
	{
		if (audio_list.empty())
		{
			return;
		}
		for (auto iter = audio_list.begin(); iter != audio_list.end(); ++iter)
		{
			if (*iter == ptr)
			{
				delete *iter;
				*iter = nullptr;
				iter=audio_list.erase(iter);
				return;
			}
		}
		logger::log_handler.PrintLogOnConsole("Fail to delete Audio", spdlog::level::level_enum::err);
		return;
	};
	void Clear_AudioList();

private:
	FMOD::System * System = nullptr;

	FMOD::ChannelGroup * ChannelGroup = nullptr;

	std::vector<Audio*> audio_list;
};

extern SoundManager* SOUND;

extern std::vector<std::string>music_list;