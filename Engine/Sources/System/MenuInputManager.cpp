/***********************************************************************
File name		: MenuInputManager.cpp
Project name	: WFPT
Author			: Maxime Cauvin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/

#include "MenuInputManager.h"

MenuInputManager::MenuInputManager() {}
MenuInputManager::~MenuInputManager() {}

int MenuInputManager::UpdateMenuNavigation(std::vector<Button *> buttons, int currentIdx)
{
	if (IMGUIAPP->IsDebuggingActive() || IMGUIAPP->IsGlobalSettingsActive()) return currentIdx;
	bool prevState = Input::Is_Triggered(GLFW_KEY_UP) || Input::Is_Triggered(GLFW_KEY_LEFT),
		 nextState = Input::Is_Triggered(GLFW_KEY_DOWN) || Input::Is_Triggered(GLFW_KEY_RIGHT);
	auto joysticks = Input::Get_Joystick();
	for (size_t i = 0; i < joysticks.size() && !prevState && !nextState; i++)
	{
		prevState = joysticks[i]->is_triggered(JOYSTICK_KEY::KEY_UP) || joysticks[i]->is_triggered(JOYSTICK_KEY::KEY_LEFT);
		if (prevState) break;
		nextState = joysticks[i]->is_triggered(JOYSTICK_KEY::KEY_DOWN) || joysticks[i]->is_triggered(JOYSTICK_KEY::KEY_RIGHT);
	}

	if (prevState)
	{
		if (currentIdx >= 0 && currentIdx < buttons.size())
			buttons[currentIdx]->SetSelected(false);
		currentIdx = (currentIdx - 1 >= 0) ? (currentIdx - 1) : (buttons.size() - 1);
		buttons[currentIdx]->SetSelected(true);
	}
	else if (nextState)
	{
		if (currentIdx >= 0 && currentIdx < buttons.size())
			buttons[currentIdx]->SetSelected(false);
		currentIdx = (currentIdx + 1 < buttons.size()) ? (currentIdx + 1) : (0);
		buttons[currentIdx]->SetSelected(true);
	}

	return currentIdx;
}

bool MenuInputManager::CheckMenuButtonBack(int key, JOYSTICK_KEY controllerKey, SceneIdentifier prevStateId)
{
	if (IMGUIAPP->IsDebuggingActive() || IMGUIAPP->IsGlobalSettingsActive()) return false;
	bool cancelState = Input::Is_Triggered(key);
	auto joysticks = Input::Get_Joystick();
	for (size_t i = 0; i < joysticks.size() && !cancelState; i++)
		cancelState = joysticks[i]->is_triggered(controllerKey);
	if (cancelState)
		STATE->Change_State(prevStateId);
	return cancelState;
}

bool MenuInputManager::CheckMenuButtonConfirm(int key, JOYSTICK_KEY controllerKey, std::vector<Button *> buttons, int currentIdx)
{
	if (IMGUIAPP->IsDebuggingActive() || IMGUIAPP->IsGlobalSettingsActive()) return false;
	bool confirmState = Input::Is_Triggered(key);
	auto joysticks = Input::Get_Joystick();
	for (size_t i = 0; i < joysticks.size() && !confirmState; i++)
		confirmState = joysticks[i]->is_triggered(controllerKey);
	if (confirmState)
	{
		if (currentIdx >= 0 && currentIdx < buttons.size())
			buttons[currentIdx]->TriggerCallbackClick();
	}
	return confirmState;
}
