/***********************************************************************
File name		: Application.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Application.h"
#include "Input.h"
#include "IMGUI/imgui.h"
#include "State.h"
#include "Logger.h"
#include "SoundManager.h"
#include "Graphic/texture.h"
#include "Engine.h"

#include <iostream>
#include <Windows.h>

Application* APP = nullptr;

Application::Application() :window(nullptr), Width(1280), Height(720)
{
    if (APP != nullptr)
    {
        std::cout << "Error: APP is already exist\n";
        return;
    }
    APP = this;

    graphic = new Graphic();
}

Application::~Application()
{
}

void Application::Initialize()
{
    logger::log_handler.PrintLogOnConsole("Initialization Application start");

    glfwSetErrorCallback(errorCallback);

    if (!glfwInit())
    {
        std::cerr << "Error: GLFW" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    window = glfwCreateWindow(Width, Height, "Window Title", NULL, NULL);
    monitor = glfwGetPrimaryMonitor();

    if (!window)
    {
        glfwTerminate();
        std::exit(EXIT_FAILURE);
    }
    //glfwSwapInterval(1);
    glfwMakeContextCurrent(window);
    glfwSetWindowUserPointer(window, this);
    glfwGetWindowPos(window, &X_pos, &Y_pos);

	glfwSetWindowPosCallback(window, window_position_callback);
    glfwSetWindowSizeCallback(window, window_size_callback);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetCursorPosCallback(window, cursor_position_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    glfwSetWindowCloseCallback(window, window_close_callback);
    glfwSetScrollCallback(window, scroll_callback);
	glfwSetDropCallback(window, drop_callback);
	glfwSetJoystickCallback(joystick_callback);

    Set_Window_Size(Width, Height);

	Input::Initialize();
    logger::log_handler.PrintLogOnConsole("Initialization application complete");

    graphic->Initialize();
}

void Application::Update(float)
{
    Input::Update();
    glfwPollEvents();
    glfwMakeContextCurrent(window);
    if (Input::Is_Triggered(GLFW_KEY_F))
    {
        Toggle_Fullscreen();
    }

	int viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	graphic->UpdateProjection(vector2(static_cast<float>(viewport[2]), static_cast<float>(viewport[3])));
}

void Application::Close()
{
    graphic->Close();

	delete graphic;

	Input::Close();

	glfwTerminate();
}

GLFWwindow * Application::Get_Window()
{
    return window;
}

vector2 Application::GetWindowSize()
{
    vector2 result(static_cast<float>(Width), static_cast<float>(Height));
    return result;
}

Graphic* Application::Get_Graphic()
{
	return graphic;
}

void Application::Set_Window_Size(int width, int height)
{
    Width = width;
    Height = height;
}

void Application::Show_FramePerSecond(float fps)
{
    std::string fps_string = std::string(" fps : ") + std::to_string((int)std::roundf(fps));
    std::string showTitle = Title + fps_string;
    glfwSetWindowTitle(window, showTitle.c_str());
}

void Application::Set_Window_Title(const std::string& title)
{
    Title = title;
}

void Application::Toggle_Fullscreen()
{
    if (!Is_Fullscreen)
    {
        glfwGetWindowPos(window, &X_pos, &Y_pos);
        glfwGetWindowSize(window, &Previous_Width, &Previous_Height);

        const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, 0);

		window_size_callback(window, mode->width, mode->height);
		//glfwSwapInterval(1);
	}
	else
	{
		glfwSetWindowMonitor(window, nullptr, X_pos, Y_pos, Previous_Width, Previous_Height, 0);
        //glfwSwapInterval(1);
    }
	Is_Fullscreen = !Is_Fullscreen;
}

void errorCallback(int /*errorCode*/, const char* errorDescription)
{
    std::cerr << "Error: " << errorDescription << std::endl;
}

void window_position_callback(GLFWwindow * /*window*/, int /*width*/, int /*height*/)
{
	ENGINE->GetTimer().ResetTime();
}

void window_size_callback(GLFWwindow* /*window*/, int width, int height)
{
    APP->Set_Window_Size(width, height);
    glViewport(0, 0, width, height);
}

void keyCallback(GLFWwindow* /*window*/, int key, int /*scancode*/, int action, int /*mods*/)
{
	if(key >= 348||key<0)
	{
		return;
	}
	if(key < 0)
	{
		return;
	}

    Input::Set_Keyboard(static_cast<KEY_STATE>(action), key);

    //ImGui Stuff
    ImGuiIO& io = ImGui::GetIO();
    if (action == GLFW_PRESS)
        io.KeysDown[key] = true;
    if (action == GLFW_RELEASE)
        io.KeysDown[key] = false;

    io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
    io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
    io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
    io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];

    //if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    //	glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void cursor_position_callback(GLFWwindow* /*window*/, double xpos, double ypos)
{
    Input::Set_Mouse_Position(static_cast<float>(xpos), static_cast<float>(ypos));
    Input::Get_Mouse_Position();
}

void mouse_button_callback(GLFWwindow* /*window*/, int button, int action, int /*mods*/)
{
    Input::Set_Mouse(static_cast<KEY_STATE>(action), button);
}

void window_close_callback(GLFWwindow* window)
{
    glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void scroll_callback(GLFWwindow* /*window*/, double xoffset, double yoffset)
{
    Input::SetScroll(static_cast<float>(yoffset));
    //ImGui Stuff
    ImGuiIO& io = ImGui::GetIO();
    io.MouseWheelH += static_cast<float>(xoffset);
    io.MouseWheel += static_cast<float>(yoffset);
}

void drop_callback(GLFWwindow* /*window*/, int count, const char** paths)
{
	int i;
	for (i = 0; i < count; i++)
	{
		std::string temp = paths[i];

		if (temp.find(".png")!= std::string::npos)
		{
			std::string filename=temp.substr(temp.find_last_of("\\") + 1);
			CopyFile(temp.c_str(), ("asset\\"+filename).c_str(), false);

			filename="asset/" + filename;

			FILE* fp;
			auto err= fopen_s(&fp,"data\\texture.json", "r");
			char readBuffer[65536];
			rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

			rapidjson::Document d, d2;
			d.ParseStream(is);
			auto& array=d["Texture"];
			assert(array.IsArray());
			_fcloseall();
			d2.SetObject();
			rapidjson::Value json_objects(rapidjson::kObjectType);
			json_objects.AddMember("Name",rapidjson::StringRef(filename.c_str()) , d2.GetAllocator());
			json_objects.AddMember("FrameSize", 1, d2.GetAllocator());
			json_objects.AddMember("FrameSpeed", 1.0, d2.GetAllocator());
			array.PushBack(json_objects, d2.GetAllocator());

			FILE* outfile;
			err= fopen_s(&outfile,"data\\texture.json", "w");
			char writeBuffer[65536];
			rapidjson::FileWriteStream os(outfile, writeBuffer, sizeof(writeBuffer));

			rapidjson::PrettyWriter<rapidjson::FileWriteStream> writer(os);
			d.Accept(writer);
			_fcloseall();

			Texture * texture = new Texture();
			texture->InitWithTexturePath(filename, 1);
			texture->SetFrameSpeed(1.0);
			APP->Get_Graphic()->InsertNewTexture(texture->GetName(), texture);
		}
		else if(temp.find(".wav") != std::string::npos || temp.find(".mp3") != std::string::npos)
		{
			std::string filename = temp.substr(temp.find_last_of("\\") + 1);
			CopyFile(temp.c_str(), ("asset\\Sound\\" + filename).c_str(), false);
			filename = "asset/Sound/" + filename;

			FILE* fp;
			auto err = fopen_s(&fp, "data\\sound.json", "r");
			char readBuffer[65536];
			rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

			rapidjson::Document d, d2;
			d.ParseStream(is);
			auto& array = d["Sound"];
			assert(array.IsArray());
			_fcloseall();
			d2.SetObject();
			rapidjson::Value json_objects(rapidjson::kObjectType);
			json_objects.AddMember("Name", rapidjson::StringRef(filename.c_str()), d2.GetAllocator());
			array.PushBack(json_objects, d2.GetAllocator());

			FILE* outfile;
			err = fopen_s(&outfile, "data\\sound.json", "w");
			char writeBuffer[65536];
			rapidjson::FileWriteStream os(outfile, writeBuffer, sizeof(writeBuffer));

			rapidjson::PrettyWriter<rapidjson::FileWriteStream> writer(os);
			d.Accept(writer);
			_fcloseall();

			music_list.emplace_back(filename);
		}


	}
}

void joystick_callback(int joystick, int joystick_event)
{
	if(joystick_event == GLFW_DISCONNECTED)
	{
		Input::Delete_Specific_Joystick(joystick);
		std::cout << joystick << "pad disconnect" << std::endl;
	}
	else
	{
		Input::Add_Joystick(joystick);
		std::cout << joystick << "pad connect" << std::endl;
	}
}
