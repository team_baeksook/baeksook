/***********************************************************************
File name		: Timer.h
Project name	: WFPT
Author			: MinSuk Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once

#include <chrono>

class Timer
{
private:
    std::chrono::high_resolution_clock::time_point PreviousTime = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point LastTime = std::chrono::high_resolution_clock::now();
public:
    float GetAndResetTime();
    float GetTime();
    void ResetTime();
};
