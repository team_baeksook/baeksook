/***********************************************************************
File name		: System.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
class System
{
public:
	virtual void Initialize() = 0;
	virtual void Update(float dt) = 0;
    virtual void Close() = 0;
	virtual ~System() {};
};

