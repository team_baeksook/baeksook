#include "User.h"
#include "Input.h"
#include "Logger.h"
#include "Component/Player.h"
#include "System/State.h"
#include "Imgui_app.h"
#include "Object/Object.h"

#include <iostream>

bool User::PLAY = false;

User::User(int player_num, bool joy) : m_playernum(player_num), m_isjoystick(joy)
{
}

User::~User()
{

}

void User::Initialize()
{
}

void User::Update(float /*dt*/)
{
	if (User::PLAY && m_playerptr)
	{
		m_playerptr->SetZeroKey();

		if (!m_input)
		{
			return;
		}

		//for the joystick input
		float scale_x_movement = 1.0f;
		if (m_playernum >= 1)
		{
			vector2 leftstick_pos = Input::Get_Left_Stick_Position(m_playernum);
			scale_x_movement = (std::abs(leftstick_pos.x) >= 0.5f) ? 0.5f : std::abs(leftstick_pos.x);
			m_playerptr->SetWalkSpeed(scale_x_movement * 2.0f);
		}

		if (Input::Is_Pressed(CONTROL_SETTING::KEY_JUMP, m_playernum) || 
			Input::Is_Pressed(CONTROL_SETTING::KEY_JUMP_ALT, m_playernum))
		{
			m_playerptr->Jump = true;
		}
		if (Input::Is_Triggered(CONTROL_SETTING::KEY_JUMP, m_playernum) ||
			Input::Is_Triggered(CONTROL_SETTING::KEY_JUMP_ALT, m_playernum))
		{
			m_playerptr->DoubleJump = true;
		}
		if (Input::Is_Pressed(CONTROL_SETTING::KEY_RIGHT, m_playernum))
		{
			m_playerptr->Right = true;
		}
		if (Input::Is_Pressed(CONTROL_SETTING::KEY_LEFT, m_playernum))
		{
			m_playerptr->Left = true;
		}
		if (Input::Is_Pressed(CONTROL_SETTING::KEY_DOWN, m_playernum))
		{
			m_playerptr->Down = true;
		}
		if (Input::Is_Pressed(CONTROL_SETTING::KEY_FIRE, m_playernum))
		{
			m_playerptr->m_fire = true;
		}
		if (Input::Is_Triggered(CONTROL_SETTING::KEY_THROW, m_playernum))
		{
			m_playerptr->Throw_Weapon();
		}
		UpdateCursor();
	}
}

void User::Setplayer(Player * player)
{
	m_playerptr = player;
	m_objptr = player->GetOwner();
}

void User::ChangeCurrentState(State * state)
{
	m_currentstate = state;
}

void User::InitPlayState()
{
	m_objptr = new Object(m_currentstate);
	LoadArchetypeinfo(m_objptr, "Player" + std::to_string(m_playernum + 1));

	if (m_playerptr = m_objptr->GetComponentByTemplate<Player>(); m_playerptr == nullptr)
	{
		logger::log_handler.PrintLogOnConsoleAndFile("The object does not have player component!", spdlog::level::level_enum::err);
	}

	m_playerptr->SetPlayerNum(m_playernum);
	m_currentstate->AddRegisterObject(m_objptr);
	m_playerid = m_objptr->GetID();
	User::PLAY = true;
}

void User::Close()
{
	m_objptr = nullptr;
	User::PLAY = false;
	m_playerptr = nullptr;
}

unsigned int User::GetPlayerNum() const
{
	return m_playernum;
}

void User::Setinput(bool input)
{
	m_input = input;
}

void User::UpdateCursor()
{
	if (m_objptr == nullptr)
	{
		return;
	}
	if (m_playernum >= 0)
	{
		vector2 leftstick_pos = Input::Get_Left_Stick_Position(m_playernum);
		if (Magnitude(leftstick_pos) >= 0.3f)
		{
			float degree = Angle_Degree(leftstick_pos);
			m_playerptr->SetWeaponAngle(degree);
		}
		vector2 rightstick_pos = Input::Get_Right_Stick_Position(m_playernum);
		if (Magnitude(rightstick_pos) >= 0.3f)
		{
			float degree = Angle_Degree(rightstick_pos);
			m_playerptr->SetWeaponAngle(degree);
		}
	}
	else
	{
		vector2 obj_pos = m_objptr->GetTransform()->GetWorldPosition();
		vector2 pos = m_currentstate->GetCamera(0)->GetLocalPosition(Input::Get_Mouse_Position()) - obj_pos;
		float degree = Angle_Degree(pos);
		m_playerptr->SetWeaponAngle(degree);
	}
}
