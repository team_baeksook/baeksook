#pragma once

class Player;
class Object;
class State;

class User
{
public:
	User(int player_num = 0, bool joy = false);
	~User();
	void Initialize();
	void Update(float dt);
	void Setplayer(Player* player);
	void ChangeCurrentState(State* state);
	void InitPlayState();
	void Close();
	static bool PLAY;

	unsigned int GetPlayerNum() const;

	void Setinput(bool input);

private:
	State* m_currentstate = nullptr;
	Object* m_objptr = nullptr;
	Player* m_playerptr = nullptr;
	bool m_isjoystick = false;
	bool m_input = true;
	int m_playernum = 0;
	unsigned int m_playerid = 0;

	void UpdateCursor();
};