/***********************************************************************
File name		: State.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Graphic/Color.h"
#include "Graphic/Graphic.h"
#include "Component/Camera.h"
#include "Object/TileMap.h"
#include "Badge/Badge.h"

class Particle;
class ObjectManager;

class State
{
protected:
    Color BackgroundColor;
    Graphic * graphic_system = nullptr;
    ObjectManager * m_objmanger = nullptr;

    std::vector<Camera *> m_CameraList;
	int m_state_num = 0;

	std::vector<Particle*> m_particle;
	void UpdateParticle(float dt);

	std::vector<Object*> m_playerlist;

	bool m_shouldRestart = false;
	bool m_shouldNext = false;
	bool m_initialized = false;

	TileMap Tile_Map;
	Badge* m_badge = nullptr;
public:
    State(Graphic * graphic);
    virtual ~State() = default;
    virtual void Initialize() = 0;
    virtual void Update(float dt) = 0;
    void Update_Object(float dt);
    virtual void Close();

    void Make_ObjectManager();
	void Set_TileMap();

    void SetBackGroundColor(Color color);
    Color GetBackGroundColor();
    Graphic * GetGraphicSystem() const;
	ObjectManager * GetObjectManager() const;
	int GetStateNumber() const;

    void AddCamera(Camera * camera);
    Camera * GetCamera(int idx = 0);
    std::vector<Camera *> * GetCameraContainer();

	void AddRegisterObject(Object *obj);

	//this function is for registering object by specific id
	//be careful when you call this function.
	void AddRegisterObjectByID(Object * obj, unsigned int id);

	void AddParticle(Particle* particle);

	void AddPlayer(Object* obj);
	std::vector<Object*> GetPlayerlist();

	bool ShouldRestart() const;;
	bool ShouldNext() const;

	void SetTileMap(unsigned int levelnum);
	TileMap* GetTile();
	void SetStateNumber(int number);

	bool IsInitialized();
	Badge* GetBadge() const;
};
