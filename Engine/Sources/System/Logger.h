/***********************************************************************
File name : Logger.h
Project name : WFPT
Author : MinSuk Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <string>
#include <spdlog/common.h>

namespace spdlog
{
    class logger;
}

enum class LoggerType { LOGGER_CONSOLE = 0, LOGGER_FILE = 1 };

class Logger
{
private:
    std::shared_ptr<spdlog::logger> m_logger_console;
    std::shared_ptr<spdlog::logger> m_logger_file;
public:
    Logger();
    ~Logger();
    void PrintLogOnConsole(std::string message, spdlog::level::level_enum level = spdlog::level::level_enum::info);
    void PrintLogOnFile(std::string message, spdlog::level::level_enum level = spdlog::level::level_enum::info);
    void PrintLogOnConsoleAndFile(std::string message, spdlog::level::level_enum level = spdlog::level::level_enum::info);
};

namespace logger
{
    extern Logger log_handler;
}