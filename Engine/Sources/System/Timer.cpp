/***********************************************************************
File name		: Timer.cpp
Project name	: WFPT
Author			: MinSuk Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Timer.h"

float Timer::GetAndResetTime()
{
    LastTime = std::chrono::high_resolution_clock::now();
    float result = std::chrono::duration<float>(LastTime - PreviousTime).count();
    PreviousTime = std::chrono::high_resolution_clock::now();

    return result;
}

float Timer::GetTime()
{
    LastTime = std::chrono::high_resolution_clock::now();
    float result = std::chrono::duration<float>(LastTime - PreviousTime).count();

    return result;
}

void Timer::ResetTime()
{
    PreviousTime = std::chrono::high_resolution_clock::now();
}
