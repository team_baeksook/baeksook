/***********************************************************************
File name		: State.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "State.h"
#include "ObjectManager.h"
#include "Object/Object.h"
#include "Component/Player.h"

#include <GL/glew.h>
#include "Imgui_app.h"

void State::UpdateParticle(float dt)
{
	graphic_system->StartParticle();
	for (auto particle = m_particle.rbegin(); particle != m_particle.rend();)
	{
		(*particle)->Draw(dt, this->GetGraphicSystem());
		if ((*particle)->IsDead())
		{
			(*particle)->Close();
			delete (*particle);
			(*particle) = nullptr;
			particle = std::make_reverse_iterator(m_particle.erase((++particle).base()));
		}
		else
		{
			++particle;
		}
	}
}

State::State(Graphic * graphic) : graphic_system(graphic), BackgroundColor(0, 0, 0, 255) {}

void State::Update_Object(float dt)
{
    graphic_system->SetFrameBuffer(BackgroundColor);

    m_objmanger->Update(dt);

	graphic_system->Update();

	Tile_Map.Update(dt);
	UpdateParticle(dt);

	graphic_system->DrawPostProcess(dt);
}

void State::Close()
{
	for (auto& part : m_particle)
	{
		part->Close();
		delete part;
	}
	m_particle.clear();

	m_objmanger->Close();
	m_CameraList.clear();

	m_playerlist.clear();

	m_shouldRestart = false;
	m_shouldNext = false;

    delete m_objmanger;
	m_objmanger = nullptr;
}

void State::Make_ObjectManager()
{
	if (m_objmanger)
	{
		return;
	}

    m_objmanger = new ObjectManager(this);

    m_objmanger->Initialize();
}

//Tile_map initialize
void State::Set_TileMap()
{
	Tile_Map.Set_Graphic(graphic_system);
	if (m_state_num >= 0)
	{
		Tile_Map.Load_TileMap(m_state_num);
	}
}

void State::SetBackGroundColor(Color color)
{
    BackgroundColor = color;
}

Color State::GetBackGroundColor()
{
    return BackgroundColor;
}

Graphic* State::GetGraphicSystem() const
{
    return graphic_system;
}

ObjectManager* State::GetObjectManager() const
{
    return m_objmanger;
}

int State::GetStateNumber() const
{
	return m_state_num;
}

void State::AddCamera(Camera* camera)
{
    m_CameraList.push_back(camera);
}

Camera* State::GetCamera(int idx)
{
	if (m_CameraList.empty())
	{
		return nullptr;
	}

    return m_CameraList.at(idx);
}

std::vector<Camera*> * State::GetCameraContainer()
{
    return &m_CameraList;
}

void State::AddRegisterObject(Object* obj)
{
	m_objmanger->Add_Object(obj);
}

void State::AddRegisterObjectByID(Object * obj, unsigned int id)
{
	m_objmanger->Add_Object(obj, id);
}

void State::AddParticle(Particle* particle)
{
	m_particle.push_back(particle);
}

void State::AddPlayer(Object* obj)
{
	m_playerlist.push_back(obj);

	std::sort(m_playerlist.begin(), m_playerlist.end(), 
		[](Object* obj1, Object* obj2)
		{
			Player* player1 = obj1->GetComponentByTemplate<Player>();
			Player* player2 = obj2->GetComponentByTemplate<Player>();

			return player1->GetPlayerNum() > player2->GetPlayerNum();
	});
}

std::vector<Object*> State::GetPlayerlist()
{
	return m_playerlist;
}

bool State::ShouldRestart() const
{
	return m_shouldRestart;
}

bool State::ShouldNext() const
{
	return m_shouldNext;
}

void State::SetTileMap(unsigned int levelnum)
{
	Tile_Map.Load_TileMap(levelnum);
}

TileMap* State::GetTile()
{
	return &Tile_Map;
}

void State::SetStateNumber(int number)
{
	m_state_num = number;
}

bool State::IsInitialized()
{
	return m_initialized;
}

Badge* State::GetBadge() const
{
	return m_badge;
}
