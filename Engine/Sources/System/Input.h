/***********************************************************************
File name : Input.h
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Application.h"
#include <bitset>

enum KEY_STATE
{
	KEY_UP,
	KEY_TRIGGERED,
	KEY_DOWN
};

//describe here the enum type of key pressed ex) Jump = GLFW_KEY_SPACEBAR
enum class CONTROL_SETTING
{
	KEY_JUMP = 0, KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, KEY_PICK,
	KEY_FIRE, KEY_ATTACK, KEY_DASH, 
	KEY_JUMP_ALT, KEY_THROW, KEY_MAX
};

class JoyStick;

class Input
{
public:
	static void Initialize();

	static void Set_Keyboard(KEY_STATE state, int key_num);
	static bool Is_Pressed(CONTROL_SETTING key_num, int playerNum = 0);
	static bool Is_Triggered(CONTROL_SETTING key_num, int playerNum = 0);
	static bool Is_Released(CONTROL_SETTING key_num, int playerNum = 0);

	static bool Is_Pressed(int key_num);
	static bool Is_Triggered(int key_num);
	static bool Is_Released(int key_num);

	static void Reset();
	static void ResetAll();

	static void Update();

	static void Set_Mouse_Position(float x, float y);
	static vector2 Get_Mouse_Position();

	static std::vector<JoyStick*> Get_Joystick();
	static bool Is_Joystick_Exist(int player_num);
	static void Delete_Specific_Joystick(int index);
	static void Add_Joystick(int index);
    static vector2 Get_Left_Stick_Position(int playerNum);
    static vector2 Get_Right_Stick_Position(int playerNum);

	static void Set_Mouse(KEY_STATE state, int key_num);
    static void SetScroll(float amount);

    static float GetScrollInfo();

	static void Close();
	static bool IsProceed();
private:
	static std::bitset<GLFW_KEY_LAST> k_pressed;
	static std::bitset<GLFW_KEY_LAST> k_triggered;
	static vector2 m_pos;
    static float m_scroll;

	static std::vector<JoyStick*> m_joystick;
	static std::map<CONTROL_SETTING, int> m_key_binding;
};

