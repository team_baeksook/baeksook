#include "JoyStick.h"
#include <iostream>

JoyStick::JoyStick(int id, int playerNum) : m_id(id), m_playerNum(playerNum)
{
	m_keybinding[CONTROL_SETTING::KEY_JUMP] = 4;
    m_keybinding[CONTROL_SETTING::KEY_JUMP_ALT] = 0;
	m_keybinding[CONTROL_SETTING::KEY_PICK] = 12;
	m_keybinding[CONTROL_SETTING::KEY_FIRE] = 15;
	m_keybinding[CONTROL_SETTING::KEY_THROW] = 5;
    //m_keybinding[CONTROL_SETTING::KEY_DASH] = 14;

	m_keybinding[CONTROL_SETTING::KEY_LEFT] = 13;
	m_keybinding[CONTROL_SETTING::KEY_RIGHT] = 11;
	m_keybinding[CONTROL_SETTING::KEY_DOWN] = 12;
	m_keybinding[CONTROL_SETTING::KEY_UP] = 10;
}

vector2 JoyStick::Getlstick() const
{
	return m_lstick;
}

vector2 JoyStick::Getrstick() const
{
	return m_rstick;
}

int JoyStick::Get_PlayerNum() const
{
	return m_playerNum;
}

int JoyStick::Get_JoystickId() const
{
	return m_id;
}

bool JoyStick::is_pressed(CONTROL_SETTING key_num) const
{
	if(m_keybinding.find(key_num) == m_keybinding.end())
	{
		return false;
	}
	return m_pressed_button[m_keybinding.at(key_num)];
}

bool JoyStick::is_pressed(JOYSTICK_KEY key_num) const
{
	return m_pressed_button[(int)key_num];
}

bool JoyStick::is_triggered(CONTROL_SETTING key_num) const
{
	if (m_keybinding.find(key_num) == m_keybinding.end())
	{
		return false;
	}
	return m_triggered_button[m_keybinding.at(key_num)];
}

bool JoyStick::is_triggered(JOYSTICK_KEY key_num) const
{
	return m_triggered_button[(int)key_num];
}

bool JoyStick::is_released(CONTROL_SETTING key_num) const
{
	if (m_keybinding.find(key_num) == m_keybinding.end())
	{
		return false;
	}
	return !m_pressed_button[m_keybinding.at(key_num)];
}

bool JoyStick::is_released(JOYSTICK_KEY key_num) const
{
	return !m_pressed_button[(int)key_num];
}

void JoyStick::Update()
{
	m_triggered_button.reset();
	m_lstick.SetZero();
	m_rstick.SetZero();

	int button_count;
	const unsigned char* name = glfwGetJoystickButtons(m_id, &button_count);
	if(name==nullptr)
	{
		return;
	}
	for(int i = 0; i < button_count; ++i)
	{
		if(i>=14)	//it is related to stick(movement and LT,RT Button)
		{
			continue;
		}
		if(name[i] == GLFW_PRESS)
		{
			if(m_pressed_button[i] == false)
			{
				m_triggered_button[i] = true;
			}
			m_pressed_button[i] = true;
		}
		else
		{
			m_pressed_button[i] = false;
		}
	}

	int axes_count;
	const float* axes = glfwGetJoystickAxes(m_id, &axes_count);
	m_lstick = vector2(axes[0], -axes[1]);

	if (abs(m_lstick.x) > 0.1f)
	{
		if (m_lstick.x > 0.1f)
		{
			if (m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_RIGHT]] == false)
			{
				m_triggered_button[m_keybinding[CONTROL_SETTING::KEY_RIGHT]] = true;
			}
			m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_RIGHT]] = true;
		}
		else  // NOLINT(readability-misleading-indentation)
		{
			m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_RIGHT]] = false;
		}
		if (m_lstick.x < -0.1f)
		{
			if (m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_LEFT]] == false)
			{
				m_triggered_button[m_keybinding[CONTROL_SETTING::KEY_LEFT]] = true;
			}
			m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_LEFT]] = true;
		}
		else  // NOLINT(readability-misleading-indentation)
		{
			m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_LEFT]] = false;
		}
	}

    if (m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_DOWN]]==false &&m_lstick.y < -0.7f)
    {
		if (m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_PICK]] == false)
		{
			//TODO::control feeling is suck change item pick up method
			//m_triggered_button[m_keybinding[CONTROL_SETTING::KEY_PICK]] = true;
			m_triggered_button[m_keybinding[CONTROL_SETTING::KEY_DOWN]] = true;
		}
        m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_PICK]] = true;
		//TODO::Temp item pickup solution for joystick
		m_triggered_button[m_keybinding[CONTROL_SETTING::KEY_PICK]] = true;
		m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_DOWN]] = true;
    }
	else if(m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_DOWN]] == false)
	{
		m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_PICK]] = false;
		m_pressed_button[m_keybinding[CONTROL_SETTING::KEY_DOWN]] = false;
	}



	m_rstick = vector2(axes[2], -axes[3]);
	m_pressed_button[14] = false;
	m_pressed_button[15] = false;
	if(axes[4] > 0.5f)
	{
		if (m_pressed_button[14] == false)
		{
			m_triggered_button[14] = true;
		}
		m_pressed_button[14] = true;
	}
	if(axes[5] > 0.5f)
	{
		if(m_pressed_button[15] == false)
		{
			m_triggered_button[15] = true;
		}
		m_pressed_button[15] = true;
	}
}

void JoyStick::ResetAll()
{
	m_pressed_button.reset();
	m_triggered_button.reset();
}

void JoyStick::Close()
{
	m_keybinding.clear();
}
