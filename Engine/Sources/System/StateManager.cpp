/***********************************************************************
File name		: StateManager.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "StateManager.h"
#include "State.h"
#include "Application.h"
#include "Input.h"
#include <iostream>
#include <iomanip>

#include "Imgui_app.h"

#include "Object/Object.h"
#include "Component/Player.h"
#include "User.h"

StateManager * STATE = nullptr;

StateManager::StateManager()
{
	if (STATE != nullptr)
	{
		std::cout << "Error: STATE already exists\n";
		return;
	}
	STATE = this;
}

void StateManager::Initialize()
{
	m_currentState = nullptr;
	m_pause = false;
	m_restart = false;

    //states[0]->Make_ObjectManager();
    //states[0]->Initialize();

	/*Add_State(new World(APP->graphic));
	states[1]->Make_ObjectManager();
	states[1]->Initialize();
	m_pPaused = states[1];*/
}

void StateManager::Add_State(State * state)
{
    states.push_back(state);

	state->Make_ObjectManager();

	if (m_currentState == nullptr)
		Change_State(SPLASHSCREEN);

	if (state == m_currentState)
	{
		state->Initialize();
		state->Set_TileMap();
		IMGUIAPP->Set_TileMap(m_currentState->GetTile());
		IMGUIAPP->Set_Tile(m_currentState->GetTile()->Get_ImGui_Tile());
	}
}

void StateManager::Set_StartState(State * state)
{
    m_pFirst = state;
}

void StateManager::Set_CurrentState(State* state)
{
    m_currentState = state;
}

void StateManager::Set_PauseState(State * state)
{
    m_pPaused = state;
}

State* StateManager::Get_CurrentState()
{
	return m_currentState;
}

void StateManager::Change_State(SceneIdentifier stageID)
{
	if (stageID > -1)
	{
		m_currentState = states[stageID];
		for(auto user : m_users)
		{
			user->ChangeCurrentState(m_currentState);
			if (stageID == SceneIdentifier::GAMESCREEN|| stageID == SceneIdentifier::TUTORIAL || stageID == SceneIdentifier::TUTORIAL2)
			{
				user->InitPlayState();
			}
			else
			{
				user->Close();
			}
		}

		if (!m_currentState->IsInitialized())
			m_currentState->Initialize();
		m_currentState->Set_TileMap();
		IMGUIAPP->RegisterCurrentState(m_currentState);
		IMGUIAPP->Set_TileMap(m_currentState->GetTile());
		IMGUIAPP->Set_Tile(m_currentState->GetTile()->Get_ImGui_Tile());
	}
}
void StateManager::Change_to_Menu_State()
{
	m_currentState->Close();
	m_currentState->Make_ObjectManager();
	Change_State(SceneIdentifier::MAINMENU);
}

void StateManager::Restart()
{
    m_restart = true;
}

void StateManager::Pause()
{
    m_pause = true;
}

void StateManager::Update(float dt)
{
	for (auto user : m_users)
	{
		user->Update(dt);
	}

	if (Input::Is_Triggered(GLFW_KEY_R))
	{
		STATE->Restart();
	}
	if (Input::Is_Triggered(GLFW_KEY_P))
	{
		if(m_pPaused!=nullptr)
			m_pause = !m_pause;
	}
    if (m_pause == false)
    {
        m_currentState->Update(dt);
        m_currentState->Update_Object(dt);
		if(m_currentState->ShouldNext())
		{
			m_currentState->Close();
			m_currentState = m_pNext;
			m_restart = true;
		}
		if(m_currentState->ShouldRestart())
		{
			m_restart = true;
		}
    }
	else
	{
		m_pPaused->Update(dt);
		m_pPaused->Update_Object(dt);
	}

	if(m_restart==true)
	{
		m_currentState->Close();
		m_currentState->Make_ObjectManager();
		m_currentState->Initialize();
		if (User::PLAY)
		{
			for (auto user : m_users)
			{
				user->InitPlayState();
			}
		}
		m_restart = false;
	}
}

void StateManager::Close()
{
	for(auto& state : states)
	{
		state->Close();
		delete state;
	}
    states.clear();
	for (auto& user : m_users)
	{
		user->Close();
		delete user;
	}
	m_users.clear();
}
