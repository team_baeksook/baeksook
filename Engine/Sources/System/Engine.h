/***********************************************************************
File name : Engine.h
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <vector>
#include "System.h"
#include "Timer.h"
#include "Audio.h"

class Engine
{
public:
	Engine();
	~Engine();

	void MainLoop();

	void Initailize();
	void AddSystem(System* system);
	void Quit();
	Audio* BackGroundMusic=nullptr;

	Timer& GetTimer() { return timer; };

	void SetQuit();
private:
	std::vector<System*>Systems;
	bool m_verticalsync = true;
	int m_refreshrate;
	bool Is_Running;
    Timer timer;
};

extern Engine* ENGINE;
