#include "Badge.h"

Badge::Badge(int playernum) : m_max(playernum)
{
	for (int i = 0; i < BADGE_MAX; ++i)
	{
		m_badgelist[i] = -1;
	}
}

void Badge::Close()
{
	m_data.clear();
}

void Badge::AddData(BADGE_DATA dat)
{
	m_data.push_back(dat);
}

void Badge::AddKilllog(unsigned int A, unsigned int B)
{
	m_data.push_back({ A, B });
}

void Badge::Decipher()
{
	int kill[4] = { 0 };
	int death[4] = { 0 };
	int suicide[4] = { 0 };
	for (auto dat : m_data)
	{
		++kill[dat.A];
		++death[dat.B];
		if (m_badgelist[BADGE_FIRSTBLOOD] == -1)
		{
			m_badgelist[BADGE_FIRSTBLOOD] = dat.A;
		}
		if (dat.A == dat.B)
		{
			++suicide[dat.A];
		}
	}

	unsigned int slaughter = -1;
	int most_kill = 0;

	unsigned int dumb = -1;
	int most_sd = 0;

	for (int i = 0; i < m_max; ++i)
	{
		if (kill[i] > most_kill)
		{
			slaughter = i;
			most_kill = kill[i];
		}
		if (suicide[i] > most_sd)
		{
			dumb = i;
			most_sd = suicide[i];
		}
		if (kill[i] == 0)
		{
			m_badgelist[BADGE_GANDHI] = i;
		}
	}
	m_badgelist[BADGE_DOMINATOR] = slaughter;
	//m_badgelist[BADGE_DUMB] = dumb;
}

unsigned int Badge::Getbadgedata(BADGELIST index)
{
	return m_badgelist[index];
}
