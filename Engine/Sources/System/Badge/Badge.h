#pragma once
#include <vector>

enum BADGELIST
{
	BADGE_FIRSTBLOOD = 0,	//first kill				done
	BADGE_DUMB,				//suicide					done
	BADGE_DOMINATOR,		//most kill					done
	BADGE_CHAINKILL,		//most successive kill
	BADGE_SUPPRESSOR,		//most damage
	BADGE_ACEKILLER,		//kill the 1st
	BADGE_COLLECTOR,		//obtain most weapon
	BADGE_GANDHI,			//kill nothing
	BADGE_KILLSTEAL,		//damage less kill more
	BADGE_BLIND,			//LOW_ACCURANCY     
	BADGE_MAX
};

struct BADGE_DATA
{
	unsigned int A;
	unsigned int B;
};

class Badge
{
public:
	Badge(int playernum);
	void Close();
	void AddData(BADGE_DATA dat);
	void AddKilllog(unsigned int A, unsigned int B);
	void Decipher();
	unsigned int Getbadgedata(BADGELIST index);
private:
	std::vector<BADGE_DATA> m_data;
	unsigned int m_badgelist[BADGE_MAX];
	int m_max = 0;
};