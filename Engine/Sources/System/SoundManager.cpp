/***********************************************************************
File name		: SoundManager.cpp
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include <spdlog/logger.h>
#include "SoundManager.h"
#include <iostream>
#include "Engine_Utility.h"
#include <Rapidjson/filereadstream.h>
#include <Rapidjson/document.h>

SoundManager* SOUND = nullptr;
std::vector<std::string>music_list;

void SoundManager::Initialize()
{
	if(SOUND!=nullptr)
	{
		std::cout << "ERROR: SOUND is already exist\n";
		return;
	}
	SOUND = this;

	if(FMOD::System_Create(&System)!=FMOD_OK)
	{
		//TODO::ERROR!
		return;
	}
	int driverCount = 0;

	System->getNumDrivers(&driverCount);

	if(driverCount==0)
	{
		//TODO::ERROR!
		return;
	}
	System->init(36, FMOD_INIT_NORMAL, nullptr);
	
	Load_All_Sound();
}

void SoundManager::Update(float)
{
	System->update();
	for (auto& audio : audio_list)
	{
		if (audio->Is_Dead)
		{
			if (!audio->Get_Playing())
			{
				Delete_AudioinList(audio);
			}
		}
	}
}

void SoundManager::Close()
{
	for (auto iter=audio_list.begin();iter!=audio_list.end();)
	{
		(*iter)->Close();
		delete (*iter);
		*iter = nullptr;
		iter = audio_list.erase(iter);
	}
	System->close();
	System->release();

	music_list.clear();
}

void SoundManager::Load_All_Sound()
{
	//spdlog::logger::log_handler.PrintLogOnConsole("Loading the Music List"); //TODO:: insert Log
	errno_t err;
	FILE* fp;
	std::string filename = "data/sound.json";
	err = fopen_s(&fp, filename.c_str(), "rb");
	if (err != 0)
		return;

	char readBuffer[65536] = { 0 };
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	rapidjson::Document d;
	d.ParseStream(is);

	const rapidjson::Value& comp = d["Sound"];
	for (rapidjson::SizeType i = 0; i < comp.Size(); i++)
	{
		music_list.emplace_back(comp[i].FindMember("Name")->value.GetString());
	}
	fclose(fp);
}

void SoundManager::Create_Sound(FMOD::Sound ** pSound, const char * filepath)
{
	System->createSound(filepath, FMOD_DEFAULT | FMOD_LOOP_NORMAL, 0, pSound);
}

void SoundManager::Play_Sound(FMOD::Sound * pSound,FMOD::Channel ** pChannel, bool loop)
{
	if (!loop)
	{
		pSound->setMode(FMOD_LOOP_OFF);
	}
	else
	{
		pSound->setMode(FMOD_LOOP_NORMAL);
		pSound->setLoopCount(-1);
	}

	System->playSound(pSound,ChannelGroup, false,pChannel);
}

void SoundManager::Clear_AudioList()
{
	audio_list.clear();
}
