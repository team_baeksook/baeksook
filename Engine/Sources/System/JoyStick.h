#pragma once
#include "Math/vector2.hpp"
#include <bitset>
#include "Input.h"

constexpr int JOYSTICK_MAX = GLFW_JOYSTICK_LAST + 1;

//********************Joystick key list*********************//
//A		: 0													//
//B		: 1													//
//X		: 2													//
//Y		: 3													//
//LB	: 4													//
//RB	: 5													//
//BACK	: 6													//
//START : 7													//
//L3	: 8													//
//R3	: 9													//
//UP	: 10												//
//RIGHT : 11												//
//DOWN	: 12												//
//LEFT	: 13												//
//LT	: 14												//
//RT	: 15												//
//**********************************************************//

enum class JOYSTICK_KEY
{
	KEY_A = 0, KEY_B, KEY_X, KEY_Y, KEY_LB, KEY_RB, 
	KEY_BACK, KEY_START, KEY_L3, KEY_R3, KEY_UP,
	KEY_RIGHT, KEY_DOWN, KEY_LEFT, KEY_LT, KEY_RT
};

class JoyStick
{
private:
	int m_id;
	int m_playerNum=-1;
	vector2 m_lstick;
	vector2 m_rstick;

	std::map<CONTROL_SETTING, int> m_keybinding;
public:
	std::bitset<JOYSTICK_MAX> m_pressed_button;
	std::bitset<JOYSTICK_MAX> m_triggered_button;

	JoyStick(int id, int playerNum = -1);
	vector2 Getlstick() const;
	vector2 Getrstick() const;
	int Get_PlayerNum() const;
	int Get_JoystickId() const;

	bool is_pressed(CONTROL_SETTING key_num) const;
	bool is_pressed(JOYSTICK_KEY key_num) const;
	bool is_triggered(CONTROL_SETTING key_num) const;
	bool is_triggered(JOYSTICK_KEY key_num) const;
	bool is_released(CONTROL_SETTING key_num) const;
	bool is_released(JOYSTICK_KEY key_num) const;

	void Update();

	void ResetAll();

	void Close();
};
