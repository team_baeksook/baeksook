/***********************************************************************
File name : Application.h
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <string>
#include <GLFW/glfw3.h>
#include "System.h"
#include "Graphic/Graphic.h"
#include "Math/affine2d.hpp"

class State;

class Application : public System
{
    friend class Input;
    friend class StateManager;
public:
    Application();
    ~Application();
    virtual void Initialize() override;
    virtual void Update(float dt) override;
    virtual void Close() override;

    GLFWwindow* Get_Window();

    vector2 GetWindowSize();
	Graphic* Get_Graphic();

    void Toggle_Fullscreen();
    void Set_Window_Size(int width, int height);
    void Show_FramePerSecond(float fps);

    void Set_Window_Title(const std::string& title);
private:
    GLFWwindow * window;
    GLFWmonitor * monitor;
    int Width;
    int Height;
    int Previous_Width;
    int Previous_Height;
    int X_pos;
    int Y_pos;
    std::string Title;
    bool Is_Fullscreen = false;
    Graphic * graphic;

    affine2d m_projmat;
};

void errorCallback(int errorCode, const char* errorDescription);
void window_position_callback(GLFWwindow* window, int width, int height);
void window_size_callback(GLFWwindow* window, int width, int height);
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void window_close_callback(GLFWwindow* window);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void drop_callback(GLFWwindow* window, int count, const char** paths);
void joystick_callback(int joystick, int joystick_event);

extern Application* APP;
