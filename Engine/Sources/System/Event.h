#pragma once

template<typename OBJ>
class Event
{
private:
	bool (OBJ::*m_event)(float);
	float m_time = 0.0f;
	std::vector<Event*> m_nexts;
	OBJ* m_owner = nullptr;
public:
	Event(bool(OBJ::*e)(float), OBJ* owner) : m_event(e), m_owner(owner) {}
	bool Update(float dt)
	{
		m_time += dt;
		return (m_owner->*m_event)(m_time);
	}
	Event* SetNext(Event* next)
	{
		m_nexts.push_back(next);
		return next;
	}
	Event* SetNext(bool(OBJ::*e)(float))
	{
		Event* next = new Event(e, m_owner);
		m_nexts.push_back(next);
		return next;
	}
	std::vector<Event*> GetNexts() const
	{
		return m_nexts;
	}
	void Close()
	{
		for (auto next : m_nexts)
		{
			next->Close();
			delete next;
		}
		m_nexts.clear();
	}
};