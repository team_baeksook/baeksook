#pragma once
#include "Event.h"
#include <vector>

template<typename OBJ>
class EventManager
{
private:
	OBJ* m_owner = nullptr;
public:
	std::vector<Event<OBJ>*> m_queue;
	void Update(float dt)
	{
		size_t size = m_queue.size();
		for (int i = 0; i < size; ++i)
		{
			Event<OBJ>* queue = m_queue.at(i);
			if (queue->Update(dt))
			{
				if (std::vector<Event<OBJ>*> nexts = queue->GetNexts(); nexts.empty() != true)
				{
					m_queue.insert(m_queue.end(), nexts.begin(), nexts.end());
					size += nexts.size();
				}
				delete queue;
				m_queue.erase(m_queue.begin() + i);
				--size;
				--i;
			}
		}
	}
	void SetOwner(OBJ* obj)
	{
		m_owner = obj;
	}
	Event<OBJ>* AddEvent(bool(OBJ::*e)(float))
	{
		Event<OBJ>* temp = new Event<OBJ>(e, m_owner);
		m_queue.push_back(temp);
		return temp;
	}
	void Close()
	{
		for (auto q : m_queue)
		{
			q->Close();
			delete q;
		}
		m_queue.clear();
	}
};