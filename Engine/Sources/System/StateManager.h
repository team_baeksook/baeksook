/***********************************************************************
File name		: StateManager.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "System.h"
#include <vector>
#include "State.h"
#include "SceneIdentifier.h"

class User;

class StateManager : public System
{
public:
	StateManager();

    void Add_State(State* state);
    void Set_StartState(State* state);
    void Set_CurrentState(State* state);
    void Set_PauseState(State* state);
	State* Get_CurrentState();
    void Change_State(SceneIdentifier stateID);
	void Change_to_Menu_State();
    void Restart();
    void Pause();

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;
	std::vector<User*> m_users;
private:
    State *m_pNext = nullptr, *m_pFirst = nullptr,
          *m_pPaused = nullptr, *m_pRestart = nullptr,
          *m_currentState = nullptr;

    std::vector<State*> states;
	bool m_restart = false, m_pause = false;
	
};

extern StateManager * STATE;