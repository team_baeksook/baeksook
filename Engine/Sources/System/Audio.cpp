/***********************************************************************
File name : Audio.cpp
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Audio.h"
#include "SoundManager.h"
#include "IMGUI/imgui.h"
#include "Imgui_app.h"

Audio::Audio() :channel(nullptr), sound(nullptr)
{
	SOUND->Add_AudiotoList(this);
}

Audio::Audio(const char* filepath):channel(nullptr), sound(nullptr)
{
	File_Path = filepath;
	SOUND->Create_Sound(&sound, File_Path);
	SOUND->Add_AudiotoList(this);
}


Audio::~Audio()
{
}

void Audio::Play_Audio(bool loop)
{
	Is_Loop = loop;
	SOUND->Play_Sound(sound, &channel, Is_Loop);
	channel->setVolume(Volume);
	Set_Pause(Is_Pause);
}

void Audio::Set_Pause(bool pause)
{
	Is_Pause = pause;
	channel->setPaused(Is_Pause);
}

void Audio::Set_Volume(float volume)
{
	Volume = volume;
}

void Audio::Set_Name(std::string name)
{
	Audio_Name = name;
}

void Audio::Set_Dead(bool dead)
{
	Is_Dead = dead;
}

bool Audio::Get_Dead()
{
	return Is_Dead;
}

bool Audio::Get_Playing()
{
	bool bIsPlaying = false;
	FMOD_RESULT fRes = channel->isPlaying(&bIsPlaying);
	if (fRes == FMOD_ERR_INVALID_HANDLE || fRes == FMOD_ERR_CHANNEL_STOLEN)
	{
		return false; // Expected error
	}

	return bIsPlaying;
}

void Audio::Load_Audio_From_File(const char * filepath)
{
	sound->release();
	File_Path = filepath;
	SOUND->Create_Sound(&sound, File_Path);
}

void Audio::ImGui_Setting()
{
	if (ImGui::CollapsingHeader(Audio_Name.c_str()))
	{
		ImGui::Checkbox("Is_Loop", &Is_Loop);	//If you want to apply this. Please Restart.

		ImGui::Combo(" ", &Current_Music_Number, vector_getter,
			&music_list, static_cast<int>(music_list.size()));
		ImGui::SameLine();
		// TODO:: If add new component fill this
		if (ImGui::Button("Change Music"))
		{
			File_Path=music_list[Current_Music_Number].c_str();
			Load_Audio_From_File(File_Path);
			if (!Is_Pause)
				Play_Audio(Is_Loop);
		}

		if(ImGui::SliderFloat("Volume",&Volume,0,10.f))
		{
			channel->setVolume(Volume);
		}

		if (Is_Pause)
		{
			if (ImGui::Button("Play Audio"))
			{
				Is_Pause=false;
				Set_Pause(Is_Pause);
			}
		}
		else
		{
			if (ImGui::Button("Pause Audio"))
			{
				Is_Pause = true;
				Set_Pause(Is_Pause);
			}
		}
		ImGui::SameLine();
		if(ImGui::Button("Restart"))
		{
			Load_Audio_From_File(File_Path);
			Play_Audio(Is_Loop);
		}
	}
}

void Audio::Close()
{
	sound->release();
	sound = nullptr;
	channel = nullptr;
}
