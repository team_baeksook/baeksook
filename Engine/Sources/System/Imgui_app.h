/***********************************************************************
File name : Imgui_app.h
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "System.h"
#include "Application.h"
#include "Component/Component.h"
#include "Object/TileMap.h"

enum
{
	OBJECT_EDIT = 0,
	TILE_MAP_EDIT=1,
	ARCHETYPE_EDIT=2,
};

class Object;

class IMGUI_app :public System
{
public:
	void Initialize() override;
	void Update(float dt) override;

    void Close() override;
	IMGUI_app();
    ~IMGUI_app() override;

	void Set_Object(Object*);
	void Set_Tile(Tile*);
	void Set_TileMap(TileMap*);

	int Get_Edit_Type() const;
	bool GetIsCollisionDraw();

	void Object_Edit();
	void Tile_Map_Edit();
	void Archetype_Edit();

	void RegisterCurrentState(State* state);
	void Load_Archetype();
	void Save_Archetype();

	Object* GetObject() const;
	bool IsDebuggingActive() const;
	bool IsGlobalSettingsActive() const;
private:
	GLFWwindow * window;
	const char* glsl_version;

	//Debugging
	Object* Current_Object;
	Tile* Current_Tile_Info;
	TileMap* Current_Tile_Map;
	Object* Current_Archetype = nullptr;
	Object* Current_Dragging_Object = nullptr;

	unsigned int m_current_tile_num = 0;
	unsigned int m_max_tile_number;

	bool gamedebugging_active=false;
	bool Is_Collision_draw = false;
	int Current_Edit_Type = ARCHETYPE_EDIT;
	//bool Is_Demo_Active=true;

	std::vector<std::string>component_list;
	std::vector<std::string>tile_type_list;

	int Current_Component_Number=NOT_EXIST;
	int Current_Tile_Type_Number = EMPTY;

	//Global settings
	bool globalsettings_active = false;
	//Graphics
	std::vector<std::pair<std::string, std::vector<std::string>>> screen_resolution;
	const char* screenratio_current;
	int screenratio_currentIndex;
	const char* resolution_current;
	int resolution_currentIndex;

	//Audio
	int general_audio;
	int sfx_audio;
	int music_audio;

	State* m_currentstate = nullptr;

	void ShowObjmanager();
	void ShowArchetypemanager();

	void ListObject(Object* obj);
	void ListArchetype(Object* obj);

	void GameDebuggingInitialize();
	void GameDebuggingUpdate();

	void GlobalSettingsInitialize();
	void GlobalSettingsUpdate();
};

void LoadArchetypeinfo(Object* object, int num);

//takes more time
void LoadArchetypeinfo(Object* object, std::string name);

extern std::vector<Object*>Archetype_Container;

extern IMGUI_app * IMGUIAPP;

static auto vector_getter = [](void* vec, int idx, const char** out_text)
{
	auto& vector = *static_cast<std::vector<std::string>*>(vec);
	if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
	*out_text = vector.at(idx).c_str();
	return true;
};