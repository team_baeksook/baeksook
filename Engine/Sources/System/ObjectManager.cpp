/***********************************************************************
File name : ObjectManager.cpp
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "ObjectManager.h"
#include "Rapidjson/prettywriter.h"
#include "Rapidjson/filewritestream.h"
#include "Rapidjson/filereadstream.h"
#include "Rapidjson/document.h"
#include "Object/Object.h"
#include "Component/Sprite.h"
#include "Component/Physics.h"
#include "Imgui_app.h"
#include "Input.h"
#include "Component/SpriteText.h"
#include "Component/Camera.h"
#include <iostream>
#include "Logger.h"
#include "Engine_Utility.h"
#include "Component/Player.h"
#include "Component/Weapon/Revolver.h"
#include "StateManager.h"
#include "Component/Background.h"

using namespace rapidjson;

vector2 Round_Position(vector2 vec)
{
	vec /= 10.f;
	vec.x = round(vec.x);
	if (vec.y < 0&&vec.y - static_cast<int>(vec.y)==-0.5f)
	{
		vec.y = round(vec.y)+1.f;
	}
	vec.y = round(vec.y);

	vec *= 10.f;

    return vec;
}

ObjectManager::ObjectManager(State* state)
{
	m_state = state;
}

ObjectManager::~ObjectManager()
{
	//ImGui_Object->Close();
	//Write_Object_to_json();
}

void ObjectManager::Initialize()
{
	if (m_state->GetStateNumber() >= 0)
	{
		auto temp1 = std::make_unique<Object>(m_state, "Camera");
		temp1->Add_Init_Component(new Camera(temp1.get(), "Camera"));
		temp1->GetTransform()->SetPosition(vector2(0.0f, 50.0f));
		Add_Object(std::move(temp1));

		ImGui_Object = new Object(m_state);
		ImGui_Object->Add_Init_Component(new Sprite(ImGui_Object, "Sprite"));
		ImGui_Object->GetComponentByTemplate<Sprite>()->GetMesh()->MakeTexture();
		ImGui_Object->Add_Init_Component(new Collision(ImGui_Object));

		IMGUIAPP->Set_Object(object.at(1).get());
	}
}

void ObjectManager::Update(float dt)
{
    vector2 pos = this->Get_ObjectByName("Camera")->GetComponentByTemplate<Camera>()->GetLocalPosition(Input::Get_Mouse_Position());
	if (IMGUIAPP->GetIsCollisionDraw())
	{
		m_state->GetGraphicSystem()->DrawRect(Round_Position(pos), 10.f, Color(0, 255, 255, 255));
	}

    for (auto i = object.begin(); i != object.end(); ++i)
    {
		if ((*i).second->HasParent())
		{
			continue;
		}
        (*i).second->Update(dt);
    }

	for (auto obj_iter = object.begin(); obj_iter != object.end();)
	{
		if ((*obj_iter).second->IsDead())
		{
			if (ImGui_Object!=nullptr&&
				IMGUIAPP->GetObject()->GetID() == (*obj_iter).second->GetID())
			{
				IMGUIAPP->Set_Object((*object.begin()).second.get());
			}
			Object* obj_ptr = (*obj_iter).second.get();
			delete obj_ptr;
			obj_ptr = nullptr;
			(*obj_iter).second.release();
			obj_iter = object.erase(obj_iter);
		}
		else
		{
			++obj_iter;
		}
	}


	if (ImGui_Object == nullptr)
	{
		return;
	}
	Collision* imguiCollision = ImGui_Object->GetComponentByTemplate<Collision>();

    vector2 temp_mouse_pos = this->Get_ObjectByName("Camera")->GetComponentByTemplate<Camera>()->GetLocalPosition(Input::Get_Mouse_Position());

    if (IMGUIAPP->Get_Edit_Type() == OBJECT_EDIT)
    {
        //TODO::Is this place is right for this stuff?
        if (Input::Is_Triggered(GLFW_MOUSE_BUTTON_RIGHT))
        {
            ImGui_Object->GetTransform()->SetTransform(temp_mouse_pos, vector2(1.f, 1.f), 0);
            for (auto i = object.begin(); i != object.end(); ++i)
            {
                if (imguiCollision->IsColliding(i->second.get()))
                {
                    printf("It's colliding!\n");
                    IMGUIAPP->Set_Object(i->second.get());
                    break;
                }
            }
        }
        else if (Input::Is_Triggered(GLFW_MOUSE_BUTTON_MIDDLE))
        {
            ImGui_Object->GetTransform()->SetTransform(temp_mouse_pos, vector2(1.f, 1.f), 0);
            auto temp = std::make_unique<Object>(m_state);
            temp->Add_Component(new Sprite(temp.get(), "Sprite"));
            temp->GetComponentByTemplate<Sprite>()->SetTextureName("Tomahawk");
            temp->GetComponentByTemplate<Sprite>()->SetColor(Color(255, 0, 0, 255));
            temp->GetComponentByTemplate<Sprite>()->Init();
            temp->GetTransform()->SetTransform(temp_mouse_pos, vector2(10.f, 10.f), 0);
            IMGUIAPP->Set_Object(temp.get());
			Add_Object(std::move(temp));
		}

		if(Input::Is_Triggered(GLFW_MOUSE_BUTTON_LEFT))
		{
			ImGui_Object->GetTransform()->SetTransform(temp_mouse_pos, vector2(1.f, 1.f), 0);
			for (auto i = object.begin(); i != object.end(); ++i)
			{
				if (imguiCollision->IsColliding(i->second.get()))
				{
					m_dragged_objects.push_back(i->second.get());
					m_diff.push_back(i->second.get()->GetTransform()->GetWorldPosition() - pos);
				}
			}
		}

		if(Input::Is_Pressed(GLFW_MOUSE_BUTTON_LEFT))
		{
			if (m_dragged_objects.size() != 0)
			{
				for(int i=0;i < static_cast<int>(m_dragged_objects.size());++i)
				{
					vector2 temp_dragged_pos = m_diff[i] + pos;
					m_dragged_objects[i]->GetTransform()->SetPosition(temp_dragged_pos);
				}
			}
		}
		if(Input::Is_Released(GLFW_MOUSE_BUTTON_LEFT))
		{
			m_dragged_objects.clear();
			m_dragged_objects.resize(0);
			m_diff.clear();
			m_diff.resize(0);
		}
    }
}

void ObjectManager::Close()
{
	object.clear();
	//for (auto obj_iter = object.begin(); obj_iter != object.end();)
	//{
	//	//Object* obj_ptr = (*obj_iter).second.get();
	//	//delete obj_ptr;
	//	//obj_ptr = nullptr;
	//	//(*obj_iter).second.release();
	//	obj_iter = object.erase(obj_iter);
	//}
	delete ImGui_Object;
	object.clear();
}

void ObjectManager::Add_Object(std::unique_ptr<Object>a)
{
	//when there is an object with m_currentID id.
	while (object.find(m_currentID) != object.end())
	{
		++m_currentID;
	}

	a->SetID(m_currentID);
	object[m_currentID] = (std::move(a));
	++m_currentID;
}

void ObjectManager::Add_Object(Object* obj)
{
	//when there is an object with m_currentID id.
	while (object.find(m_currentID) != object.end())
	{
		++m_currentID;
	}

	obj->SetID(m_currentID);
	std::unique_ptr<Object> temp(obj);
	object[m_currentID] = (std::move(temp));
	++m_currentID;
}

void ObjectManager::Add_Object(Object * obj, unsigned int id)
{
	unsigned int target_index = id;

	//when there is an object in specific index
	if (object.find(id) != object.end())
	{
		//put original object with certain location
		while (object.find(target_index) != object.end())
		{
			++target_index;
		}

		SwapObjectPosition(target_index, id);
	}

	obj->SetID(id);
	std::unique_ptr<Object> temp(obj);
	object[id] = (std::move(temp));

	m_currentID = target_index + 1;
}

void ObjectManager::Delete_Object(Object* pObejct)
{
	for (auto i = object.begin(); i != object.end(); ++i)
	{
		if ((*i).second.get() == pObejct)
		{
			(*i).second->Detach();
				//(*i)->Close();
			( *i).second.release();
			object.erase(i);
			return;
		}
	}
}

void ObjectManager::Write_Object_to_json()
{
	errno_t err;
	FILE* fp;
	std::string filename = "data/";
	filename += std::to_string(m_state->GetStateNumber()) + "levelobject.json";
	err = fopen_s(&fp, filename.c_str(), "wb"); // non-Windows use "r"
	char writeBuffer[65536];
	FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
	PrettyWriter<FileWriteStream> writer(os);
	writer.StartObject();
	writer.Key("Object");
	writer.StartArray();
	for (auto i = object.begin(); i != object.end(); ++i)
	{
		(*i).second->Serialization(writer);
	}
	writer.EndArray();
	writer.EndObject();
	fclose(fp);

	return;
}

void ObjectManager::Read_Object_from_json()
{
	errno_t err;
	FILE* fp;
	std::string filename = "data/";
	filename += std::to_string(m_state->GetStateNumber()) + "levelobject.json";
	err = fopen_s(&fp, filename.c_str(), "rb"); // non-Windows use "r"
	if (err != 0)   //if file cannot open
		return;

	char readBuffer[65536] = { 0 };
	FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	Document d;
	d.ParseStream(is);

	const Value& Whole_Object = d["Object"];

	for (SizeType i = 0; i < Whole_Object.Size(); ++i)
	{
		auto temp = std::make_unique<Object>(m_state);
		temp->Deserialization(Whole_Object[i]);
		Add_Object(std::move(temp));
	}
	fclose(fp);
}

Object* ObjectManager::Get_ObjectByName(std::string Name)
{
	for (auto&& i : object)
	{
		if (i.second->GetName() == Name)
			return i.second.get();
	}
	return nullptr;
}

Object* ObjectManager::GetObjectByID(unsigned int id)
{
	if (object.find(id) != object.end())
	{
		return object.at(id).get();
	}
	return nullptr;
}

std::map<unsigned int, std::unique_ptr<Object>>& ObjectManager::GetContainer()
{
	return object;
}

void ObjectManager::SwapObjectPosition(int id_1,int id_2)
{
	std::swap(object[id_1], object[id_2]);
}