/***********************************************************************
File name : Audio.h
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "FMOD/fmod.hpp"
#include <string>

class Audio
{
	friend class SoundManager;
public:
	Audio();
	Audio(const char* filepath);
	~Audio();

	void Play_Audio(bool loop);
	void Set_Pause(bool pause);
	void Set_Volume(float volume);
	void Set_Name(std::string name);
	void Set_Dead(bool dead);
	bool Get_Dead();
	bool Get_Playing();
	void Load_Audio_From_File(const char* filepath);
	void ImGui_Setting();
	void Close();
private:
	std::string Audio_Name = "NoName";
	const char* File_Path = nullptr;
	FMOD::Channel * channel;
	FMOD::Sound * sound;
	bool Is_Loop = false;
	bool Is_Pause = false;
	bool Is_Dead = false;

	int Current_Music_Number = 0;
	float Volume = 5.f;
};

