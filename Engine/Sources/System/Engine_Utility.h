/***********************************************************************
File name : Engine_Utility.h
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <string>
#include <spdlog/fmt/fmt.h>

namespace utility
{
    template<typename... Args>
    std::string MakeStringUsingFormat(const char *fmt, const Args &... args)
    {
        ::fmt::memory_buffer result;
        ::fmt::format_to(result, fmt, args...);
        return result.data();
    }
}
