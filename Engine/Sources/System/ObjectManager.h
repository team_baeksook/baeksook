/***********************************************************************
File name		: ObjectManager.h
Project name	: WFPT
Author			: SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <memory>
#include <vector>
#include "State.h"
#include "Component/Collision.h"
#include "Object/TileMap.h"
#include "Graphic/Particle.h"

class Object;

class ObjectManager
{
private:
    std::map<unsigned int, std::unique_ptr<Object>>object;
	State * m_state = nullptr;
	Object* ImGui_Object;

	unsigned int m_currentID = 1;

	std::vector<vector2> m_diff;
	std::vector<Object*> m_dragged_objects;
public:
    ObjectManager(State * state);
    ~ObjectManager();

    void Initialize();
    void Update(float dt);
    void Close();

    void Add_Object(std::unique_ptr<Object>a);
	void Add_Object(Object* obj);
	void Add_Object(Object* obj, unsigned int id);

	void Delete_Object(Object* pObejct);
    void Write_Object_to_json();
    void Read_Object_from_json();
    Object* Get_ObjectByName(std::string);
	Object* GetObjectByID(unsigned int id);
	void SwapObjectPosition(int id_1, int id_2);
	std::map<unsigned int, std::unique_ptr<Object>>& GetContainer();
};

vector2 Round_Position(vector2);