/***********************************************************************
File name : Engine.cpp
Project name : WFPT
Author : SeongWook Shin

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#include "Engine.h"
#include "Application.h"
#include "ObjectManager.h"
#include "Input.h"
#include "Imgui_app.h"
#include "StateManager.h"
#include "SoundManager.h"
#include "Logger.h"

#include <iostream>

Engine* ENGINE = nullptr;

Engine::Engine():Is_Running(true)
{
	if (ENGINE != nullptr)
	{
		printf("Error: engine is already exist\n");
		return;
	}
	ENGINE = this;
}

Engine::~Engine()
{
}

void Engine::MainLoop()
{
    int frame_count = 0;
    float frame_time = 0.0f;

	float elapsed_time = 0.0f;
	while (Is_Running)
	{
        float dt = timer.GetAndResetTime();

        ++frame_count;
        frame_time += dt;
        if (frame_time >= 1.0)
        {
            float fps = frame_count / frame_time;
            frame_count = 0;
            frame_time = 0;
            APP->Show_FramePerSecond(fps);
        }
		
		/*if (m_verticalsync)
		{
			elapsed_time += dt;
			if (elapsed_time > 1.0f / m_refreshrate)
			{*/
				for (auto i = Systems.begin(); i != Systems.end(); ++i)
				{
					(*i)->Update(dt);//elapsed_time);
				}
		/*		elapsed_time = 0.0f;
			}
			else
			{
				--frame_count;
			}
		}
		else
		{
			for (auto i = Systems.begin(); i != Systems.end(); ++i)
			{
				(*i)->Update(dt);
			}
		}*/

        if (Input::Is_Triggered(GLFW_KEY_ESCAPE) || glfwWindowShouldClose(APP->Get_Window()))
        {
            Is_Running = !Is_Running;
			//TODO::solve!
			//STATE->Close();
        }
		glfwSwapBuffers(APP->Get_Window());

    }
}

void Engine::Initailize()
{
    logger::log_handler.PrintLogOnConsole("Initialization Engine Start");

	AddSystem(new Application);
	AddSystem(new SoundManager);
    AddSystem(new StateManager);
	AddSystem(new IMGUI_app);

	for (auto i = Systems.begin(); i != Systems.end(); ++i)
	{
		(*i)->Initialize();
	}

    APP->Set_Window_Title("Chicken Fight");
	//
	glfwSwapInterval(1);//60fps
	BackGroundMusic = new Audio();
	BackGroundMusic->Load_Audio_From_File("asset/Sound/game_background.wav");
	BackGroundMusic->Set_Name("BackGroundMusic");
	BackGroundMusic->Set_Volume(0.3f);
	BackGroundMusic->Play_Audio(true);


	m_refreshrate = glfwGetVideoMode(glfwGetPrimaryMonitor())->refreshRate;
	std::cout << "refresh : " << m_refreshrate << std::endl;
}

void Engine::AddSystem(System * system)
{
	Systems.push_back(system);
}

void Engine::Quit()
{
	BackGroundMusic->Close();
	SOUND->Delete_AudioinList(BackGroundMusic);
	for(auto& system : Systems)
	{
		system->Close();
		delete system;
		system = nullptr;
	}
	Systems.clear();
	//Systems.resize(0);
}

void Engine::SetQuit()
{
	Is_Running = false;
}
