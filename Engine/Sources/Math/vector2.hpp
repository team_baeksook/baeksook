/***********************************************************************
	File name		: vector2.hpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Basic_Math.hpp"

struct vector2
{
public:
    float x;
    float y;
    vector2(float value = 0);
    vector2(float xtemp, float ytemp);
    //vector addition
    vector2 operator+(vector2 v) const;
    vector2& operator+=(const vector2 v);
    //vector subtraction
    vector2 operator-(vector2 v) const;
    vector2& operator-=(const vector2 v);
    //vector negation
    vector2 operator-() const;
    //vector multiplication
    vector2 operator*(float scalar) const;
    vector2& operator*=(float scalar);
    //vector division
    vector2 operator/(float scalar) const;
    vector2& operator/=(float scalar);
    //vector comparison
    bool operator==(vector2 v) const;
	bool operator!=(vector2 v) const;
    bool operator<=(vector2 v) const;
    bool operator>=(vector2 v) const;
    bool operator<(vector2 v) const;
    bool operator>(vector2 v) const;
    //vector dot production
    float operator*(vector2 v) const;

	//might be a useful function : set vector 0
	void SetZero();

	//might be a useful function : swap the x and y value
	void SwapXY();

    //vector projection a.Projection(b) means project a onto b
    vector2 Projection(vector2 v);

	vector2 Scale_the_vector(vector2 v) const;

    friend std::ostream& operator<<(std::ostream& os, const vector2& v);
};

//vector multiplication for exchange
vector2 operator*(float scalar, vector2 vector);

//distance of two vector
float Distance(const vector2 v1, const vector2 v2);
float Distance_Squared(const vector2 v1, const vector2 v2);

//angle of two vector
float Angle(const vector2 v1, const vector2 v2 = vector2(1.0f, 0.0f));
float Angle_Degree(const vector2 v1, const vector2 v2 = vector2(1.0f, 0.0f));

//calculate perpendicular vector
vector2 Perpendicular(vector2 vector);

//magnitude of vector
float Magnitude(vector2 vector);
float Magnitude_Squared(vector2 vector);

//vector normalize
vector2 Normalizing(vector2 vector);

//vector print
std::ostream& operator<<(std::ostream& os, const vector2& v);

//get rotated vector of (1, 0) vector with counter-clockwise order(Degree)
vector2 GetVectorByDegree(float degree);

//get rotated vector of (1, 0) vector with counter-clockwise order(Radian)
vector2 GetVectorByRadian(float radian);