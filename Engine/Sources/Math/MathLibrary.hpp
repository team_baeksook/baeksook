/***********************************************************************
	File name		: MathLibrary.hpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once

#include "vector2.hpp"
#include "vector3.hpp"
#include "matrix2.hpp"
#include "affine2d.hpp"
