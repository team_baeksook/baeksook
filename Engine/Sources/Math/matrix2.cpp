/***********************************************************************
	File name		: matrix.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "matrix2.hpp"

matrix2::matrix2(float value) : column1(value), column2(value) {}

matrix2::matrix2(vector2 column_v1, vector2 column_v2) : column1(column_v1), column2(column_v2) {}

matrix2::matrix2(float num1, float num2, float num3, float num4) : column1(num1, num2), column2(num3, num4) {}

float& matrix2::operator()(int column, int row)
{
    if(column == 0)
    {
        if(row == 0)
        {
            return column1.x;
        }
        if(row == 1)
        {
            return column1.y;
        }
    }
    if(row == 0)
    {
        return column2.x;
    }
    return column2.y;
}

float matrix2::operator()(int column, int row) const
{
    if (column == 0)
    {
        if (row == 0)
        {
            return column1.x;
        }
        if (row == 1)
        {
            return column1.y;
        }
    }
    if (row == 0)
    {
        return column2.x;
    }
    return column2.y;
}

vector2 matrix2::GetRow(int index) const
{
    vector2 result;
    if (index == 0)
    {
        result = vector2(this->column1.x, this->column2.x);
    }
    if (index == 1)
    {
        result = vector2(this->column1.y, this->column2.y);
    }
    return result;
}

matrix2 matrix2::operator*(const matrix2 matrix) const
{
    matrix2 result;
    result(0, 0) = this->GetRow(0) * matrix.column1;
    result(0, 1) = this->GetRow(1) * matrix.column1;
    result(1, 0) = this->GetRow(0) * matrix.column2;
    result(1, 1) = this->GetRow(1) * matrix.column2;
    return result;
}

matrix2 matrix2::operator*=(const matrix2 matrix)
{
    *this = *this * matrix;
    return *this;
}

vector2 matrix2::operator*(const vector2 v) const
{
    vector2 result;
    result.x = this->GetRow(0) * v;
    result.y = this->GetRow(1) * v;
    return result;
}

matrix2 matrix2::operator*(const float scalar) const
{
    matrix2 result;
    result.column1 = this->column1 * scalar;
    result.column2 = this->column2 * scalar;
    return result;
}

matrix2 matrix2::operator*=(const float scalar)
{
    this->column1 *= scalar;
    this->column2 *= scalar;
    return *this;
}

matrix2 matrix2::operator/(const float scalar) const
{
    matrix2 result;
    result.column1 = this->column1 / scalar;
    result.column2 = this->column2 / scalar;
    return result;
}

matrix2 matrix2::operator/=(const float scalar)
{
    this->column1 /= scalar;
    this->column2 /= scalar;
    return *this;
}

matrix2 Rotation_Matrix_Degree(float temp)
{
    matrix2 result;
    temp = To_RADIAN(temp);
    float cos_value = cos(temp);
    float sin_value = sin(temp);
    result(0, 0) = cos_value;
    result(1, 0) = -sin_value;
    result(0, 1) = sin_value;
    result(1, 1) = cos_value;
    return result;
}

matrix2 Rotation_Matrix_Radian(float temp)
{
    matrix2 result;
	float cos_value = cos(temp);
	float sin_value = sin(temp);
    result(0, 0) = cos_value;
    result(1, 0) = -sin_value;
    result(0, 1) = sin_value;
    result(1, 1) = cos_value;
    return result;
}

matrix2 Identity_Matrix()
{
    matrix2 result(1.0f, 0.0f, 0.0f, 1.0f);
    return result;
}

matrix2 Scale_Matrix(float scale)
{
    matrix2 result(scale, 0.0f, 0.0f, scale);
    return result;
}

matrix2 Scale_Matrix(float x_scale, float y_scale)
{
    matrix2 result(x_scale, 0.0f, 0.0f, y_scale);
    return result;
}

matrix2 Transpose(matrix2 matrix)
{
    matrix2 result(matrix.GetRow(0), matrix.GetRow(1));
    return result;
}

float Determinant(matrix2 matrix)
{
    float result = matrix.column1.x * matrix.column2.y - matrix.column1.y * matrix.column2.x;
    return result;
}

matrix2 Inverse(matrix2 matrix)
{
    matrix2 result(matrix.column2.y, -(matrix.column1.y), -(matrix.column2.x), matrix.column1.x);
    result /= Determinant(matrix);
    return result;
}

std::ostream& operator<<(std::ostream& os, const matrix2& m)
{
    os << m.GetRow(0) << std::endl;
    os << m.GetRow(1);
    return os;
}
