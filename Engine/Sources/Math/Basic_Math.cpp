/***********************************************************************
	File name		: Basic_Math.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Basic_Math.hpp"
#include "vector2.hpp"
#include "vector3.hpp"
#include "matrix2.hpp"

#include <stdarg.h>
#include <iostream>

float To_RADIAN(float degree)
{
    float temp;
    temp = degree * PI / 180.0f;
    return temp;
}

float To_DEGREE(float radian)
{
    float temp;
    temp = radian * 180.0f / PI;
    return temp;
}

vector2 WhereIntersect(vector2 v1_first, vector2 v1_end, vector2 v2_first, vector2 v2_end)
{
    matrix2 mat1 = matrix2(v1_first.x - v1_end.x, v1_first.y - v1_end.y, v2_first.x - v2_end.x, v2_first.y - v2_end.y);
    float determinant1 = Determinant(mat1);

    if(determinant1 == 0)
    {
        return NULL;
    }

    matrix2 mat2 = matrix2(v1_first, v1_end);
    matrix2 mat3 = matrix2(v2_first, v2_end);

    float determinant2 = Determinant(mat2);
    float determinant3 = Determinant(mat3);

    vector2 result;
    result.x = (determinant2 * mat1(1, 0) - mat1(0, 0) * determinant3) / determinant1;
    result.y = (determinant2 * mat1(1, 1) - mat1(0, 1) * determinant3) / determinant1;

    if((!isOnLine(v1_first, v1_end, result)) || (!isOnLine(v2_first, v2_end, result)))
    {
        return NULL;
    }

    return result;
}

bool isOnLine(vector2 line_first, vector2 line_end, vector2 pt)
{
    if (pt.x > WhatIsBig(line_first.x, line_end.x) || pt.x < WhatIsSmall(line_first.x, line_end.x))
    {
        return false;
    }
    if (pt.y > WhatIsBig(line_first.y, line_end.y) || pt.y < WhatIsSmall(line_first.y, line_end.y))
    {
        return false;
    }
    return true;
}

float Distance_between_line_point(vector2 line_first, vector2 line_end, vector2 pt)
{
    vector2 closestPt;

    vector2 line_dir = line_end - line_first;
    vector2 diff = pt - line_first;
    vector2 proj = diff.Projection(line_dir);
    closestPt = proj + line_first;

    float scale = proj.x / line_dir.x;
    if(scale < 0)
    {
        closestPt = line_first;
    }
    if(scale > 1)
    {
        closestPt = line_end;
    }

    return Magnitude(pt - closestPt);
}

float Distance_between_line_rect(vector2 line_first, vector2 line_end, vector2 rectpos1, vector2 rectpos2, vector2 rectpos3, vector2 rectpos4)
{
	float data[4];

	vector2 line_dir = line_end - line_first;

	if (line_dir * (line_first - rectpos1) < 0)
	{
		line_dir = vector2(line_dir.y, -line_dir.x);
		vector3 half_space = vector3(line_dir, -line_dir * line_first);

		int neg_count = 0;
		if (half_space * vector3(rectpos1, 1.0f) < 0.0f)
		{
			++neg_count;
		}
		if (half_space * vector3(rectpos2, 1.0f) < 0.0f)
		{
			++neg_count;
		}
		if (half_space * vector3(rectpos3, 1.0f) < 0.0f)
		{
			++neg_count;
		}
		if (half_space * vector3(rectpos4, 1.0f) < 0.0f)
		{
			++neg_count;
		}

		if (neg_count == 2)
		{
			return 0.0f;
		}
	}

	data[0] = Distance_between_line_point(line_first, line_end, rectpos1);
	data[1] = Distance_between_line_point(line_first, line_end, rectpos2);
	data[2] = Distance_between_line_point(line_first, line_end, rectpos3);
	data[3] = Distance_between_line_point(line_first, line_end, rectpos4);

	float smaller1 = WhatIsSmall(data[0], data[1]);
	float smaller2 = WhatIsSmall(data[2], data[3]);

	return WhatIsSmall(smaller1, smaller2);
}

bool Rectangle_Circle_CollisionCheck(vector2 circlePos, float radius, vector2 rectPos, vector2 rectScale, float degree)
{
    matrix2 rotation_mat = Rotation_Matrix_Degree(degree);

    vector2 temp_ball_pos = rotation_mat * (circlePos - rectPos);

    vector2 left_bottom = -(rectScale / 2.0f);
    vector2 right_top = rectScale / 2.0f;
    vector2 closestPt = vector2(WhatIsBig(left_bottom.x, WhatIsSmall(temp_ball_pos.x, right_top.x)),
        WhatIsBig(left_bottom.y, WhatIsSmall(temp_ball_pos.y, right_top.y)));
    //To calculate which point is closest with the circle
    float distance = Magnitude(closestPt - temp_ball_pos);

    if (distance < radius)
    {
        return true;
    }
    return false;
}

//checking rectangle point collision using AABB algorithm
bool Rectangle_Point_CollisionCheck(vector2 point, vector2 rectpos, vector2 rectscale)
{
	vector2 min_point = rectpos - rectscale / 2;
	vector2 max_point = rectpos + rectscale / 2;

	if (point.x < min_point.x)
	{
		return false;
	}
	if (point.x > max_point.x)
	{
		return false;
	}
	if (point.y < min_point.y)
	{
		return false;
	}
	if (point.y > max_point.y)
	{
		return false;
	}

	return true;
}

int GetFactorial(int data)
{
    int result = 1;
    for(int i = 1; i <= data; ++i)
    {
        result *= i;
    }
    return result;
}

//for the axis alligned box
vector2 GetLeftBottomPoint(int count, ...)
{
	vector2 result;

	va_list arglist;
	va_start(arglist, count);
	for (int j = 0; j < count; ++j)
	{
		vector2 pt = va_arg(arglist, vector2);
		if (j == 0)
		{
			result = pt;
			continue;
		}
		if (result.x > pt.x)
		{
			result.x = pt.x;
		}
		if (result.y > pt.y)
		{
			result.y = pt.y;
		}
	}
	va_end(arglist);

	return result;
}

vector2 GetRightTopPoint(int count, ...)
{
	vector2 result;

	va_list arglist;
	va_start(arglist, count);
	for (int j = 0; j < count; ++j)
	{
		vector2 pt = va_arg(arglist, vector2);
		if (j == 0)
		{
			result = pt;
			continue;
		}
		if (result.x < pt.x)
		{
			result.x = pt.x;
		}
		if (result.y < pt.y)
		{
			result.y = pt.y;
		}
	}
	va_end(arglist);

	return result;
}