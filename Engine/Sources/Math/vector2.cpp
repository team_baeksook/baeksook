/***********************************************************************
	File name		: vector2.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "vector2.hpp"

vector2::vector2(float value) : x(value), y(value) {}

vector2::vector2(float xtemp, float ytemp) : x(xtemp), y(ytemp) {}

vector2 vector2::operator+(const vector2 v) const
{
    vector2 result(this->x + v.x, this->y + v.y);
    return result;
}

vector2& vector2::operator+=(const vector2 v)
{
    this->x += v.x;
    this->y += v.y;
    return *this;
}

vector2 vector2::operator-(const vector2 v) const
{
    vector2 result(this->x - v.x, this->y - v.y);
    return result;
}

vector2& vector2::operator-=(const vector2 v)
{
    this->x -= v.x;
    this->y -= v.y;
    return *this;
}

vector2 vector2::operator-() const
{
    vector2 result;
    result.x = -(this->x);
    result.y = -(this->y);
    return result;
}

vector2 vector2::operator*(float scalar) const
{
    vector2 result(this->x * scalar, this->y * scalar);
    return result;
}

vector2& vector2::operator*=(float scalar)
{
    this->x *= scalar;
    this->y *= scalar;
    return *this;
}

vector2 vector2::operator/(float scalar) const
{
    vector2 result(this->x / scalar, this->y / scalar);
    return result;
}

vector2& vector2::operator/=(float scalar)
{
    this->x /= scalar;
    this->y /= scalar;
    return *this;
}

bool vector2::operator==(vector2 v) const
{
    if(this->x == v.x)
    {
        if(this->y == v.y)
        {
            return true;
        }
    }
    return false;
}

bool vector2::operator!=(vector2 v) const
{
	if(this->x == v.x)
	{
		if(this->y == v.y)
		{
			return false;
		}
	}
	return true;
}

bool vector2::operator<=(vector2 v) const
{
    if (this->x <= v.x)
    {
        if (this->y <= v.y)
        {
            return true;
        }
    }
    return false;
}

bool vector2::operator>=(vector2 v) const
{
    if (this->x >= v.x)
    {
        if (this->y >= v.y)
        {
            return true;
        }
    }
    return false;
}

bool vector2::operator<(vector2 v) const
{
    if (this->x < v.x)
    {
        if (this->y < v.y)
        {
            return true;
        }
    }
    return false;
}

bool vector2::operator>(vector2 v) const
{
    if (this->x > v.x)
    {
        if (this->y > v.y)
        {
            return true;
        }
    }
    return false;
}

float vector2::operator*(vector2 v) const
{
    float result = x * v.x + y * v.y;
    return result;
}

void vector2::SetZero()
{
	this->x = 0.0f;
	this->y = 0.0f;
}

void vector2::SwapXY()
{
	float temp = this->x;
	this->x = this->y;
	this->y = temp;
}

vector2 vector2::Projection(vector2 v)
{
    float scalar = (*this) * v / Magnitude_Squared(v);
    return v * scalar;
}

vector2 vector2::Scale_the_vector(vector2 v) const
{
	return vector2{ this->x * v.x, this->y * v.y };
}

vector2 operator*(float scalar, vector2 vector)
{
    vector2 result = vector * scalar;
    return result;
}

float Distance(const vector2 v1, const vector2 v2)
{
    float result = Magnitude(v1 - v2);
    return result;
}

float Distance_Squared(const vector2 v1, const vector2 v2)
{
    float result = Magnitude_Squared(v1 - v2);
    return result;
}

float Angle(const vector2 v1, const vector2 v2)
{
    float a = atan2(v1.y, v1.x);
    float b = atan2(v2.y, v2.x);
    float result = a - b;
    return result;
}

float Angle_Degree(const vector2 v1, const vector2 v2)
{
    float result = Angle(v1, v2);
    result = To_DEGREE(result);
    return result;
}

vector2 Perpendicular(vector2 vector)
{
    vector2 result(-(vector.y), vector.x);
    return result;
}

float Magnitude(vector2 vector)
{
    float result = sqrt(Magnitude_Squared(vector));
    return result;
}

float Magnitude_Squared(vector2 vector)
{
    float result = pow(vector.x, 2) + pow(vector.y, 2);
    return result;
}

vector2 Normalizing(vector2 vector)
{
    if(Magnitude(vector) == 0.0f)
    {
        return vector;
    }
    vector2 result = vector;
    result /= Magnitude(result);
    return result;
}

std::ostream& operator<<(std::ostream& os, const vector2& v)
{
    os << "(" << v.x << "," << v.y << ")";
    return os;
}

vector2 GetVectorByDegree(float degree)
{
	float rad = To_RADIAN(degree);
	return vector2(std::cos(rad), std::sin(rad));
}

vector2 GetVectorByRadian(float radian)
{
	return vector2(std::cos(radian), std::sin(radian));
}
