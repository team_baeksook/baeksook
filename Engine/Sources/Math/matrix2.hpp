/***********************************************************************
	File name		: matrix2.hpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once

#include "vector2.hpp"

class matrix2
{
public:
    vector2 column1;
    vector2 column2;
    //matrix constructor
    matrix2(float value = 0.0f);
    matrix2(vector2 column_v1, vector2 column_v2);
    matrix2(float num1, float num2, float num3, float num4);
    //getting & setting matrix
    float& operator()(int column, int row);
    float operator()(int column, int row) const;
    vector2 GetRow(int index) const;
    //matrix multiplication
    matrix2 operator*(const matrix2 matrix) const;
    matrix2 operator*=(const matrix2 matrix);
    vector2 operator*(const vector2 v) const;
    matrix2 operator*(const float scalar) const;
    matrix2 operator*=(const float scalar);
    //matrix division
    matrix2 operator/(const float scalar) const;
    matrix2 operator/=(const float scalar);

    friend std::ostream& operator<<(std::ostream& os, const matrix2& m);
};

//rotation matrix
matrix2 Rotation_Matrix_Degree(float temp = 0);
matrix2 Rotation_Matrix_Radian(float temp = 0);
//matrix identity
matrix2 Identity_Matrix();
//matrix scale
matrix2 Scale_Matrix(float scale);
matrix2 Scale_Matrix(float x_scale, float y_scale);
// matrix transpose
matrix2 Transpose(matrix2 matrix);
//matrix determinate
float Determinant(matrix2 matrix);
//matrix inverse
matrix2 Inverse(matrix2 matrix);

std::ostream& operator<<(std::ostream& os, const matrix2& m);