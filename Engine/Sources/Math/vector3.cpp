/***********************************************************************
	File name		: vector3.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "vector3.hpp"
#include "vector2.hpp"

vector3::vector3(float value) : x(value), y(value), z(value) {}

vector3::vector3(float xtemp, float ytemp, float ztemp) : x(xtemp), y(ytemp), z(ztemp) {}

vector3::vector3(vector2 vector, float zvalue) : x(vector.x), y(vector.y), z(zvalue) {}

vector3 vector3::operator+(const vector3 v) const
{
    vector3 result(this->x + v.x, this->y + v.y, this->z + v.z);
    return result;
}

vector3& vector3::operator+=(const vector3 v)
{
    this->x += v.x;
    this->y += v.y;
    this->z += v.z;
    return *this;
}

vector3 vector3::operator-(const vector3 v) const
{
    vector3 result(this->x - v.x, this->y - v.y, this->z - v.z);
    return result;
}

vector3& vector3::operator-=(const vector3 v)
{
    this->x -= v.x;
    this->y -= v.y;
    this->z -= v.z;
    return *this;
}

vector3 vector3::operator-()
{
	vector3 result;
    result.x = -(this->x);
    result.y = -(this->y);
    result.z = -(this->z);
    return result;
}

vector3 vector3::operator*(float scalar) const
{
    vector3 result(this->x * scalar, this->y * scalar, this->z * scalar);
    return result;
}

vector3& vector3::operator*=(float scalar)
{
    this->x *= scalar;
    this->y *= scalar;
    this->z *= scalar;
    return *this;
}

vector3 vector3::operator/(float scalar) const
{
    vector3 result(this->x / scalar, this->y / scalar, this->z / scalar);
    return result;
}

vector3& vector3::operator/=(float scalar)
{
    this->x /= scalar;
    this->y /= scalar;
    this->z /= scalar;
    return *this;
}

bool vector3::operator==(vector3 v) const
{
    if (this->x == v.x)
    {
        if (this->y == v.y)
        {
            if (this->z == v.z)
            {
                return true;
            }
        }
    }
    return false;
}

bool vector3::operator!=(vector3 v) const
{
	if(this->x == v.x)
	{
		if(this->y == v.y)
		{
			if(this->z == v.z)
			{
				return false;
			}
		}
	}
	return true;
}

bool vector3::operator<=(vector3 v) const
{
    if (this->x <= v.x)
    {
        if (this->y <= v.y)
        {
            if (this->z <= v.z)
            {
                return true;
            }
        }
    }
    return false;
}

bool vector3::operator>=(vector3 v) const
{
    if (this->x >= v.x)
    {
        if (this->y >= v.y)
        {
            if (this->z >= v.z)
            {
                return true;
            }
        }
    }
    return false;
}

bool vector3::operator<(vector3 v) const
{
    if (this->x < v.x)
    {
        if (this->y < v.y)
        {
            if (this->z < v.z)
            {
                return true;
            }
        }
    }
    return false;
}

bool vector3::operator>(vector3 v) const
{
    if (this->x > v.x)
    {
        if (this->y > v.y)
        {
            if (this->z > v.z)
            {
                return true;
            }
        }
    }
    return false;
}

float vector3::operator*(vector3 v) const
{
    float result = x * v.x + y * v.y + z * v.z;
    return result;
}

vector2 vector3::GetVector2() const
{
    return vector2(x, y);
}

vector3 operator*(float scalar, vector3 vector)
{
    vector3 result = vector * scalar;
    return result;
}

float Distance(const vector3 v1, const vector3 v2)
{
    float result = Magnitude(v1 - v2);
    return result;
}

float Distance_Squared(const vector3 v1, const vector3 v2)
{
    float result = Magnitude_Squared(v1 - v2);
    return result;
}

float Angle(const vector3 v1, const vector3 v2)
{
    float up = v1 * v2;
    float down = Magnitude(v1) * Magnitude(v2);
    float angle = acos(up / down);
    return angle;
}

float Angle_Degree(const vector3 v1, const vector3 v2)
{
    float result = Angle(v1, v2);
    result = To_DEGREE(result);
    return result;
}

vector3 CrossProduct(vector3 v1, vector3 v2)
{
    vector3 result(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
    return result;
}

float Magnitude(vector3 vector)
{
    float result = sqrt(Magnitude_Squared(vector));
    return result;
}

float Magnitude_Squared(vector3 vector)
{
    float result = pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2);
    return result;
}

vector3 Normalizing(vector3 vector)
{
    if (Magnitude(vector) == 0.0f)
    {
        return vector;
    }
    vector3 result = vector;
    result /= Magnitude(vector);
    return result;
}

std::ostream& operator<<(std::ostream& os, const vector3& v)
{
    os << "(" << v.x << "," << v.y << "," << v.z << ")";
    return os;
}
