/***********************************************************************
	File name		: affine2d.cpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "affine2d.hpp"

affine2d::affine2d(float value) : column1(value), column2(value), column3(value) {}

affine2d::affine2d(vector3 column_v1, vector3 column_v2, vector3 column_v3) : column1(column_v1), column2(column_v2), column3(column_v3) {}

affine2d::affine2d(float value1, float value2, float value3, float value4, float value5, float value6, float value7,
    float value8, float value9) : column1(value1, value2, value3), column2(value4, value5, value6), column3(value7, value8, value9) {}

affine2d::affine2d(matrix2 matrix, vector2 vector) : column1(matrix.column1.x, matrix.column1.y, 0),
column2(matrix.column2.x, matrix.column2.y, 0), column3(vector.x, vector.y, 1.0f) {}

float affine2d::operator()(int column, int row) const
{
    if(column == 0)
    {
        if(row == 0)
        {
            return column1.x;
        }
        if(row == 1)
        {
            return column1.y;
        }
        if(row == 2)
        {
            return column1.z;
        }
    }
    if(column == 1)
    {
        if(row == 0)
        {
            return column2.x;
        }
        if(row == 1)
        {
            return column2.y;
        }
        if(row == 2)
        {
            return column2.z;
        }
    }
    if(row == 0)
    {
        return column3.x;
    }
    if(row == 1)
    {
        return column3.y;
    }
    return column3.z;
}

float& affine2d::operator()(int column, int row)
{
    if (column == 0)
    {
        if (row == 0)
        {
            return column1.x;
        }
        if (row == 1)
        {
            return column1.y;
        }
        if (row == 2)
        {
            return column1.z;
        }
    }
    if (column == 1)
    {
        if (row == 0)
        {
            return column2.x;
        }
        if (row == 1)
        {
            return column2.y;
        }
        if (row == 2)
        {
            return column2.z;
        }
    }
    if (row == 0)
    {
        return column3.x;
    }
    if (row == 1)
    {
        return column3.y;
    }
    return column3.z;
}

vector3 affine2d::GetRow(int index) const
{
    vector3 result;
    if(index == 0)
    {
        result = vector3(column1.x, column2.x, column3.x);
    }
    if(index == 1)
    {
        result = vector3(column1.y, column2.y, column3.y);
    }
    if(index == 2)
    {
        result = vector3(column1.z, column2.z, column3.z);
    }
    return result;
}

affine2d affine2d::operator*(affine2d affine) const
{
    affine2d result(this->GetRow(0) * affine.column1, this->GetRow(1) * affine.column1,
        this->GetRow(2) * affine.column1, this->GetRow(0) * affine.column2,
        this->GetRow(1) * affine.column2, this->GetRow(2) * affine.column2,
        this->GetRow(0) * affine.column3, this->GetRow(1) * affine.column3,
        this->GetRow(2) * affine.column3);
    return result;
}

affine2d affine2d::operator*=(affine2d affine)
{
    *this = *this * affine;
    return *this;
}

vector3 affine2d::operator*(vector3 vector) const
{
    vector3 result(this->GetRow(0) * vector, this->GetRow(1) * vector, this->GetRow(2) * vector);
    return result;
}

affine2d Transpose(affine2d affine)
{
    affine2d result(affine.GetRow(0), affine.GetRow(1), affine.GetRow(2));
    return result;
}

affine2d Rotation_Affine_Degree(float Degree)
{
    vector2 vector(0.0f, 0.0f);
    affine2d result(Rotation_Matrix_Degree(Degree), vector);
    return result;
}

affine2d Rotation_Affine_Radian(float Radian)
{
	vector2 vector(0.0f, 0.0f);
    affine2d result(Rotation_Matrix_Radian(Radian), vector);
    return result;
}

affine2d Rotation_Affine_Vector(vector2 vector)
{
    return Rotation_Affine_Degree(Angle_Degree(vector));
}

affine2d Identity_Affine()
{
    affine2d result(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
    return result;
}

affine2d Scale_Affine(vector2 x)
{
    matrix2 matrix = Scale_Matrix(x.x, x.y);
    vector2 vector(0.0f, 0.0f);
    affine2d result(matrix, vector);
    return result;
}

affine2d Scale_Affine(float x, float y)
{
    matrix2 matrix = Scale_Matrix(x, y);
    vector2 vector(0.0f, 0.0f);
    affine2d result(matrix, vector);
    return result;
}

affine2d Translate_Affine(float x, float y)
{
    matrix2 matrix(1.0f, 0.0f, 0.0f, 1.0f);
    vector2 vector(x, y);
    affine2d result(matrix, vector);
    return result;
}

affine2d Translate_Affine(vector2 v)
{
    affine2d result = Translate_Affine(v.x, v.y);
    return result;
}

std::ostream& operator<<(std::ostream& os, const affine2d& m)
{
    os << m.GetRow(0) << std::endl;
    os << m.GetRow(1) << std::endl;
    os << m.GetRow(2);
    return os;
}
