/***********************************************************************
	File name		: Ease.h
	Project name	: WFPT
	Author			: HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <cmath>//for trig functions

/*********************************************************
 NOTE ON VARIABLES:
	time     - time it take between each frame
	begin    - starting value
	change   - how much value should be changed
	duration - how long the procedure should take
 *********************************************************/
namespace Easing
{
//public:
	//Linear
	float easeNone_Linear(float time, float begin, float change, float duration);
	//Sine
	float easeIn_Sine(float time, float begin, float change, float duration);
	float easeOut_Sine(float time, float begin, float change, float duration);
	float easeInOut_Sine(float time, float begin, float change, float duration);
	//Circ
	float easeIn_Circ(float time, float begin, float change, float duration);
	float easeOut_Circ(float time, float begin, float change, float duration);
	float easeInOut_Circ(float time, float begin, float change, float duration);
	//Elastic
	float easeIn_Elastic(float time, float begin, float change, float duration);
	//Bounce
	float easeInOut_Bounce(float time, float begin, float change, float duration);
	float easeOut_Bounce(float t, float b, float c, float d);
	float easeIn_Bounce(float t, float b, float c, float d);
	float ease_Bounce_animation(float t, float b, float c, float d);

	float easeIn_Cubic(float t, float b, float c, float d);
	float easeOut_Cubic(float t, float b, float c, float d);
//private:
	const float Easing_Pi = 4.0f * std::atan(1.0f);
};

//example of how it was used
//if (is_A_Pushed && !shouldMoveLeft)
//{
//	//Sine
//	gameObjects[0].transform.SetTranslation({ easing.easeIn_Sine(time, 0, 200, 2), 0 });
//	gameObjects[1].transform.SetTranslation({ easing.easeOut_Sine(time, 0, 200, 2), 20 });
//	gameObjects[2].transform.SetTranslation({ easing.easeInOut_Sine(time, 0, 200, 2), 40 });
//	//Circ
//	gameObjects[3].transform.SetTranslation({ easing.easeIn_Circ(time, 0, 200, 2), 60 });
//	gameObjects[4].transform.SetTranslation({ easing.easeOut_Circ(time, 0, 200, 2), 80 });
//	gameObjects[5].transform.SetTranslation({ easing.easeInOut_Circ(time, 0, 200, 2), 100 });
//	//Linear
//	gameObjects[6].transform.SetTranslation({ easing.easeNone_Linear(time, 0, 200, 2), 120 });
//	//Elastic
//	gameObjects[7].transform.SetTranslation({ easing.easeIn_Elastic(time, 0, 200, 2), 140 });
//	time += dt;
//	if (time >= 2.0f)
//	{
//		time = 0.0f;
//		shouldMoveLeft = true;
//	}
//}
//if (is_A_Pushed && shouldMoveLeft)
//{
//	//Sine
//	gameObjects[0].transform.SetTranslation({ easing.easeIn_Sine(time,200, -200, 2), 0 });
//	gameObjects[1].transform.SetTranslation({ easing.easeOut_Sine(time, 200, -200, 2), 20 });
//	gameObjects[2].transform.SetTranslation({ easing.easeInOut_Sine(time, 200, -200, 2), 40 });
//	//Circ
//	gameObjects[3].transform.SetTranslation({ easing.easeIn_Circ(time, 200, -200, 2), 60 });
//	gameObjects[4].transform.SetTranslation({ easing.easeOut_Circ(time, 200, -200, 2), 80 });
//	gameObjects[5].transform.SetTranslation({ easing.easeInOut_Circ(time, 200, -200, 2), 100 });
//	//Linear
//	gameObjects[6].transform.SetTranslation({ easing.easeNone_Linear(time, 200, -200, 2), 120 });
//	//Elastic
//	gameObjects[7].transform.SetTranslation({ easing.easeIn_Elastic(time, 200, -200, 2), 140 });
//	time += dt;
//	if (time >= 2.0f)
//	{
//		time = 0.0f;
//		shouldMoveLeft = false;
//	}
//}