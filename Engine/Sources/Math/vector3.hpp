/***********************************************************************
	File name		: vector3.hpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include "Basic_Math.hpp"

struct vector3
{
public:
    float x;
    float y;
    float z;
    vector3(float value = 0);
    vector3(float xtemp, float ytemp, float ztemp);
    vector3(vector2 vector, float zvalue);
    //vector addition
    vector3 operator+(vector3 v) const;
    vector3& operator+=(const vector3 v);
    //vector subtraction
    vector3 operator-(vector3 v) const;
    vector3& operator-=(const vector3 v);
    //vector negation
    vector3 operator-();
    //vector multiplication
    vector3 operator*(float scalar) const;
    vector3& operator*=(float scalar);
    //vector division
    vector3 operator/(float scalar) const;
    vector3& operator/=(float scalar);
    //vector comparison
    bool operator==(vector3 v) const;
	bool operator!=(vector3 v) const;
    bool operator<=(vector3 v) const;
    bool operator>=(vector3 v) const;
    bool operator<(vector3 v) const;
    bool operator>(vector3 v) const;
    //vector dot production
    float operator*(vector3 v) const;

    vector2 GetVector2() const;

    friend std::ostream& operator<<(std::ostream& os, const vector3& v);
};

//vector multiplication for exchange
vector3 operator*(float scalar, vector3 vector);

//distance of two vector
float Distance(const vector3 v1, const vector3 v2);
float Distance_Squared(const vector3 v1, const vector3 v2);

//angle of two vector
float Angle(const vector3 v1, const vector3 v2);
float Angle_Degree(const vector3 v1, const vector3 v2);

//vector cross product
vector3 CrossProduct(vector3 v1, vector3 v2);

//magnitude of vector
float Magnitude(vector3 vector);
float Magnitude_Squared(vector3 vector);

//vector normalize
vector3 Normalizing(vector3 vector);

std::ostream& operator<<(std::ostream& os, const vector3& v);