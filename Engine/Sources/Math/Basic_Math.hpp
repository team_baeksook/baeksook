/***********************************************************************
	File name		: Basic_Math.hpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once
#include <cmath>
#include <chrono>
#include <random>

struct vector2;

//Declare of pi
const float PI = 4.0f * std::atan(1.0f);

//convert m_Degree to Radian
float To_RADIAN(float degree);

//convert Radian to m_Degree
float To_DEGREE(float radian);

//return the bigger one
template <typename T>
T WhatIsBig(T t1, T t2)
{
    if(t1 >= t2)
    {
        return t1;
    }
    return t2;
}

template <typename T>
T WhatIsSmall(T t1, T t2)
{
    if(t1 <= t2)
    {
        return t1;
    }
    return t2;
}

//calculating the intersection point of two lines segment. If two lines segment does not intersect, return NULL
vector2 WhereIntersect(vector2 v1_first, vector2 v1_end, vector2 v2_first, vector2 v2_end);

//check if the certain point is on line segment
bool isOnLine(vector2 line_first, vector2 line_end, vector2 pt);

//calcuating the minimal distance between line segment and the point
float Distance_between_line_point(vector2 line_first, vector2 line_end, vector2 pt);

//calcuating the minimal distance between line segment and the rectangle
float Distance_between_line_rect(vector2 line_first, vector2 line_end, vector2 rectpos1, vector2 rectpos2, vector2 rectpos3, vector2 rectpos4);

//collision check between rectangle and circle
bool Rectangle_Circle_CollisionCheck(vector2 circlePos, float radius, vector2 rectPos, vector2 rectScale, float degree = 0.0f);

//collision check between rectangle and point
bool Rectangle_Point_CollisionCheck(vector2 point, vector2 rectpos, vector2 rectscale);

int GetFactorial(int data);

//get the arbitrary number with certain range
template<typename T>
T GetRandomNumber(T min, T max)
{
    int current_time = static_cast<int>(std::chrono::system_clock::now().time_since_epoch().count());
    std::mt19937 rand(current_time);

    std::uniform_real_distribution<T> distribution(min, max);

    return static_cast<T>(distribution(rand));
}

template <typename T>
void Swap(T& a, T& b)
{
	T temp = a;
	a = b;
	b = temp;
}

//for the axis alligned boundary box, get the left bottom point of the box (minx, miny)
vector2 GetLeftBottomPoint(int count, ...);

//for the axis alligned boundary box, get the right top point of the box (maxx, maxy)
vector2 GetRightTopPoint(int count, ...);
