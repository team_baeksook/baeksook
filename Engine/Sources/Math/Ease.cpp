/***********************************************************************
	File name		: Ease.cpp
	Project name	: WFPT
	Author			: HyunSeok Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#include "Ease.h"


float Easing::easeNone_Linear(float time, float begin, float change, float duration)
{
	return change * time / duration + begin;
}

float Easing::easeIn_Sine(float time, float begin, float change, float duration)
{
	return -change * cos(time / duration * (Easing_Pi / 2)) + change + begin;
}
float Easing::easeOut_Sine(float time, float begin, float change, float duration)
{
	return change * sin(time / duration * (Easing_Pi / 2)) + begin;
}
float Easing::easeInOut_Sine(float time, float begin, float change, float duration)
{
	return -change / 2 * (cos(Easing_Pi*time / duration) - 1) + begin;
}

float Easing::easeIn_Circ(float time, float begin, float change, float duration)
{
	return -change * (sqrt(1 - (time /= duration)*time) - 1) + begin;
}
float Easing::easeOut_Circ(float time, float begin, float change, float duration)
{
	return change * sqrt(1 - (time = time / duration - 1)*time) + begin;
}
float Easing::easeInOut_Circ(float time, float begin, float change, float duration)
{
	if ((time /= duration / 2) < 1) return -change / 2 * (sqrt(1 - time * time) - 1) + begin;
	return change / 2 * (sqrt(1 - time * (time -= 2)) + 1) + begin;
}

float Easing::easeIn_Elastic(float time, float begin, float change, float duration)
{
	if (time == 0) return begin;
	if ((time /= duration) == 1) return begin + change;
	float p = duration * .3f;
	float a = change;
	float s = p / 4;
	float postFix = a * powf(2, 10 * (time -= 1)); // this is a fix, again, with post-increment operators
	return -(postFix * sin((time*duration - s)*(2 * Easing_Pi) / p)) + begin;
}

float Easing::easeInOut_Bounce(float time, float begin, float change, float duration)
{
	if ((time /= duration) < (1 / 2.75f)) 
	{
		return change * (7.5625f*time*time) + begin;
	}
	else if (time < (2 / 2.75f)) 
	{
		float postFix = time -= (1.5f / 2.75f);
		return change * (7.5625f*(postFix)*time + .75f) + begin;
	}
	else if (time < (2.5 / 2.75)) 
	{
		float postFix = time -= (2.25f / 2.75f);
		return change * (7.5625f*(postFix)*time + .9375f) + begin;
	}
	else 
	{
		float postFix = time -= (2.625f / 2.75f);
		return change * (7.5625f*(postFix)*time + .984375f) + begin;
	}
}

float Easing::easeOut_Bounce(float t, float b, float c, float d) {

	if ((t /= d) < (1 / 2.75f)) {
		return c * (7.5625f*t*t) + b;
	}
	else if (t < (2 / 2.75f)) {
		float postFix = t -= (1.5f / 2.75f);
		return c * (7.5625f*(postFix)*t + .75f) + b;
	}
	else if (t < (2.5 / 2.75)) {
		float postFix = t -= (2.25f / 2.75f);
		return c * (7.5625f*(postFix)*t + .9375f) + b;
	}
	else {
		float postFix = t -= (2.625f / 2.75f);
		return c * (7.5625f*(postFix)*t + .984375f) + b;
	}
}

float Easing::ease_Bounce_animation(float t, float b, float c, float d)
{
	float half_d = d / 5;
	if (t < half_d)
	{
		return easeOut_Cubic(t, b, c, half_d);
	}
	else
	{
		t -= half_d;
		return c - easeIn_Cubic(t, 0, c, d - half_d) + b;
	}
	//else
	//{
	//	if (is_bounce)
	//	{
	//		t -= half_d;
	//		return c - Easing::easeOut_Bounce(t, 0, c, d - half_d) + b;
	//	}
	//	else
	//	{
	//		return b + c;
	//	}
	//}
}

float Easing::easeIn_Cubic(float t, float b, float c, float d)
{
	return c * (t /= d)*t*t + b;
}

float Easing::easeOut_Cubic(float t, float b, float c, float d)
{
	return c * ((t = t / d - 1)*t*t + 1) + b;
}

float easeIn_Bounce(float t, float b, float c, float d)
{
	return c - Easing::easeOut_Bounce(d - t, 0, c, d) + b;
}
