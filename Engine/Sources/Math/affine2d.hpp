/***********************************************************************
	File name		: affine2d.hpp
	Project name	: WFPT
	Author			: MinSuk Kim

	All content (C) 2018 DigiPen (USA) Corporation, all rights reserved.
***********************************************************************/
#pragma once

#include "vector3.hpp"
#include "matrix2.hpp"

class affine2d
{
public:
    vector3 column1;
    vector3 column2;
    vector3 column3;
    //affine constructor
    affine2d(float value = 0.0f);
    affine2d(vector3 column_v1, vector3 column_v2, vector3 column_v3);
    affine2d(float value1, float value2, float value3, float value4, 
        float value5, float value6, float value7, float value8, float value9);
    affine2d(matrix2 matrix, vector2 vector);
    //getting and setting affine
    float operator()(int column, int row) const;
    float& operator()(int column, int row);
    vector3 GetRow(int index) const;
    //affine multiplication
    affine2d operator*(affine2d affine) const;
    affine2d operator*=(affine2d affine);
    vector3 operator*(vector3 vector) const;

    friend std::ostream& operator<<(std::ostream& os, const affine2d& m);
};

//affine transpose
affine2d Transpose(affine2d affine);

//affine rotation
affine2d Rotation_Affine_Degree(float Degree);
affine2d Rotation_Affine_Radian(float Radian);
affine2d Rotation_Affine_Vector(vector2 vector);

//identity affine
affine2d Identity_Affine();

//scale affine
affine2d Scale_Affine(vector2 x);
affine2d Scale_Affine(float x, float y);

//translation affine
affine2d Translate_Affine(float x, float y);
affine2d Translate_Affine(vector2 v);

std::ostream& operator<<(std::ostream& os, const affine2d& m);
