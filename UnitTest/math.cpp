/***********************************************************************
File name		: math.cpp
Project name	: WFPT
Author			: MinSuk Kim

All content(C) 2018 DigiPen(USA) Corporation, all rights reserved.
***********************************************************************/
#define CATCH_CONFIG_MAIN
#pragma warning(push)
#pragma warning(disable : 4244) // const int to float conversion
#include "catch/catch.hpp"
#pragma warning(pop)
#include "Math/MathLibrary.hpp"
#include <cmath>
#include "Graphic/Color.h"

namespace
{
    const float epsilon = std::numeric_limits<float>::epsilon();
}

//vector2 test
TEST_CASE("vector2 constructor")
{
    vector2 zero;
    REQUIRE((zero.x == 0.0f && zero.y == 0.0f));

    vector2 fill_float(1.0f);
    REQUIRE((fill_float.x == 1.0f && fill_float.y == 1.0f));

    vector2 custom_xy(1.0f, 2.0f);
    REQUIRE((custom_xy.x == 1.0f && custom_xy.y == 2.0f));

    vector2 zero_curly{};
    REQUIRE((zero_curly.x == 0.0f && zero_curly.y == 0.0f));

    vector2 fill_float_curly{2.0f};
    REQUIRE((fill_float_curly.x == 2.0f && fill_float_curly.y == 2.0f));

    vector2 custom_xy_curly{1.0f, 2.0f};
    REQUIRE((custom_xy_curly.x == 1.0f && custom_xy_curly.y == 2.0f));
}

TEST_CASE("vector2 addition")
{
     vector2 one(1.0f);
     vector2 negative_one(-1.0f);
     vector2 zero = one + negative_one;
     REQUIRE((zero.x == 0.0f && zero.y == 0.0f));

     zero = one;
     zero += negative_one;
     REQUIRE((zero.x == 0.0f && zero.y == 0.0f));
}

TEST_CASE("vector2 subtraction")
{
    vector2 one(1.0f);
    vector2 zero = one - one;
    REQUIRE((zero.x == 0.0f && zero.y == 0.0f));

    zero = one;
    zero -= one;
    REQUIRE((zero.x == 0.0f && zero.y == 0.0f));
}

 TEST_CASE("vector2 negation")
 {
     vector2 minus_ten(-10.0f);
     vector2 ten = -minus_ten;
     REQUIRE((ten.x == 10.0f && ten.y == 10.0f));
 }

 TEST_CASE("vector2 scaling")
 {
     vector2 ten(2.0f);
     ten *= 5.0f;
     REQUIRE((ten.x == 10.0f && ten.y == 10.0f));

     ten = 2 * vector2(5.0f);
     REQUIRE((ten.x == 10.0f && ten.y == 10.0f));

     ten = vector2(5.0f) * 2;
     REQUIRE((ten.x == 10.0f && ten.y == 10.0f));

     ten = vector2(50.0f);
     ten /= 5.0f;
     REQUIRE((ten.x == 10.0f && ten.y == 10.0f));

     ten = vector2(100.0f) / 10.f;
     REQUIRE((ten.x == 10.0f && ten.y == 10.0f));
 }

 TEST_CASE("vector2 comparison")
 {
     vector2 one(2.0f, 3.0f);
     vector2 ten(2.0f, 3.0f);
     REQUIRE(one == ten);

     ten = vector2(2.0f, 4.0f);
     REQUIRE(ten >= one);
     REQUIRE(one <= ten);
     ten = vector2(3.0f, 4.0f);
     REQUIRE(one < ten);
     REQUIRE(ten > one);
 }

 TEST_CASE("vector2 dot product function")
 {
     float x1 = -10.123f, y1 = 3.12456f;
     float x2 = 0.10123f, y2 = 300.12456f;
     vector2 a{x1, y1};
     vector2 b{x2, y2};
     REQUIRE((a * b) == (x1 * x2 + y1 * y2));
     REQUIRE((a * b) == (b * a));
 }

 TEST_CASE("vector2 perpendicular_to function")
 {
     float x1 = -10.123f, y1 = 3.12456f;
     vector2 original{x1, y1};
     vector2 perp = Perpendicular(original);
     REQUIRE((original * perp) == 0.0f);
 }

 TEST_CASE("vector2 magnitude and magnitude_squared functions")
 {
     float x1 = -6.0f, y1 = 8.0f;
     vector2 original{ x1, y1 };
     float squared = Magnitude_Squared(original);
     float non = Magnitude(original);
     REQUIRE(squared == 100.0f);
     REQUIRE(non == 10.0f);
 }

 TEST_CASE("vector2 normalize function")
 {
     vector2 unit_vector{-std::sqrt(0.5f), std::sqrt(0.5f)};
     vector2 norm_vector = Normalizing(vector2{ -500.0f, 500.0f });
     REQUIRE(unit_vector == norm_vector);

     vector2 normalized_vector = Normalizing(vector2{-970987.564564f, 531.0545f});
     REQUIRE(Magnitude(normalized_vector) - 1.0f <= epsilon);
     REQUIRE(Magnitude_Squared(normalized_vector) - 1.0f <= epsilon);
 }

 TEST_CASE("vector2 distance_between function")
 {
     REQUIRE(Distance(vector2{0.0f, 1.0f}, vector2{0.0f, -1.0f}) == 2.0f);
     REQUIRE(Distance_Squared(vector2{0.0f, 1.0f}, vector2{0.0f, -1.0f}) == 4.0f);

     REQUIRE(Distance(vector2{1.0f, 0.0f}, vector2{-1.0f, 0.0f}) == 2.0f);
     REQUIRE(Distance_Squared(vector2{1.0f, 0.0f}, vector2{-1.0f, 0.0f}) == 4.0f);

     float x1 = -10.f, y1 = 3.12f;
     float x2 = 0.10f, y2 = 300.12f;
     REQUIRE(Distance(vector2{x1, y1}, vector2{x2, y2}) ==
             std::sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
     REQUIRE(Distance_Squared(vector2{x1, y1}, vector2{x2, y2}) ==
             ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
 }

TEST_CASE("vector2 angle")
{
    vector2 v1(10.0f, 0.0f);
    vector2 v2(10.0f, 0.0f);
    REQUIRE(Angle(v1, v2) == 0);
    v1 = vector2(0.0f, 10.0f);
    REQUIRE(Angle(v1, v2) - 90.0f < epsilon);
}

TEST_CASE("vector2 projection")
{
    vector2 v1(10.0f, 5.0f);
    vector2 v2(1.0f, 0.0f);
    REQUIRE(v1.Projection(v2) == vector2(10.0f, 0.0f));
    v1 = vector2(20.0f, 0.0f);
    REQUIRE(v1.Projection(v2) == v1);
}

//vector 3 test

TEST_CASE("vector3 constructor")
{
    vector3 zero;
    REQUIRE((zero.x == 0.0f && zero.y == 0.0f && zero.z == 0.0f));

    vector3 fill_float(1.0f);
    REQUIRE((fill_float.x == 1.0f && fill_float.y == 1.0f && fill_float.z == 1.0f));

    vector3 custom_xyz(1.0f, 2.0f, 3.0f);
    REQUIRE((custom_xyz.x == 1.0f && custom_xyz.y == 2.0f && custom_xyz.z == 3.0f));

    vector3 zero_curly{};
    REQUIRE((zero_curly.x == 0.0f && zero_curly.y == 0.0f && zero_curly.z == 0.0f));

    vector3 fill_float_curly{ 2.0f };
    REQUIRE((fill_float_curly.x == 2.0f && fill_float_curly.y == 2.0f && fill_float_curly.z == 2.0f));

    vector3 custom_xy_curly{ 1.0f, 2.0f, 3.0f };
    REQUIRE((custom_xy_curly.x == 1.0f && custom_xy_curly.y == 2.0f && custom_xy_curly.z == 3.0f));
}

TEST_CASE("vector3 addition")
{
    vector3 one(1.0f);
    vector3 negative_one(-1.0f);
    vector3 zero = one + negative_one;
    REQUIRE((zero.x == 0.0f && zero.y == 0.0f && zero.z == 0.0f));

    zero = one;
    zero += negative_one;
    REQUIRE((zero.x == 0.0f && zero.y == 0.0f && zero.z == 0.0f));
}

TEST_CASE("vector3 subtraction")
{
    vector3 one(1.0f);
    vector3 negative_one(-1.0f);
    vector3 zero = one - one;
    REQUIRE((zero.x == 0.0f && zero.y == 0.0f && zero.z == 0.0f));

    zero = one;
    zero -= one;
    REQUIRE((zero.x == 0.0f && zero.y == 0.0f && zero.z == 0.0f));
}

TEST_CASE("vector3 negation")
{
    vector3 minus_ten(-10.0f);
    vector3 ten = -minus_ten;
    REQUIRE((ten.x == 10.0f && ten.y == 10.0f && ten.z == 10.0f));
}

TEST_CASE("vector3 scaling")
{
    vector3 ten(2.0f);
    ten *= 5.0f;
    REQUIRE((ten.x == 10.0f && ten.y == 10.0f && ten.z == 10.0f));

    ten = 2 * vector3(5.0f);
    REQUIRE((ten.x == 10.0f && ten.y == 10.0f && ten.z == 10.0f));

    ten = vector3(5.0f) * 2;
    REQUIRE((ten.x == 10.0f && ten.y == 10.0f && ten.z == 10.0f));

    ten = vector3(50.0f);
    ten /= 5.0f;
    REQUIRE((ten.x == 10.0f && ten.y == 10.0f && ten.z == 10.0f));

    ten = vector3(100.0f) / 10.f;
    REQUIRE((ten.x == 10.0f && ten.y == 10.0f && ten.z == 10.0f));
}

TEST_CASE("vector3 comparison")
{
    vector3 one(2.0f, 3.0f, 4.0f);
    vector3 ten(2.0f, 3.0f, 4.0f);
    REQUIRE(one == ten);

    ten = vector3(2.0f, 4.0f, 4.0f);
    REQUIRE(ten >= one);
    REQUIRE(one <= ten);
    ten = vector3(3.0f, 4.0f, 5.0f);
    REQUIRE(one < ten);
    REQUIRE(ten > one);
}

TEST_CASE("vector3 dot product function")
{
    float x1 = -10.123f, y1 = 3.12456f, z1 = 2.1415f;
    float x2 = 0.10123f, y2 = 300.12456f, z2 = 49.10729f;
    vector3 a{ x1, y1, z1 };
    vector3 b{ x2, y2, z2 };
    REQUIRE((a * b) == (x1 * x2 + y1 * y2 + z1 * z2));
    REQUIRE((a * b) == (b * a));
}

TEST_CASE("vector3 cross product function")
{
    float x1 = -5.0f, y1 = 3.0f, z1 = 2.0f;
    float x2 = 0.0f, y2 = 3.0f, z2 = 4.0f;
    vector3 a{ x1, y1, z1 };
    vector3 b{ x2, y2, z2 };
    vector3 c = CrossProduct(a, b);
    REQUIRE((a * c < epsilon));
    REQUIRE((b * c < epsilon));
}

TEST_CASE("vector3 magnitude and magnitude_squared functions")
{
    float x1 = -6.0f, y1 = 8.0f, z1 = 5.0f;
    vector3 original{ x1, y1, z1 };
	float non = Magnitude(original);
    float squared = Magnitude_Squared(original);
	REQUIRE((non * non) == (x1 * x1 + y1 * y1 + z1 * z1));
    REQUIRE(squared == (x1 * x1 + y1 * y1 + z1 * z1));
}

TEST_CASE("vector3 normalize function")
{
    vector3 normalized_vector = Normalizing({ -970987.564564f, 531.0545f, 1231.1251f });
    REQUIRE(Magnitude(normalized_vector) - 1.0f <= epsilon);
    REQUIRE(Magnitude_Squared(normalized_vector) - 1.0f <= epsilon);
}

TEST_CASE("vector3 distance_between function")
{
    REQUIRE(Distance({ 0.0f, 1.0f, 4.0f }, { 0.0f, -1.0f, 4.0f }) == 2.0f);
    REQUIRE(Distance_Squared({ 0.0f, 1.0f, 4.0f }, { 0.0f, -1.0f, 4.0f }) == 4.0f);

    float x1 = -10.123f, y1 = 3.12456f, z1 = 3.1241f;
    float x2 = 0.10123f, y2 = 300.12456f, z2 = 525.231f;
    REQUIRE(Distance({ x1, y1, z1 }, { x2, y2, z2 }) ==
        std::sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2)));
    REQUIRE(Distance_Squared({ x1, y1, z1 }, { x2, y2, z2 }) ==
        ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2)));
}

TEST_CASE("vector3 angle")
{
    vector3 v1(10.0f, 0.0f, 3.0f);
    vector3 v2(10.0f, 0.0f, 3.0f);
    REQUIRE(Angle(v1, v2) == 0);
    v1 = vector3(0.0f, 10.0f, 3.0f);
    REQUIRE(Angle(v1, v2) - 90.0f < epsilon);
}

//matrix2 test

TEST_CASE("matrix2 constructor & element access")
{
    matrix2 zero;
    REQUIRE(zero(0, 0) == 0.0f);
    REQUIRE(zero(0, 1) == 0.0f);
    REQUIRE(zero(1, 0) == 0.0f);
    REQUIRE(zero(1, 1) == 0.0f);

    vector2 column1{1.0f, 2.0f};
    vector2 column2{3.0f, 4.0f};
    matrix2 column_init{column1, column2};
    REQUIRE(column_init(0, 0) == column1.x);
    REQUIRE(column_init(0, 1) == column1.y);
    REQUIRE(column_init(1, 0) == column2.x);
    REQUIRE(column_init(1, 1) == column2.y);

    float column0_row0 = 4.0f, column0_row1 = 3.0f, column1_row0 = 2.0f, column1_row1 = 1.0f;
    matrix2 explicit_init{column0_row0, column0_row1, column1_row0, column1_row1};
    REQUIRE(explicit_init(0, 0) == column0_row0);
    REQUIRE(explicit_init(0, 1) == column0_row1);
    REQUIRE(explicit_init(1, 0) == column1_row0);
    REQUIRE(explicit_init(1, 1) == column1_row1);
}

TEST_CASE("matrix2 getting setting")
{
    matrix2 zero;
    zero(0, 0) = 1.0f;
    zero(0, 1) = 2.0f;
    zero(1, 0) = 3.0f;
    zero(1, 1) = 4.0f;
    REQUIRE(zero(0, 0) == 1.0f);
    REQUIRE(zero(0, 1) == 2.0f);
    REQUIRE(zero(1, 0) == 3.0f);
    REQUIRE(zero(1, 1) == 4.0f);
    REQUIRE(zero(0, 0) * zero(1, 1) == 4.0f);
    REQUIRE(zero(0, 0) == 1.0f);
}

TEST_CASE("matrix2 matrix multiplication")
{
    const matrix2 identity{1.0f, 0.0f, 0.0f, 1.0f};
    const matrix2 four321{4.0f, 3.0f, 2.0f, 1.0f};
    matrix2 identity_result = identity * four321;
    REQUIRE(identity_result(0, 0) == four321(0, 0));
    REQUIRE(identity_result(0, 1) == four321(0, 1));
    REQUIRE(identity_result(1, 0) == four321(1, 0));
    REQUIRE(identity_result(1, 1) == four321(1, 1));

    identity_result = four321 * identity;
    REQUIRE(identity_result(0, 0) == four321(0, 0));
    REQUIRE(identity_result(0, 1) == four321(0, 1));
    REQUIRE(identity_result(1, 0) == four321(1, 0));
    REQUIRE(identity_result(1, 1) == four321(1, 1));

    vector2 u = Normalizing(vector2(6.0f, -8.0f));
    vector2 v = Perpendicular(u);
    matrix2 uv{u, v};
    matrix2 uv_transpose{{u.x, v.x}, {u.y, v.y}};
    matrix2 to_identity = uv * uv_transpose;
    REQUIRE(std::abs(to_identity(0, 0) - identity(0, 0)) < epsilon);
    REQUIRE(to_identity(0, 1) == identity(0, 1));
    REQUIRE(to_identity(1, 0) == identity(1, 0));
    REQUIRE(std::abs(to_identity(1, 1) - identity(1, 1)) < epsilon);

    to_identity = uv_transpose * uv;
    REQUIRE(std::abs(to_identity(0, 0) - identity(0, 0)) < epsilon);
    REQUIRE(to_identity(0, 1) == identity(0, 1));
    REQUIRE(to_identity(1, 0) == identity(1, 0));
    REQUIRE(std::abs(to_identity(1, 1) - identity(1, 1)) < epsilon);

    float cos45 = std::cos(PI / 4.0f);
    float sin45 = std::cos(PI / 4.0f);
    matrix2 rotate45AndScale1020{{cos45, sin45}, {-sin45, cos45}};
    matrix2 scale = {{10.0f, 0.0f}, {0.0f, 20.0f}};
    rotate45AndScale1020 *= scale;
    REQUIRE(rotate45AndScale1020(0, 0) == 10.0f * cos45);
    REQUIRE(rotate45AndScale1020(0, 1) == 10.0f * sin45);
    REQUIRE(rotate45AndScale1020(1, 0) == -20.0f * sin45);
    REQUIRE(rotate45AndScale1020(1, 1) == 20.0f * cos45);
}

TEST_CASE("matrix2 tranform vector")
{
    matrix2 matrix(1.0f, 2.0f, 3.0f, 4.0f);
    vector2 vector(2.0f, 5.0f);
    vector2 test = matrix * vector;
    REQUIRE(test.x == 17.0f);
    REQUIRE(test.y == 24.0f);
}

TEST_CASE("matrix2 transpose")
{
    vector2 column_u{-1, 3.5f};
    vector2 column_v{2.5f, 4.0f};
    matrix2 original{column_u, column_v};
    matrix2 transposed = Transpose(original);

    REQUIRE(transposed(0, 0) == column_u.x);
    REQUIRE(transposed(0, 1) == column_v.x);
    REQUIRE(transposed(1, 0) == column_u.y);
    REQUIRE(transposed(1, 1) == column_v.y);
}

TEST_CASE("matrix2 roatation")
{
    matrix2 Rot = Rotation_Matrix_Degree(90.0f);
    vector2 vector(1.0f, 0.0f);
    vector2 test = Rot * vector;
    REQUIRE(std::abs(test.x - 0.0f) < epsilon);
    REQUIRE(test.y == 1.0f);

    Rot = Rotation_Matrix_Degree(45.0f);
    test = Rot * vector;
    REQUIRE(std::abs(Angle_Degree(test, vector) - 45.0f) < epsilon);
    REQUIRE(std::abs(Magnitude(test) - 1.0f) < epsilon);
}

TEST_CASE("matrix2 identity")
{
    const matrix2 identity{1.0f, 0.0f, 0.0f, 1.0f};
    matrix2 identity_result = Identity_Matrix();
    REQUIRE(identity_result(0, 0) == identity(0, 0));
    REQUIRE(identity_result(0, 1) == identity(0, 1));
    REQUIRE(identity_result(1, 0) == identity(1, 0));
    REQUIRE(identity_result(1, 1) == identity(1, 1));
}

TEST_CASE("matrix2 scale")
{
    const matrix2 scale_uniform = Scale_Matrix(2.0f);
    vector2 vector(4.0f, 7.0f);
    vector = scale_uniform * vector;
    REQUIRE(vector.x == 8.0f);
    REQUIRE(vector.y == 14.0f);
    const matrix2 scale_nonuniform = Scale_Matrix(3.0f, 0.5f);
    vector = vector2(3.0f, 8.0f);
    vector = scale_nonuniform * vector;
    REQUIRE(vector.x == 9.0f);
    REQUIRE(vector.y == 4.0f);
}

TEST_CASE("matrix2 determinate")
{
    matrix2 identity = Identity_Matrix();
    REQUIRE(Determinant(identity) == 1.0f);

    float a = 1, b = -2, c = 3, d = -4;
    float det = a * d - b * c;
    matrix2 m{{a, c}, {b, d}};
    REQUIRE(Determinant(m) == det);
}

TEST_CASE("matrix2 inverse")
{
    matrix2 test(2.0f, 5.0f, 3.0f, 8.0f);
    test = Inverse(test);
    REQUIRE(test(0, 0) == 8.0f);
    REQUIRE(test(0, 1) == -5.0f);
    REQUIRE(test(1, 0) == -3.0f);
    REQUIRE(test(1, 1) == 2.0f);
}

//affine2d test

TEST_CASE("affine2d constructor & element access")
{
    affine2d zero;
    REQUIRE(zero(0, 0) == 0.0f);
    REQUIRE(zero(0, 1) == 0.0f);
    REQUIRE(zero(0, 2) == 0.0f);
    REQUIRE(zero(1, 0) == 0.0f);
    REQUIRE(zero(1, 1) == 0.0f);
    REQUIRE(zero(1, 2) == 0.0f);
    REQUIRE(zero(2, 0) == 0.0f);
    REQUIRE(zero(2, 1) == 0.0f);
    REQUIRE(zero(2, 2) == 0.0f);

    vector3 column1{1.0f, 2.0f, 3.0f};
    vector3 column2{5.0f, 5.0f, 6.0f};
    vector3 column3{7.0f, 8.0f, 9.0f};
    affine2d column_init{column1, column2, column3};
    REQUIRE(column_init(0, 0) == column1.x);
    REQUIRE(column_init(0, 1) == column1.y);
    REQUIRE(column_init(0, 2) == column1.z);
    REQUIRE(column_init(1, 0) == column2.x);
    REQUIRE(column_init(1, 1) == column2.y);
    REQUIRE(column_init(1, 2) == column2.z);
    REQUIRE(column_init(2, 0) == column3.x);
    REQUIRE(column_init(2, 1) == column3.y);
    REQUIRE(column_init(2, 2) == column3.z);

    float column0_row0 = 9.0f, column0_row1 = 9.0f, column0_row2 = 7.0f, column1_row0 = 6.0f,
          column1_row1 = 5.0f, column1_row2 = 4.0f, column2_row0 = 3.0f, column2_row1 = 2.0f,
          column2_row2 = 1.0f;
    affine2d explicit_init{column0_row0, column0_row1, column0_row2, column1_row0, column1_row1,
                           column1_row2, column2_row0, column2_row1, column2_row2};
    REQUIRE(explicit_init(0, 0) == column0_row0);
    REQUIRE(explicit_init(0, 1) == column0_row1);
    REQUIRE(explicit_init(0, 2) == column0_row2);
    REQUIRE(explicit_init(1, 0) == column1_row0);
    REQUIRE(explicit_init(1, 1) == column1_row1);
    REQUIRE(explicit_init(1, 2) == column1_row2);
    REQUIRE(explicit_init(2, 0) == column2_row0);
    REQUIRE(explicit_init(2, 1) == column2_row1);
    REQUIRE(explicit_init(2, 2) == column2_row2);
}

TEST_CASE("affine2d element access")
{
    const affine2d const_matrix{1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f};
    REQUIRE(const_matrix(0, 0) == 1.0f);
    REQUIRE(const_matrix(0, 1) == 2.0f);
    REQUIRE(const_matrix(0, 2) == 3.0f);
    REQUIRE(const_matrix(1, 0) == 4.0f);
    REQUIRE(const_matrix(1, 1) == 5.0f);
    REQUIRE(const_matrix(1, 2) == 6.0f);
    REQUIRE(const_matrix(2, 0) == 7.0f);
    REQUIRE(const_matrix(2, 1) == 8.0f);
    REQUIRE(const_matrix(2, 2) == 9.0f);

    affine2d m;
    m(0, 0) = 1.0f;
    REQUIRE(m(0, 0) == 1.0f);
    m(0, 1) = 2.0f;
    REQUIRE(m(0, 1) == 2.0f);
    m(0, 2) = 3.0f;
    REQUIRE(m(0, 2) == 3.0f);
    m(1, 0) = 4.0f;
    REQUIRE(m(1, 0) == 4.0f);
    m(1, 1) = 5.0f;
    REQUIRE(m(1, 1) == 5.0f);
    m(1, 2) = 6.0f;
    REQUIRE(m(1, 2) == 6.0f);
    m(2, 0) = 7.0f;
    REQUIRE(m(2, 0) == 7.0f);
    m(2, 1) = 8.0f;
    REQUIRE(m(2, 1) == 8.0f);
    m(2, 2) = 9.0f;
    REQUIRE(m(2, 2) == 9.0f);
}

TEST_CASE("affine2d matrix multiplcation")
{
    const affine2d identity{1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
    const affine2d one23456789{1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f};
    affine2d identity_result = identity * one23456789;
    REQUIRE(identity_result(0, 0) == one23456789(0, 0));
    REQUIRE(identity_result(0, 1) == one23456789(0, 1));
    REQUIRE(identity_result(0, 2) == one23456789(0, 2));
    REQUIRE(identity_result(1, 0) == one23456789(1, 0));
    REQUIRE(identity_result(1, 1) == one23456789(1, 1));
    REQUIRE(identity_result(1, 2) == one23456789(1, 2));
    REQUIRE(identity_result(2, 0) == one23456789(2, 0));
    REQUIRE(identity_result(2, 1) == one23456789(2, 1));
    REQUIRE(identity_result(2, 2) == one23456789(2, 2));

    identity_result = one23456789 * identity;
    REQUIRE(identity_result(0, 0) == one23456789(0, 0));
    REQUIRE(identity_result(0, 1) == one23456789(0, 1));
    REQUIRE(identity_result(0, 2) == one23456789(0, 2));
    REQUIRE(identity_result(1, 0) == one23456789(1, 0));
    REQUIRE(identity_result(1, 1) == one23456789(1, 1));
    REQUIRE(identity_result(1, 2) == one23456789(1, 2));
    REQUIRE(identity_result(2, 0) == one23456789(2, 0));
    REQUIRE(identity_result(2, 1) == one23456789(2, 1));
    REQUIRE(identity_result(2, 2) == one23456789(2, 2));

    vector2 u = Normalizing(vector2{1.0f, -2.0f});
    vector2 v = Perpendicular(u);
    vector2 t = {-3.0f, 1.0f};
    affine2d uvt{{u.x, u.y, 0}, {v.x, v.y, 0}, {t.x, t.y, 1}};
    affine2d uvt_inverse{{u.x, v.x, 0}, {u.y, v.y, 0}, {-(u * t), -(v * t), 1}};
    affine2d to_identity = uvt * uvt_inverse;

    REQUIRE(std::abs(to_identity(0, 0) - identity(0, 0)) <= epsilon);
    REQUIRE(std::abs(to_identity(0, 1) - identity(0, 1)) <= epsilon);
    REQUIRE(std::abs(to_identity(0, 2) - identity(0, 2)) <= epsilon);
    REQUIRE(std::abs(to_identity(1, 0) - identity(1, 0)) <= epsilon);
    REQUIRE(std::abs(to_identity(1, 1) - identity(1, 1)) <= epsilon);
    REQUIRE(std::abs(to_identity(1, 2) - identity(1, 2)) <= epsilon);
    REQUIRE(std::abs(to_identity(2, 1) - identity(2, 1)) <= epsilon);
    REQUIRE(std::abs(to_identity(2, 2) - identity(2, 2)) <= epsilon);

    to_identity = uvt_inverse * uvt;
    REQUIRE(std::abs(to_identity(0, 0) - identity(0, 0)) <= epsilon);
    REQUIRE(std::abs(to_identity(0, 1) - identity(0, 1)) <= epsilon);
    REQUIRE(std::abs(to_identity(0, 2) - identity(0, 2)) <= epsilon);
    REQUIRE(std::abs(to_identity(1, 0) - identity(1, 0)) <= epsilon);
    REQUIRE(std::abs(to_identity(1, 1) - identity(1, 1)) <= epsilon);
    REQUIRE(std::abs(to_identity(1, 2) - identity(1, 2)) <= epsilon);
    REQUIRE(std::abs(to_identity(2, 0) - identity(2, 0)) <= epsilon);
    REQUIRE(std::abs(to_identity(2, 1) - identity(2, 1)) <= epsilon);
    REQUIRE(std::abs(to_identity(2, 2) - identity(2, 2)) <= epsilon);

    float cos45 = std::cos(PI / 4.0f);
    float sin45 = std::cos(PI / 4.0f);
    affine2d rotate45AndScale1020{{cos45, sin45, 0}, {-sin45, cos45, 0}, {0, 0, 1}};
    affine2d scale = {{10.0f, 0, 0}, {0, 20.0f, 0}, {0, 0, 1}};
    rotate45AndScale1020 *= scale;
    REQUIRE(rotate45AndScale1020(0, 0) == 10.0f * cos45);
    REQUIRE(rotate45AndScale1020(0, 1) == 10.0f * sin45);
    REQUIRE(rotate45AndScale1020(1, 0) == -20.0f * sin45);
    REQUIRE(rotate45AndScale1020(1, 1) == 20.0f * cos45);
}

TEST_CASE("affine2d transform vector")
{
    vector2 u2d = Normalizing(vector2{-20.4f, 33.9f});
    vector2 v2d = Perpendicular(u2d);
    vector3 u = {u2d.x, u2d.y, 0};
    vector3 v = {v2d.x, v2d.y, 0};
    vector3 t = {2.98f, -1.23f, 1};
    affine2d localToWorld{u, v, t};

    vector3 worldX = localToWorld * vector3{1.0f, 0.0f, 0};
    vector3 worldY = localToWorld * vector3{0.0f, 1.0f, 0};
    vector3 worldOrigin = localToWorld * vector3{0, 0, 1};
    REQUIRE(worldX == u);
    REQUIRE(worldY == v);
    REQUIRE(worldOrigin == t);

    affine2d worldToLocal{{u.x, v.x, 0}, {u.y, v.y, 0}, {-(u * t), -(v * t), 1}};
    vector3 localX = worldToLocal * u;
    vector3 localY = worldToLocal * v;
    REQUIRE(localX == vector3{1.0f, 0.0f, 0});
    REQUIRE(localY == vector3{0.0f, 1.0f, 0});
}

TEST_CASE("affine2d transpose")
{
    vector3 column_u{-1, 3.5f, 0};
    vector3 column_v{2.5f, 4.0f, 0};
    vector3 column_w{5.3f, -2.4f, 1};
    affine2d original{column_u, column_v, column_w};
    affine2d transposed = Transpose(original);

    REQUIRE(transposed(0, 0) == column_u.x);
    REQUIRE(transposed(0, 1) == column_v.x);
    REQUIRE(transposed(0, 2) == column_w.x);
    REQUIRE(transposed(1, 0) == column_u.y);
    REQUIRE(transposed(1, 1) == column_v.y);
    REQUIRE(transposed(1, 2) == column_w.y);
    REQUIRE(transposed(2, 0) == column_u.z);
    REQUIRE(transposed(2, 1) == column_v.z);
    REQUIRE(transposed(2, 2) == column_w.z);
}

TEST_CASE("affine2d rotation")
{
    float cos225 = std::cos(3.0f * PI / 8.0f);
    float sin225 = std::sin(3.0f * PI / 8.0f);
    const affine2d rotate45{{cos225, sin225, 0}, {-sin225, cos225, 0}, {0, 0, 1}};
    affine2d rotation = Rotation_Affine_Radian(3.0f * PI / 8.0f);

    REQUIRE(rotation(0, 0) == rotate45(0, 0));
    REQUIRE(rotation(0, 1) == rotate45(0, 1));
    REQUIRE(rotation(1, 0) == rotate45(1, 0));
    REQUIRE(rotation(1, 1) == rotate45(1, 1));

    rotation = Rotation_Affine_Radian(PI);
    REQUIRE(rotation(0, 0) == -1.0f);
    REQUIRE(std::abs(rotation(0, 1)) < epsilon);
    REQUIRE(rotation(0, 2) == 0);
    REQUIRE(std::abs(rotation(1, 0)) < epsilon);
    REQUIRE(rotation(1, 1) == -1.0f);
    REQUIRE(rotation(1, 2) == 0);
    REQUIRE(rotation(2, 0) == 0);
    REQUIRE(rotation(2, 1) == 0);
    REQUIRE(rotation(2, 2) == 1);
}

TEST_CASE("affine2d identity")
{
    const affine2d identity{1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
    affine2d identity_result = Identity_Affine();
    REQUIRE(identity_result(0, 0) == identity(0, 0));
    REQUIRE(identity_result(0, 1) == identity(0, 1));
    REQUIRE(identity_result(0, 2) == identity(0, 2));
    REQUIRE(identity_result(1, 0) == identity(1, 0));
    REQUIRE(identity_result(1, 1) == identity(1, 1));
    REQUIRE(identity_result(1, 2) == identity(1, 2));
    REQUIRE(identity_result(2, 0) == identity(2, 0));
    REQUIRE(identity_result(2, 1) == identity(2, 1));
    REQUIRE(identity_result(2, 2) == identity(2, 2));
}

TEST_CASE("affine2d scale")
{
    const affine2d ten{10.0f, 0.0f, 0.0f, 0.0f, 10.0f, 0.0f, 0, 0, 1};
    affine2d uniform_scale = Scale_Affine(10.0f);
    REQUIRE(uniform_scale(0, 0) == ten(0, 0));
    REQUIRE(uniform_scale(0, 1) == ten(0, 1));
    REQUIRE(uniform_scale(0, 2) == ten(0, 2));
    REQUIRE(uniform_scale(1, 0) == ten(1, 0));
    REQUIRE(uniform_scale(1, 1) == ten(1, 1));
    REQUIRE(uniform_scale(1, 2) == ten(1, 2));
    REQUIRE(uniform_scale(2, 0) == ten(2, 0));
    REQUIRE(uniform_scale(2, 1) == ten(2, 1));
    REQUIRE(uniform_scale(2, 2) == ten(2, 2));


    const affine2d ten_twenty{10.0f, 0.0f, 0.0f, 0.0f, 20.0f, 0.0f, 0, 0, 1};
    affine2d nonuniform_scale = Scale_Affine(10.0f, 20.0f);
    REQUIRE(nonuniform_scale(0, 0) == ten_twenty(0, 0));
    REQUIRE(nonuniform_scale(0, 1) == ten_twenty(0, 1));
    REQUIRE(nonuniform_scale(0, 2) == ten_twenty(0, 2));
    REQUIRE(nonuniform_scale(1, 0) == ten_twenty(1, 0));
    REQUIRE(nonuniform_scale(1, 1) == ten_twenty(1, 1));
    REQUIRE(nonuniform_scale(1, 2) == ten_twenty(1, 2));
    REQUIRE(nonuniform_scale(2, 0) == ten_twenty(2, 0));
    REQUIRE(nonuniform_scale(2, 1) == ten_twenty(2, 1));
    REQUIRE(nonuniform_scale(2, 2) == ten_twenty(2, 2));
}

TEST_CASE("affine2d translation")
{
    float tx = 4.0f, ty = -3.0f;
    const affine2d right4down3 = {{1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {tx, ty, 1.0f}};
    affine2d translation_matrix = Translate_Affine(tx, ty);
    REQUIRE(std::abs(translation_matrix(0, 0) - right4down3(0, 0)) < epsilon);
    REQUIRE(translation_matrix(0, 1) == right4down3(0, 1));
    REQUIRE(translation_matrix(0, 2) == right4down3(0, 2));
    REQUIRE(translation_matrix(1, 0) == right4down3(1, 0));
    REQUIRE(translation_matrix(1, 1) == right4down3(1, 1));
    REQUIRE(translation_matrix(1, 2) == right4down3(1, 2));
    REQUIRE(translation_matrix(2, 0) == right4down3(2, 0));
    REQUIRE(translation_matrix(2, 1) == right4down3(2, 1));
    REQUIRE(translation_matrix(2, 2) == right4down3(2, 2));
}

//angle convert test
TEST_CASE("degree_to_radian")
{
    float radian = To_RADIAN(180.0f);
    REQUIRE(std::abs(radian - PI) < epsilon);
    float degree = To_DEGREE(radian);
    REQUIRE(std::abs(degree - 180.0f) < epsilon);
}

//test for hsv and rgb conversion
TEST_CASE("RGB_TO_HSV")
{
	Color col(43, 125, 91, 255);
	HSV hsv = col.ConvertHSV();
	REQUIRE(hsv.Hue == static_cast<unsigned char>(155));
	REQUIRE(hsv.Saturation == 65.6f);
	REQUIRE(hsv.Value == 49.0f);

	Color color(15, 0, 79, 255);
	HSV s = color.ConvertHSV();
	REQUIRE(s.Hue == static_cast<unsigned char>(251));
	REQUIRE(s.Saturation == 100.f);
	REQUIRE(s.Value == 31.0f);
}

TEST_CASE("HSV_TO_RGB")
{
	HSV hsv(245, 55, 75);
	Color col = hsv.ConvertRGB();
	REQUIRE(col.Red == 94);
	REQUIRE(col.Green == 86);
	REQUIRE(col.Blue == 191);
}